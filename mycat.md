### mycat安装

```powershell
[root@localhost ~]# cd /opt
#将Mycat-server-1.6.7.1-release-20190627191042-linux.tar.gz上传至该文件夹
[root@localhost opt]# ll
[root@localhost opt]# tar -zxvf Mycat-server-1.6.7.1-release-20190627191042-linux.tar.gz 

#递归拷贝mycat文件夹及内容至 /usr/local 目录下
[root@localhost opt]# 
[root@localhost opt]# cd /usr/local/mycat
[root@localhost mycat]# pwd
/usr/local/mycat
[root@localhost mycat]# cd conf/
[root@localhost conf]# ll
[root@localhost conf]# vim server.xml
[root@localhost conf]# cat server.xml 
```

### server.xml 

### **修改了name="mycat"**

```xml
 <user name="mycat" defaultAccount="true">
        <property name="password">123456</property>
        <!--mycat逻辑库名字-->
        <property name="schemas">TESTDB</property>

        <!-- 表级 DML 权限设置 -->
        <!--
        <privileges check="false">
            <schema name="TESTDB" dml="0110" >
                <table name="tb01" dml="0000"></table>
                <table name="tb02" dml="1111"></table>
            </schema>
        </privileges>
         -->
    </user>
```



```powershell
[root@localhost opt]# cd /usr/local/mycat/conf/
[root@localhost conf]# vim schema.xml 
[root@localhost conf]# vim schema.xml 
```

#### schema.xml

```xml
<?xml version="1.0"?>
<!DOCTYPE mycat:schema SYSTEM "schema.dtd">
<mycat:schema xmlns:mycat="http://io.mycat/">

	<schema name="TESTDB" checkSQLschema="false" sqlMaxLimit="100" dataNode="dn1">
	</schema>
	<dataNode name="dn1" dataHost="host1" database="testdb"/>
	<dataHost name="host1" maxCon="1000" minCon="10" balance="0"
			  writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="192.168.40.128:3306" user="root"
				   password="123456">
			<readHost host="hostS1" url="192.168.40.129:3306" user="root" password="123456" />
		</writeHost>
	</dataHost>
</mycat:schema>
```

#### 验证远程访问

```powershell
#验证远程访问
#mycat安装主机
[root@localhost ~]# mysql -uroot -p123456 -h192.168.40.128 -P3306
[root@localhost ~]# mysql -uroot -p123456 -h192.168.40.129 -P3306


[root@localhost conf]# cd ..
[root@localhost mycat]# cd bin/
[root@localhost bin]# ll
-rwxr-xr-x. 1 root root   3658 6月  24 2019 dataMigrate.sh
-rwxr-xr-x. 1 root root   1272 6月  24 2019 init_zk_data.sh
-rwxr-xr-x. 1 root root  15714 6月  27 2019 mycat
-rwxr-xr-x. 1 root root   3028 6月  24 2019 rehash.sh
-rwxr-xr-x. 1 root root   2568 6月  24 2019 startup_nowrap.sh
-rwxr-xr-x. 1 root root 140198 6月  27 2019 wrapper-linux-ppc-64
-rwxr-xr-x. 1 root root  99401 6月  27 2019 wrapper-linux-x86-32
-rwxr-xr-x. 1 root root 111027 6月  27 2019 wrapper-linux-x86-64
[root@localhost bin]# pwd
/usr/local/mycat/bin
[root@localhost bin]# ll
总用量 384
-rwxr-xr-x. 1 root root   3658 6月  24 2019 dataMigrate.sh
-rwxr-xr-x. 1 root root   1272 6月  24 2019 init_zk_data.sh
-rwxr-xr-x. 1 root root  15714 6月  27 2019 mycat
-rwxr-xr-x. 1 root root   3028 6月  24 2019 rehash.sh
-rwxr-xr-x. 1 root root   2568 6月  24 2019 startup_nowrap.sh
-rwxr-xr-x. 1 root root 140198 6月  27 2019 wrapper-linux-ppc-64
-rwxr-xr-x. 1 root root  99401 6月  27 2019 wrapper-linux-x86-32
-rwxr-xr-x. 1 root root 111027 6月  27 2019 wrapper-linux-x86-64
#启动mycat
[root@localhost bin]# ./mycat console
Running Mycat-server...
wrapper  | --> Wrapper Started as Console
wrapper  | Launching a JVM...
jvm 1    | Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=64M; support was removed in 8.0
jvm 1    | Wrapper (Version 3.2.3) http://wrapper.tanukisoftware.org
jvm 1    |   Copyright 1999-2006 Tanuki Software, Inc.  All Rights Reserved.
jvm 1    | 
jvm 1    | MyCAT Server startup successfully. see logs in logs/mycat.log
```



```powershell
bin   usr/bin   usr/local/bin
sbin   usr/sbin   usr/local/sbin
#这6个目录下的可执行文件不用加 ./ 即可运行
```

#### server.xml

设置无密码登录mycat

```xml
<property name="nonePasswordLogin">1</property> <!-- 0为需要密码登陆、1为不需要密码登陆 ,默认为0，设置为1则需要指定默认账户-->
```

#### 登录

```powershell
[root@localhost ~]# mysql -umycat -p -P9066 -h192.168.40.128

[root@localhost ~]# mysql -umycat -p -P8066 -h192.168.40.128
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.6.29-mycat-1.6.7.1-release-20190627191042 MyCat Server (OpenCloudDB)

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```

### 一主一从读写分离

#### 主机配置(192.168.40.128)

```powershell
[root@localhost ~]# vim /etc/my.cnf

#主服务器唯一ID
server-id=1
#启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库(可设置多个)
binlog-ignore-db=mysql
binlog-ignore-db=information_schema
#设置需要复制的数据库
binlog-do-db=testdb
#设置logbin格式
binlog_format=STATEMENT

```

从机配置(192.168.40.129)

```powershell
修改配置文件： vim /etc/my.cnf
[root@localhost ~]# vim /etc/my.cnf

#从服务器唯一ID
server-id=2
log-bin=mysql-bin       #日志文件名称
binlog_format=STATEMENT  #设置logbin格式
#设置需要复制的数据库
binlog-do-db=testdb

##启用中继日志
relay-log=mysql-relay

```



```mysql
mysql> CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
Query OK, 0 rows affected (0.07 sec)

mysql> GRANT all privileges ON *.* TO 'slave'@'%';
Query OK, 0 rows affected (0.01 sec)
```

```powershell
#重启两个mysql服务
service mysqld restart

systemctl restart mysqld
```



```
登录主
[root@localhost ~]# mysql -uroot -p123456

mysql> show master status;
+------------------+----------+--------------+--------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB         | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------+-------------------+
| mysql-bin.000001 |      155 | testdb       | mysql,information_schema |                   |
+------------------+----------+--------------+--------------------------+-------------------+
1 row in set (0.00 sec)



```

```

mysql> CHANGE MASTER TO MASTER_HOST='192.168.40.128',
    -> MASTER_USER='slave',
    -> MASTER_PASSWORD='123456',
    -> MASTER_LOG_FILE='mysql-bin.000001',MASTER_LOG_POS=155;
Query OK, 0 rows affected, 2 warnings (0.07 sec)

mysql> start slave;
Query OK, 0 rows affected (0.01 sec)



```

```
 stop slave;
 reset master;

CHANGE MASTER TO
MASTER_HOST='192.168.40.128',
MASTER_USER='slave',
MASTER_PASSWORD='123456',
MASTER_LOG_FILE='master-a-bin.000108',
MASTER_LOG_POS=155;



```

```
[root@localhost ~]# hostnamectl set-hostname 128
[root@localhost ~]# hostnamectl set-hostname 129

[root@localhost opt]# scp -r root@192.168.40.128:/opt/mysql8 /opt/soft/mysql8

```





