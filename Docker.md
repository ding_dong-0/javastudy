---
typora-root-url: md相关图片

hostnamectl set-hostname localhost
---



```

```



[docker docs](https://docs.docker.com/install/linux/docker-ce/centos/)



### 1.docker的安装

### Docker CE 镜像（阿里云）

https://developer.aliyun.com/mirror/docker-ce?spm=a2c6h.13651102.0.0.e19c1b11KdDrgN

```powershell
#查看系统
[root@localhost ~]# cd /etc/
[root@localhost etc]# ll |grep centos
#卸载之前安装的docker
$ sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
#查看Centos版本                  
[root@localhost ~]# cat /etc/redhat-release 
CentOS Linux release 8.0.1905 (Core) 

#安装gcc
[root@localhost ~]# yum -y install gcc
[root@localhost ~]# yum -y install gcc-c++
#查看 Red Hat 8.2.1-3版本号
[root@localhost ~]# gcc -v

#通过主流yum仓库的方式安装
[root@localhost ~]# yum install -y yum-utils \
>   device-mapper-persistent-data \
>   lvm2
#这个是国外的比较慢，用下边的
[root@localhost ~]# yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

#添加阿里云的docker仓库
[root@localhost ~]# yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

添加仓库自：http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
#查看docker软件源地址

[root@localhost ~]# cat /etc/yum.repos.d/docker-ce.repo 

# 更新并安装Docker-CE，默认安装的是最新版本
[root@localhost ~]# yum makecache
# dnf install docker-ce –nobest
[root@localhost ~]#  yum install docker-ce

#安装最新版本containerd.io-1.2.6-3.3.el7
  dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
[root@localhost ~]# yum install docker-ce
[root@localhost ~]# systemctl start docker

[root@localhost ~]# docker run hello-world
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
hello-world         latest              fce289e99eb9        12 months ago       1.84kB
[root@localhost ~]# mkdir -p /etc/docker
[root@localhost ~]# vim /etc/docker/daemon.json

{ "graph": "/mnt/docker-data", "storage-driver": "overlay" }
#网易云
{"registry-mirrors": ["http://hub-mirror.c.163.com"] }
 #阿里云
{
  "registry-mirrors": ["https://｛自已的编码｝.mirror.aliyuncs.com"]
}

#查询docker状态
[root@localhost docker]# systemctl status docker.service
#修改配置重新载入
[root@localhost ~]# systemctl daemon-reload
#启动docker
[root@localhost ~]# systemctl start docker
#重启 docker
[root@localhost ~]# systemctl restart docker
[root@localhost ~]# ps -ef|grep docker
#
[root@localhost ~]# ps -ef|grep docker |grep -v grep
[root@localhost ~]# docker search tomcat

#停止docker服务
systemctl stop docker
#卸载docker
sudo yum remove docker-ce
#删除相关文件
sudo rm -rf /var/lib/docker
```



```powershell
#rpm的安装方式
rpm -ivh docker-ce-19.03.5-3.el7.x86_64.rpm docker-ce-cli-19.03.5-3.el7.x86_64.rpm
```



### 阿里云docker镜像加速器的配置

```powershell

#阿里云docker镜像加速器的配置
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://x0ue3znl.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
#开机自起
sudo systemctl enable docker

```

注册的

阿里云镜像加速器配置：

https://cr.console.aliyun.com/cn-beijing/instances/mirrors

账号：houyingcaho

密码：jinzhong1002

https://cr.console.aliyun.com/cn-beijing/instances/repositories



Linux搭建自己的Docker镜像仓库-云栖社区-阿里云  https://yq.aliyun.com/articles/719971?spm=a2c4e.11155472.0.0.35fd752dtwjoM5

docker-ce镜像-镜像站-阿里云开发者社区__云计算社区  https://developer.aliyun.com/mirror/docker-ce?spm=a2c6h.13651102.0.0.53322f70BWXrYZ   阿里云docker官方安装说明。





### docker-compose 安装

```powershell

[root@localhost ~]# cd /usr/local/bin
# 将从github 上下载的安装包 docker-compose放在该目录 
[root@localhost bin]# 
[root@localhost bin]# ll
总用量 25144
-rw-r--r--. 1 root root 25747456 7月  18 10:58 docker-compose
[root@localhost bin]# chmod +x /usr/local/bin/docker-compose
[root@localhost bin]# ll
总用量 25144
-rwxr-xr-x. 1 root root 25747456 7月  18 10:58 docker-compose
[root@localhost bin]# docker-compose version
Docker Compose version v2.6.1
[root@localhost bin]# cd 
[root@localhost ~]# docker-compose version
Docker Compose version v2.6.1
```





#### 安装过程出现的问题：

```powershell
#安装docker
[root@localhost ~]# yum -y install docker-ce
上次元数据过期检查：0:05:58 前，执行于 2020年01月02日 星期四 11时30分05秒。
错误：
 问题: package docker-ce-3:19.03.5-3.el7.x86_64 requires containerd.io >= 1.2.2-3, but none of the providers can be installed
  - cannot install the best candidate for the job
  - package containerd.io-1.2.10-3.2.el7.x86_64 is excluded
  - package containerd.io-1.2.2-3.3.el7.x86_64 is excluded
  - package containerd.io-1.2.2-3.el7.x86_64 is excluded
  - package containerd.io-1.2.4-3.1.el7.x86_64 is excluded
  - package containerd.io-1.2.5-3.1.el7.x86_64 is excluded
  - package containerd.io-1.2.6-3.3.el7.x86_64 is excluded
(try to add '--skip-broken' to skip uninstallable packages or '--nobest' to use not only best candidate packages)
#最新版docker-ce依赖的 containerd.io版本过低，指定要安装的docker-ce版本

#安装最新版本containerd.io-1.2.6-3.3.el7
  dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
```

[如果报错：Problem: package docker-ce-3:19.03.4-3.el7.x86_64 requires containerd.io >= 1.2.2-3 那就先装新版的 containerd.io](https://www.cnblogs.com/zbseoag/p/11736006.html)（可以单独现在下来通过 rpm -ivh containerd.io-1.2.6-3.3.el7.x86_64 安装 ）

```powershell
 dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
```



#### 安装指定版本docker

```powershell
# 注意：
# 官方软件源默认启用了最新的软件，您可以通过编辑软件源的方式获取各个版本的软件包。例如官方并没有将测试版本的软件源置为可用，您可以通过以下方式开启。同理可以开启各种测试版本等。
# vim /etc/yum.repos.d/docker-ee.repo
#   将[docker-ce-test]下方的enabled=0修改为enabled=1
#
# 安装指定版本的Docker-CE:
# Step 1: 查找Docker-CE的版本:
# yum list docker-ce.x86_64 --showduplicates | sort -r
#   Loading mirror speeds from cached hostfile
#   Loaded plugins: branch, fastestmirror, langpacks
#   docker-ce.x86_64            17.03.1.ce-1.el7.centos            docker-ce-stable
#   docker-ce.x86_64            17.03.1.ce-1.el7.centos            @docker-ce-stable
#   docker-ce.x86_64            17.03.0.ce-1.el7.centos            docker-ce-stable
#   Available Packages
# Step2: 安装指定版本的Docker-CE: (VERSION例如上面的17.03.0.ce.1-1.el7.centos)
# sudo yum -y install docker-ce-[VERSION]


#安装指定版本docker
#查找Docker-CE的版本:
[root@localhost ~]# yum list docker-ce.x86_64 --showduplicates | sort -r
docker-ce.x86_64            3:19.03.5-3.el7                     docker-ce-stable
docker-ce.x86_64            3:19.03.4-3.el7                     docker-ce-stable
docker-ce.x86_64            3:19.03.3-3.el7                     docker-ce-stable
docker-ce.x86_64            3:19.03.2-3.el7                     docker-ce-stable
docker-ce.x86_64            3:19.03.1-3.el7                     docker-ce-stable
docker-ce.x86_64            3:19.03.0-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.9-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.8-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.7-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.6-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.5-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.4-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.3-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.2-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.1-3.el7                     docker-ce-stable
docker-ce.x86_64            3:18.09.0-3.el7                     docker-ce-stable
docker-ce.x86_64            18.06.3.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.2.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.1.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.0.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.03.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            18.03.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.12.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.12.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.09.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.09.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.06.2.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.06.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.06.0.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.03.3.ce-1.el7                    docker-ce-stable
docker-ce.x86_64            17.03.2.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.03.1.ce-1.el7.centos             docker-ce-stable
docker-ce.x86_64            17.03.0.ce-1.el7.centos             docker-ce-stable
#安装 docker-ce.x86_64            18.03.1.ce-1.el7.centos 
[root@localhost ~]#  yum install docker-ce
[root@localhost ~]# systemctl start docker
[root@localhost ~]# docker version
Client:
 Version:      18.03.1-ce
 API version:  1.37
 Go version:   go1.9.5
 Git commit:   9ee9f40
 Built:        Thu Apr 26 07:20:16 2018
 OS/Arch:      linux/amd64
 Experimental: false
 Orchestrator: swarm

Server:
 Engine:
  Version:      18.03.1-ce
  API version:  1.37 (minimum version 1.12)
  Go version:   go1.9.5
  Git commit:   9ee9f40
  Built:        Thu Apr 26 07:23:58 2018
  OS/Arch:      linux/amd64
  Experimental: false  
 
 
systemctl daemon-reload 
#重启docker
systemctl restart docker 
#设置开机自起
systemctl enable docker
```



[docker 启动失败 Job for docker.service failed because the control process exited with error code. See "systemctl status docker.service" and "journalctl -xe" for details.]

https://www.cnblogs.com/dalianmao890710/p/9241752.html

### 华为开源镜像站_软件开发服务_华为云

  https://mirrors.huaweicloud.com/

```powershell
1、若您安装过**docker**，需要先删掉，之后再安装依赖:

sudo yum remove docker docker-common docker-selinux docker-engine
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

2、根据版本不同，下载repo文件。您使用的发行版： 

wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.huaweicloud.com/docker-ce/linux/centos/docker-ce.repo

软件仓库地址替换为：

sudo sed -i 's+download.docker.com+mirrors.huaweicloud.com/docker-ce+' /etc/yum.repos.d/docker-ce.repo

3、更新索引文件并安装

sudo yum makecache 

 dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm

sudo yum install docker-ce
```

### docker常用命令





## docker镜像的导入导出

```powershell
#保存docker中的镜像
docker save -o cadvisor.tar google/cadvisor
docker save -o prometheus.tar prom/prometheus
docker save -o grafana.tar grafana/grafana
mkdir jk
mv cadvisor.tar grafana.tar prometheus.tar jk/
#将文件夹发送到128的root目录中
scp -r jk/  root@192.168.40.128:/root/
#将镜像导入到docker中
docker load -i prometheus.tar 
docker load -i cadvisor.tar
```

```powershell
#192.168.40.100
[root@localhost ~]# docker save -o mysql.tar mysql
[root@localhost ~]# scp mysql.tar root@192.168.40.129:/root/
#192.168.40.129
[root@localhost ~]# docker load -i mysql.tar
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mysql               5.7                 f965319e89de        2 months ago        448MB
```



```powershell
[root@localhost ~]# docker version
[root@localhost ~]# docker info
[root@localhost ~]# docker help
```

```powershell
# 当前 shell 下 attach 连接指定运行镜像
attach      Attach to a running container                
# 通过 Dockerfile 定制镜像
build       Build an image from a Dockerfile             
# 提交当前容器为新的镜像
commit      Create a new image from a container changes   
#从容器中拷贝指定文件或者目录到宿主机中
cp          Copy files/folders from the containers filesystem to the host path   
# 创建一个新的容器，同 run，但不启动容器
create      Create a new container                       
# 查看 docker 容器变化
diff        Inspect changes on a container's filesystem  
# 从 docker 服务获取容器实时事件
events      Get real time events from the server          
 # 在已存在的容器上运行命令
exec        Run a command in an existing container     
（docker exec -it mysql /bin/bash ）//进入运行的容器内部
# 导出容器的内容流作为一个 tar 归档文件[对应 import ]
export      Stream the contents of a container as a tar archive   
# 展示一个镜像形成历史
history    Show the history of an image 
（docker  history zookeepe）
(docker history prom/prometheus)
# 列出系统当前镜像
images     List images   
（docker images）
# 从tar包中的内容创建一个新的文件系统映像[对应export]
import     Create a new filesystem image from the contents of a tarball
#将一个镜像保存成tar包
docker save -o cadvisor.tar google/cadvisor
#保存docker中的镜像
docker save -o cadvisor.tar google/cadvisor
docker save -o prometheus.tar prom/prometheus
docker save -o grafana.tar grafana/grafana
mkdir jk
mv cadvisor.tar grafana.tar prometheus.tar jk/
#将文件夹发送到128的root目录中
scp -r jk/  root@192.168.40.128:/root/
#将镜像导入到docker中
docker load -i prometheus.tar 
docker load -i cadvisor.tar

# 显示系统相关信息
info       Display system-wide information              
（docker info）
inspect    Return low-level information on a container   # 查看容器详细信息
（docker inspect mysql）//此处为容器名
kill       Kill a running container                      # kill 指定 docker 容器
（docker kill  zk02）//zk02容器名称
（docker kill 207e11ef8a6f ）//207e11ef8a6f容器id
load       Load an image from a tar archive            # 从一个 tar 包中加载一个镜像[对应 save]
（docker load -i prometheus.tar ）
（docker load -i cadvisor.tar）
login      Register or Login to the docker registry server    # 注册或者登陆一个 docker 源服务器

logout     Log out from a Docker registry server          # 从当前 Docker registry 退出

logs       Fetch the logs of a container                 # 输出当前容器日志信息
（docker logs mysql ）//mysql容器名字
port       Lookup the public-facing port which is NAT-ed to PRIVATE_PORT    # 查看映射端口对应的容器内部源端口
（docker port mysql ）
[root@localhost ~]# docker port mysql
3306/tcp -> 0.0.0.0:3306
[root@localhost ~]# docker port redis
6379/tcp -> 0.0.0.0:6379
[root@localhost ~]# docker port zk02
2181/tcp -> 0.0.0.0:2181
2888/tcp -> 0.0.0.0:2888
3888/tcp -> 0.0.0.0:3888

pause      Pause all processes within a container        # 暂停容器

docker ps         List containers                               # 列出容器列表

docker pull       Pull an image or a repository from the docker registry server   # 从docker镜像源服务器拉取指定镜像或者库镜像

docker push       Push an image or a repository to the docker registry server    # 推送指定镜像或者库镜像至docker源服务器

docker restart    Restart a running container                   # 重启运行的容器

docker rm         Remove one or more containers                 # 移除一个或者多个容器

docker rmi        Remove one or more images             # 移除一个或多个镜像[无容器使用该镜像才可删除，否则需删除相关容器才可继续或 -f 强制删除]

docker run        Run a command in a new container              # 创建一个新的容器并运行一个命令

docker save       Save an image to a tar archive       # 保存一个镜像为一个 tar 包[对应 load]
#将一个镜像保存成tar包
docker save -o cadvisor.tar google/cadvisor

docker search     Search for an image on the Docker Hub         # 在 docker hub 中搜索镜像

docker start      Start a stopped containers                    # 启动容器

docker stop       Stop a running containers                     # 停止容器

docker tag        Tag an image into a repository                # 给源中镜像打标签

docker top        Lookup the running processes of a container   # 查看容器中运行的进程信息
[root@localhost ~]# docker top  mysql
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
systemd+            54366               54350               2                   16:35               ?                   00:00:00            mysqld

unpause                Unpause a paused container                  # 取消暂停容器
docker version         Show the docker version information         # 查看 docker 版本号



wait    Block until a container stops, then print its exit code # 截取容器停止时的退出状态值
```



### 镜像命令

https://hub.docker.com/

```powershell
#查看本地镜像
[root@localhost ~]# docker images
[root@localhost ~]# docker images -a
#查询当前镜像的id
[root@localhost ~]# docker images -q
[root@localhost ~]# docker images -qa
#显示镜像的摘要信息
[root@localhost ~]# docker images --digests
[root@localhost ~]# docker images --no-trunc
[root@localhost ~]# docker search tomcat
#点赞书超过30的tomcat
[root@localhost ~]# docker search -s 300 tomcat

[root@localhost ~]# docker search -s 30 --no-trunc tomcat
[root@localhost ~]# docker search -s 30 mysql
#拉取最新版tomcat
[root@localhost ~]# docker pull tomcat
#删除镜像，
[root@localhost ~]# docker rmi hello-world
Error response from daemon: conflict: unable to remove repository reference "hello-world" (must force) - container 0f5ccae51727 is using its referenced image fce289e99eb9
[root@localhost ~]# docker rmi -f hello-world
Untagged: hello-world:latest
Untagged: hello-world@sha256:4fe721ccc2e8dc7362278a29dc660d833570ec2682f4e4194f4ee23e415e1064
Deleted: sha256:fce289e99eb9bca977dae136fbe2a82b6b7d4c372474c9235adc1741675f587e
[root@localhost ~]# 
#拉取镜像
[root@localhost ~]# docker pull nginx
#显示镜像
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
tomcat              latest              94e31e5297d1        3 days ago          507MB
nginx               latest              f7bb5701a33c        4 days ago          126MB
hello-world         latest              fce289e99eb9        12 months ago       1.84kB
#删除多个镜像
[root@localhost ~]# docker rmi -f hello-world nginx
#查询本机镜像id
[root@localhost ~]# docker images -qa
#删除所有镜像
[root@localhost ~]# docker rmi -f $(docker images -qa)
```



```powershell
#拉取centos镜像
[root@localhost ~]# docker pull centos
#在本地新建并启动容器（it启动交互式容器）
[root@localhost ~]# docker run -it  centos
[root@d6304fc75753 ~]# ps -ef
#查询宿主机centos的运行进程
[root@localhost ~]# ps
#查询docker 中的centos运行进程
[root@localhost ~]# docker ps

#docker中centos退出（关闭并退出）
[root@d6304fc75753 ~]# exit
#容器不停止退出
             ctrl+p+q        
             

#查看容器运行记录
[root@localhost ~]# docker ps -a
#查询近3 次运行的容器
[root@localhost ~]# docker ps -n 3
#查看运行中容器id
[root@localhost ~]# docker ps -q
[root@localhost ~]# docker ps -lq
[root@localhost ~]# docker run -it --name mycentos001 
#查询容器的IP地址
[root@localhost ~]# docker inspect --format='{{.NetworkSettings.IPAddress}}' 5f821fb0cae1
172.17.0.2


[root@localhost ~]#  docker ps
CONTAINER ID  IMAGE   COMMAND     CREATED     STATUS            PORTS        NAMES
28887f043c65        centos   "/bin/bash"   2 minutes ago       Up 2 minutes    mycentos001

#容器不停止退出    ctrl+p+q
[root@localhost ~]# docker start mycentos001
[root@localhost ~]# docker restart mycentos001
[root@localhost ~]# docker ps
#正常关机
[root@localhost ~]# docker stop mycentos001
#直接拔电源
[root@localhost ~]# docker kill mycentos001


[root@localhost ~]# docker ps -n 2
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
28887f043c65        centos              "/bin/bash"         23 minutes ago      Exited (0) 3 minutes ago                        mycentos001
d6304fc75753        centos              "/bin/bash"         42 minutes ago      Exited (0) 29 minutes ago                       quirky_mccarthy
#删除指定id的容器
[root@localhost ~]# docker rm d6304fc75753
d6304fc75753
[root@localhost ~]# docker ps -n 2
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                     PORTS               NAMES
28887f043c65        centos              "/bin/bash"         24 minutes ago      Exited (0) 3 minutes ago                       mycentos001
0f5ccae51727        fce289e99eb9        "/hello"            2 hours ago         Exited (0) 2 hours ago                         interesting_shtern

```



```powershell
#启动docker
[root@localhost ~]# systemctl start docker
#查询镜像
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
tomcat              latest              94e31e5297d1        4 days ago          507MB
nginx               latest              f7bb5701a33c        4 days ago          126MB
centos              latest              0f3e07c0138f        3 months ago        220MB
#运行centos
[root@localhost ~]# docker run  -it centos
[root@3317bd38c63f /]# pwd
/
#显示docker中centos实例的根目录内容
[root@3317bd38c63f /]# ls -l
total 0
lrwxrwxrwx.   1 root root   7 May 11  2019 bin -> usr/bin
drwxr-xr-x    5 root root 360 Jan  2 11:10 dev
drwxr-xr-x.   1 root root  66 Jan  2 11:10 etc
drwxr-xr-x.   2 root root   6 May 11  2019 home
lrwxrwxrwx.   1 root root   7 May 11  2019 lib -> usr/lib
lrwxrwxrwx.   1 root root   9 May 11  2019 lib64 -> usr/lib64
drwx------.   2 root root   6 Sep 27 17:13 lost+found
drwxr-xr-x.   2 root root   6 May 11  2019 media
drwxr-xr-x.   2 root root   6 May 11  2019 mnt
drwxr-xr-x.   2 root root   6 May 11  2019 opt
dr-xr-xr-x  205 root root   0 Jan  2 11:10 proc
dr-xr-x---.   2 root root 162 Sep 27 17:13 root
drwxr-xr-x.  11 root root 163 Sep 27 17:13 run
lrwxrwxrwx.   1 root root   8 May 11  2019 sbin -> usr/sbin
drwxr-xr-x.   2 root root   6 May 11  2019 srv
dr-xr-xr-x   13 root root   0 Jan  2 11:10 sys
drwxrwxrwt.   7 root root 145 Sep 27 17:13 tmp
drwxr-xr-x.  12 root root 144 Sep 27 17:13 usr
drwxr-xr-x.  20 root root 262 Sep 27 17:13 var
#后台运行centos
[root@localhost ~]# docker run -d centos
389545c3f9ebe88f304ee232b55e2531eacf6c4a881b705470709aa1c81ea7da
#docker 容器正在运行的实例
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
3317bd38c63f        centos              "/bin/bash"         3 minutes ago       Up 3 minutes                            funny_hypatia

#后台保持容器运行的方法
[root@localhost ~]# docker run -d centos /bin/sh -c "while true;do echo hello zzyy;sleep 2;done"
b39998492332501958321cf212404e9d1cd087dc6273302897eb87bc09e8a875
#查看docker中运行的容器
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
b39998492332        centos              "/bin/sh -c 'while t…"   14 seconds ago      Up 13 seconds                           silly_swirles
3317bd38c63f        centos              "/bin/bash"              12 minutes ago      Up 12 minutes                           funny_hypatia
#查看docker日志
[root@localhost ~]# docker logs b39998492332
hello zzyy
hello zzyy
#查看容器id为 b39998492332的包含时间的日志记录
[root@localhost ~]# docker logs -t  b39998492332
2020-01-02T11:23:05.403719894Z hello zzyy
2020-01-02T11:23:07.413302471Z hello zzyy
2020-01-02T11:23:09.416572345Z hello zzyy
[root@localhost ~]# docker logs -t -f b39998492332
#关闭容器
[root@localhost ~]# docker top 3317bd38c63f
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                2124                2107                0                   19:10               pts/0               00:00:00            /bin/bash
[root@localhost ~]# 

#查看容器内部细节
[root@localhost ~]# docker inspect 3317bd38c63f

#以交互的方式运行centos
[root@localhost ~]# docker run -it centos
[root@c37f5faf8779 /]# [root@localhost ~]# 
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS               NAMES
c37f5faf8779        centos              "/bin/bash"         About a minute ago   Up About a minute                       festive_pare



#重新进入docker容器
[root@localhost ~]# docker attach c37f5faf8779
[root@c37f5faf8779 /]# ls -l
total 0
lrwxrwxrwx.   1 root root   7 May 11  2019 bin -> usr/bin
drwxr-xr-x    5 root root 360 Jan  2 11:38 dev
drwxr-xr-x.   1 root root  66 Jan  2 11:38 etc
drwxr-xr-x.   2 root root   6 May 11  2019 home
lrwxrwxrwx.   1 root root   7 May 11  2019 lib -> usr/lib
lrwxrwxrwx.   1 root root   9 May 11  2019 lib64 -> usr/lib64
drwx------.   2 root root   6 Sep 27 17:13 lost+found
drwxr-xr-x.   2 root root   6 May 11  2019 media
drwxr-xr-x.   2 root root   6 May 11  2019 mnt
drwxr-xr-x.   2 root root   6 May 11  2019 opt
dr-xr-xr-x  208 root root   0 Jan  2 11:38 proc
dr-xr-x---.   2 root root 162 Sep 27 17:13 root
drwxr-xr-x.  11 root root 163 Sep 27 17:13 run
lrwxrwxrwx.   1 root root   8 May 11  2019 sbin -> usr/sbin
drwxr-xr-x.   2 root root   6 May 11  2019 srv
dr-xr-xr-x   13 root root   0 Jan  2 11:10 sys
drwxrwxrwt.   7 root root 145 Sep 27 17:13 tmp
drwxr-xr-x.  12 root root 144 Sep 27 17:13 usr
drwxr-xr-x.  20 root root 262 Sep 27 17:13 var
[root@c37f5faf8779 /]# ls -l /tmp
total 8
-rwx------. 1 root root 1379 Sep 27 17:13 ks-script-0n44nrd1
-rwx------. 1 root root  671 Sep 27 17:13 ks-script-w6m6m_20
[root@localhost ~]# docker exec -t c37f5faf8779 ls -l /tmp
total 8
-rwx------. 1 root root 1379 Sep 27 17:13 ks-script-0n44nrd1
-rwx------. 1 root root  671 Sep 27 17:13 ks-script-w6m6m_20
[root@localhost ~]# 



#从容器内拷贝文件到主机上
[root@localhost ~]# docker cp c37f5faf8779:/root/original-ks.cfg /root

```











```powershell
#开启docker
[root@localhost ~]# systemctl start docker
#查看docker版本
[root@localhost ~]# docker version
#交互启动docker中的tomcat
[root@localhost ~]# docker run -it -p 8092:8080 tomcat
[root@localhost ~]# docker run -it -p 8080:8080 tomcat
# 后台启动docker中的tomcat
[root@localhost ~]# docker run -d -p 8085:8080 tomcat
#查看宿主机中运行的线程
[root@localhost ~]#  ps
#查看docker中运行的线程
[root@localhost ~]# docker ps
#关闭运行中的容器
[root@localhost ~]# docker stop ad0ec5d93680

```



### DockerFile命令

```powershell
docker rin -it镜像名
#
docker rin -it -v /myDayaVolume:/dataVolumeContainer 镜像名
#docker中只读
docker rin -it -v /myDayaVolume:/dataVolumeContainer:ro 镜像名



docker build 镜像名字
docker push 镜像名
docker pull 镜像名
docker save/export 文件名
docker load/import 文件名

#暂停容器
docker pause 容器名
#恢复容器
docker unpause 容器名


docker inspect 容器名/镜像名

docker commit 容器名
```





### docker上安装MySQL

```powershell
docker run -p 3306:3306 --name mysql 
                     -v /zzyyuse/mysql/conf:/etc/mysql/conf.d 
                     -v /zzyyuse/mysql/logs:/logs 
                     -v /zzyyuse/mysql/data:/var/lib/mysql 
       -e MYSQL_ROOT_PASSWORD=123456 -d mysql
 
命令说明：
-p 3306:3306
		：将主机的3306端口映射到docker容器的3306端口。
--name mysql
		：运行服务名字
-v /zzyyuse/mysql/conf:/etc/mysql/conf.d 
		：将主机/zzyyuse/mysql录下的conf/my.cnf 挂载到容器的 /etc/mysql/conf.d
-v /zzyyuse/mysql/logs:/logs：
		：将主机/zzyyuse/mysql目录下的 logs 目录挂载到容器的 /logs。
-v /zzyyuse/mysql/data:/var/lib/mysql 
		：将主机/zzyyuse/mysql目录下的data目录挂载到容器的 /var/lib/mysql 
-e MYSQL_ROOT_PASSWORD=123456
		：初始化 root 用户的密码。
-d mysql 
		: 后台程序运行mysql
```







```powershell
[root@localhost ~]# systemctl start docker
[root@localhost ~]# docker images
[root@localhost ~]# docker search mysql
[root@localhost ~]# docker pull  mysql

docker run -p 3306:3306 --name mysql -v /zzyyuse/mysql/conf:/etc/mysql/conf.d -v /zzyyuse/mysql/logs:/logs -v /zzyyuse/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql



[root@localhost ~]# docker run -p 3306:3306 --name mysql -v /zzyyuse/mysql/conf:/etc/mysql/conf.d -v /zzyyuse/mysql/logs:/logs -v /zzyyuse/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql
a92a542426cd9bd229a97b4fac4d829ec9a62ddd19e9a4df97b8ce1083c62ec5
#查看容器进程
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
a92a542426cd        mysql               "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        33060/tcp, 0.0.0.0:3326->3306/tcp   mysql
#进入容器
[root@localhost ~]# docker exec -it a92a542426cd /bin/bash
root@a92a542426cd:/# pwd
/
root@a92a542426cd:/# mysql -uroot -p123456
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.18 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 


```

```powershell
#容器名重复
root@localhost ~]# docker run -p 3326:3306 --name mysql -v /zzyyuse/mysql/conf:/etc/mysql/conf.d -v /zzyyuse/mysql/logs:/logs -v /zzyyuse/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql
docker: Error response from daemon: Conflict. The container name "/mysql" is already in use by container "22213821d59aaa1385aeed9934798a879609816444abc16d052f366bb6724057". You have to remove (or rename) that container to be able to reuse that name.
See 'docker run --help'.
#删除重复的容器名
[root@localhost ~]# docker rm -f 22213821d59aaa1385aeed9934798a879609816444abc16d052f366bb6724057
22213821d59aaa1385aeed9934798a879609816444abc16d052f366bb6724057
[root@localhost ~]# 



```

### docker上安装redis

```powershell
[root@localhost ~]# docker pull redis
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
redis               latest              9b188f5fb1e6        25 hours ago        98.2MB

docker run -p 6379:6379 -v /zzyyuse/myredis/data:/data -v /zzyyuse/myredis/conf/redis.conf:/usr/local/etc/redis/redis.conf  -d redis redis-server /usr/local/etc/redis/redis.conf --appendonly yes

[root@localhost ~]# docker run -p 6379:6379 -v /zzyyuse/myredis/data:/data -v /zzyyuse/myredis/conf/redis.conf:/usr/local/etc/redis/redis.conf  -d redis redis-server /usr/local/etc/redis/redis.conf --appendonly yes
c75c606f20660f858e368faeec4a82034f443ccadad76959c271ac774a9aa67b
[root@localhost ~]# vim /zzyyuse/myredis/conf/redis.conf/redis.conf
[root@localhost ~]# cd /zzyyuse/myredis/conf/redis.conf
[root@localhost redis.conf]# ls
redis.conf
[root@localhost redis.conf]# ll
总用量 48
-rw-r--r-- 1 root root 47842 1月   4 10:55 redis.conf
[root@localhost redis.conf]# vim redis.conf 
[root@localhost redis.conf]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
c75c606f2066        redis               "docker-entrypoint.s…"   15 minutes ago      Up 15 minutes       0.0.0.0:6379->6379/tcp              unruffled_williamson
166397bce260        mysql               "docker-entrypoint.s…"   About an hour ago   Up About an hour    33060/tcp, 0.0.0.0:3326->3306/tcp   mysql
[root@localhost redis.conf]# docker exec -it c75c606f2066 redis-cli
127.0.0.1:6379> set key1 v1
OK
127.0.0.1:6379> set key2 v2
OK
127.0.0.1:6379> shutdown
[root@localhost redis.conf]# pwd
/zzyyuse/myredis/conf/redis.conf
[root@localhost redis.conf]# cd ..
[root@localhost conf]# ll
总用量 0
drwxr-xr-x 2 root root 24 1月   4 10:57 redis.conf
[root@localhost conf]# cd ..
[root@localhost myredis]# ll
总用量 0
drwxr-xr-x 3 root             root 24 1月   4 10:42 conf
drwxr-xr-x 2 systemd-coredump root 28 1月   4 10:42 data
[root@localhost myredis]# cd data/
[root@localhost data]# ll
总用量 4
-rw-r--r-- 1 systemd-coredump input 85 1月   4 11:00 appendonly.aof
[root@localhost data]# vim appendonly.aof 
[root@localhost data]# 


```

### docker常用命令

```powershell
#启动docker:  
systemctl start docker
#守护进程重启   
systemctl daemon-reload
#重启docker服务  
systemctl restart docker
service docker restart
#关闭docker   
service docker stop   
systemctl stop docker
#查看正在运行的容器
docker ps
#删除所有运行的容器
docker rm -f $(docker ps -q)
#查看所有的容器
docker ps -a
#删除所有容器
docker rm -f $(docker ps -q -a )
#查看本地镜像
docker images
#从镜像中运行/停止一个新实例
docker run containerID
（docker run redis）
docker stop containerID
（docker stop redis）
docker start containerID
（docker start redis）
#Docker版本
docker --version
#搜索Docker Image
#搜索 ubuntu镜像
docker search mysql
#通过docker命令下载ubuntu镜像
docker pull mysql
#从指定image里生成一个container并在其中运行一个命令
docker run [image] [cmd]
#在container里运行交互式命令，比如shell
docker run -i -t [image] [cmd]
#例如
docker run -i -t ubuntu /bin/bash
#在container里运行后台任务
docker run -d [image] [cmd]
#列出最近一个运行过的container不加-l则只列出正在运行的container（比如后台任务）
#删除所有运行的容器

#查看镜像、容器、数据卷所占用的空间
 docker system df
[root@localhost ~]#  docker system df 
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              4                   4                   913.8MB             69.21MB (7%)
Containers          5                   2                   38.73kB             38.61kB (99%)
Local Volumes       19                  5                   509.3MB             505MB (99%)
Build Cache         0                   0                   0B                  0B

docker rm -f $(docker ps -q)
#删除所有容器
docker rm -f $(docker ps -q -a )

 docker ps -l
#查看container详情
docker inspect c75c606f2066
#删除某个container其中container_id不需要输入完整，只要能保证唯一即可。运行中的Docker容器是无法删除的，必须先通过docker stop或者docker kill命令停止。
docker rm [container]
docker rm `docker ps -a -q`  #删除所有容器，-q表示只返回容器的ID

#查询正在运行容器的id
docker ps  -q
# 查看docker创建的所有容器
docker ps -a
#查询运行过的容器的id
docker ps -a -q
#再次运行某个container
docker start c75c606f2066
#查看正在运行的容器
docker ps 

#删除所有镜像
 docker rmi -f $(docker images)


#退出
exit



#查看某个container的运行日志
docker logs  c75c606f2066
docker logs -f c75c606f2066
#切换到后台任务container, 需要当前容器正常运行 注意：切换到后台任务以后无法用Ctrl-C退出
docker attach c75c606f2066
#中止后台任务container
docker stop c75c606f2066
#将container保存为一个image
docker commit [container] [image_name]
#将image上传到仓库
docker push [image_name]
#删除images
docker rmi [image id]
#为容器指定名称，容器的名称是唯一
docker run --name edison -i -t ubuntu /bin/bash
docker run --name myfirstubuntu -i -t ubuntu /bin/bash
	--name：是给容器起一个名字，可省略，省略的话docker会随机产生一个名字
	-i：允许我们对容器内的 (STDIN) 进行交互
	-t：在新容器内指定一个伪终端或终端
	-v：是挂在宿机目录， /docker_test是宿机目录，/yufei是当前docker容器的目录，宿机目录必须是绝对的。
	
#有三种方式可以唯一指代容器
短UUID: 716d3c16dc65（12位）
长UUID：716d3c16dc654230ada14f555faadd036474231dfca0ca44b597574a5c618565（64位）
名称: edison

#当前Docker宿主机的信息
docker info

#查看容器内部的进程信息
docker top c75c606f2066

#在容器中运行后台任务，只对正在运行的容器有效。
docker exec -d [container] [cmd]
docker exec -d edison touch /home/haha

#在容器中运行交付式任务，只对正在运行的容器有效。
docker exec -t -i c75c606f2066 /bin/bash
注：在/var/lib/docker中，可以查看Docker Image、Container和Volumes等细节信息。

#进入一个已经启动的docker容器
docker attach ContainerID ,exit 退出就会导致容器stop
docker exec -it ContainerID /bin/bash 这个命令exit退出，容器还在运行



```

```powershell
#动态刷新docker容器的CPU，内存使用情况

[root@localhost ~]# docker stats
CONTAINER ID        NAME                CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
ec9bf88cdcee        zk02                0.13%               65.48MiB / 1.764GiB   3.63%               4.93kB / 0B         118MB / 8.19kB      40
207e11ef8a6f        myrabbitmq          0.19%               97.88MiB / 1.764GiB   5.42%               4.93kB / 0B         99.7MB / 664kB      92
1e5d05ff3ea8        redis               0.14%               11.32MiB / 1.764GiB   0.63%               4.82kB / 0B         38.6MB / 0B         5
72a3345e6d3e        mysql               0.06%               189.8MiB / 1.764GiB   10.51%              4.75kB / 0B         145MB / 25.4MB      27
```



### docker中mysql3326

```powershell
docker run -p 12345:3306 --name mysql 
		-v /zzyyuse/mysql/conf:/etc/mysql/conf.d 
		-v /zzyyuse/mysql/logs:/logs 
		-v /zzyyuse/mysql/data:/var/lib/mysql 
		-e MYSQL_ROOT_PASSWORD=123456 
		-d mysql
 
命令说明：
-p 12345:3306
	：将主机的12345端口映射到docker容器的3306端口。
--name mysql
	：运行服务名字
-v /zzyyuse/mysql/conf:/etc/mysql/conf.d 
	：将主机/zzyyuse/mysql录下的conf/my.cnf 挂载到容器的 /etc/mysql/conf.d
-v /zzyyuse/mysql/logs:/logs
	：将主机/zzyyuse/mysql目录下的 logs 目录挂载到容器的 /logs。
-v /zzyyuse/mysql/data:/var/lib/mysql 
	：将主机/zzyyuse/mysql目录下的data目录挂载到容器的 /var/lib/mysql 
-e MYSQL_ROOT_PASSWORD=123456
	：初始化 root 用户的密码。
-d mysql 
	: 后台程序运行mysql
```

```powershell
docker run -P --name mysqlsui2 
	-v /zzyyuse/mysql/conf:/etc/mysql/conf.d 
	-v /zzyyuse/mysql/logs:/logs 
	-v /zzyyuse/mysql/data:/var/lib/mysql 
	-e MYSQL_ROOT_PASSWORD=123456 
	-d mysql 
```



```powershell
[root@localhost ~]# docker run -p 3326:3306 --name mysql3326 -v /zzyyuse/mysql/conf:/etc/mysql/conf.d -v /zzyyuse/mysql/logs:/logs -v /zzyyuse/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql
74f429e99e01542786dea7190e6f64a6a2149e93773c447a5adbfb3decf20058
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                               NAMES
74f429e99e01        mysql               "docker-entrypoint.s…"   About a minute ago   Up About a minute   33060/tcp, 0.0.0.0:3326->3306/tcp   mysql3326
48d7444ce21e        ubuntu              "/bin/bash"              18 minutes ago       Up 15 minutes                                           myfirstubuntu
#进入一个已经启动的docker容器
[root@localhost ~]# docker exec -it 74f429e99e01  /bin/bash
root@74f429e99e01:/# mysql -uroot -p123456
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.18 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
Initializing database

```

### 安装完mysql后的安全初始化

```powershell
root@85a866b3344d:/# mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root: 
The 'validate_password' component is installed on the server.
The subsequent steps will run with the existing configuration
of the component.
Using existing password for root.

Estimated strength of the password: 25 
Change the password for root ? ((Press y|Y for Yes, any other key for No) : j

 ... skipping.
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : ^[[A^[[A^H^H^H^[^C
root@85a866b3344d:/# mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root: 
The 'validate_password' component is installed on the server.
The subsequent steps will run with the existing configuration
of the component.
Using existing password for root.

Estimated strength of the password: 25 
Change the password for root ? ((Press y|Y for Yes, any other key for No) : 

 ... skipping.
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : 

 ... skipping.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : 

 ... skipping.
By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : 

 ... skipping.
Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : 

 ... skipping.
All done! 

```



# [docker tag 详解](https://www.cnblogs.com/pzk7788/p/10180919.html)

docker tag 用于给镜像打标签，语法如下：

```powershell
docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

① 比如我现在有一个 centos 镜像：

```powershell
[root@localhost ~]$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
centos              latest              1e1148e4cc2c        2 weeks ago         202MB
```

② 我对 centos 进行开发，开发了第一个版本，我就可以对这个版本打标签，打完标签后会生成新的镜像：

```powershell
[root@localhost ~]$ docker tag centos centos:v1
[root@localhost ~]$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
centos              latest              1e1148e4cc2c        2 weeks ago         202MB
centos              v1                  1e1148e4cc2c        2 weeks ago         202MB
```

③ 我继续对 centos 进行开发，开发了第二个版本，继续打标签：

```powershell
[root@localhost ~]$ docker tag centos centos:v2
[root@localhost ~]$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
centos              latest              1e1148e4cc2c        2 weeks ago         202MB
centos              v1                  1e1148e4cc2c        2 weeks ago         202MB
centos              v2                  1e1148e4cc2c        2 weeks ago         202MB
```

④ 以此类推，每开发一个版本打一个标签，如果以后我想回滚版本，就可以使用指定标签的镜像来创建容器：

```powershell
[root@localhost ~]$ docker run -itd centos:v1
```

# Docker容器内不能联网的6种解决方案

 更新时间：2018年10月15日 14:18:02  作者：数据架构师  [![img](https://www.jb51.net/skin/2018/images/text-message.png) 我要评论](https://www.jb51.net/article/148874.htm#comments)

今天小编就为大家分享一篇关于Docker容器内不能联网的6种解决方案，小编觉得内容挺不错的，现在分享给大家，具有很好的参考价值，需要的朋友一起跟随小编来看看吧

**Docker容器内不能联网的6种解决方案**

注：下面的方法是在容器内能ping通公网IP的解决方案，如果连公网IP都ping不通，那主机可能也上不了网（尝试ping 8.8.8.8）

**1.使用–net:host选项**



```powershell
sudo docker run --net:host --name ubuntu_bash -i -t ubuntu:latest /bin/bash
```

**2.使用–dns选项**



```powershell
sudo docker run --dns 8.8.8.8 --dns 8.8.4.4 --name ubuntu_bash -i -t ubuntu:latest /bin/bash
```

**3.改dns server**

vi /etc/default/docker
去掉“docker_OPTS="–dns 8.8.8.8 --dns 8.8.4.4"”前的#号

**4.不用dnsmasq**

vi /etc/NetworkManager/NetworkManager.conf
在dns=dnsmasq前加个#号注释掉



```powershell
sudo restart network-manager``sudo restart docker
```

**5.重建docker0网络**



```powershell
pkill docker       #终止进程``
iptables -t nat -F     #清空nat表的所有链``
ifconfig docker0 down  #停止docker默认网桥``
brctl delbr docker0    #删除网桥``
docker -d
```

6.**直接在docker内修改/etc/hosts**

**总结**

以上就是这篇文章的全部内容了，希望本文的内容对大家的学习或者工作具有一定的参考学习价值，谢谢大家对脚本之家的支持。如果你想了解更多相关内容请查看下面相关链接

docker安装centos7镜像

拉取centos7镜像

```powershell
[root@localhost ~]# docker pull centos:7
```

拉取centos7镜像

启动镜像centos7，如果不指定 /bin/bash，容器运行后会自动停止

```powershell
[root@localhost ~]# docker run -d -i -t <IMAGE ID> /bin/bash
```

进入容器

```powershell
[root@localhost ~]# docker exec -it <CONTAINER ID> bash
```

进入后发现没有ifconfig，直接yum安装

```powershell
[root@8f10fbd6bd5a /]# yum install -y net-tools
```

无法ping外网，查找资料重建docker0网络解决，资料地址

```powershell

[root@localhost ~]# pkill docker   
[root@localhost ~]# iptables -t nat -F
[root@localhost ~]# ifconfig docker0 down
[root@localhost ~]# brctl delbr docker0
[root@localhost ~]# systemctl restart docker
[root@localhost ~]# docker start <CONTAINER ID>
```

————————————————
版权声明：本文为CSDN博主「marke_huang」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/marke_huang/article/details/82996909

(2条消息)（centos7）创建支持ssh服务的docker镜像_u013140345的博客-CSDN博客  https://blog.csdn.net/u013140345/article/details/79777311



### 阿里云docker镜像加速器的配置

```powershell
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://x0ue3znl.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### docker安装mysql:5.7

```powershell
#设置docker开机自起
[root@localhost ~]# systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[root@localhost ~]# systemctl start docker
[root@localhost ~]# docker images
[root@localhost ~]# docker pull mysql:5.7
#创建mysql实例
[root@localhost ~]# docker run -p 3306:3306 --name mysql -v /mydata/mysql/log:/var/log/mysql -v/mydata/mysql/data:/var/lib/mysql -v /mydata/mysql/conf:/etc/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
72a3345e6d3ea66bf1281f07cee789607f9dd2b6ac648e42728ec88275267e90
```

```powershell
docker run -p 3306:3306 --name mysql 
	-v /mydata/mysql/log:/var/log/mysql 
	-v/mydata/mysql/data:/var/lib/mysql 
	-v /mydata/mysql/conf:/etc/mysql 
	-e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
	
-p 3306:3306                              将容器的3306端口映射到主机的3306端口
--name mysql                              给容器命名
-v /mydata/mysql/log:/var/log/mysql       将日志文件夹挂载到主机
-v/mydata/mysql/data:/var/lib/mysql       将数据文件夹挂载到主机
-v /mydata/mysql/conf:/etc/mysql          将配置文件夹挂载到主机
-e MYSQL_ROOT_PASSWORD=123456             初始化root密码
-d mysql:5.7



```

### my.cnf 

```powershell
[client]
port = 3306
default-character-set=utf8
port = 3306
[mysql]
default-character-set=utf8
[mysqld]
#此配置用于解决 mysql 使用时的乱码问题。
init_connect='SET collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve
```



```powershell
#以交互的方式进入mysql容器的内部
[root@localhost ~]# docker exec -it  mysql /bin/bash
root@72a3345e6d3e:/# 
root@72a3345e6d3e:/# whereis mysql
mysql: /usr/bin/mysql /usr/lib/mysql /etc/mysql /usr/share/mysql
root@72a3345e6d3e:/# mysql -uroot -p123456
#重新查找
[root@localhost conf]# cat my.cnf 
[client]
default-character-set=utf8
[mysqld]
character_set_server=utf8
collation-server=utf8_general_ci
[mysql]
default-character-set=utf8

root@72a3345e6d3e:~# mysql --help | grep my.cnf
                      order of preference, my.cnf, $MYSQL_TCP_PORT,
/etc/my.cnf /etc/mysql/my.cnf ~/.my.cnf 
root@72a3345e6d3e:~# 
root@72a3345e6d3e:/# mysql -uroot -p123456
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.30 MySQL Community Server (GPL)

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show variables like '%char%'
    -> ;
+--------------------------+----------------------------+
| Variable_name            | Value                      |
+--------------------------+----------------------------+
| character_set_client     | utf8                       |
| character_set_connection | utf8                       |
| character_set_database   | utf8                       |
| character_set_filesystem | binary                     |
| character_set_results    | utf8                       |
| character_set_server     | utf8                       |
| character_set_system     | utf8                       |
| character_sets_dir       | /usr/share/mysql/charsets/ |
+--------------------------+----------------------------+
8 rows in set (0.03 sec)

mysql> \q
Bye



[root@localhost conf]# docker restart mysql
mysql

设置docker中mysql自动启动
[root@localhost ~]# docker update mysql --restart=always
mysql

```



### docker安装redis

```powershell
[root@localhost ~]# mkdir -p /mydata/redis/conf/
[root@localhost ~]# touch /mydata/redis/conf/redis.conf

[root@localhost ~]# docker pull redis
[root@localhost conf]# docker run -p 6379:6379 --name redis -v /mydata/redis/data:/data -v /mydata/redis/conf/redis.conf:/etc/redis/redis.conf -d redis redis-server /etc/redis/redis.conf
1e5d05ff3ea8cb036c5e3a8b920a9b8ef6ff0162369fe12fd67732cb7de0d598
[root@localhost conf]# docker exec -it redis redis-cli
127.0.0.1:6379> set a b 
OK
127.0.0.1:6379> get a
"b"
127.0.0.1:6379>exit
[root@localhost conf]# docker restart redis
redis
127.0.0.1:6379> get a
(nil)
127.0.0.1:6379> exit
[root@localhost conf]# pwd
/mydata/redis/conf
#开启持久化
[root@localhost conf]# vim redis.conf 
[root@localhost conf]# cat redis.conf 
appendonly yes
[root@localhost conf]# docker restart redis
redis
[root@localhost conf]# docker exec -it redis redis-cli
127.0.0.1:6379> set a b
OK
127.0.0.1:6379> exit
[root@localhost conf]# docker exec -it redis redis-cli
127.0.0.1:6379> get a
"b"
127.0.0.1:6379> 
#
[root@localhost ~]# docker update redis --restart=always

```



### docker安装rabbitmq

### RabbitMQ 的 docker 镜像使用

1.下载镜像（management版本的才带有web管理界面）

```powershell
docker pull rabbitmq:3.7.18-management
```

2.创建容器（监听端口：5672，web管理界面端口：15672）

```powershell
docker run --name myrabbitmq -d -p 5672:5672 -p 15672:15672 rabbitmq:3.7.18-management
```

3.使用默认用户名、密码：guest/guest 登陆

http://192.168.40.100:15672

```powershell
http://192.168.40.100:15672
```

```powershell
#1.安装rabbitmq，使用镜像加速
#下载的是有管理界面的镜像
[root@localhost ~]# docker pull rabbitmq:3.7.18-management
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
rabbitmq            3.7.18-management   2a0e4a1d8b3e        8 months ago        179MB
#2.运行rabbitmq
##### 端口：5672 客户端和rabbitmq通信 15672：管理界面的web页面

docker run -d -p 5672:5672 -p 15672:15672 --name myrabbitmq rabbitmq:3.7.18-management

#3.查看运行
docker ps
#设置docker中myrabbitmq自动启动
[root@localhost ~]# docker update myrabbitmq --restart=always
myrabbitmq
```



http://192.168.40.100:15672/

### 安装Zookeeper



```powershell
[root@localhost ~]# docker pull zookeeper
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
zookeeper           latest              411f5595aa97        13 days ago         252MB
[root@localhost ~]# docker run -d --name zk02 -p 2181:2181  zookeeper  --restart always

zookeeper client port 2181
follower port 2888
election port 3888
docker run -d --name zk02 -p 2181:2181 -p 2888:2888 -p 3888:3888 --restart always zookeeper  
[root@localhost ~]# docker exec -it zk02 bash
```



### 3、Dubbo、Zookeeper整合



# [Docker：docker国内镜像加速](https://www.cnblogs.com/nhdlb/p/12567154.html)





#### [启动java项目时出现 com.sun.xml.internal.bind.v2.ContextFactory 异常](https://blog.csdn.net/weixin_44339867/article/details/100605838)





# docker中配置主从复制



### docker创建MySQL1  、MySQL2

```powershell
[root@localhost ~]# mkdir -p /mydata/mysql1/log
[root@localhost ~]# mkdir -p /mydata/mysql1/data
[root@localhost ~]# mkdir -p /mydata/mysql1/conf
[root@localhost ~]# 

docker run -p 3308:3306 --name mysql1 -v /mydata/mysql1/log:/var/log/ -v/mydata/mysql1/data:/var/lib/mysql -v /mydata/mysql1/conf:/etc/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7

 mkdir -p /mydata/mysql2/log
 mkdir -p /mydata/mysql2/data
 mkdir -p /mydata/mysql2/conf
docker run -p 3309:3306 --name mysql2 -v /mydata/mysql2/log:/var/log/ -v/mydata/mysql2/data:/var/lib/mysql -v /mydata/mysql2/conf:/etc/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
```

### my.cnf 

```
[client]
default-character-set=utf8
[mysqld]
character_set_server=utf8
collation-server=utf8_general_ci
[mysql]
default-character-set=utf8
```

### 主库mysql1配置文件my.cnf 

```
[root@localhost conf]# cd 
[root@localhost ~]# cd /mydata/mysql1/conf
[root@localhost conf]# vim my.cnf
[root@localhost conf]# cat my.cnf 
```

```
[client]
port = 3306
default-character-set=utf8

[mysql]
port = 3306
default-character-set=utf8

[mysqld]
character_set_server=utf8
collation-server=utf8_general_ci


#开启日志
log_bin=mysql-bin
#设置服务id，主从不能一致
server-id=1
#设置需要同步的数据库
binlog-do-db=user_db
#屏蔽系统库同步
binlog-ignore-db=mysql
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
```

主库mysql2配置文件my.cnf 

```
[root@localhost ~]# cd /mydata/mysql2/conf
[root@localhost conf]# vim my.cnf
[root@localhost conf]# cat my.cnf 
```

```
[client]
port = 3306
default-character-set=utf8

[mysql]
port = 3306
default-character-set=utf8

[mysqld]
character_set_server=utf8
collation-server=utf8_general_ci

#开启日志
log-bin = mysql-bin
#设置服务id，主从不能一致
server-id = 2


#设置需要同步的数据库
replicate_wild_do_table=user_db.%
#屏蔽系统库同步
replicate_wild_ignore_table=mysql.%
replicate_wild_ignore_table=information_schema.%
replicate_wild_ignore_table=performance_schema.%

```

```powershell
[root@localhost conf]# docker restart mysql1
mysql1
[root@localhost conf]# docker restart mysql2
mysql2
[root@localhost conf]# docker logs mysql1
[root@localhost conf]# docker logs mysql2
```

### 主库MySQL1操作

```
#以交互的方式进入mysql容器的内部
docker exec -it  mysql1 /bin/bash
mysql -uroot -p123456

[root@localhost ~]# docker exec -it  mysql1 /bin/bash
root@89542e93e640:/# mysql -uroot -p123456
GRANT REPLICATION SLAVE ON *.* TO 'db_sync'@'%' IDENTIFIED BY 'db_sync';
#show master status;
mysql> show master status\G
*************************** 1. row ***************************
             File: mysql-bin.000004
         Position: 154
     Binlog_Do_DB: user_db
 Binlog_Ignore_DB: mysql,information_schema,performation_schema,sys
Executed_Gtid_Set: 
1 row in set (0.00 sec)


ERROR: 
No query specified
mysql>  GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456'  ;
mysql> flush privileges; 

```

### 从库MySQL2操作

```powershell
mysql> [root@localhost ~]# docker exec -it  mysql2 /bin/bash
root@fb8a9fe039e6:/# mysql -uroot -p123456
mysql> stop slave;
mysql>
CHANGE MASTER TO 
MASTER_HOST='192.168.40.129',
MASTER_PORT=3308,
MASTER_USER='db_sync',
MASTER_PASSWORD='db_sync',
MASTER_LOG_FILE='mysql-bin.000004',
MASTER_LOG_POS=154; 

mysql> START SLAVE;
Query OK, 0 rows affected (0.00 sec)


mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.40.129
                  Master_User: db_sync
                  Master_Port: 3308
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000004
          Read_Master_Log_Pos: 154
               Relay_Log_File: fb8a9fe039e6-relay-bin.000002
                Relay_Log_Pos: 320
        Relay_Master_Log_File: mysql-bin.000004
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: user_db.%
  Replicate_Wild_Ignore_Table: mysql.%,information_schema.%,performance_schema.%
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 154
              Relay_Log_Space: 534
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: cf2e8578-c4ec-11ea-9870-0242ac110002
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)

ERROR: 
No query specified
mysql>  GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456'  ;
mysql> flush privileges; 





```





### minio

```shell
# 9000  端口是API 通信端口， 9001 是web管理台地址
docker run -p 9000:9000 -p 9001:9001 --name minio \
  -e "MINIO_ACCESS_KEY=lengleng" \
  -e "MINIO_SECRET_KEY=lengleng" \
  minio/minio:RELEASE.2021-08-25T00-41-18Z.fips server /data --console-address ":9001"
  
  
  docker update minio --restart=always
```

http://192.168.40.128:9001/



access-key : lengleng

secert-key : lengleng



