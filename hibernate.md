

```xml
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-core</artifactId>
    <version>5.4.9.Final</version>
</dependency>
```

```xml
<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE hibernate-configuration PUBLIC
        "-//Hibernate/Hibernate Configuration DTD//EN"
        "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd">
<hibernate-configuration>
    <session-factory>
        <!--使用Tomcat数据源连接MySQL数据库-->
        <property name="hibernate.connection.datasource"></property>
        <!--SQL方言-->
        <property name="hibernate.dialect">org.hibernate.dialect.MySQL8Dialect</property>
        <!--启用Hibernate自动会话环境管理-->
        <property name="current_session_context_class">thread</property>
        <!--不启用hibernate二级缓存-->
        <property name="cache.provider_class">org.hibernate.cache.internal.NoCachingRegionFactory</property>
        <!--显示生成的SQL语句-->
        <property name="show_sql">true</property>
        <!--引入类与表的映射文件-->
        <!-- <mapping resource=""/>-->

    </session-factory>
</hibernate-configuration>
```

Tomcat配置连接池 conf子目录下context.xml文件

```xml
<?xml version='1.0' encoding='utf-8'?>
<content>
    name="crmdb"
    auth="Container"
    type="javax.sql.DataSource"
    driverClassName="com.mysql.cj.jdbc.Driver"
    maxIdle="2"
    maxWait="5000"
    url="jdbc:mysql://localhost:3306/crmdb?"
    username="root"
    password="123456"
    maxActive="20"
    removeAbandoned="true"
    removeAbandonedTimeOut="5"
    logAbandoned="true"
</content>
```

Hibernate配置使用C3P0;

```xml
<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE hibernate-configuration PUBLIC
        "-//Hibernate/Hibernate Configuration DTD//EN"
        "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd">
<hibernate-configuration>
    <session-factory>
        <property name="connection.url">
            <![CDATA[jdbc:mysql://localhost:3306/cityoa?useUnicode=true&serverTimezone=GMT]]></property>
        <property name="connection.driver_class">com.mysql.cj.jdbc.Driver</property>
        <property name="connection.username">root</property>
        <property name="connection.password">123456</property>
        <property name="show_sql">true</property>
        <!-- hibernate配置使用C3P0-->
        <!--最大连接数-->
        <property name="hibernate.c3p0.max_size">20</property>
        <!--最小连接数-->
        <property name="hibernate.c3p0.min_size">5</property>
        <!--获得连接的超时时间，如果超过这个时间，会抛出异常，单位秒-->
        <property name="hibernate.c3p0.timeout">120</property>
        <!--最大PreparedStatement的数量-->
        <property name="hibernate.c3p0.max_statements">100</property>
        <!--每隔120秒检查连接池的空闲连接，单位秒-->
        <property name="hibernate.c3p0.idle_test_period">120</property>
        <!--当连接池里面的连接用完的时候，C3P0一下获取的新的连接数-->
        <property name="hibernate.c3p0.acquire_increment">2</property>
        <property name="current_session_context_class">thread</property>
        <mapping resource="OaDepartment.hbm.xml"/>
       
    </session-factory>
</hibernate-configuration>
```

![image-20191211111243794](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20191211111243794.png)



使用连接池框架Proxool连接数据库

