### 把光盘设备中的系统镜像挂载到/media/cdrom 目录

```powershell
[root@localhost ~]# mkdir -p /media/cdrom
[root@localhost ~]# mount /dev/cdrom /media/cdrom/
mount: /media/cdrom: WARNING: device write-protected, mounted read-only.
[root@localhost ~]# vim /etc/yum.repos.d/CentOS-Base.repo

[rhel8]
name=rhel7
baseurl=file:///media/cdrom
enabled=1
gpgcheck=0



```





###  yum makecache

### centos练习

```
[root@localhost ~]# yum install httpd
[root@localhost ~]# systemctl start httpd
[root@localhost ~]# systemctl enable httpd
```

在宿主机浏览器输入http://192.168.40.100/



```powershell
网站数据目录 /var/www/html
[root@localhost ~]# vim /var/www/html/index.html 
[root@localhost ~]# cat /var/www/html/index.html 
黑胡子哈哈哈
在宿主机浏览器输入http://192.168.40.100/即可显示上述文字
#修改网站数据默认目录
[root@localhost ~]# mkdir /home/wwwroot
#Apache主配置文件
[root@localhost ~]# vim /etc/httpd/conf/httpd.conf 
#自定义网站的默认访问文夹夹
DocumentRoot "/home/wwwroot"
#
# Relax access to content within /var/www.
#
<Directory "/home/wwwroot">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>
#重启服务
[root@localhost ~]# systemctl restart httpd
[root@localhost ~]# systemctl enable httpd
在宿主机浏览器输入http://192.168.40.100/
[root@localhost ~]# vim /home/wwwroot/index.html
[root@localhost ~]# cat /home/wwwroot/index.html
ihduashdhak
在宿主机浏览器输入http://192.168.40.100/
被禁止没权限
[root@localhost ~]# ls -ldZ /var/www/html/
drwxr-xr-x. 2 root root system_u:object_r:httpd_sys_content_t:s0 24 4月  28 13:46 /var/www/html/
[root@localhost ~]# ls -ldZ /home/wwwroot/
drwxr-xr-x. 2 root root unconfined_u:object_r:user_home_dir_t:s0 24 4月  28 13:56 /home/wwwroot/
##SELinux 域临时关闭
[root@localhost ~]# setenforce 0
在宿主机浏览器输入http://192.168.40.100/
可访问
[root@localhost ~]# setenforce 1

 #SELinux 域
[root@localhost ~]# vim /etc/selinux/config
[root@localhost ~]# cat /etc/selinux/config

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=enforcing
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected. 
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted

[root@localhost ~]# getenforce
Enforcing
[root@localhost ~]# semanage fcontext -a -t httpd_sys_content_t
file_spec option is needed for add

[root@localhost ~]# semanage fcontext -a -t httpd_sys_content_t /home/wwwroot
[root@localhost ~]# 
[root@localhost ~]# semanage fcontext -a -t httpd_sys_content_t /home/wwwroot/*
[root@localhost ~]# restorecon -Rv /home/wwwroot
Relabeled /home/wwwroot from unconfined_u:object_r:user_home_dir_t:s0 to unconfined_u:object_r:httpd_sys_content_t:s0
Relabeled /home/wwwroot/index.html from unconfined_u:object_r:user_home_t:s0 to unconfined_u:object_r:httpd_sys_content_t:s0















```

