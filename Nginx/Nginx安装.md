### 1.安装 pcre 依赖

```powershell
[root@localhost ~]# cd /usr/src
#将下载好的pcre-8.43.tar.gz 复制到当前目录下
[root@localhost src]# rz -E
rz waiting to receive.
[root@localhost src]# ls
debug  kernels  nginx-1.17.7  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz  pcre-8.43.tar.gz
[root@localhost src]# tar -xvf pcre-8.43.tar.gz 
[root@localhost src]# cd pcre-8.43/
[root@localhost pcre-8.43]# ./configure
[root@localhost pcre-8.43]# make && make install
[root@localhost pcre-8.43]# pcre-config --version
8.43


```

```powershell

#出现如下，可安装第二项；可能是因为没有安装gcc-c++的缘故
[root@localhost pcre-8.43]# make && make install
make: *** 没有指明目标并且找不到 makefile。 停止。


#安装gcc环境
yum install gcc-c++
#查看是否安装gcc
rpm -qa|grep gcc
```



### 2.安装其他依赖

```powershell
[root@localhost pcre2-10.34]# yum -y install make zlib zlib-devel gcc-c++ libtool 
```

### 3.安装Nginx

```powershell
[root@localhost src]# ls
debug  kernels  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz
[root@localhost src]# tar -xvf nginx-1.17.7.tar.gz 
[root@localhost src]# ls
debug  kernels  nginx-1.17.7  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz
[root@localhost src]# cd nginx-1.17.7/
[root@localhost nginx-1.17.7]# ls
auto  CHANGES  CHANGES.ru  conf  configure  contrib  html  LICENSE  man  README  src
[root@localhost nginx-1.17.7]# ./configure
[root@localhost pcre-8.43]# make && make install
```

### 4.启动

```powershell
[root@localhost src]# cd ../ 
[root@localhost usr]# cd local/
[root@localhost local]# ls
bin  etc  games  include  lib  lib64  libexec  nginx  redis  sbin  share  src
[root@localhost local]# cd nginx/
[root@localhost nginx]# ls
conf  html  logs  sbin
[root@localhost sbin]# cd /usr/local/nginx/sbin
[root@localhost sbin]# ls
nginx
#启动nginx
[root@localhost sbin]# ./nginx 
#查看进程
[root@localhost sbin]# ps -ef | grep nginx
root      22618   1856  0 11:16 ?        00:00:00 nginx: master process /usr/local/nginx/sbin/nginx
nobody    22619  22618  0 11:16 ?        00:00:00 nginx: worker process
root      22634  17686  0 11:18 pts/0    00:00:00 grep --color=auto nginx
[root@localhost sbin]# 

```



### 5.访问



```powershell

[root@localhost conf]# pwd
/usr/local/nginx/conf
#查看nginx的端口号为80
[root@localhost conf]# vim nginx.config
#查看防火墙状态
[root@localhost sbin]# firewall-cmd --state
running
#查看开放的端口
[root@localhost sbin]# firewall-cmd --list-ports
8080/tcp 6379/tcp 3306/tcp
#打开80端口防火墙
[root@localhost sbin]# firewall-cmd --zone=public --add-port=80/tcp --permanent
success
[root@localhost sbin]# firewall-cmd --list-ports
8080/tcp 6379/tcp 3306/tcp
#重新加载防火墙配置
[root@localhost sbin]# firewall-cmd --reload
success
#开放80端口后，查看开放的端口
[root@localhost sbin]# firewall-cmd --list-ports
8080/tcp 6379/tcp 3306/tcp 80/tcp
[root@localhost sbin]# 

```

6.连接

http://192.168.40.129:80



### nginx操作的常用命令

```powershell

[root@localhost ~]# cd /usr/local/nginx/sbin/
#启动nginx
[root@localhost sbin]# ./nginx 
#查看版本号
[root@localhost sbin]# ./nginx -v
nginx version: nginx/1.17.7
[root@localhost sbin]# 
#启动nginx
[root@localhost sbin]# ./nginx 
#查看进程
[root@localhost sbin]# ps -ef | grep nginx
root      22226      1  0 13:16 ?        00:00:00 nginx: master process ./nginx
nobody    22227  22226  0 13:16 ?        00:00:00 nginx: worker process
root      22238  22120  0 13:16 pts/2    00:00:00 grep --color=auto nginx
#关闭
[root@localhost sbin]# ./nginx -s stop
#重新加载
[root@localhost sbin]# ./nginx -s reload

```

### nginx配置文件

```powershell
#配置文件的位置
[root@localhost conf]# ls
fastcgi.conf          fastcgi_params.default  mime.types          nginx.conf.default   uwsgi_params
fastcgi.conf.default  koi-utf                 mime.types.default  scgi_params          uwsgi_params.default
fastcgi_params        koi-win                 nginx.conf          scgi_params.default  win-utf
[root@localhost conf]# pwd
/usr/local/nginx/conf




```

C:\Windows\System32\drivers\etc

文件hosts中添加：

192.168.40.132  www.123.com



### [解决nginx配置负载均衡时invalid host in upstream报错](https://www.cnblogs.com/zerofc/p/10577322.html)

### [配置 nginx server 出现nginx: emerg\] "root" directive is duplicate in /etc/nginx/server/blogs.conf:7](https://www.cnblogs.com/wangkongming/p/4290520.html)





