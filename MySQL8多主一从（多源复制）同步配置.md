## MySQL8多主一从（多源复制）同步配置 



多主一从，也称为多源复制，数据流向：

- 主库1 -> 从库s
- 主库2 -> 从库s
- 主库n -> 从库s



# 应用场景

- 数据汇总，可将多个主数据库同步汇总到一个从数据库中，方便数据统计分析。
- 读写分离，从库只用于查询，提高数据库整体性能。



# 部署环境

  注：使用docker部署mysql实例，方便快速搭建演示环境。但本文重点是讲解主从配置，因此简略描述docker环境构建mysql容器实例。

- 数据库：MySQL 8.018 （相比5.5，5.6而言，5.7同步性能更好，支持多源复制，可实现多主一从，主从库版本应保证一致）

- 操作系统：CentOS 8.x

- 容器：Docker 17.09.0-ce

- 镜像：mysql

- 主库300：IP=192.168.40.129; PORT=4300; server-id=300; database=test3; table=user

- 主库400：IP=192.168.40.129; PORT=4400; server-id=400; database=test4; table=user

- 主库500：IP=192.168.40.129; PORT=4500; server-id=500; database=test5; table=user

- 从库10345：IP=192.168.40.129; PORT=4345; server-id=10345; database=test3,test4,test5; table=user

  



# 开放端口防火墙

```powershell

[root@localhost ~]# firewall-cmd --zone=public --add-port=4300/tcp --permanent
[root@localhost ~]# firewall-cmd --zone=public --add-port=4400/tcp --permanent
[root@localhost ~]# firewall-cmd --zone=public --add-port=4500/tcp --permanent
[root@localhost ~]# firewall-cmd --zone=public --add-port=4345/tcp --permanent
#重启防火墙
[root@localhost ~]# systemctl restart firewalld.service

```







# 配置约束

- 主从库必须保证网络畅通可访问
- 主库必须开启binlog日志
- 主从库的server-id必须不同



# 【主库300】操作及配置



## 配置my.cnf

```powershell
[client]
port = 3306
default-character-set = utf8mb4

[mysql]
port = 3306
default-character-set = utf8mb4

[mysqld]
##########################
# summary
##########################
#bind-address = 0.0.0.0
#port = 3306
#datadir=/datavol/mysql/data #数据存储目录

##########################
# log bin
##########################
server-id = 300			#必须唯一
log_bin = mysql-bin 	#开启及设置二进制日志文件名称
binlog_format = MIXED
sync_binlog = 1
expire_logs_days =7		#二进制日志自动删除/过期的天数。默认值为0，表示不自动删除。

#binlog_cache_size = 128m
#max_binlog_cache_size = 512m
#max_binlog_size = 256M

binlog-do-db = test3 	#要同步的数据库

binlog-ignore-db = mysql 	#不需要同步的数据库 
binlog_ignore_db = information_schema
binlog_ignore_db = performation_schema
binlog_ignore_db = sys
			

##########################
# character set
##########################
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```



## 安装启动

```powershell
[root@localhost ~]# docker run -d -p 4300:3306 --name=mysql-300 -v /datavol/mysql-300/conf:/etc/mysql/conf.d -v /datavol/mysql-300/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
507d424daf4f1186f48a4bb2567da33e6b54d8e92d7974d5fab88b368bc7feeb
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
507d424daf4f        mysql               "docker-entrypoint.s…"   6 minutes ago       Up 6 minutes        33060/tcp, 0.0.0.0:4300->3306/tcp   mysql-300
[root@localhost ~]# docker exec -it mysql-300 /bin/bash
root@507d424daf4f:/# mysql -uroot -p
Enter password: 
```

注：若不熟悉docker，可使用传统方式安装mysql，效果相同。

```powershell
[root@localhost ~]# firewall-cmd --zone=public --add-port=4300/tcp --permanent
success
[root@localhost ~]# systemctl restart firewalld.service
[root@localhost ~]# firewall-cmd --list-ports 
8080/tcp 6379/tcp 3306/tcp 80/tcp 3326/tcp 8092/tcp 33060/tcp 4300/tcp

```



## 创建授权用户

连接mysql主数据库，键入命令mysql -u root -p，输入密码后登录数据库。创建用户用于从库同步复制，授予复制、同步访问的权限

```powershell
mysql> grant replication slave on *.* to 'slave'@'%' identified by '123456';
Query OK, 0 rows affected (0.00 sec)
若提示不能用grant创建用户，mysql8.0以前的版本可以使用grant在授权的时候隐式的创建用户，8.0以后已经不支持，所以必须先创建用户，然后再授权，命令如下：
```

```powershell
#mysql8.o以上使用下边授权
CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
GRANT all privileges ON *.* TO 'slave'@'%';
```



## log_bin是否开启

```
mysql> show variables like 'log_bin';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_bin       | ON    |
+---------------+-------+
1 row in set
```



## 查看master状态

```powershell
mysql>  show master status \G;
*************************** 1. row ***************************
             File: mysql-bin.000003
         Position: 854
     Binlog_Do_DB: test3
 Binlog_Ignore_DB: mysql,information_schema,performation_schema,sys
Executed_Gtid_Set: 
1 row in set (0.00 sec)
```



# 【主库400】配置及操作



## 配置my.cnf

```
[client]
port = 3306
default-character-set = utf8mb4

[mysql]
port = 3306
default-character-set = utf8mb4

[mysqld]
##########################
# summary
##########################
#bind-address = 0.0.0.0
#port = 3306
#datadir=/datavol/mysql/data #数据存储目录

##########################
# log bin
##########################
server-id = 400			#必须唯一
log_bin = mysql-bin 	#开启及设置二进制日志文件名称
binlog_format = MIXED
sync_binlog = 1
expire_logs_days =7		#二进制日志自动删除/过期的天数。默认值为0，表示不自动删除。

#binlog_cache_size = 128m
#max_binlog_cache_size = 512m
#max_binlog_size = 256M

binlog-do-db = test4 		#要同步的数据库

binlog-ignore-db = mysql 	#不需要同步的数据库 
binlog_ignore_db = information_schema
binlog_ignore_db = performation_schema
binlog_ignore_db = sys
			

##########################
# character set
##########################
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```



## 安装启动

```powershell
[root@localhost ~]# docker run -d -p 4400:3306 --name=mysql-400 -v /datavol/mysql-400/conf:/etc/mysql/conf.d -v /datavol/mysql-400/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
39210bd10d6fcef8d7033c24c13caca21959160679decd639de1f2e5d1b61f6c
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
39210bd10d6f        mysql               "docker-entrypoint.s…"   28 seconds ago      Up 21 seconds       33060/tcp, 0.0.0.0:4400->3306/tcp   mysql-400
af3ac584994e        mysql               "docker-entrypoint.s…"   About an hour ago   Up About an hour    33060/tcp, 0.0.0.0:4345->3306/tcp   mysql-10345
507d424daf4f        mysql               "docker-entrypoint.s…"   2 hours ago         Up 2 hours          33060/tcp, 0.0.0.0:4300->3306/tcp   mysql-300
[root@localhost ~]# docker exec -it mysql-400 /bin/bash
root@39210bd10d6f:/# mysql -u root -p
Enter password:
```



## 创建授权用户

创建用户用于从库同步复制，授予复制、同步访问的权限

```
mysql> grant replication slave on *.* to 'slave'@'%' identified by '123456';
Query OK, 0 rows affected (0.00 sec)
```

```powershell
#mysql8.o以上使用下边授权
CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
GRANT all privileges ON *.* TO 'slave'@'%';
```



## log_bin是否开启

```
mysql> show variables like 'log_bin';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_bin       | ON    |
+---------------+-------+
1 row in set
```



## 查看master状态

```powershell
mysql> show master status \G;
*************************** 1. row ***************************
             File: mysql-bin.000003
         Position: 657
     Binlog_Do_DB: test4
 Binlog_Ignore_DB: mysql,information_schema,performation_schema,sys
Executed_Gtid_Set: 
1 row in set (0.00 sec)she
```



# 【主库500】配置及操作



## 配置my.cnf

```powershell
[client]
port = 3306
default-character-set = utf8mb4

[mysql]
port = 3306
default-character-set = utf8mb4

[mysqld]
##########################
# summary
##########################
#bind-address = 0.0.0.0
#port = 3306
#datadir=/datavol/mysql/data #数据存储目录

##########################
# log bin
##########################
server-id = 500			#必须唯一
log_bin = mysql-bin 	#开启及设置二进制日志文件名称
binlog_format = MIXED
sync_binlog = 1
expire_logs_days =7		#二进制日志自动删除/过期的天数。默认值为0，表示不自动删除。

#binlog_cache_size = 128m
#max_binlog_cache_size = 512m
#max_binlog_size = 256M

binlog-do-db = test5 		#要同步的数据库

binlog-ignore-db = mysql 	#不需要同步的数据库 
binlog_ignore_db = information_schema
binlog_ignore_db = performation_schema
binlog_ignore_db = sys
			

##########################
# character set
##########################
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```



## 安装启动

```
[root@localhost ~]# docker run -d -p 4500:3306 --name=mysql-500 -v /datavol/mysql-500/conf:/etc/mysql/conf.d -v /datavol/mysql-500/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
129439b3bdddc1dc76c44e86cc6b72905e7deb601845eaa6a1c34851d9329afb
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE                               COMMAND                  CREATED             STATUS              PORTS                                                NAMES
129439b3bddd        mysql:5.7                           "docker-entrypoint..."   4 seconds ago       Up 3 seconds        33060/tcp, 0.0.0.0:4500->3306/tcp                    mysql-500
[root@localhost ~]# docker exec -it mysql-500 /bin/bash
root@129439b3bddd:/# mysql -u root -p
Enter password: 
```



## 创建授权用户

创建用户用于从库同步复制，授予复制、同步访问的权限

```
mysql> grant replication slave on *.* to 'slave'@'%' identified by '123456';
Query OK, 0 rows affected (0.00 sec)
```

```powershell
#mysql8.o以上使用下边授权
CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
GRANT all privileges ON *.* TO 'slave'@'%';
```



## log_bin是否开启

```
mysql> show variables like 'log_bin';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_bin       | ON    |
+---------------+-------+
1 row in set
```



## 查看master状态

```
mysql> show master status \G;
*************************** 1. row ***************************
             File: mysql-bin.000003
         Position: 657
     Binlog_Do_DB: test5
 Binlog_Ignore_DB: mysql,information_schema,performation_schema,sys
Executed_Gtid_Set: 
1 row in set (0.00 sec)

```



# 【从库10345】配置及操作



## 配置my.cnf

```
[client]
port = 3306
default-character-set = utf8mb4

[mysql]
port = 3306
default-character-set = utf8mb4

[mysqld]
##########################
# summary
##########################
#bind-address = 0.0.0.0
#port = 3306
#datadir=/datavol/mysql/data 	#数据存储目录

##########################
# log bin
##########################
server-id = 10345
master_info_repository      = table
relay_log_info_repository   = table		

##########################
# character set
##########################
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```



## 安装启动

```powershell
[root@localhost ~]# docker run -d -p 4345:3306 --name=mysql-10345 -v /datavol/mysql-10345/conf:/etc/mysql/conf.d -v /datavol/mysql-10345/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
af3ac584994ef378e08d8cc5f8321043aeb6a308a6fbce602d68189f038aa3fd
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
af3ac584994e        mysql               "docker-entrypoint.s…"   44 seconds ago      Up 38 seconds       33060/tcp, 0.0.0.0:4345->3306/tcp   mysql-10345
507d424daf4f        mysql               "docker-entrypoint.s…"   22 minutes ago      Up 22 minutes       33060/tcp, 0.0.0.0:4300->3306/tcp   mysql-300
[root@localhost ~]# docker exec -it mysql-10345 /bin/bash
root@af3ac584994e:/# mysql -u root -p
Enter password: 

```

```powershell
[root@localhost ~]# firewall-cmd --zone=public --add-port=4345/tcp --permanent
success
[root@localhost ~]# systemctl restart firewalld.service
[root@localhost ~]# 
[root@localhost ~]# firewall-cmd --list-ports 
8080/tcp 6379/tcp 3306/tcp 80/tcp 3326/tcp 8092/tcp 33060/tcp 4300/tcp 4345/tcp

```



## 设置【主库】信息

登录【从库10345】，进入mysql命令行。

```powershell
mysql> stop slave;
Query OK, 0 rows affected

mysql> CHANGE MASTER TO 
MASTER_HOST='192.168.40.129',
MASTER_PORT=4300,
MASTER_USER='slave',
MASTER_PASSWORD='123456',
MASTER_LOG_FILE='mysql-bin.000003',
MASTER_LOG_POS=854 
for channel '300';
Query OK, 0 rows affected

mysql> CHANGE MASTER TO 
MASTER_HOST='192.168.40.129',
MASTER_PORT=4400,
MASTER_USER='slave',
MASTER_PASSWORD='123456',
MASTER_LOG_FILE='mysql-bin.000003',
MASTER_LOG_POS=657 
for channel '400';
Query OK, 0 rows affected

mysql> CHANGE MASTER TO 
MASTER_HOST='192.168.40.129',
MASTER_PORT=4500,
MASTER_USER='slave',
MASTER_PASSWORD='123456',
MASTER_LOG_FILE='mysql-bin.000003',
MASTER_LOG_POS=657 
for channel '500';
Query OK, 0 rows affected

mysql> start slave;
Query OK, 0 rows affected
```

stop slave;   //停止同步
start slave;   //开始同步
//必须和【主库】的信息匹配。
CHANGE MASTER TO
MASTER_HOST='192.168.40.129',   //主库IP
MASTER_PORT=4300,            //主库端口
MASTER_USER='slave',           //访问主库且有同步复制权限的用户
MASTER_PASSWORD='123456',    //登录密码
**//【关键处】从主库的该log_bin文件开始读取同步信息，主库show master status返回结果**
MASTER_LOG_FILE='mysql-bin.000003',
**//【关键处】从文件中指定位置开始读取，主库show master status返回结果**
MASTER_LOG_POS=438
for channel '300';      //定义通道名称



## 查看同步状态

```powershell
mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.40.129
                  Master_User: slave
                  Master_Port: 4300
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000003
          Read_Master_Log_Pos: 854
               Relay_Log_File: af3ac584994e-relay-bin-300.000006
                Relay_Log_Pos: 322
        Relay_Master_Log_File: mysql-bin.000003
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 854
              Relay_Log_Space: 708
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 300
                  Master_UUID: e00130d8-2f67-11ea-9474-0242ac110002
             Master_Info_File: mysql.slave_master_info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 300
           Master_TLS_Version: 
       Master_public_key_path: 
        Get_master_public_key: 0
            Network_Namespace: 
*************************** 2. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.40.129
                  Master_User: slave
                  Master_Port: 4400
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000003
          Read_Master_Log_Pos: 657
               Relay_Log_File: af3ac584994e-relay-bin-400.000004
                Relay_Log_Pos: 322
        Relay_Master_Log_File: mysql-bin.000003
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 657
              Relay_Log_Space: 708
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 400
                  Master_UUID: ce26c835-2f76-11ea-b9d6-0242ac110004
             Master_Info_File: mysql.slave_master_info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 400
           Master_TLS_Version: 
       Master_public_key_path: 
        Get_master_public_key: 0
            Network_Namespace: 
*************************** 3. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.40.129
                  Master_User: slave
                  Master_Port: 4500
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000003
          Read_Master_Log_Pos: 657
               Relay_Log_File: af3ac584994e-relay-bin-500.000003
                Relay_Log_Pos: 322
        Relay_Master_Log_File: mysql-bin.000003
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 657
              Relay_Log_Space: 708
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 500
                  Master_UUID: d06155fb-2f78-11ea-b321-0242ac110005
             Master_Info_File: mysql.slave_master_info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 500
           Master_TLS_Version: 
       Master_public_key_path: 
        Get_master_public_key: 0
            Network_Namespace: 
3 rows in set (0.00 sec)

ERROR: 
No query specified

mysql> 

```

可以看见设置三个的主从同步通道的所有状态信息。
只有【Slave_IO_Running】和【Slave_SQL_Running】都是Yes，则同步是正常的。
如果是No或者Connecting都不行，可查看mysql-error.log，以排查问题。

```
mysql> show variables like 'log_error%';
+---------------------+--------+
| Variable_name       | Value  |
+---------------------+--------+
| log_error           | stderr |
| log_error_verbosity | 3      |
+---------------------+--------+
2 rows in set
```

配置完成，则【从库10345】开始自动同步。

若需要单独启动或停止某个同步通道，可使用如下命令：
start slave for channel '300';   //启动名称为300的同步通道
stop slave for channel '300';   //停止名称为300的同步通道



# 验证数据同步



## 建库

使用root账号登录【主库300】，创建test3数据库 

```
mysql> CREATE DATABASE test3;
Query OK, 1 row affected (0.00 sec)

mysql> USE test3;
Database changed
```



## 建表

在【主库300】中创建user表

```mysql
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
```



## 新增

在【主库300】中向user表插入一条数据：

```mysql
mysql> use test3;
Database changed
mysql> INSERT INTO user (id, name, age) VALUES (300, 'Tom', 18);
Database changed
mysql> SELECT * FROM user;
+-----+------+-----+
| id  | name | age |
+-----+------+-----+
| 300 | Tom  |  18 |
+-----+------+-----+
1 row in set (0.00 sec)
```

在【从库10345】中查询user表数据：

```
mysql> use test3;
Database changed
mysql> SELECT * FROM user;
+-----+------+-----+
| id  | name | age |
+-----+------+-----+
| 300 | Tom  |  18 |
+-----+------+-----+
1 row in set (0.00 sec)
```

新增记录同步成功。



## 更新

在【主库300】中修改刚才插入的数据：

```
mysql> UPDATE user SET name='Peter' where id=300;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from user;
+-----+-------+-----+
| id  | name  | age |
+-----+-------+-----+
| 300 | Peter |  18 |
+-----+-------+-----+
1 row in set (0.00 sec)
```

在【从库10345】中查询user表数据：

```
mysql> select * from user;
+-----+-------+-----+
| id  | name  | age |
+-----+-------+-----+
| 300 | Peter |  18 |
+-----+-------+-----+
1 row in set (0.00 sec)
```

更新记录同步成功。



## 删除

在【主库300】中删除刚才更新的数据：

```
mysql> DELETE FROM user WHERE id=300;
Query OK, 1 row affected (0.00 sec)

mysql> select * from user;
Empty set (0.00 sec)
```

在【从库10345】中查询user表数据：

```
mysql> select * from user;
Empty set (0.00 sec)
```

删除记录同步成功。
注：【主库400】、【主库500】的验证操作与上述类似。

补充：

- 如果【主服务器】重启mysql服务，【从服务器】会等待与【主服务器】重连。当主服务器恢复正常后，从服务器会自动重新连接上主服务器，并正常同步数据。
- 如果某段时间内，【从数据库】服务器异常导致同步中断（可能是同步点位置不匹配），可以尝试以下恢复方法：进入【主数据库】服务器（正常），在bin-log中找到【从数据库】出错前的position，然后在【从数据库】上执行change master，将master_log_file和master_log_pos重新指定后，开始同步。 