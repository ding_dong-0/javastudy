### Redis

https://redis.io/

Redis安装

 [redis-5.0.7.tar.gz](http://211.136.65.167/cache/download.redis.io/releases/redis-5.0.7.tar.gz?ich_args2=65-25125210018524_e60a775f321316a0d251397c9b042707_10001002_9c89632bd5c7f8d4953b518939a83798_63a1132906332dae875e26b3cc346d8a) 

```shell
#安装gcc环境
yum install gcc-c++
#查看是否安装gcc
rpm -qa|grep gcc
#将 redis-5.0.7.tar.gz放到/opt/soft文件夹中
cd /opt/soft
解压
tar -zxvf redis-5.0.7.tar.gz -C /opt/module/
#编译
cd redis-5.0.7/
pwd
/opt/module/redis-5.0.7
make
#安装redis
make install PREFIX=/usr/local/redis   #指定安装到目录/usr/local/redis  不指定默认为/usr/local/bin
cd /usr/local/redis
cd bin/
ls

#启动Redis
pwd
/usr/local/redis/bin
#启动
 ./redis-server
 
 
 #第一种启动方式（前端启动）
 [root@localhost bin]# ./redis-server
7266:C 25 Dec 2019 13:09:54.356 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
7266:C 25 Dec 2019 13:09:54.356 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=7266, just started
7266:C 25 Dec 2019 13:09:54.356 # Warning: no config file specified, using the default config. In order to specify a config file use ./redis-server /path/to/redis.conf
7266:M 25 Dec 2019 13:09:54.357 * Increased maximum number of open files to 10032 (it was originally set to 1024).
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 5.0.7 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                   
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 7266
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           http://redis.io        
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               

7266:M 25 Dec 2019 13:09:54.359 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
7266:M 25 Dec 2019 13:09:54.359 # Server initialized
7266:M 25 Dec 2019 13:09:54.359 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
7266:M 25 Dec 2019 13:09:54.359 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
7266:M 25 Dec 2019 13:09:54.359 * Ready to accept connections


#前端启动关闭
ctrl+c


 #第二种启动方式（后端启动）
cd /opt/redis-5.0.7/
cp redis.conf /usr/local/redis/bin/

cd /usr/local/redis/bin/

vi redis.conf 

#查找daemonize no更改为 daemonize yes（表示允许Redis在后台启动）
#bind 127.0.0.1 (注释掉这一行表示允许连接该Redis实例的地址，默认情况下只允许本地连接，将默认配置注释掉，外网就可以连接Redis了)
#添加 requirepass 123456 表示登录该Redis实例所需的密码 （我没设置）。
 
#如果没有配置环境变量只能在bin目录下启动
cd /usr/local/redis/bin
./redis-server redis.conf

[root@localhost bin]# cd /usr/local/redis/bin
[root@localhost bin]# ls
dump.rdb         redis-check-aof  redis-cli   redis-sentinel
redis-benchmark  redis-check-rdb  redis.conf  redis-server 
[root@localhost bin]# ./redis-server redis.conf
8003:C 25 Dec 2019 13:44:31.227 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
8003:C 25 Dec 2019 13:44:31.227 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=8003, just started
8003:C 25 Dec 2019 13:44:31.227 # Configuration loaded
[root@localhost bin]# 


#非正常关闭
kill 5528

#正常关闭
./redis-cli shutdown


 #连接
 #如果没有配置环境变量只能做爱bin目录下启动
cd /usr/local/redis/bin
./redis-server redis.conf

#连接redis
[root@localhost bin]# ./redis-cli -h 127.0.0.1 -p 6379
127.0.0.1:6379> 
关闭
exit


 
#远程连接redis
#关闭centos端口号6379的防火墙
firewall-cmd --zone=public --add-port=6379/tcp --permanent
#输入命令重新载入配置(#重载防火墙规则 )
firewall-cmd --reload
#查看所有打开的端口  
firewall-cmd --list-ports

```

```shell
#选择第0个数据库(0~15)
select 0
set test    123456
get test
#插入多对数值
mset username gyf   gender male   city gz
#获取多个键值
mget username test
#删除
del gender
#自增
incr num
#在原num基础上增加2
incrby num 2
#递减
decr num 3

incr items:id
#hash赋值（用的比较多）
HSET user username zhangsan

hmset user  height 170 gender male

#不常用
set person {name:zhangsan,age:18,city:beijing}


hset user age 45
hsetnx user age 45

hget user username
hmget user username age

hgetall user

hdel user age


hdel user age gender


hmset items:1001 name iphonex price 1688.90

hgetall items:1001

lpush list:数字1 2 3

rpush list:数字1 2 3
#读取所有list
lrange list:数字1 0 -1

lpop list:数字1

rpop list:数字1

rpop list:数字1



```



### [centos8开放8080端口](https://www.jb51.net/os/Ubuntu/617627.html)



### 解决redis远程连接不上的问题

redis现在的版本开启redis-server后，redis-cli只能访问到127.0.0.1，因为在配置文件中固定了ip，因此需要修改redis.conf（有的版本不是这个文件名，只要找到相对应的conf后缀的文件即可）文件以下几个地方。

1.bind 127.0.0.1改为 #bind 127.0.0.1 ， 注释掉这一行表示允许连接该Redis实例的地址，默认情况下只允许本地连接，将默认配置注释掉，外网就可以连接Redis了)

2.protected-mode yes  改为 protected-mode no  ，表示关闭保护模式

3.加入 daemonize yes (允许Redis在后台启动不占用一个主程窗口)



### [redis 开机自启配置](https://blog.csdn.net/fukai8350/article/details/80775590)

https://blog.csdn.net/aaronmer/article/details/79929960

### [如何jedis和spring-data-redis导包版本不兼容问题](https://blog.csdn.net/Qizhan_feng/article/details/90216361)



pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.tao</groupId>
    <artifactId>Redisdemo</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>2.10.1</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.2.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-redis</artifactId>
            <version>2.1.8.RELEASE</version>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>12</source>
                    <target>12</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```



### linux 下配置 redis开机自动启动

## 1. 新建一个文件

```powershell
vim /etc/init.d/redis
```

## 2. 将下面内容粘贴到文件中

```powershell
#!/bin/sh
#chkconfig:   2345 90 10
#description:  Redis is a persistent key-value database
PATH=/usr/local/bin:/sbin:/usr/bin:/bin

REDISPORT=6379                             #redis的默认端口， 要和下文中的
EXEC=/usr/local/redis/bin/redis-server     #redis服务端的命令
REDIS_CLI=/usr/local/redis/bin/redis-cli   #redis客户端的命令  这两个一般都在 
PIDFILE=/var/run/redis_6379.pid            #reids的进程文件生成的位置
CONF="/usr/local/redis/bin/redis.conf"     #redis的配置文件所在的目录 

case "$1" in  
    start)  
        if [ -f $PIDFILE ]  
        then  
                echo "$PIDFILE exists, process is already running or crashed"  
        else  
                echo "Starting Redis server..."  
                $EXEC $CONF  
        fi  
        if [ "$?"="0" ]   
        then  
              echo "Redis is running..."  
        fi  
        ;;  
    stop)  
        if [ ! -f $PIDFILE ]  
        then  
                echo "$PIDFILE does not exist, process is not running"  
        else  
                PID=$(cat $PIDFILE)  
                echo "Stopping ..."  
                $REDIS_CLI -p $REDISPORT SHUTDOWN  
                while [ -x ${PIDFILE} ]  
               do  
                    echo "Waiting for Redis to shutdown ..."  
                    sleep 1  
                done  
                echo "Redis stopped"  
        fi  
        ;;  
   restart|force-reload)  
        ${0} stop  
        ${0} start  
        ;;  
  *)  
    echo "Usage: /etc/init.d/redis {start|stop|restart|force-reload}" >&2  
        exit 1  
esac
```

## 3. 修改下面配置为自己对应的信息

```powershell
REDISPORT=6379 # 端口号
EXEC=/usr/local/bin/redis-server # 执行脚本的地址
REDIS_CLI=/usr/local/bin/redis-cli # 客户端执行脚本的地址
PIDFILE=/var/run/redis_6379.pid # 进程id文件地址，启动redis后才能看见
CONF="/myredis/redis.conf" #配置文件地址
```

## 4. 设置权限

```powershell
chmod 755 /etc/init.d/redis
```

## 5. 启动测试

```powershell
/etc/init.d/redis start
```

## 6. 启动成功会提示如下信息

```powershell
Starting Redis server...
Redis is running...
```

## 7. 设置开机自启动

```powershell
chkconfig --add /etc/init.d/redis
chkconfig redis on
```

## 如果启动失败

查看第一步的配置文件信息，是否和第二步的文件信息一致，vim粘贴会出现粘贴不全的问题



Redis设置为开机自启动 - 农夫三拳有点疼 - OSCHINA  https://my.oschina.net/songjilong/blog/3212371



```shell
[root@localhost /]# mkdir myredis
[root@localhost /]# mv /usr/local/redis/ /myredis/
[root@localhost ~]# cd /myredis/redis/bin/
[root@localhost bin]# ll
#为了和老师的环境一直
[root@localhost myredis]# mv redis /usr/local/bin/  
#用命令查看后台的redis父无有没有启动
[root@localhost myredis]# ps -ef|grep redis
root      22250  13422  0 08:39 pts/0    00:00:00 grep --color=auto redis
#指定修改过的配置文件后台启动
[root@localhost myredis]# cd /usr/local/bin/redis/bin/
[root@localhost bin]# ./redis-server /myredis/redis.conf 
22288:C 04 Jun 2020 08:41:31.198 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
22288:C 04 Jun 2020 08:41:31.198 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=22288, just started
22288:C 04 Jun 2020 08:41:31.198 # Configuration loaded
使用命令行进入redis
[root@localhost bin]# ./redis-cli 
127.0.0.1:6379> exit
[root@localhost bin]# ./redis-cli -p 6379 
127.0.0.1:6379> ping
PONG
127.0.0.1:6379> set key1 hello
OK
127.0.0.1:6379> get key1
"hello"

#此时打开另一个窗口查看redis的后台进程是否启动
[root@localhost ~]# ps -ef|grep redis
root      22289      1  0 08:41 ?        00:00:01 ./redis-server 127.0.0.1:6379
root      22395  13422  0 08:48 pts/0    00:00:00 ./redis-cli
root      22397  22372  0 08:48 pts/1    00:00:00 grep --color=auto redis



[root@localhost bin]# ./redis-server /myredis/redis.conf 
22445:C 04 Jun 2020 08:52:26.468 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
22445:C 04 Jun 2020 08:52:26.469 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=22445, just started
22445:C 04 Jun 2020 08:52:26.469 # Configuration loaded
[root@localhost bin]# ./redis-cli 
#关闭redis数据库
127.0.0.1:6379> shutdown
#关闭客户端
not connected> exit

#对标上述的操作查看redis的后台启动情况
[root@localhost ~]# ps -ef|grep redis
root      22420  13422  0 08:51 pts/0    00:00:00 ./redis-cli
root      22430  22372  0 08:51 pts/1    00:00:00 grep --color=auto redis
[root@localhost ~]# ps -ef|grep redis
root      22432  22372  0 08:51 pts/1    00:00:00 grep --color=auto redis


[root@localhost bin]# ./redis-server /myredis/redis.conf 
22926:C 04 Jun 2020 08:57:24.939 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
22926:C 04 Jun 2020 08:57:24.939 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=22926, just started
22926:C 04 Jun 2020 08:57:24.939 # Configuration loaded
#测试redis的性能
[root@localhost bin]# ./redis-benchmark 



127.0.0.1:6379> select 7  #选择数据库
OK

[root@localhost bin]# ./redis-server /myredis/redis.conf 
24238:C 04 Jun 2020 09:31:38.500 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
24238:C 04 Jun 2020 09:31:38.500 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=24238, just started
24238:C 04 Jun 2020 09:31:38.500 # Configuration loaded
[root@localhost bin]# ./redis-cli 
127.0.0.1:6379> DBSIZE
(integer) 5
127.0.0.1:6379> key *
(error) ERR unknown command `key`, with args beginning with: `*`, 
127.0.0.1:6379> keys *
1) "mylist"
2) "myset:__rand_int__"
3) "key1"
4) "key:__rand_int__"
5) "counter:__rand_int__"
127.0.0.1:6379> set k2 v2
OK
127.0.0.1:6379> set k3 v3
OK
127.0.0.1:6379> keys *
1) "mylist"
2) "k2"
3) "myset:__rand_int__"
4) "key1"
5) "k3"
6) "key:__rand_int__"
7) "counter:__rand_int__"
127.0.0.1:6379> DBSIZE
(integer) 7
127.0.0.1:6379> keys k?
1) "k2"
2) "k3"
#清空所有的数据库
127.0.0.1:6379> FLUSHALL
OK
127.0.0.1:6379> DBSIZE
(integer) 0
#清空当前数据库
127.0.0.1:6379> FLUSHDB
OK


```

### redis常用的5大数据类型

String Hash List Set Zset



List 的实现接口ArrayList(object类型的数组，初始值是10 )    LinkdList（双端循环列表，）



```powershell
127.0.0.1:6379[3]> select 3
OK
127.0.0.1:6379[3]> sadd set01 1 1 2 2 3 3
(integer) 3
127.0.0.1:6379[3]> SMEMBERS set01
1) "1"
2) "2"
3) "3"
 127.0.0.1:6379[3]> SISMEMBER set01 1
(integer) 1
127.0.0.1:6379[3]> SISMEMBER set01 9
(integer) 0
 127.0.0.1:6379[3]> SCARD set01
(integer) 3
127.0.0.1:6379[3]> SREM set01 3
(integer) 1
127.0.0.1:6379[3]> SMEMBERS set01
1) "1"
2) "2"
```





### log4j的8个日志级别（OFF、FATAL、ERROR、WARN、INFO、DEBUG、TRACE、 ALL）

https://blog.csdn.net/yh_zeng2/article/details/83046560



### redis缓存清除策略（Maxmemory-policy）

 （1）volatile-lru：使用LRU算法移除key，只对设置了过期时间的键
（2）allkeys-lru：使用LRU算法移除key
（3）volatile-random：在过期集合中移除随机的key，只对设置了过期时间的键
（4）allkeys-random：移除随机的key
（5）volatile-ttl：移除那些TTL值最小的key，即那些最近要过期的key
（6）noeviction：不进行移除。针对写操作，只是返回错误信息



参数说明

### redis.conf 配置项说明如下：

1. Redis默认不是以守护进程的方式运行，可以通过该配置项修改，使用yes启用守护进程
    daemonize no
2. 当Redis以守护进程方式运行时，Redis默认会把pid写入/var/run/redis.pid文件，可以通过pidfile指定
    pidfile /var/run/redis.pid
3. 指定Redis监听端口，默认端口为6379，作者在自己的一篇博文中解释了为什么选用6379作为默认端口，因为6379在手机按键上MERZ对应的号码，而MERZ取自意大利歌女Alessia Merz的名字
    port 6379
4. 绑定的主机地址
    bind 127.0.0.1
5.当 客户端闲置多长时间后关闭连接，如果指定为0，表示关闭该功能
    timeout 300
6. 指定日志记录级别，Redis总共支持四个级别：debug、verbose、notice、warning，默认为verbose
    loglevel verbose
7. 日志记录方式，默认为标准输出，如果配置Redis为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给/dev/null
    logfile stdout
8. 设置数据库的数量，默认数据库为0，可以使用SELECT <dbid>命令在连接上指定数据库id
    databases 16
9. 指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合
    save <seconds> <changes>
    Redis默认配置文件中提供了三个条件：
    save 900 1
    save 300 10
    save 60 10000
    分别表示900秒（15分钟）内有1个更改，300秒（5分钟）内有10个更改以及60秒内有10000个更改。

10. 指定存储至本地数据库时是否压缩数据，默认为yes，Redis采用LZF压缩，如果为了节省CPU时间，可以关闭该选项，但会导致数据库文件变的巨大
    rdbcompression yes
11. 指定本地数据库文件名，默认值为dump.rdb
    dbfilename dump.rdb
12. 指定本地数据库存放目录
    dir ./
13. 设置当本机为 slave 服务时，设置 master 服务的IP地址及端口，在Redis启动时，它会自动从master进行数据同步
    slaveof <masterip> <masterport>
14. 当master服务设置了密码保护时，slav服务连接master的密码
    masterauth <master-password>
15. 设置Redis连接密码，如果配置了连接密码，客户端在连接Redis时需要通过AUTH <password>命令提供密码，默认关闭
    requirepass foobared
16. 设置同一时间最大客户端连接数，默认无限制，Redis可以同时打开的客户端连接数为Redis进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis会关闭新的连接并向客户端返回max number of clients reached错误信息
    maxclients 128
17. 指定Redis最大内存限制，Redis在启动时会把数据加载到内存中，达到最大内存后，Redis会先尝试清除已到期或即将到期的Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis新的vm机制，会把Key存放内存，Value会存放在swap区
    maxmemory <bytes>
18. 指定是否在每次更新操作后进行日志记录，Redis在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为 redis本身同步数据文件是按上面save条件来同步的，所以有的数据会在一段时间内只存在于内存中。默认为no
    appendonly no
19. 指定更新日志文件名，默认为appendonly.aof
      appendfilename appendonly.aof
20. 指定更新日志条件，共有3个可选值： 
    no：表示等操作系统进行数据缓存同步到磁盘（快） 
    always：表示每次更新操作后手动调用fsync()将数据写到磁盘（慢，安全） 
    everysec：表示每秒同步一次（折衷，默认值）
    appendfsync everysec

21. 指定是否启用虚拟内存机制，默认值为no，简单的介绍一下，VM机制将数据分页存放，由Redis将访问量较少的页即冷数据swap到磁盘上，访问多的页面由磁盘自动换出到内存中（在后面的文章我会仔细分析Redis的VM机制）
      vm-enabled no
22. 虚拟内存文件路径，默认值为/tmp/redis.swap，不可多个Redis实例共享
      vm-swap-file /tmp/redis.swap
23. 将所有大于vm-max-memory的数据存入虚拟内存,无论vm-max-memory设置多小,所有索引数据都是内存存储的(Redis的索引数据 就是keys),也就是说,当vm-max-memory设置为0的时候,其实是所有value都存在于磁盘。默认值为0
      vm-max-memory 0
24. Redis swap文件分成了很多的page，一个对象可以保存在多个page上面，但一个page上不能被多个对象共享，vm-page-size是要根据存储的 数据大小来设定的，作者建议如果存储很多小对象，page大小最好设置为32或者64bytes；如果存储很大大对象，则可以使用更大的page，如果不 确定，就使用默认值
      vm-page-size 32
25. 设置swap文件中的page数量，由于页表（一种表示页面空闲或使用的bitmap）是在放在内存中的，，在磁盘上每8个pages将消耗1byte的内存。
      vm-pages 134217728
26. 设置访问swap文件的线程数,最好不要超过机器的核数,如果设置为0,那么所有对swap文件的操作都是串行的，可能会造成比较长时间的延迟。默认值为4
      vm-max-threads 4
27. 设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启
    glueoutputbuf yes
28. 指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法
    hash-max-zipmap-entries 64
    hash-max-zipmap-value 512
29. 指定是否激活重置哈希，默认为开启（后面在介绍Redis的哈希算法时具体介绍）
    activerehashing yes
30. 指定包含其它的配置文件，可以在同一主机上多个Redis实例之间使用同一份配置文件，而同时各个实例又拥有自己的特定配置文件
    include /path/to/local.conf

### 查询redis状态的命令

```powershell
[root@localhost ~]# ps -ef|grep redis
root      35360  23261  0 15:53 pts/2    00:00:00 grep --color=auto redis
[root@localhost ~]# lsof -i :6379   
[root@localhost ~]# netstat -ap|grep redis
tcp        0      0 localhost:redis         localhost:56128         TIME_WAIT   -                   
[root@localhost bin]# ./redis-server /myredis/redis.conf 
35364:C 04 Jun 2020 15:54:16.176 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
35364:C 04 Jun 2020 15:54:16.176 # Redis version=5.0.7, bits=64, commit=00000000, modified=0, pid=35364, just started
35364:C 04 Jun 2020 15:54:16.176 # Configuration loaded
[root@localhost bin]# ./redis-cli 
127.0.0.1:6379> 


[root@localhost ~]# ps -ef|grep redis
root      35365      1  0 15:54 ?        00:00:00 ./redis-server *:6379
root      35369  13422  0 15:54 pts/0    00:00:00 ./redis-cli
root      35801  23261  0 15:55 pts/2    00:00:00 grep --color=auto redis
[root@localhost ~]# lsof -i :6379   
COMMAND     PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
redis-ser 35365 root    6u  IPv6 154227      0t0  TCP *:redis (LISTEN)
redis-ser 35365 root    7u  IPv4 154228      0t0  TCP *:redis (LISTEN)
redis-ser 35365 root    8u  IPv4 154244      0t0  TCP localhost:redis->localhost:56130 (ESTABLISHED)
redis-cli 35369 root    3u  IPv4 154733      0t0  TCP localhost:56130->localhost:redis (ESTABLISHED)
[root@localhost ~]# netstat -ap|grep redis
tcp        0      0 0.0.0.0:redis           0.0.0.0:*               LISTEN      35365/./redis-serve 
tcp        0      0 localhost:redis         localhost:56130         ESTABLISHED 35365/./redis-serve 
tcp        0      0 localhost:56130         localhost:redis         ESTABLISHED 35369/./redis-cli   
tcp6       0      0 [::]:redis              [::]:*                  LISTEN      35365/./redis-serve 

```

### redis持久化

RDB（Redis DataBase）

AOF（Append Only File）



```powershell
[root@localhost ~]# free
              total        used        free      shared  buff/cache   available
Mem:        1849464      706080      442260        1640      701124      959876
Swap:             0           0           0
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:          1.8Gi       689Mi       431Mi       1.0Mi       684Mi       937Mi
Swap:            0B          0B          0B

[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             888M     0  888M    0% /dev
tmpfs                904M     0  904M    0% /dev/shm
tmpfs                904M  1.5M  902M    1% /run
tmpfs                904M     0  904M    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  7.7G  9.4G   46% /
/dev/sda1            976M  134M  775M   15% /boot
tmpfs                181M   28K  181M    1% /run/user/42
tmpfs                181M  4.0K  181M    1% /run/user/0
tmpfs                181M     0  181M    0% /run/user/993

```





### redis中文网

http://www.redis.cn/

### docker安装redis

```powershell
[root@localhost ~]# mkdir -p /mydata/redis/conf/
[root@localhost ~]# touch /mydata/redis/conf/redis.conf
[root@localhost ~]# docker pull redis
[root@localhost conf]# docker run -p 6379:6379 --name redis -v /mydata/redis/data:/data -v /mydata/redis/conf/redis.conf:/etc/redis/redis.conf -d redis redis-server /etc/redis/redis.conf
1e5d05ff3ea8cb036c5e3a8b920a9b8ef6ff0162369fe12fd67732cb7de0d598
[root@localhost conf]# docker exec -it redis redis-cli
127.0.0.1:6379> set a b 
OK
127.0.0.1:6379> get a
"b"
127.0.0.1:6379>exit
[root@localhost conf]# docker restart redis
redis
127.0.0.1:6379> get a
(nil)
127.0.0.1:6379> exit
[root@localhost conf]# pwd
/mydata/redis/conf
#开启持久化
[root@localhost conf]# vim redis.conf 
[root@localhost conf]# cat redis.conf 
appendonly yes
[root@localhost conf]# docker restart redis
redis
[root@localhost conf]# docker exec -it redis redis-cli
127.0.0.1:6379> set a b
OK
127.0.0.1:6379> exit
[root@localhost conf]# docker exec -it redis redis-cli
127.0.0.1:6379> get a
"b"
127.0.0.1:6379> 
#
[root@localhost ~]# docker update redis --restart=always

```



http://www.redis.cn/commands.html

#### Redis常用的五大数据类型

 String(字符串)、List(列表)、Set(集合)、Hash（散列）、Zset（有序集合）

```

```