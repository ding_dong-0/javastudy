# 第二讲

##### 1.学习曲线

基础语法---->方法（代码重用）---->类（代码重用）---->jar包（多个类封装为jar,代码重用）---->框架（一个或者多个jar包，代码重用）

##### 2.什么是框架？

Framework

框架是模板，模型，模子

框架是一个可重用的半成品

##### 3.为什么要使用框架？

提高开发效率，降低学习难度

##### 4如何学习框架？   

框架是别人提供的。那么使用框架时，要遵守框架提供的规则。

学习框架就是学习框架的规则。

框架由两部分组成，可变的部分和不可变的部分。

##### 5.常见的框架有哪些：

struts2，struts1，spring，hibernate，MyBatis，shiro，nutz，jesery等

# 第三讲

##### 1.什么是Struts

strust2是一个开源，免费，轻量级的MVC框架

轻量级：如果一个框架没有侵入型，就说该框架是轻量级的。

侵入性：如果使用一个框架，必须实现框架提供的接口，或者继承框架提供的类，

strust2是基于请求的MVC

2struts2的helloworld

a)新建web项目

b)导入jar包

![image-20191206120123843](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20191206120123843.png)





c)编写web.xml文件--配置struts2 的前端控制器（分发器）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <display-name></display-name>
    <!--配置struts2的前端控制器-->
    <filter>
        <filter-name>struts2</filter-name>
        <filter-class>org.apache.struts2.dispatcher.filter.StrutsPrepareAndExecuteFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>struts2</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```



d)编写业务处理类

```java
package cn.sxt.action;

//struts2的第一个案例
public class HelloAction {
    /**
     * struts2中所有的业务方法都是public的返回值类型为String类型，
     * 所有的业务方法都没有参数方法名可以自定义，默认为execute     *
     *
     * @return
     */
    public String execute() {
        System.out.println("hello struts2");
        return "success";
    }
}
```

d)在src下，添加struts2.xml，并配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE struts PUBLIC
        "-//Apache Software Foundation//DTD Struts Configuration 2.5//EN"
        "http://struts.apache.org/dtds/struts-2.5.dtd">
<struts>
    <package name="default" namespace="/" extends="struts-default">
        <!--配置action配置URL和处理类的方法进行映射-->
        <action name="hello" class="cn.sxt.action.HelloAction">
            <result>/hello.jsp</result>
        </action>
    </package>
</struts>
```





