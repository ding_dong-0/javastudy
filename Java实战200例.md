#### 1.编写一个Java程序，用if-else语句判断某年份是否为闰年

```java
import java.util.Scanner;

/**
 * 编写一个Java程序，用if-else语句判断某年份是否为闰年。
 */
public class T1_1 {
    public static void main(String[] args) {
        //2010
        System.out.println("请输入年份：");
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        if (args.length != 0) {
            year = Integer.parseInt(args[0]);
        }
        /*
        ①、普通年能被4整除且不能被100整除的为闰年.
        ②、世纪年能被400整除的是闰年
         */
        judgingRuiNian(year);
    }

    /*
   ①、普通年能被4整除且不能被100整除的为闰年.
   ②、世纪年能被400整除的是闰年
    */
    public static void judgingRuiNian(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            System.out.println(year + "是闰年！");
        } else {
            System.out.println(year + "不是是闰年！");
        }
    }
}
```

#### 10.编写一个Java程序在屏幕上输出“你好！”

```java
public class Helloworld {
    public static void main(String[] args) {
        System.out.println("你好！");
    }
}
```

```powershell
你好！

```

#### 13.如何查看当前 Java 运行的版本?

```powershell
C:\Users\Administrator>java -version
java version "1.8.0_221"
Java(TM) SE Runtime Environment (build 1.8.0_221-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.221-b11, mixed mode)

```

#### 14.字符串比较

```java
/**
 * 字符串比较
 * <p>
 * 通过字符串函数 compareTo (string) ，compareToIgnoreCase(String) 及 compareTo(object string) 来比较两个字符串，并返回字符串中第一个字母ASCII的差值。
 */
public class StringCompareEmp {
    public static void main(String[] args) {
        String str = "Hello World";
        String anotherString = "hello world";
        Object objStr = str;
        System.out.println(str.compareTo(anotherString));
        System.out.println(str.compareToIgnoreCase(anotherString));
        System.out.println(str.compareTo(objStr.toString()));
    }
}
```

```powershell
-32
0
0
```





#### 17.如何使用java对字符串反转

```java
/**
 * 使用 Java 的反转函数 reverse() 将字符串反转：
 */
public class StringReverseExample {
    public static void main(String[] args) {
        String string="runoob";
        String reverse = new StringBuilder(string).reverse().toString();
        System.out.println("字符串反转前："+string);
        System.out.println("字符串反转后："+reverse);
    }
}
```

```powershell
字符串反转前：runoob
字符串反转后：boonur
```

#### 21.字符串小写转大写

```java
/**
 * 使用了 String toUpperCase() 方法将字符串从小写转为大写：
 */
public class StringToUpperCaseEmp {
    public static void main(String[] args) {
        String str = "string runoob";
        String strUpper = str.toUpperCase();
        System.out.println("原始字符串: " + str);
        System.out.println("转换为大写: " + strUpper);
    }
}
```

```powershell

原始字符串: string runoob
转换为大写: STRING RUNOOB

```



#### 40.如何使用 contains () 方法来查找数组中的指定元素

```

```





#### 43.如何使用 SimpleDateFormat 类的 format(date) 方法来格式化时间

```java
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 如何使用 SimpleDateFormat 类的 format(date) 方法来格式化时间
 */
public class SimpleDateFormatDemo {
    public static void main(String[] args) {
        Date date = new Date();
        String strDateFormat = "yyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
        System.out.println(sdf.format(date));
    }
}

```

```powershell
2020-11-19 16:26:47

```

#### 123.利用递归方法求 5!

```java
/**
 * 求 5!的
 */
public class FactorialNum {
    public static void main(String[] args) {
        System.out.println(factorial(5));
    }

    private static long factorial(int number) {
        if (number == 1 || number == 0) {
            return 1;
        } else {
            return number * factorial(number - 1);
        }
    }
}
```

```powershell
120
```







#### 124.求 1+2!+3!+...+20!的和

```java
/**
 * 求 1!+2!+3!+...+20!的和
 */
public class FactorialSum {
    public static void main(String[] args) {
        int min = 1;
        int max = 20;
        int total = 0;
        for (int i = min; i <= max; i++) {
            total += factorial(i);
        }
        System.out.println(total);
    }

    private static long factorial(int number) {
        if (number == 1 || number == 0) {
            return 1;
        } else {
            return number * factorial(number - 1);
        }
    }
}
```

```powershell
268040729

```



#### 127.输入三个整数 x,y,z，请把这三个数由小到大输出

```java



```



#### 145.Java 实例 - 在链表（LinkedList）的开头和结尾添加元素

```java
import java.util.LinkedList;

/**
 * 以下实例演示了如何使用 LinkedList 类的 addFirst() 和 addLast() 方法在链表的开头和结尾添加元素：
 */
public class LinkedListAdd {
    public static void main(String[] args) {
        LinkedList<String> lList = new LinkedList<>();
        lList.add("1");
        lList.add("2");
        lList.add("3");
        lList.add("4");
        lList.add("5");
        System.out.println(lList);
        lList.addFirst("0");
        System.out.println(lList);
        lList.addLast("6");
        System.out.println(lList);
    }
}
```

```powershell
[1, 2, 3, 4, 5]
[0, 1, 2, 3, 4, 5]
[0, 1, 2, 3, 4, 5, 6]
```













#### 146.Java 实例 - 获取链表（LinkedList）的第一个和最后一个元素

```java
import java.util.LinkedList;

public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList<String> lList = new LinkedList();
        lList.add("100");
        lList.add("300");
        lList.add("500");
        lList.add("400");
        System.out.println("链表的第一个元素是：" + lList.getFirst());
        System.out.println("链表的最后一个元素是：" + lList.getLast());
    }
}

```

```powershell
链表的第一个元素是：100
链表的最后一个元素是：400

```



#### 148.Java 实例 - 获取链表的元素

```java
import java.util.LinkedList;

/**
 * 以下实例演示了使用 top() 和 pop() 方法来获取链表的元素：
 */
public class LinkedListGet {
    private LinkedList<Object> list = new LinkedList<>();

    //在 LinkedList 开头添加
    public void push(Object v) {
        list.addFirst(v);
    }

    //获取 LinkedList 开头的元素
    public Object top() {
        return list.getFirst();
    }

    //从头开始删除
    public Object pop() {
        return list.removeFirst();
    }

    public static void main(String[] args) {
        LinkedListGet stack = new LinkedListGet();
        for (int i = 30; i < 40; i++) {
            stack.push(new Integer(i));
        }
        System.out.println(stack.top());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
    }
}

```

```powershell
39
39
38
37
```









#### 161.Java 实例 - 集合打乱顺序

```java
import java.util.ArrayList;
import java.util.Collections;

/**
 * 以下实例演示了如何使用 Collections 类 Collections.shuffle() 方法来打乱集合元素的顺序：
 */
public class ShuffleCollections {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        System.out.println("打乱前：");
        System.out.println(list);

        for (int i = 1; i < 6; i++) {
            System.out.println("第" + i + "次打乱：");
            Collections.shuffle(list);//打乱集合
            System.out.println(list);
        }
    }
}

```

```powershell
打乱前：
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
第1次打乱：
[5, 8, 6, 2, 7, 4, 3, 1, 9, 0]
第2次打乱：
[3, 0, 1, 2, 8, 7, 9, 4, 5, 6]
第3次打乱：
[8, 9, 3, 5, 0, 7, 4, 1, 2, 6]
第4次打乱：
[7, 4, 0, 3, 1, 8, 6, 5, 9, 2]
第5次打乱：
[3, 5, 6, 9, 1, 2, 8, 7, 0, 4]
```







