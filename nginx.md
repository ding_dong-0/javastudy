



### 1.安装 pcre 依赖

```powershell
[root@localhost ~]# cd /usr/src
#将下载好的pcre-8.43.tar.gz 复制到当前目录下
[root@localhost src]# rz -E
rz waiting to receive.
[root@localhost src]# ls
debug  kernels  nginx-1.17.7  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz  pcre-8.43.tar.gz
[root@localhost src]# tar -xvf pcre
pcre2-10.34/        pcre2-10.34.tar.gz  pcre-8.43.tar.gz    
[root@localhost src]# tar -xvf pcre-8.43.tar.gz 
[root@localhost src]# cd pcre-8.43/
[root@localhost pcre-8.43]# ./configure
[root@localhost pcre-8.43]# make && make install
[root@localhost pcre-8.43]# pcre-config --version
8.43
```



```powershell
[root@localhost ~]# cd /usr/src
[root@localhost src]# rz -E
rz waiting to receive.
[root@localhost src]# ls
debug  kernels  nginx-1.17.7  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz  pcre-8.43.tar.gz
[root@localhost src]# tar -xvf pcre
pcre2-10.34/        pcre2-10.34.tar.gz  pcre-8.43.tar.gz 
[root@localhost src]# tar -xvf pcre-8.43.tar.gz 
[root@localhost src]# ls
debug  kernels  nginx-1.17.7  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz  pcre-8.43  pcre-8.43.tar.gz
[root@localhost src]# cd pcre-8.43/
[root@localhost pcre-8.43]# ls
[root@localhost pcre-8.43]# ./configure
[root@localhost pcre-8.43]# make && make install
[root@localhost pcre-8.43]# pcre-config --version
8.43
[root@localhost pcre-8.43]# 
```



### 这个pcre2-10貌似不用安装了就放着吧

```powershell

[root@localhost ~]# cd /usr/src
[root@localhost src]# ls
debug  kernels
[root@localhost src]# ls
debug  kernels  nginx-1.17.7.tar.gz  pcre2-10.34.tar.gz
[root@localhost src]# tar -xvf pcre2-10.34.tar.gz 
pcre2-10.34/
pcre2-10.34/LICENCE
pcre2-10.34/PrepareRelease
pcre2-10.34/RunTest.bat
pcre2-10.34/132html
pcre2-10.34/CMakeLists.txt
pcre2-10.34/Makefile.am
pcre2-10.34/compile
pcre2-10.34/depcomp
pcre2-10.34/config.sub
pcre2-10.34/libpcre2-16.pc.in
pcre2-10.34/Makefile.in
pcre2-10.34/README
pcre2-10.34/cmake/
pcre2-10.34/cmake/FindReadline.cmake
pcre2-10.34/cmake/FindEditline.cmake
pcre2-10.34/cmake/FindPackageHandleStandardArgs.cmake
pcre2-10.34/cmake/COPYING-CMAKE-SCRIPTS
pcre2-10.34/aclocal.m4
pcre2-10.34/config.guess
pcre2-10.34/configure
pcre2-10.34/CleanTxt
pcre2-10.34/install-sh
pcre2-10.34/RunGrepTest.bat
pcre2-10.34/pcre2-config.in
pcre2-10.34/m4/
pcre2-10.34/m4/ltsugar.m4
pcre2-10.34/m4/libtool.m4
pcre2-10.34/m4/ltversion.m4
pcre2-10.34/m4/ltoptions.m4
pcre2-10.34/m4/pcre2_visibility.m4
pcre2-10.34/m4/ax_pthread.m4
pcre2-10.34/m4/lt~obsolete.m4
pcre2-10.34/COPYING
pcre2-10.34/INSTALL
pcre2-10.34/ChangeLog
pcre2-10.34/libpcre2-posix.pc.in
pcre2-10.34/HACKING
pcre2-10.34/test-driver
pcre2-10.34/missing
pcre2-10.34/configure.ac
pcre2-10.34/ar-lib
pcre2-10.34/config-cmake.h.in
pcre2-10.34/testdata/
pcre2-10.34/testdata/testoutput21
pcre2-10.34/testdata/testoutput24
pcre2-10.34/testdata/testoutput2
pcre2-10.34/testdata/testoutputEBC
pcre2-10.34/testdata/testoutput18
pcre2-10.34/testdata/greplist
pcre2-10.34/testdata/testinput21
pcre2-10.34/testdata/testinput25
pcre2-10.34/testdata/testoutput3A
pcre2-10.34/testdata/testoutput20
pcre2-10.34/testdata/grepoutput8
pcre2-10.34/testdata/testoutput16
pcre2-10.34/testdata/testinput17
pcre2-10.34/testdata/testoutput23
pcre2-10.34/testdata/testoutput4
pcre2-10.34/testdata/testoutput8-16-4
pcre2-10.34/testdata/grepoutputCN
pcre2-10.34/testdata/testinput8
pcre2-10.34/testdata/testoutput7
pcre2-10.34/testdata/testoutput8-8-3
pcre2-10.34/testdata/grepinput3
pcre2-10.34/testdata/testoutput22-32
pcre2-10.34/testdata/testinput13
pcre2-10.34/testdata/testinput15
pcre2-10.34/testdata/wintestoutput3
pcre2-10.34/testdata/testinput10
pcre2-10.34/testdata/testoutput14-8
pcre2-10.34/testdata/testinput1
pcre2-10.34/testdata/testoutput5
pcre2-10.34/testdata/grepoutputN
pcre2-10.34/testdata/testoutput8-32-4
pcre2-10.34/testdata/testinput24
pcre2-10.34/testdata/testoutput9
pcre2-10.34/testdata/testoutput8-32-3
pcre2-10.34/testdata/testoutput10
pcre2-10.34/testdata/testinput16
pcre2-10.34/testdata/testoutput25
pcre2-10.34/testdata/testoutput3B
pcre2-10.34/testdata/testoutput15
pcre2-10.34/testdata/testoutput13
pcre2-10.34/testdata/testoutput8-8-2
pcre2-10.34/testdata/testoutput12-16
pcre2-10.34/testdata/grepbinary
pcre2-10.34/testdata/grepinputx
pcre2-10.34/testdata/greppatN4
pcre2-10.34/testdata/testinput5
pcre2-10.34/testdata/testoutput19
pcre2-10.34/testdata/testoutput8-16-3
pcre2-10.34/testdata/testoutput6
pcre2-10.34/testdata/testinput3
pcre2-10.34/testdata/testinput23
pcre2-10.34/testdata/testinput7
pcre2-10.34/testdata/testinput18
pcre2-10.34/testdata/testoutput8-8-4
pcre2-10.34/testdata/testoutput22-8
pcre2-10.34/testdata/testoutput3
pcre2-10.34/testdata/testinput22
pcre2-10.34/testdata/grepinputM
pcre2-10.34/testdata/grepoutputC
pcre2-10.34/testdata/testinput9
pcre2-10.34/testdata/testinput20
pcre2-10.34/testdata/testinput6
pcre2-10.34/testdata/valgrind-jit.supp
pcre2-10.34/testdata/testinput12
pcre2-10.34/testdata/grepinput8
pcre2-10.34/testdata/testoutput14-16
pcre2-10.34/testdata/grepfilelist
pcre2-10.34/testdata/testinput2
pcre2-10.34/testdata/grepinputv
pcre2-10.34/testdata/grepoutput
pcre2-10.34/testdata/testinput11
pcre2-10.34/testdata/testoutput12-32
pcre2-10.34/testdata/testinput4
pcre2-10.34/testdata/grepinput
pcre2-10.34/testdata/testoutput8-32-2
pcre2-10.34/testdata/testoutput8-16-2
pcre2-10.34/testdata/wintestinput3
pcre2-10.34/testdata/testinputEBC
pcre2-10.34/testdata/testoutput14-32
pcre2-10.34/testdata/testinput14
pcre2-10.34/testdata/testoutput22-16
pcre2-10.34/testdata/testoutput11-32
pcre2-10.34/testdata/testoutput1
pcre2-10.34/testdata/testinput19
pcre2-10.34/testdata/testoutput11-16
pcre2-10.34/testdata/testoutput17
pcre2-10.34/RunTest
pcre2-10.34/src/
pcre2-10.34/src/pcre2posix.h
pcre2-10.34/src/pcre2_intmodedep.h
pcre2-10.34/src/pcre2_find_bracket.c
pcre2-10.34/src/pcre2_jit_compile.c
pcre2-10.34/src/pcre2_newline.c
pcre2-10.34/src/pcre2posix.c
pcre2-10.34/src/pcre2.h.in
pcre2-10.34/src/pcre2_substitute.c
pcre2-10.34/src/pcre2_match_data.c
pcre2-10.34/src/pcre2_tables.c
pcre2-10.34/src/pcre2_ucp.h
pcre2-10.34/src/dftables.c
pcre2-10.34/src/pcre2_jit_simd_inc.h
pcre2-10.34/src/pcre2_config.c
pcre2-10.34/src/pcre2_jit_test.c
pcre2-10.34/src/pcre2demo.c
pcre2-10.34/src/pcre2_serialize.c
pcre2-10.34/src/pcre2_compile.c
pcre2-10.34/src/pcre2_chartables.c.dist
pcre2-10.34/src/pcre2_jit_match.c
pcre2-10.34/src/pcre2_study.c
pcre2-10.34/src/pcre2_script_run.c
pcre2-10.34/src/pcre2_xclass.c
pcre2-10.34/src/pcre2_dfa_match.c
pcre2-10.34/src/pcre2_valid_utf.c
pcre2-10.34/src/pcre2_substring.c
pcre2-10.34/src/pcre2_ord2utf.c
pcre2-10.34/src/pcre2_jit_neon_inc.h
pcre2-10.34/src/pcre2.h.generic
pcre2-10.34/src/pcre2_extuni.c
pcre2-10.34/src/sljit/
pcre2-10.34/src/sljit/sljitNativeSPARC_common.c
pcre2-10.34/src/sljit/sljitNativeTILEGX-encoder.c
pcre2-10.34/src/sljit/sljitConfig.h
pcre2-10.34/src/sljit/sljitNativeMIPS_64.c
pcre2-10.34/src/sljit/sljitUtils.c
pcre2-10.34/src/sljit/sljitNativeARM_T2_32.c
pcre2-10.34/src/sljit/sljitNativePPC_64.c
pcre2-10.34/src/sljit/sljitNativeARM_64.c
pcre2-10.34/src/sljit/sljitNativeMIPS_common.c
pcre2-10.34/src/sljit/sljitNativeMIPS_32.c
pcre2-10.34/src/sljit/sljitNativeX86_common.c
pcre2-10.34/src/sljit/sljitNativeTILEGX_64.c
pcre2-10.34/src/sljit/sljitNativeARM_32.c
pcre2-10.34/src/sljit/sljitNativeSPARC_32.c
pcre2-10.34/src/sljit/sljitLir.c
pcre2-10.34/src/sljit/sljitLir.h
pcre2-10.34/src/sljit/sljitNativeX86_32.c
pcre2-10.34/src/sljit/sljitNativePPC_32.c
pcre2-10.34/src/sljit/sljitExecAllocator.c
pcre2-10.34/src/sljit/sljitProtExecAllocator.c
pcre2-10.34/src/sljit/sljitNativeX86_64.c
pcre2-10.34/src/sljit/sljitNativePPC_common.c
pcre2-10.34/src/sljit/sljitConfigInternal.h
pcre2-10.34/src/pcre2_jit_misc.c
pcre2-10.34/src/config.h.in
pcre2-10.34/src/pcre2_auto_possess.c
pcre2-10.34/src/pcre2_pattern_info.c
pcre2-10.34/src/pcre2_fuzzsupport.c
pcre2-10.34/src/pcre2_maketables.c
pcre2-10.34/src/pcre2_printint.c
pcre2-10.34/src/pcre2_string_utils.c
pcre2-10.34/src/pcre2_ucd.c
pcre2-10.34/src/pcre2test.c
pcre2-10.34/src/pcre2_convert.c
pcre2-10.34/src/pcre2_internal.h
pcre2-10.34/src/pcre2_context.c
pcre2-10.34/src/pcre2grep.c
pcre2-10.34/src/pcre2_error.c
pcre2-10.34/src/pcre2_match.c
pcre2-10.34/src/config.h.generic
pcre2-10.34/perltest.sh
pcre2-10.34/AUTHORS
pcre2-10.34/Detrail
pcre2-10.34/NON-AUTOTOOLS-BUILD
pcre2-10.34/ltmain.sh
pcre2-10.34/NEWS
pcre2-10.34/doc/
pcre2-10.34/doc/pcre2_maketables_free.3
pcre2-10.34/doc/pcre2_get_error_message.3
pcre2-10.34/doc/pcre2_jit_compile.3
pcre2-10.34/doc/pcre2_set_character_tables.3
pcre2-10.34/doc/pcre2demo.3
pcre2-10.34/doc/pcre2_match_data_free.3
pcre2-10.34/doc/pcre2.3
pcre2-10.34/doc/pcre2_maketables.3
pcre2-10.34/doc/pcre2limits.3
pcre2-10.34/doc/pcre2_substring_free.3
pcre2-10.34/doc/pcre2_set_heap_limit.3
pcre2-10.34/doc/pcre2_set_glob_escape.3
pcre2-10.34/doc/pcre2_substring_length_byname.3
pcre2-10.34/doc/pcre2partial.3
pcre2-10.34/doc/pcre2_set_substitute_callout.3
pcre2-10.34/doc/pcre2syntax.3
pcre2-10.34/doc/pcre2.txt
pcre2-10.34/doc/pcre2_general_context_copy.3
pcre2-10.34/doc/pcre2_general_context_free.3
pcre2-10.34/doc/pcre2_set_parens_nest_limit.3
pcre2-10.34/doc/pcre2_config.3
pcre2-10.34/doc/pcre2_jit_stack_create.3
pcre2-10.34/doc/pcre2-config.txt
pcre2-10.34/doc/pcre2api.3
pcre2-10.34/doc/pcre2_general_context_create.3
pcre2-10.34/doc/pcre2_match_data_create.3
pcre2-10.34/doc/pcre2_get_match_data_size.3
pcre2-10.34/doc/pcre2grep.txt
pcre2-10.34/doc/pcre2_serialize_free.3
pcre2-10.34/doc/pcre2_code_copy.3
pcre2-10.34/doc/pcre2_pattern_convert.3
pcre2-10.34/doc/pcre2_set_compile_extra_options.3
pcre2-10.34/doc/pcre2_substring_nametable_scan.3
pcre2-10.34/doc/pcre2_match_context_free.3
pcre2-10.34/doc/pcre2-config.1
pcre2-10.34/doc/pcre2sample.3
pcre2-10.34/doc/pcre2posix.3
pcre2-10.34/doc/pcre2_get_ovector_count.3
pcre2-10.34/doc/pcre2convert.3
pcre2-10.34/doc/pcre2_callout_enumerate.3
pcre2-10.34/doc/pcre2_compile.3
pcre2-10.34/doc/html/
pcre2-10.34/doc/html/pcre2test.html
pcre2-10.34/doc/html/pcre2pattern.html
pcre2-10.34/doc/html/pcre2_substring_nametable_scan.html
pcre2-10.34/doc/html/pcre2_jit_stack_free.html
pcre2-10.34/doc/html/index.html
pcre2-10.34/doc/html/pcre2_substring_get_byname.html
pcre2-10.34/doc/html/pcre2_set_compile_recursion_guard.html
pcre2-10.34/doc/html/pcre2_convert_context_free.html
pcre2-10.34/doc/html/pcre2_jit_compile.html
pcre2-10.34/doc/html/pcre2_set_heap_limit.html
pcre2-10.34/doc/html/pcre2unicode.html
pcre2-10.34/doc/html/pcre2syntax.html
pcre2-10.34/doc/html/pcre2.html
pcre2-10.34/doc/html/pcre2_match.html
pcre2-10.34/doc/html/pcre2_get_error_message.html
pcre2-10.34/doc/html/pcre2_substring_number_from_name.html
pcre2-10.34/doc/html/pcre2_jit_stack_assign.html
pcre2-10.34/doc/html/pcre2_set_parens_nest_limit.html
pcre2-10.34/doc/html/pcre2_code_free.html
pcre2-10.34/doc/html/pcre2convert.html
pcre2-10.34/doc/html/pcre2_substring_free.html
pcre2-10.34/doc/html/pcre2matching.html
pcre2-10.34/doc/html/pcre2_get_ovector_count.html
pcre2-10.34/doc/html/pcre2sample.html
pcre2-10.34/doc/html/pcre2_maketables.html
pcre2-10.34/doc/html/pcre2jit.html
pcre2-10.34/doc/html/pcre2_match_context_free.html
pcre2-10.34/doc/html/pcre2_config.html
pcre2-10.34/doc/html/pcre2_compile_context_free.html
pcre2-10.34/doc/html/pcre2_pattern_info.html
pcre2-10.34/doc/html/pcre2_substitute.html
pcre2-10.34/doc/html/pcre2_set_substitute_callout.html
pcre2-10.34/doc/html/pcre2-config.html
pcre2-10.34/doc/html/pcre2_set_recursion_memory_management.html
pcre2-10.34/doc/html/pcre2_serialize_encode.html
pcre2-10.34/doc/html/pcre2_serialize_free.html
pcre2-10.34/doc/html/pcre2_set_recursion_limit.html
pcre2-10.34/doc/html/pcre2_serialize_get_number_of_codes.html
pcre2-10.34/doc/html/pcre2_pattern_convert.html
pcre2-10.34/doc/html/pcre2_set_offset_limit.html
pcre2-10.34/doc/html/pcre2_get_startchar.html
pcre2-10.34/doc/html/pcre2grep.html
pcre2-10.34/doc/html/pcre2build.html
pcre2-10.34/doc/html/pcre2_jit_match.html
pcre2-10.34/doc/html/pcre2_callout_enumerate.html
pcre2-10.34/doc/html/pcre2_substring_copy_bynumber.html
pcre2-10.34/doc/html/pcre2_convert_context_copy.html
pcre2-10.34/doc/html/pcre2_compile.html
pcre2-10.34/doc/html/pcre2_match_context_create.html
pcre2-10.34/doc/html/pcre2_match_data_create.html
pcre2-10.34/doc/html/pcre2_general_context_create.html
pcre2-10.34/doc/html/pcre2serialize.html
pcre2-10.34/doc/html/pcre2_substring_list_free.html
pcre2-10.34/doc/html/pcre2_match_data_create_from_pattern.html
pcre2-10.34/doc/html/pcre2_substring_length_byname.html
pcre2-10.34/doc/html/pcre2_compile_context_create.html
pcre2-10.34/doc/html/pcre2_compile_context_copy.html
pcre2-10.34/doc/html/pcre2_convert_context_create.html
pcre2-10.34/doc/html/pcre2_set_max_pattern_length.html
pcre2-10.34/doc/html/pcre2_substring_length_bynumber.html
pcre2-10.34/doc/html/pcre2_general_context_copy.html
pcre2-10.34/doc/html/pcre2limits.html
pcre2-10.34/doc/html/pcre2callout.html
pcre2-10.34/doc/html/pcre2posix.html
pcre2-10.34/doc/html/pcre2_converted_pattern_free.html
pcre2-10.34/doc/html/README.txt
pcre2-10.34/doc/html/pcre2_general_context_free.html
pcre2-10.34/doc/html/pcre2_set_depth_limit.html
pcre2-10.34/doc/html/pcre2_set_glob_escape.html
pcre2-10.34/doc/html/pcre2_set_compile_extra_options.html
pcre2-10.34/doc/html/pcre2_jit_free_unused_memory.html
pcre2-10.34/doc/html/pcre2_jit_stack_create.html
pcre2-10.34/doc/html/pcre2_maketables_free.html
pcre2-10.34/doc/html/pcre2api.html
pcre2-10.34/doc/html/pcre2demo.html
pcre2-10.34/doc/html/pcre2_set_glob_separator.html
pcre2-10.34/doc/html/pcre2_substring_copy_byname.html
pcre2-10.34/doc/html/pcre2_serialize_decode.html
pcre2-10.34/doc/html/NON-AUTOTOOLS-BUILD.txt
pcre2-10.34/doc/html/pcre2compat.html
pcre2-10.34/doc/html/pcre2_set_bsr.html
pcre2-10.34/doc/html/pcre2_match_context_copy.html
pcre2-10.34/doc/html/pcre2_match_data_free.html
pcre2-10.34/doc/html/pcre2_set_character_tables.html
pcre2-10.34/doc/html/pcre2partial.html
pcre2-10.34/doc/html/pcre2_set_newline.html
pcre2-10.34/doc/html/pcre2_get_match_data_size.html
pcre2-10.34/doc/html/pcre2_set_match_limit.html
pcre2-10.34/doc/html/pcre2_code_copy.html
pcre2-10.34/doc/html/pcre2_set_callout.html
pcre2-10.34/doc/html/pcre2_get_ovector_pointer.html
pcre2-10.34/doc/html/pcre2perform.html
pcre2-10.34/doc/html/pcre2_substring_list_get.html
pcre2-10.34/doc/html/pcre2_dfa_match.html
pcre2-10.34/doc/html/pcre2_code_copy_with_tables.html
pcre2-10.34/doc/html/pcre2_get_mark.html
pcre2-10.34/doc/html/pcre2_substring_get_bynumber.html
pcre2-10.34/doc/pcre2jit.3
pcre2-10.34/doc/pcre2_serialize_decode.3
pcre2-10.34/doc/pcre2_match_context_create.3
pcre2-10.34/doc/pcre2serialize.3
pcre2-10.34/doc/pcre2perform.3
pcre2-10.34/doc/pcre2_substring_list_get.3
pcre2-10.34/doc/pcre2_set_depth_limit.3
pcre2-10.34/doc/pcre2_set_match_limit.3
pcre2-10.34/doc/index.html.src
pcre2-10.34/doc/pcre2_serialize_get_number_of_codes.3
pcre2-10.34/doc/pcre2callout.3
pcre2-10.34/doc/pcre2_match_context_copy.3
pcre2-10.34/doc/pcre2_match.3
pcre2-10.34/doc/pcre2_set_bsr.3
pcre2-10.34/doc/pcre2_substring_list_free.3
pcre2-10.34/doc/pcre2_dfa_match.3
pcre2-10.34/doc/pcre2_compile_context_create.3
pcre2-10.34/doc/pcre2_convert_context_free.3
pcre2-10.34/doc/pcre2_compile_context_copy.3
pcre2-10.34/doc/pcre2_code_copy_with_tables.3
pcre2-10.34/doc/pcre2grep.1
pcre2-10.34/doc/pcre2_convert_context_create.3
pcre2-10.34/doc/pcre2_substitute.3
pcre2-10.34/doc/pcre2_set_compile_recursion_guard.3
pcre2-10.34/doc/pcre2test.1
pcre2-10.34/doc/pcre2_converted_pattern_free.3
pcre2-10.34/doc/pcre2_substring_number_from_name.3
pcre2-10.34/doc/pcre2_set_newline.3
pcre2-10.34/doc/pcre2_set_glob_separator.3
pcre2-10.34/doc/pcre2matching.3
pcre2-10.34/doc/pcre2_set_max_pattern_length.3
pcre2-10.34/doc/pcre2_match_data_create_from_pattern.3
pcre2-10.34/doc/pcre2_set_recursion_limit.3
pcre2-10.34/doc/pcre2test.txt
pcre2-10.34/doc/pcre2_pattern_info.3
pcre2-10.34/doc/pcre2_compile_context_free.3
pcre2-10.34/doc/pcre2_jit_free_unused_memory.3
pcre2-10.34/doc/pcre2unicode.3
pcre2-10.34/doc/pcre2_set_recursion_memory_management.3
pcre2-10.34/doc/pcre2_jit_stack_free.3
pcre2-10.34/doc/pcre2_convert_context_copy.3
pcre2-10.34/doc/pcre2_jit_stack_assign.3
pcre2-10.34/doc/pcre2_code_free.3
pcre2-10.34/doc/pcre2_substring_get_byname.3
pcre2-10.34/doc/pcre2_get_startchar.3
pcre2-10.34/doc/pcre2_get_mark.3
pcre2-10.34/doc/pcre2_set_callout.3
pcre2-10.34/doc/pcre2_substring_get_bynumber.3
pcre2-10.34/doc/pcre2compat.3
pcre2-10.34/doc/pcre2_jit_match.3
pcre2-10.34/doc/pcre2_get_ovector_pointer.3
pcre2-10.34/doc/pcre2_substring_copy_bynumber.3
pcre2-10.34/doc/pcre2pattern.3
pcre2-10.34/doc/pcre2_substring_copy_byname.3
pcre2-10.34/doc/pcre2_set_offset_limit.3
pcre2-10.34/doc/pcre2_serialize_encode.3
pcre2-10.34/doc/pcre2_substring_length_bynumber.3
pcre2-10.34/doc/pcre2build.3
pcre2-10.34/libpcre2-8.pc.in
pcre2-10.34/RunGrepTest
pcre2-10.34/CheckMan
pcre2-10.34/libpcre2-32.pc.in
[root@localhost src]# ls
debug  kernels  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz
[root@localhost src]# cd pcre2-10.34/
[root@localhost pcre2-10.34]# ./configure
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /usr/bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether make supports nested variables... (cached) yes
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking whether gcc understands -c and -o together... yes
checking whether make supports the include directive... yes (GNU style)
checking dependency style of gcc... gcc3
checking how to run the C preprocessor... gcc -E
checking for grep that handles long lines and -e... /usr/bin/grep
checking for egrep... /usr/bin/grep -E
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking minix/config.h usability... no
checking minix/config.h presence... no
checking for minix/config.h... no
checking whether it is safe to define __EXTENSIONS__... yes
checking for ar... ar
checking the archiver (ar) interface... ar
checking for int64_t... yes
checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
checking how to print strings... printf
checking for a sed that does not truncate output... /usr/bin/sed
checking for fgrep... /usr/bin/grep -F
checking for ld used by gcc... /usr/bin/ld
checking if the linker (/usr/bin/ld) is GNU ld... yes
checking for BSD- or MS-compatible name lister (nm)... /usr/bin/nm -B
checking the name lister (/usr/bin/nm -B) interface... BSD nm
checking whether ln -s works... yes
checking the maximum length of command line arguments... 1572864
checking how to convert x86_64-pc-linux-gnu file names to x86_64-pc-linux-gnu format... func_convert_file_noop
checking how to convert x86_64-pc-linux-gnu file names to toolchain format... func_convert_file_noop
checking for /usr/bin/ld option to reload object files... -r
checking for objdump... objdump
checking how to recognize dependent libraries... pass_all
checking for dlltool... dlltool
checking how to associate runtime and link libraries... printf %s\n
checking for archiver @FILE support... @
checking for strip... strip
checking for ranlib... ranlib
checking command to parse /usr/bin/nm -B output from gcc object... ok
checking for sysroot... no
checking for a working dd... /usr/bin/dd
checking how to truncate binary pipes... /usr/bin/dd bs=4096 count=1
checking for mt... no
checking if : is a manifest tool... no
checking for dlfcn.h... yes
checking for objdir... .libs
checking if gcc supports -fno-rtti -fno-exceptions... no
checking for gcc option to produce PIC... -fPIC -DPIC
checking if gcc PIC flag -fPIC -DPIC works... yes
checking if gcc static flag -static works... no
checking if gcc supports -c -o file.o... yes
checking if gcc supports -c -o file.o... (cached) yes
checking whether the gcc linker (/usr/bin/ld -m elf_x86_64) supports shared libraries... yes
checking whether -lc should be explicitly linked in... no
checking dynamic linker characteristics... GNU/Linux ld.so
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... yes
checking whether ln -s works... yes
checking whether the -Werror option is usable... yes
checking for simple visibility declarations... yes
checking for ANSI C header files... (cached) yes
checking limits.h usability... yes
checking limits.h presence... yes
checking for limits.h... yes
checking for sys/types.h... (cached) yes
checking for sys/stat.h... (cached) yes
checking dirent.h usability... yes
checking dirent.h presence... yes
checking for dirent.h... yes
checking windows.h usability... no
checking windows.h presence... no
checking for windows.h... no
checking sys/wait.h usability... yes
checking sys/wait.h presence... yes
checking for sys/wait.h... yes
checking for an ANSI C-conforming const... yes
checking for size_t... yes
checking for bcopy... yes
checking for memmove... yes
checking for strerror... yes
checking for mkostemp... yes
checking for secure_getenv... yes
checking zlib.h usability... no
checking zlib.h presence... no
checking for zlib.h... no
checking for gzopen in -lz... no
checking bzlib.h usability... no
checking bzlib.h presence... no
checking for bzlib.h... no
checking for libbz2... no
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating libpcre2-8.pc
config.status: creating libpcre2-16.pc
config.status: creating libpcre2-32.pc
config.status: creating libpcre2-posix.pc
config.status: creating pcre2-config
config.status: creating src/pcre2.h
config.status: creating src/config.h
config.status: executing depfiles commands
config.status: executing libtool commands
config.status: executing script-chmod commands
config.status: executing delete-old-chartables commands

pcre2-10.34 configuration summary:

    Install prefix ..................... : /usr/local
    C preprocessor ..................... : gcc -E
    C compiler ......................... : gcc
    Linker ............................. : /usr/bin/ld -m elf_x86_64
    C preprocessor flags ............... : 
    C compiler flags ................... : -O2 -fvisibility=hidden
    Linker flags ....................... : 
    Extra libraries .................... : 

    Build 8-bit pcre2 library .......... : yes
    Build 16-bit pcre2 library ......... : no
    Build 32-bit pcre2 library ......... : no
    Include debugging code ............. : no
    Enable JIT compiling support ....... : no
    Use SELinux allocator in JIT ....... : no
    Enable Unicode support ............. : yes
    Newline char/sequence .............. : lf
    \R matches only ANYCRLF ............ : no
    \C is disabled ..................... : no
    EBCDIC coding ...................... : no
    EBCDIC code for NL ................. : n/a
    Rebuild char tables ................ : no
    Internal link size ................. : 2
    Nested parentheses limit ........... : 250
    Heap limit ......................... : 20000000 kibibytes
    Match limit ........................ : 10000000
    Match depth limit .................. : MATCH_LIMIT
    Build shared libs .................. : yes
    Build static libs .................. : yes
    Use JIT in pcre2grep ............... : no
    Enable callouts in pcre2grep ....... : yes
    Enable fork in pcre2grep callouts .. : yes
    Initial buffer size for pcre2grep .. : 20480
    Maximum buffer size for pcre2grep .. : 1048576
    Link pcre2grep with libz ........... : no
    Link pcre2grep with libbz2 ......... : no
    Link pcre2test with libedit ........ : no
    Link pcre2test with libreadline .... : no
    Valgrind support ................... : no
    Code coverage ...................... : no
    Fuzzer support ..................... : no
    Use %zu and %td .................... : auto

[root@localhost pcre2-10.34]# make && make install
rm -f src/pcre2_chartables.c
ln -s /usr/src/pcre2-10.34/src/pcre2_chartables.c.dist /usr/src/pcre2-10.34/src/pcre2_chartables.c
make  all-am
make[1]: 进入目录“/usr/src/pcre2-10.34”
  CC       src/pcre2grep-pcre2grep.o
  CC       src/libpcre2_8_la-pcre2_auto_possess.lo
  CC       src/libpcre2_8_la-pcre2_compile.lo
  CC       src/libpcre2_8_la-pcre2_config.lo
  CC       src/libpcre2_8_la-pcre2_context.lo
  CC       src/libpcre2_8_la-pcre2_convert.lo
  CC       src/libpcre2_8_la-pcre2_dfa_match.lo
  CC       src/libpcre2_8_la-pcre2_error.lo
  CC       src/libpcre2_8_la-pcre2_extuni.lo
  CC       src/libpcre2_8_la-pcre2_find_bracket.lo
  CC       src/libpcre2_8_la-pcre2_jit_compile.lo
  CC       src/libpcre2_8_la-pcre2_maketables.lo
  CC       src/libpcre2_8_la-pcre2_match.lo
  CC       src/libpcre2_8_la-pcre2_match_data.lo
  CC       src/libpcre2_8_la-pcre2_newline.lo
  CC       src/libpcre2_8_la-pcre2_ord2utf.lo
  CC       src/libpcre2_8_la-pcre2_pattern_info.lo
  CC       src/libpcre2_8_la-pcre2_script_run.lo
  CC       src/libpcre2_8_la-pcre2_serialize.lo
  CC       src/libpcre2_8_la-pcre2_string_utils.lo
  CC       src/libpcre2_8_la-pcre2_study.lo
  CC       src/libpcre2_8_la-pcre2_substitute.lo
  CC       src/libpcre2_8_la-pcre2_substring.lo
  CC       src/libpcre2_8_la-pcre2_tables.lo
  CC       src/libpcre2_8_la-pcre2_ucd.lo
  CC       src/libpcre2_8_la-pcre2_valid_utf.lo
  CC       src/libpcre2_8_la-pcre2_xclass.lo
  CC       src/libpcre2_8_la-pcre2_chartables.lo
  CCLD     libpcre2-8.la
  CCLD     pcre2grep
  CC       src/pcre2test-pcre2test.o
  CC       src/libpcre2_posix_la-pcre2posix.lo
  CCLD     libpcre2-posix.la
  CCLD     pcre2test
make[1]: 离开目录“/usr/src/pcre2-10.34”
make  install-am
make[1]: 进入目录“/usr/src/pcre2-10.34”
make[2]: 进入目录“/usr/src/pcre2-10.34”
 /usr/bin/mkdir -p '/usr/local/lib'
 /bin/sh ./libtool   --mode=install /usr/bin/install -c   libpcre2-8.la libpcre2-posix.la '/usr/local/lib'
libtool: install: /usr/bin/install -c .libs/libpcre2-8.so.0.9.0 /usr/local/lib/libpcre2-8.so.0.9.0
libtool: install: (cd /usr/local/lib && { ln -s -f libpcre2-8.so.0.9.0 libpcre2-8.so.0 || { rm -f libpcre2-8.so.0 && ln -s libpcre2-8.so.0.9.0 libpcre2-8.so.0; }; })
libtool: install: (cd /usr/local/lib && { ln -s -f libpcre2-8.so.0.9.0 libpcre2-8.so || { rm -f libpcre2-8.so && ln -s libpcre2-8.so.0.9.0 libpcre2-8.so; }; })
libtool: install: /usr/bin/install -c .libs/libpcre2-8.lai /usr/local/lib/libpcre2-8.la
libtool: warning: relinking 'libpcre2-posix.la'
libtool: install: (cd /usr/src/pcre2-10.34; /bin/sh "/usr/src/pcre2-10.34/libtool"  --silent --tag CC --mode=relink gcc -DPCRE2_CODE_UNIT_WIDTH=8 -fvisibility=hidden -O2 -version-info 2:3:0 -o libpcre2-posix.la -rpath /usr/local/lib src/libpcre2_posix_la-pcre2posix.lo libpcre2-8.la )
libtool: install: /usr/bin/install -c .libs/libpcre2-posix.so.2.0.3T /usr/local/lib/libpcre2-posix.so.2.0.3
libtool: install: (cd /usr/local/lib && { ln -s -f libpcre2-posix.so.2.0.3 libpcre2-posix.so.2 || { rm -f libpcre2-posix.so.2 && ln -s libpcre2-posix.so.2.0.3 libpcre2-posix.so.2; }; })
libtool: install: (cd /usr/local/lib && { ln -s -f libpcre2-posix.so.2.0.3 libpcre2-posix.so || { rm -f libpcre2-posix.so && ln -s libpcre2-posix.so.2.0.3 libpcre2-posix.so; }; })
libtool: install: /usr/bin/install -c .libs/libpcre2-posix.lai /usr/local/lib/libpcre2-posix.la
libtool: install: /usr/bin/install -c .libs/libpcre2-8.a /usr/local/lib/libpcre2-8.a
libtool: install: chmod 644 /usr/local/lib/libpcre2-8.a
libtool: install: ranlib /usr/local/lib/libpcre2-8.a
libtool: install: /usr/bin/install -c .libs/libpcre2-posix.a /usr/local/lib/libpcre2-posix.a
libtool: install: chmod 644 /usr/local/lib/libpcre2-posix.a
libtool: install: ranlib /usr/local/lib/libpcre2-posix.a
libtool: finish: PATH="/opt/jdk-13.0.1/bin:/usr/share/Modules/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/.dotnet/tools:/root/bin:/sbin" ldconfig -n /usr/local/lib
----------------------------------------------------------------------
Libraries have been installed in:
   /usr/local/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the '-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the 'LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the 'LD_RUN_PATH' environment variable
     during linking
   - use the '-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to '/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
 /usr/bin/mkdir -p '/usr/local/bin'
  /bin/sh ./libtool   --mode=install /usr/bin/install -c pcre2grep pcre2test '/usr/local/bin'
libtool: install: /usr/bin/install -c .libs/pcre2grep /usr/local/bin/pcre2grep
libtool: install: /usr/bin/install -c .libs/pcre2test /usr/local/bin/pcre2test
 /usr/bin/mkdir -p '/usr/local/bin'
 /usr/bin/install -c pcre2-config '/usr/local/bin'
 /usr/bin/mkdir -p '/usr/local/share/doc/pcre2'
 /usr/bin/install -c -m 644 AUTHORS COPYING ChangeLog LICENCE NEWS README doc/pcre2.txt doc/pcre2-config.txt doc/pcre2grep.txt doc/pcre2test.txt '/usr/local/share/doc/pcre2'
 /usr/bin/mkdir -p '/usr/local/share/doc/pcre2/html'
 /usr/bin/install -c -m 644 doc/html/NON-AUTOTOOLS-BUILD.txt doc/html/README.txt doc/html/index.html doc/html/pcre2-config.html doc/html/pcre2.html doc/html/pcre2_callout_enumerate.html doc/html/pcre2_code_copy.html doc/html/pcre2_code_copy_with_tables.html doc/html/pcre2_code_free.html doc/html/pcre2_compile.html doc/html/pcre2_compile_context_copy.html doc/html/pcre2_compile_context_create.html doc/html/pcre2_compile_context_free.html doc/html/pcre2_config.html doc/html/pcre2_convert_context_copy.html doc/html/pcre2_convert_context_create.html doc/html/pcre2_convert_context_free.html doc/html/pcre2_converted_pattern_free.html doc/html/pcre2_dfa_match.html doc/html/pcre2_general_context_copy.html doc/html/pcre2_general_context_create.html doc/html/pcre2_general_context_free.html doc/html/pcre2_get_error_message.html doc/html/pcre2_get_mark.html doc/html/pcre2_get_match_data_size.html doc/html/pcre2_get_ovector_count.html doc/html/pcre2_get_ovector_pointer.html doc/html/pcre2_get_startchar.html doc/html/pcre2_jit_compile.html doc/html/pcre2_jit_free_unused_memory.html doc/html/pcre2_jit_match.html doc/html/pcre2_jit_stack_assign.html doc/html/pcre2_jit_stack_create.html doc/html/pcre2_jit_stack_free.html doc/html/pcre2_maketables.html doc/html/pcre2_maketables_free.html doc/html/pcre2_match.html doc/html/pcre2_match_context_copy.html doc/html/pcre2_match_context_create.html doc/html/pcre2_match_context_free.html '/usr/local/share/doc/pcre2/html'
 /usr/bin/install -c -m 644 doc/html/pcre2_match_data_create.html doc/html/pcre2_match_data_create_from_pattern.html doc/html/pcre2_match_data_free.html doc/html/pcre2_pattern_convert.html doc/html/pcre2_pattern_info.html doc/html/pcre2_serialize_decode.html doc/html/pcre2_serialize_encode.html doc/html/pcre2_serialize_free.html doc/html/pcre2_serialize_get_number_of_codes.html doc/html/pcre2_set_bsr.html doc/html/pcre2_set_callout.html doc/html/pcre2_set_character_tables.html doc/html/pcre2_set_compile_extra_options.html doc/html/pcre2_set_compile_recursion_guard.html doc/html/pcre2_set_depth_limit.html doc/html/pcre2_set_glob_escape.html doc/html/pcre2_set_glob_separator.html doc/html/pcre2_set_heap_limit.html doc/html/pcre2_set_match_limit.html doc/html/pcre2_set_max_pattern_length.html doc/html/pcre2_set_offset_limit.html doc/html/pcre2_set_newline.html doc/html/pcre2_set_parens_nest_limit.html doc/html/pcre2_set_recursion_limit.html doc/html/pcre2_set_recursion_memory_management.html doc/html/pcre2_set_substitute_callout.html doc/html/pcre2_substitute.html doc/html/pcre2_substring_copy_byname.html doc/html/pcre2_substring_copy_bynumber.html doc/html/pcre2_substring_free.html doc/html/pcre2_substring_get_byname.html doc/html/pcre2_substring_get_bynumber.html doc/html/pcre2_substring_length_byname.html doc/html/pcre2_substring_length_bynumber.html doc/html/pcre2_substring_list_free.html doc/html/pcre2_substring_list_get.html doc/html/pcre2_substring_nametable_scan.html doc/html/pcre2_substring_number_from_name.html doc/html/pcre2api.html doc/html/pcre2build.html '/usr/local/share/doc/pcre2/html'
 /usr/bin/install -c -m 644 doc/html/pcre2callout.html doc/html/pcre2compat.html doc/html/pcre2convert.html doc/html/pcre2demo.html doc/html/pcre2grep.html doc/html/pcre2jit.html doc/html/pcre2limits.html doc/html/pcre2matching.html doc/html/pcre2partial.html doc/html/pcre2pattern.html doc/html/pcre2perform.html doc/html/pcre2posix.html doc/html/pcre2sample.html doc/html/pcre2serialize.html doc/html/pcre2syntax.html doc/html/pcre2test.html doc/html/pcre2unicode.html '/usr/local/share/doc/pcre2/html'
 /usr/bin/mkdir -p '/usr/local/include'
 /usr/bin/install -c -m 644 src/pcre2posix.h '/usr/local/include'
 /usr/bin/mkdir -p '/usr/local/share/man/man1'
 /usr/bin/install -c -m 644 doc/pcre2-config.1 doc/pcre2grep.1 doc/pcre2test.1 '/usr/local/share/man/man1'
 /usr/bin/mkdir -p '/usr/local/share/man/man3'
 /usr/bin/install -c -m 644 doc/pcre2.3 doc/pcre2_callout_enumerate.3 doc/pcre2_code_copy.3 doc/pcre2_code_copy_with_tables.3 doc/pcre2_code_free.3 doc/pcre2_compile.3 doc/pcre2_compile_context_copy.3 doc/pcre2_compile_context_create.3 doc/pcre2_compile_context_free.3 doc/pcre2_config.3 doc/pcre2_convert_context_copy.3 doc/pcre2_convert_context_create.3 doc/pcre2_convert_context_free.3 doc/pcre2_converted_pattern_free.3 doc/pcre2_dfa_match.3 doc/pcre2_general_context_copy.3 doc/pcre2_general_context_create.3 doc/pcre2_general_context_free.3 doc/pcre2_get_error_message.3 doc/pcre2_get_mark.3 doc/pcre2_get_match_data_size.3 doc/pcre2_get_ovector_count.3 doc/pcre2_get_ovector_pointer.3 doc/pcre2_get_startchar.3 doc/pcre2_jit_compile.3 doc/pcre2_jit_free_unused_memory.3 doc/pcre2_jit_match.3 doc/pcre2_jit_stack_assign.3 doc/pcre2_jit_stack_create.3 doc/pcre2_jit_stack_free.3 doc/pcre2_maketables.3 doc/pcre2_maketables_free.3 doc/pcre2_match.3 doc/pcre2_match_context_copy.3 doc/pcre2_match_context_create.3 doc/pcre2_match_context_free.3 doc/pcre2_match_data_create.3 doc/pcre2_match_data_create_from_pattern.3 doc/pcre2_match_data_free.3 doc/pcre2_pattern_convert.3 '/usr/local/share/man/man3'
 /usr/bin/install -c -m 644 doc/pcre2_pattern_info.3 doc/pcre2_serialize_decode.3 doc/pcre2_serialize_encode.3 doc/pcre2_serialize_free.3 doc/pcre2_serialize_get_number_of_codes.3 doc/pcre2_set_bsr.3 doc/pcre2_set_callout.3 doc/pcre2_set_character_tables.3 doc/pcre2_set_compile_extra_options.3 doc/pcre2_set_compile_recursion_guard.3 doc/pcre2_set_depth_limit.3 doc/pcre2_set_glob_escape.3 doc/pcre2_set_glob_separator.3 doc/pcre2_set_heap_limit.3 doc/pcre2_set_match_limit.3 doc/pcre2_set_max_pattern_length.3 doc/pcre2_set_offset_limit.3 doc/pcre2_set_newline.3 doc/pcre2_set_parens_nest_limit.3 doc/pcre2_set_recursion_limit.3 doc/pcre2_set_recursion_memory_management.3 doc/pcre2_set_substitute_callout.3 doc/pcre2_substitute.3 doc/pcre2_substring_copy_byname.3 doc/pcre2_substring_copy_bynumber.3 doc/pcre2_substring_free.3 doc/pcre2_substring_get_byname.3 doc/pcre2_substring_get_bynumber.3 doc/pcre2_substring_length_byname.3 doc/pcre2_substring_length_bynumber.3 doc/pcre2_substring_list_free.3 doc/pcre2_substring_list_get.3 doc/pcre2_substring_nametable_scan.3 doc/pcre2_substring_number_from_name.3 doc/pcre2api.3 doc/pcre2build.3 doc/pcre2callout.3 doc/pcre2compat.3 doc/pcre2convert.3 doc/pcre2demo.3 '/usr/local/share/man/man3'
 /usr/bin/install -c -m 644 doc/pcre2jit.3 doc/pcre2limits.3 doc/pcre2matching.3 doc/pcre2partial.3 doc/pcre2pattern.3 doc/pcre2perform.3 doc/pcre2posix.3 doc/pcre2sample.3 doc/pcre2serialize.3 doc/pcre2syntax.3 doc/pcre2unicode.3 '/usr/local/share/man/man3'
 /usr/bin/mkdir -p '/usr/local/include'
 /usr/bin/install -c -m 644 src/pcre2.h '/usr/local/include'
 /usr/bin/mkdir -p '/usr/local/lib/pkgconfig'
 /usr/bin/install -c -m 644 libpcre2-8.pc libpcre2-posix.pc '/usr/local/lib/pkgconfig'
make[2]: 离开目录“/usr/src/pcre2-10.34”
make[1]: 离开目录“/usr/src/pcre2-10.34”
[root@localhost pcre2-10.34]# 
[root@localhost pcre2-10.34]# pcre2-config --version
10.34
[root@localhost pcre2-10.34]# 


```

### 安装其他依赖

```powershell
[root@localhost pcre2-10.34]# yum -y install make zlib zlib-devel gcc-c++ libtool 
```



```powershell
[root@localhost pcre2-10.34]# yum -y install make zlib zlib-devel gcc-c++ libtool openssl openssl-devel
上次元数据过期检查：0:40:47 前，执行于 2019年12月31日 星期二 09时41分57秒。
Package make-1:4.2.1-9.el8.x86_64 is already installed.
Package zlib-1.2.11-10.el8.x86_64 is already installed.
Package gcc-c++-8.2.1-3.5.el8.x86_64 is already installed.
Package libtool-2.4.6-25.el8.x86_64 is already installed.
Package openssl-1:1.1.1-8.el8.x86_64 is already installed.
依赖关系解决。
==========================================================================================================================
 软件包                              架构                   版本                             仓库                    大小
==========================================================================================================================
Installing:
 openssl-devel                       x86_64                 1:1.1.1-8.el8                    BaseOS                 2.3 M
 zlib-devel                          x86_64                 1.2.11-10.el8                    BaseOS                  56 k
安装依赖关系:
 keyutils-libs-devel                 x86_64                 1.5.10-6.el8                     BaseOS                  48 k
 krb5-devel                          x86_64                 1.16.1-22.el8                    BaseOS                 546 k
 libcom_err-devel                    x86_64                 1.44.3-2.el8                     BaseOS                  37 k
 libkadm5                            x86_64                 1.16.1-22.el8                    BaseOS                 184 k
 libselinux-devel                    x86_64                 2.8-6.el8                        BaseOS                 199 k
 libsepol-devel                      x86_64                 2.8-2.el8                        BaseOS                  85 k
 libverto-devel                      x86_64                 0.3.0-5.el8                      BaseOS                  18 k
 pcre2-devel                         x86_64                 10.32-1.el8                      BaseOS                 605 k
 pcre2-utf32                         x86_64                 10.32-1.el8                      BaseOS                 220 k

事务概要
==========================================================================================================================
安装  11 软件包

总下载：4.2 M
安装大小：7.5 M
下载软件包：
(1/11): libcom_err-devel-1.44.3-2.el8.x86_64.rpm                                           27 kB/s |  37 kB     00:01    
(2/11): libkadm5-1.16.1-22.el8.x86_64.rpm                                                 883 kB/s | 184 kB     00:00    
(3/11): keyutils-libs-devel-1.5.10-6.el8.x86_64.rpm                                        30 kB/s |  48 kB     00:01    
(4/11): libsepol-devel-2.8-2.el8.x86_64.rpm                                               876 kB/s |  85 kB     00:00    
(5/11): libverto-devel-0.3.0-5.el8.x86_64.rpm                                             179 kB/s |  18 kB     00:00    
(6/11): openssl-devel-1.1.1-8.el8.x86_64.rpm                                              2.1 MB/s | 2.3 MB     00:01    
(7/11): pcre2-devel-10.32-1.el8.x86_64.rpm                                                1.6 MB/s | 605 kB     00:00    
(8/11): pcre2-utf32-10.32-1.el8.x86_64.rpm                                                795 kB/s | 220 kB     00:00    
(9/11): libselinux-devel-2.8-6.el8.x86_64.rpm                                              97 kB/s | 199 kB     00:02    
(10/11): zlib-devel-1.2.11-10.el8.x86_64.rpm                                              401 kB/s |  56 kB     00:00    
(11/11): krb5-devel-1.16.1-22.el8.x86_64.rpm                                              141 kB/s | 546 kB     00:03    
--------------------------------------------------------------------------------------------------------------------------
总计                                                                                      1.1 MB/s | 4.2 MB     00:03     
运行事务检查
事务检查成功。
运行事务测试
事务测试成功。
运行事务
  准备中      :                                                                                                       1/1 
  Installing  : zlib-devel-1.2.11-10.el8.x86_64                                                                      1/11 
  Installing  : pcre2-utf32-10.32-1.el8.x86_64                                                                       2/11 
  Installing  : pcre2-devel-10.32-1.el8.x86_64                                                                       3/11 
  Installing  : libverto-devel-0.3.0-5.el8.x86_64                                                                    4/11 
  Installing  : libsepol-devel-2.8-2.el8.x86_64                                                                      5/11 
  Installing  : libselinux-devel-2.8-6.el8.x86_64                                                                    6/11 
  Installing  : libkadm5-1.16.1-22.el8.x86_64                                                                        7/11 
  Installing  : libcom_err-devel-1.44.3-2.el8.x86_64                                                                 8/11 
  Installing  : keyutils-libs-devel-1.5.10-6.el8.x86_64                                                              9/11 
  Installing  : krb5-devel-1.16.1-22.el8.x86_64                                                                     10/11 
  Installing  : openssl-devel-1:1.1.1-8.el8.x86_64                                                                  11/11 
  运行脚本    : openssl-devel-1:1.1.1-8.el8.x86_64                                                                  11/11 
  验证        : keyutils-libs-devel-1.5.10-6.el8.x86_64                                                              1/11 
  验证        : krb5-devel-1.16.1-22.el8.x86_64                                                                      2/11 
  验证        : libcom_err-devel-1.44.3-2.el8.x86_64                                                                 3/11 
  验证        : libkadm5-1.16.1-22.el8.x86_64                                                                        4/11 
  验证        : libselinux-devel-2.8-6.el8.x86_64                                                                    5/11 
  验证        : libsepol-devel-2.8-2.el8.x86_64                                                                      6/11 
  验证        : libverto-devel-0.3.0-5.el8.x86_64                                                                    7/11 
  验证        : openssl-devel-1:1.1.1-8.el8.x86_64                                                                   8/11 
  验证        : pcre2-devel-10.32-1.el8.x86_64                                                                       9/11 
  验证        : pcre2-utf32-10.32-1.el8.x86_64                                                                      10/11 
  验证        : zlib-devel-1.2.11-10.el8.x86_64                                                                     11/11 

已安装:
  openssl-devel-1:1.1.1-8.el8.x86_64    zlib-devel-1.2.11-10.el8.x86_64         keyutils-libs-devel-1.5.10-6.el8.x86_64   
  krb5-devel-1.16.1-22.el8.x86_64       libcom_err-devel-1.44.3-2.el8.x86_64    libkadm5-1.16.1-22.el8.x86_64             
  libselinux-devel-2.8-6.el8.x86_64     libsepol-devel-2.8-2.el8.x86_64         libverto-devel-0.3.0-5.el8.x86_64         
  pcre2-devel-10.32-1.el8.x86_64        pcre2-utf32-10.32-1.el8.x86_64         

完毕！
[root@localhost pcre2-10.34]# 

```



```powershell
[root@localhost src]# ls
debug  kernels  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz
[root@localhost src]# tar -xvf nginx-1.17.7.tar.gz 
[root@localhost src]# ls
debug  kernels  nginx-1.17.7  nginx-1.17.7.tar.gz  pcre2-10.34  pcre2-10.34.tar.gz
[root@localhost src]# cd nginx-1.17.7/
[root@localhost nginx-1.17.7]# ls
auto  CHANGES  CHANGES.ru  conf  configure  contrib  html  LICENSE  man  README  src
[root@localhost nginx-1.17.7]# ./configure


checking for OS
 + Linux 4.18.0-80.11.2.el8_0.x86_64 x86_64
checking for C compiler ... found
 + using GNU C compiler
 + gcc version: 8.2.1 20180905 (Red Hat 8.2.1-3) (GCC) 
checking for gcc -pipe switch ... found
checking for -Wl,-E switch ... found
checking for gcc builtin atomic operations ... found
checking for C99 variadic macros ... found
checking for gcc variadic macros ... found
checking for gcc builtin 64 bit byteswap ... found
checking for unistd.h ... found
checking for inttypes.h ... found
checking for limits.h ... found
checking for sys/filio.h ... not found
checking for sys/param.h ... found
checking for sys/mount.h ... found
checking for sys/statvfs.h ... found
checking for crypt.h ... found
checking for Linux specific features
checking for epoll ... found
checking for EPOLLRDHUP ... found
checking for EPOLLEXCLUSIVE ... found
checking for O_PATH ... found
checking for sendfile() ... found
checking for sendfile64() ... found
checking for sys/prctl.h ... found
checking for prctl(PR_SET_DUMPABLE) ... found
checking for prctl(PR_SET_KEEPCAPS) ... found
checking for capabilities ... found
checking for crypt_r() ... found
checking for sys/vfs.h ... found
checking for nobody group ... found
checking for poll() ... found
checking for /dev/poll ... not found
checking for kqueue ... not found
checking for crypt() ... not found
checking for crypt() in libcrypt ... found
checking for F_READAHEAD ... not found
checking for posix_fadvise() ... found
checking for O_DIRECT ... found
checking for F_NOCACHE ... not found
checking for directio() ... not found
checking for statfs() ... found
checking for statvfs() ... found
checking for dlopen() ... not found
checking for dlopen() in libdl ... found
checking for sched_yield() ... found
checking for sched_setaffinity() ... found
checking for SO_SETFIB ... not found
checking for SO_REUSEPORT ... found
checking for SO_ACCEPTFILTER ... not found
checking for SO_BINDANY ... not found
checking for IP_TRANSPARENT ... found
checking for IP_BINDANY ... not found
checking for IP_BIND_ADDRESS_NO_PORT ... found
checking for IP_RECVDSTADDR ... not found
checking for IP_SENDSRCADDR ... not found
checking for IP_PKTINFO ... found
checking for IPV6_RECVPKTINFO ... found
checking for TCP_DEFER_ACCEPT ... found
checking for TCP_KEEPIDLE ... found
checking for TCP_FASTOPEN ... found
checking for TCP_INFO ... found
checking for accept4() ... found
checking for eventfd() ... found
checking for int size ... 4 bytes
checking for long size ... 8 bytes
checking for long long size ... 8 bytes
checking for void * size ... 8 bytes
checking for uint32_t ... found
checking for uint64_t ... found
checking for sig_atomic_t ... found
checking for sig_atomic_t size ... 4 bytes
checking for socklen_t ... found
checking for in_addr_t ... found
checking for in_port_t ... found
checking for rlim_t ... found
checking for uintptr_t ... uintptr_t found
checking for system byte ordering ... little endian
checking for size_t size ... 8 bytes
checking for off_t size ... 8 bytes
checking for time_t size ... 8 bytes
checking for AF_INET6 ... found
checking for setproctitle() ... not found
checking for pread() ... found
checking for pwrite() ... found
checking for pwritev() ... found
checking for sys_nerr ... found
checking for localtime_r() ... found
checking for clock_gettime(CLOCK_MONOTONIC) ... found
checking for posix_memalign() ... found
checking for memalign() ... found
checking for mmap(MAP_ANON|MAP_SHARED) ... found
checking for mmap("/dev/zero", MAP_SHARED) ... found
checking for System V shared memory ... found
checking for POSIX semaphores ... not found
checking for POSIX semaphores in libpthread ... found
checking for struct msghdr.msg_control ... found
checking for ioctl(FIONBIO) ... found
checking for ioctl(FIONREAD) ... found
checking for struct tm.tm_gmtoff ... found
checking for struct dirent.d_namlen ... not found
checking for struct dirent.d_type ... found
checking for sysconf(_SC_NPROCESSORS_ONLN) ... found
checking for sysconf(_SC_LEVEL1_DCACHE_LINESIZE) ... found
checking for openat(), fstatat() ... found
checking for getaddrinfo() ... found
checking for PCRE library ... found
checking for PCRE JIT support ... found
checking for zlib library ... found
creating objs/Makefile

Configuration summary
  + using system PCRE library
  + OpenSSL library is not used
  + using system zlib library

  nginx path prefix: "/usr/local/nginx"
  nginx binary file: "/usr/local/nginx/sbin/nginx"
  nginx modules path: "/usr/local/nginx/modules"
  nginx configuration prefix: "/usr/local/nginx/conf"
  nginx configuration file: "/usr/local/nginx/conf/nginx.conf"
  nginx pid file: "/usr/local/nginx/logs/nginx.pid"
  nginx error log file: "/usr/local/nginx/logs/error.log"
  nginx http access log file: "/usr/local/nginx/logs/access.log"
  nginx http client request body temporary files: "client_body_temp"
  nginx http proxy temporary files: "proxy_temp"
  nginx http fastcgi temporary files: "fastcgi_temp"
  nginx http uwsgi temporary files: "uwsgi_temp"
  nginx http scgi temporary files: "scgi_temp"

[root@localhost nginx-1.17.7]# 



```





### yum 安装 nginx

```powershell
yum install vim
sudo yum install epel-release
yum update
yum install nginx -y
rpm -ql nginx
cd /etc/nginx/
ll
systemctl enable nginx
systemctl start nginx
vim nginx.conf



```

#### 查看 `Nginx` 的安装信息

```powershell
[root@localhost nginx]# rpm -ql nginx
/etc/logrotate.d/nginx
/etc/nginx/fastcgi.conf
/etc/nginx/fastcgi.conf.default
/etc/nginx/fastcgi_params
/etc/nginx/fastcgi_params.default
/etc/nginx/koi-utf
/etc/nginx/koi-win
/etc/nginx/mime.types
/etc/nginx/mime.types.default
/etc/nginx/nginx.conf
/etc/nginx/nginx.conf.default
/etc/nginx/scgi_params
/etc/nginx/scgi_params.default
/etc/nginx/uwsgi_params
/etc/nginx/uwsgi_params.default
/etc/nginx/win-utf
/usr/bin/nginx-upgrade
/usr/lib/systemd/system/nginx.service
/usr/lib64/nginx/modules
/usr/sbin/nginx
/usr/share/doc/nginx-1.20.1
/usr/share/doc/nginx-1.20.1/CHANGES
/usr/share/doc/nginx-1.20.1/README
/usr/share/doc/nginx-1.20.1/README.dynamic
/usr/share/doc/nginx-1.20.1/UPGRADE-NOTES-1.6-to-1.10
/usr/share/licenses/nginx-1.20.1
/usr/share/licenses/nginx-1.20.1/LICENSE
/usr/share/man/man3/nginx.3pm.gz
/usr/share/man/man8/nginx-upgrade.8.gz
/usr/share/man/man8/nginx.8.gz
/usr/share/nginx/html/404.html
/usr/share/nginx/html/50x.html
/usr/share/nginx/html/en-US
/usr/share/nginx/html/icons
/usr/share/nginx/html/icons/poweredby.png
/usr/share/nginx/html/img
/usr/share/nginx/html/index.html
/usr/share/nginx/html/nginx-logo.png
/usr/share/nginx/html/poweredby.png
/usr/share/nginx/modules
/usr/share/vim/vimfiles/ftdetect/nginx.vim
/usr/share/vim/vimfiles/ftplugin/nginx.vim
/usr/share/vim/vimfiles/indent/nginx.vim
/usr/share/vim/vimfiles/syntax/nginx.vim
/var/lib/nginx
/var/lib/nginx/tmp
/var/log/nginx
/var/log/nginx/access.log
/var/log/nginx/error.log
```

```powershell
# Nginx配置文件
/etc/nginx/nginx.conf # nginx 主配置文件
/etc/nginx/nginx.conf.default

# 可执行程序文件
/usr/bin/nginx-upgrade
/usr/sbin/nginx

# nginx库文件
/usr/lib/systemd/system/nginx.service # 用于配置系统守护进程
/usr/lib64/nginx/modules # Nginx模块目录

# 帮助文档
/usr/share/doc/nginx-1.16.1
/usr/share/doc/nginx-1.16.1/CHANGES
/usr/share/doc/nginx-1.16.1/README
/usr/share/doc/nginx-1.16.1/README.dynamic
/usr/share/doc/nginx-1.16.1/UPGRADE-NOTES-1.6-to-1.10

# 静态资源目录
/usr/share/nginx/html/404.html
/usr/share/nginx/html/50x.html
/usr/share/nginx/html/index.html

# 存放Nginx日志文件
/var/log/nginx
```

主要关注的文件夹有两个：

1. `/etc/nginx/conf.d/` 是子配置项存放处， `/etc/nginx/nginx.conf` 主配置文件会默认把这个文件夹中所有子配置项都引入；
2. `/usr/share/nginx/html/` 静态文件都放在这个文件夹，也可以根据你自己的习惯放在其他地方；

#### Nginx 常用命令

```powershell
# 开机配置
systemctl enable nginx # 开机自动启动
systemctl disable nginx # 关闭开机自动启动

# 启动Nginx
systemctl start nginx # 启动Nginx成功后，可以直接访问主机IP，此时会展示Nginx默认页面

# 停止Nginx
systemctl stop nginx

# 重启Nginx
systemctl restart nginx

# 重新加载Nginx
systemctl reload nginx

# 查看 Nginx 运行状态
systemctl status nginx

# 查看Nginx进程
ps -ef | grep nginx

# 杀死Nginx进程
kill -9 pid # 根据上面查看到的Nginx进程号，杀死Nginx进程，-9 表示强制结束进程
```

#### `Nginx` 应用程序命令：

```powershell
nginx -s reload  # 向主进程发送信号，重新加载配置文件，热重启
nginx -s reopen	 # 重启 Nginx
nginx -s stop    # 快速关闭
nginx -s quit    # 等待工作进程处理完成后关闭
nginx -T         # 查看当前 Nginx 最终的配置
nginx -t         # 检查配置是否有问题
```





#### 配置文件结构

```powershell
# main段配置信息
user  nginx;                        # 运行用户，默认即是nginx，可以不进行设置
worker_processes  auto;             # Nginx 进程数，一般设置为和 CPU 核数一样
error_log  /var/log/nginx/error.log warn;   # Nginx 的错误日志存放目录
pid        /var/run/nginx.pid;      # Nginx 服务启动时的 pid 存放位置

# events段配置信息
events {
    use epoll;     # 使用epoll的I/O模型(如果你不知道Nginx该使用哪种轮询方法，会自动选择一个最适合你操作系统的)
    worker_connections 1024;   # 每个进程允许最大并发数
}

# http段配置信息
# 配置使用最频繁的部分，代理、缓存、日志定义等绝大多数功能和第三方模块的配置都在这里设置
http { 
    # 设置日志模式
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;   # Nginx访问日志存放位置

    sendfile            on;   # 开启高效传输模式
    tcp_nopush          on;   # 减少网络报文段的数量
    tcp_nodelay         on;
    keepalive_timeout   65;   # 保持连接的时间，也叫超时时间，单位秒
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;      # 文件扩展名与类型映射表
    default_type        application/octet-stream;   # 默认文件类型

    include /etc/nginx/conf.d/*.conf;   # 加载子配置项
    
    # server段配置信息
    server {
    	listen       80;       # 配置监听的端口
    	server_name  localhost;    # 配置的域名
      
    	# location段配置信息
    	location / {
    		root   /usr/share/nginx/html;  # 网站根目录
    		index  index.html index.htm;   # 默认首页文件
    		deny 172.168.22.11;   # 禁止访问的ip地址，可以为all
    		allow 172.168.33.44；# 允许访问的ip地址，可以为all
    	}
    	
    	error_page 500 502 503 504 /50x.html;  # 默认50x对应的访问页面
    	error_page 400 404 error.html;   # 同上
    }
}



```

- `main` 全局配置，对全局生效；
- `events` 配置影响 `Nginx` 服务器与用户的网络连接；
- `http` 配置代理，缓存，日志定义等绝大多数功能和第三方模块的配置；
- `server` 配置虚拟主机的相关参数，一个 `http` 块中可以有多个 `server` 块；
- `location` 用于配置匹配的 `uri` ；
- `upstream` 配置后端服务器具体地址，负载均衡配置不可或缺的部分；





用一张图清晰的展示它的层级结构：
![未命名文件 (images/87fffe0360aa4f34adb6258a955aad38_tplv-k3u1fbpfcp-zoom-in-crop-mark_3024_0_0_0-1660617619525.awebp).png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/87fffe0360aa4f34adb6258a955aad38~tplv-k3u1fbpfcp-zoom-in-crop-mark:3024:0:0:0.awebp)







### Nginx 启动报 [emerg] bind() to 0.0.0.0:XXXX failed (13: Permission denied)错误处理



系统启动Nginx后，报 [emerg] bind() to 0.0.0.0:XXXX failed (13: Permission denied)错误的处理方式，分为两种：

第一种：端口小于1024的情况：

[emerg] bind() to 0.0.0.0:80 failed (13: Permission denied)
原因是1024以下端口启动时需要root权限，所以sudo nginx即可。

第二种：端口大于1024的情况：

[emerg] bind() to 0.0.0.0:8380 failed (13: Permission denied)
这种情况，需要如下操作：

首先，查看http允许访问的端口：

semanage port -l | grep http_port_t
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
其次，将要启动的端口加入到如上端口列表中

```powershell
semanage port -a -t http_port_t  -p tcp 8090

```



如此即可解决如上问题。

cenos7安装semanage命令参考：https://blog.csdn.net/RunSnail2018/article/details/81185653

原文链接：https://blog.csdn.net/RunSnail2018/article/details/81185138



```powershell
在ContOS 7上安装了Nginx服务，为了项目需要必须修改Nginx的默认80端口为8088,修改配置文件后重启Nginx服务，查看日志报以下错误：

[emerg] 9011#0: bind() to 0.0.0.0:8088 failed (13: Permission denied)

权限被拒绝，开始以为是端口被别的程序占用了，查看活动端口然而没有程序使用此端口，网上搜索说是需要root权限，可我执行的是root用户啊，这就挺郁闷的,后来还是给力的google给了答案，是因为selinux默认只允许80,81,443,8008,8009,8443,9000用作HTTP端口使用

要查看selinux允许的http端口必须使用semanage命令，下面首先安装semanage命令工具

在安装semanage工具之前，我们先安装一个tab键补齐二级命令功能工具bash-completion：

yum -y install bash-completion

直接通过yum安装发现semanage发现没有此包：

# yum install semange

...

NO package semanage available.

那先查找semanage命令是哪个软件包提供此命令

# yum provides semanage

或者使用下面的命令：

# yum whatprovides /usr/sbin/semanage

我们发现需要安装包policycoreutils-Python才能使用semanage命令

现在我们通过yum安装此软件包，可以使用tab补齐：

# yum install policycoreutils-python.x86_64

现在终于可以使用semanage了，我们先查看下http允许访问的端口：

# semanage port -l | grep http_port_t

http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000

然后我们将需要使用的端口8088加入到端口列表中：

# semanage port -a -t http_port_t -p tcp 8088

# semanage port -l | grep http_port_t

http_port_t                    tcp      8088, 80, 81, 443, 488, 8008, 8009, 8443, 9000

好了现在nginx可以使用8088端口了

selinux的日志在/var/log/audit/audit.log

但此文件记录的信息不够明显，很难看出来，我们可以借助audit2why和audit2allow工具查看，这两个工具也是policycoreutils-python软件包提供的。

# audit2why < /var/log/audit/audit.log

收集selinux工具的日志，还有另外一个工具setroubleshoot，对应的软件包为setroubleshoot-server
```





```powershell
curl http://192.168.1.105:8040/testnginx
curl http://192.168.40.150:8088/testnginx
```

