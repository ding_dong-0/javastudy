## CentOS 8界面设置

CentOS 8界面上什么图标都没了，应用程序也都藏起来了，开启的程序也不显示在界面最下端，简直反人类，原先CentOS 7.x好好的。

找到了个办法恢复成CentOS 7.x那样，即运行dnf install gnome-tweaks。然后在Activities里找到Tweaks程序，把Desktop icons、Applications menu、Window list都选成ON，就变成CentOS 7.x的模样了

```shell
#安装 gnome-tweaks
dnf install gnome-tweaks
#从命令行直接启动图形桌面环境（或者重启电脑）
startx

```

![image-20191229154754506](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20191229154754506.png)





### centos系统查看软件安装路径


使用rpm命令
查看软件是否安装。首先我们需要查看软件是否已经安装，或者说查看安装的软件包名称。如查找是否安装jenkins

①列出所有安装的Jenkins

rpm -qa | grep jenkins
1
②软件是否安装；例如：jenkins是否安装

rpm -q | grep jenkins
1
③rpm -ql 列出软件包安装的文件

rpm -ql jenkins
1
④可以直接使用 rpm -qal |grep mysql 查看mysql所有安装包的文件存储位置

 rpm -qal |grep jenkins #查看jenkins所有安装包的文件存储位置



```powershell
[root@localhost local]# whereis nginx
nginx:
[root@localhost local]# rpm -qa | grep nginx
[root@localhost local]# rpm -ql nginx
未安装软件包 nginx 

#安装之后显示
[root@localhost ~]# whereis nginx
nginx: /usr/local/nginx
```



### 远程连接服务器软件介绍

远程连接服务器的连接软件

Xshell

可用于上传文件；

Xftp

```shell
#查询ip
ifconfig
[root@localhost ~]# ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.40.128  netmask 255.255.255.0  broadcast 192.168.40.255
        inet6 fe80::48d9:b814:b7a1:48cf  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:37:ea:2f  txqueuelen 1000  (Ethernet)
        RX packets 1154  bytes 85166 (83.1 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1180  bytes 270495 (264.1 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 948  bytes 118612 (115.8 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 948  bytes 118612 (115.8 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:41:5a:8e  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

```shell
#添加用户小明
useradd xiaoming
#并设置密码
passwd xiaoming

#添加用户xiaohong并指定家目录dog
useradd -d /home/dog xiaohong
#并设置密码
passwd xiaohong 

#显示文件的绝对路径
pwd

#查看sshd的状态
service sshd status
#查看22号端口的状态
netstat -anp|more
#查看所有打开的端口
firewall-cmd --list-ports                    
#重启ssh服务
service sshd restart
```

### 复制文件至其他系统

```powershell
[root@localhost soft]# scp Mycat-server-1.6.7.1-release-20190627191042-linux.tar.gz root@192.168.40.100:/opt/module/
Mycat-server-1.6.7.1-release-20190627191042-linux.tar.gz                                            100%   17MB  36.7MB/s   00:00    
[root@localhost soft]# 
```

https://www.runoob.com/linux/linux-comm-scp.html

# 安装JDK的步骤

```powershell
#安装JDK的步骤

#将下载的jdk-13.0.1_linux-x64_bin.tar.gz放在 #/opt 文件夹中
cd /opt
tar -zxvf jdk-13.0.1_linux-x64_bin.tar.gz
#环境变量在/etc/profile文件中配置
vim /etc/profile
#配置jdk环境变量：（在文件profile末尾添加下列内容）
JAVA_HOME=/opt/jdk-13.0.1
PATH=/opt/jdk-13.0.1/bin:$PATH
export JAVA_HOME PATH

JAVA_HOME=/opt/module/jdk1.8.0_144
PATH=/opt/module/jdk1.8.0_144/bin:$PATH
export JAVA_HOME PATH

#esc退出保存文件
:wq
#重启生效
#使环境变量立即生效
[root@localhost shell]# source /etc/profile
```

# 安装tomcat

```powershell
#安装tomcat
#将下载的apache-tomcat-9.0.30.tar.gz放在 /opt 文件夹中
cd /opt
解压
tar -zxvf apache-tomcat-9.0.30.tar.gz
cd apache-tomcat-9.0.30/bin
pwd
/opt/apache-tomcat-9.0.30/bin
./startup.sh 
#tomcat日志文件位置
[root@localhost logs]# pwd
/opt/apache-tomcat-9.0.30/logs
[root@localhost logs]# ls
catalina.2019-12-29.log  host-manager.2019-12-29.log  localhost.2019-12-31.log             manager.2019-12-29.log
catalina.2019-12-31.log  host-manager.2019-12-31.log  localhost_access_log.2019-12-29.txt  manager.2019-12-31.log
catalina.out             localhost.2019-12-29.log     localhost_access_log.2019-12-31.txt
#查看tomcat启动状态
[root@localhost logs]# tail -f catalina.out
31-Dec-2019 13:46:36.369 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/opt/apache-tomcat-9.0.30/webapps/docs] has finished in [54] ms
31-Dec-2019 13:46:36.372 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory 把web 应用程序部署到目录 [/opt/apache-tomcat-9.0.30/webapps/examples]
31-Dec-2019 13:46:37.438 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/opt/apache-tomcat-9.0.30/webapps/examples] has finished in [1,065] ms
31-Dec-2019 13:46:37.438 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory 把web 应用程序部署到目录 [/opt/apache-tomcat-9.0.30/webapps/host-manager]
31-Dec-2019 13:46:37.537 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/opt/apache-tomcat-9.0.30/webapps/host-manager] has finished in [99] ms
31-Dec-2019 13:46:37.538 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory 把web 应用程序部署到目录 [/opt/apache-tomcat-9.0.30/webapps/manager]
31-Dec-2019 13:46:37.588 信息 [main] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/opt/apache-tomcat-9.0.30/webapps/manager] has finished in [50] ms
31-Dec-2019 13:46:37.594 信息 [main] org.apache.coyote.AbstractProtocol.start 开始协议处理句柄["http-nio-8080"]
31-Dec-2019 13:46:37.636 信息 [main] org.apache.coyote.AbstractProtocol.start 开始协议处理句柄["ajp-nio-8009"]
31-Dec-2019 13:46:37.644 信息 [main] org.apache.catalina.startup.Catalina.start Server startup in [2,341] milliseconds
#启动tomcat并打印日志
cd /opt/apache-tomcat-9.0.30/bin
./startup.sh &tail -f /opt/apache-tomcat-9.0.30/logs/catalina.out



本地登录：
http://localhost:8080/

#开放8080端口外部才能访问tomcat
systemctl stop firewalld.service                #停止防火墙 
systemctl start firewalld.service               #启动防火墙  
firewall-cmd --query-port=8080/tcp              # 查询端口是否开放
firewall-cmd --permanent --add-port=8080/tcp    # 开放8080端口
#输入命令重启防火墙；
systemctl restart firewalld.service
#输入命令重新载入配置
firewall-cmd --reload

（刚开始学的时候，添加完8080端口没有生效，重启了下系统，才能生效了，可以访问了）

```

```powershell
#JDK安装详细
[root@localhost ~]# cd /opt
[root@localhost opt]# ls
apache-tomcat-9.0.30.tar.gz  ideaIU-2019.3.tar.gz  jdk-13.0.1_linux-x64_bin.tar.gz
[root@localhost opt]# tar -zxvf jdk-13.0.1_linux-x64_bin.tar.gz 
jdk-13.0.1/bin/jaotc
jdk-13.0.1/bin/jar
jdk-13.0.1/bin/jarsigner
jdk-13.0.1/bin/java
...

[root@localhost opt]# ls
apache-tomcat-9.0.30.tar.gz  ideaIU-2019.3.tar.gz  jdk-13.0.1  jdk-13.0.1_linux-x64_bin.tar.gz
[root@localhost opt]# cd jdk-13.0.1/
[root@localhost jdk-13.0.1]# cd bin/
[root@localhost bin]# pwd
/opt/jdk-13.0.1/bin
#未配置环境变量，进入到jdk安装目录的bin目录中
[root@localhost bin]# ./java
用法：java [options] <主类> [args...]
           （执行类）
   或  java [options] -jar <jar 文件> [args...]
           （执行 jar 文件）
...
#证明安装成功



#编辑环境变量文件
[root@localhost ~]# vim /etc/profile

# /usr/share/doc/setup-*/uidgid file
if [ $UID -gt 199 ] && [ "`id -gn`" = "`id -un`" ]; then
    umask 002
else
    umask 022
fi

for i in /etc/profile.d/*.sh /etc/profile.d/sh.local ; do
    if [ -r "$i" ]; then
        if [ "${-#*i}" != "$-" ]; then
            . "$i"
        else
            . "$i" >/dev/null
        fi
    fi
done

unset i
unset -f pathmunge

if [ -n "${BASH_VERSION-}" ] ; then
        if [ -f /etc/bashrc ] ; then
                # Bash login shells run only /etc/profile
                # Bash non-login shells run only /etc/bashrc
                # Check for double sourcing is done in /etc/bashrc.
                . /etc/bashrc
       fi
fi


下面为添加的内容
JAVA_HOME=/opt/jdk-13.0.1
PATH=/opt/jdk-13.0.1/bin:$PATH
export JAVA_HOME PATH

:wq


注意需要注销下才能生效
#使环境变量立即生效
[root@localhost shell]# source /etc/profile
```



Maven安装

```powershell
#上传 rapache-maven-3.6.3-bin.tar.gz至/opt目录
[root@server1 ~]# cd /opt
#解压
[root@server1 opt]# tar -zxvf apache-maven-3.6.3-bin.tar.gz 
...
[root@server1 ~]# vim /etc/profile
#Adding to PATH
export PATH=/opt/apache-maven-3.6.3/bin:$PATH
#重启系统，如下则安装成功
#使环境变量立即生效
[root@localhost shell]# source /etc/profile
[root@server1 ~]# mvn -v
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /opt/apache-maven-3.6.3
Java version: 13.0.1, vendor: Oracle Corporation, runtime: /opt/jdk-13.0.1
Default locale: zh_CN, platform encoding: UTF-8
OS name: "linux", version: "4.18.0-80.11.2.el8_0.x86_64", arch: "amd64", family: "unix"

```



# mysql安装

[mysql-boost-8.0.18.tar.gz](https://dev.mysql.com/downloads/file/?id=490337)

检查是否安装过mysql，并删除

```shell
#查询是否安装mysql
rpm -qa|grep -i mysql
#普通删除
rpm -e mysql
#强制删除
rpm -e --nodeps mysql
[root@localhost ~]# rpm -qa | grep -i mysql
mysql57-community-release-el7-9.noarch
[root@localhost ~]# rpm -e mysql57-community-release 
[root@localhost ~]# 
```



```shell
yum -y install make gcc-c++ cmake bison-devel ncurses-devel
cd /opt/
tar -zxvf mysql-boost-8.0.18.tar.gz 
cd mysql-8.0.18
```

```shell
#下载yum仓库（29kb好像）
wget https://dev.mysql.com/get/mysql80-community-release-el8-1.noarch.rpm

rpm -ivh mysql80-community-release-el8-1.noarch.rpm 
ls /etc/yum.repos.d/
yum makecache
yum repolist
md5sum mysql80-community-release-el8-1.noarch.rpm 
yum list |grep mysql-com

yum -y install 
```



### mysql8.0.18数据库的安装（已成功）

官方的下载链接：

[mysql-8.0.18-1.el8.x86_64.rpm-bundle.tar.tar](https://cdn.mysql.com//Downloads/MySQL-8.0/mysql-8.0.18-1.el8.x86_64.rpm-bundle.tar)



[参考了CentOS7安装MySQL教程（离线安装）](https://blog.csdn.net/csdn_000000000000001/article/details/90412651)

```powershell
#将下载下来的mysql-8.0.18-1.el8.x86_64.rpm-bundle.tar.tar安装包放在 /opt文件夹下
[root@localhost ~]# cd /opt
[root@localhost opt]# ls
apache-tomcat-9.0.30         ideaIU-2019.3.tar.gz             mysql-8.0.18-1.el8.x86_64.rpm-bundle.tar.tar
apache-tomcat-9.0.30.tar.gz  jdk-13.0.1                       redis-5.0.7
ideaIU-2019.3                jdk-13.0.1_linux-x64_bin.tar.gz  redis-5.0.7.tar.gz
#解压
[root@localhost opt]# tar -xvf mysql-8.0.18-1.el8.x86_64.rpm-bundle.tar.tar 
mysql-community-common-8.0.18-1.el8.x86_64.rpm
mysql-community-debugsource-8.0.18-1.el8.x86_64.rpm
mysql-community-debuginfo-8.0.18-1.el8.x86_64.rpm
mysql-community-client-debuginfo-8.0.18-1.el8.x86_64.rpm
mysql-community-test-8.0.18-1.el8.x86_64.rpm
mysql-community-server-debug-8.0.18-1.el8.x86_64.rpm
mysql-community-server-8.0.18-1.el8.x86_64.rpm
mysql-community-client-8.0.18-1.el8.x86_64.rpm
mysql-community-libs-8.0.18-1.el8.x86_64.rpm
mysql-community-server-debug-debuginfo-8.0.18-1.el8.x86_64.rpm
mysql-community-libs-debuginfo-8.0.18-1.el8.x86_64.rpm
mysql-community-test-debuginfo-8.0.18-1.el8.x86_64.rpm
mysql-community-server-debuginfo-8.0.18-1.el8.x86_64.rpm
mysql-community-devel-8.0.18-1.el8.x86_64.rpm
#显示当前目录
[root@localhost opt]# pwd
/opt
#彻底卸载MySQL(如果以前没有安装过，则跳过此步骤)
[root@localhost opt]#  yum remove  mysql  mysql-server  mysql-libs  mysql-server
未找到匹配的参数： mysql
未找到匹配的参数： mysql-server
未找到匹配的参数： mysql-libs
未找到匹配的参数： mysql-server
没有软件包需要移除。
依赖关系解决。
无需任何处理。
完毕
#说明之前没有安装过mysql

#查看系统中是否以rpm包安装的mysql
[root@localhost opt]# rpm -qa | grep -i mysql
mysql80-community-release-el8-1.noarch
#如果有执行卸载mysql（使用强制卸载）
[root@localhost opt]# rpm -e --nodeps mysql80-community-release-el8-1.noarch
[root@localhost opt]# rpm -qa | grep -i mysql
[root@localhost opt]# whereis mysql 
mysql:
#清空相关mysql的所有目录以及文件
[root@localhost opt]# rm -rf /usr/share/mysql
[root@localhost opt]# rm -rf /usr/lib/mysql
[root@localhost opt]# rm -rf /usr/share/mysql
[root@localhost opt]# rm -rf /usr/my.cnf
#按顺序安装
#必要安装（注意顺序）
#安装common
#安装lib 依赖于common，确保已经卸载mariadb
#依赖于libs
#依赖于client、common


###第一步安装common
[root@localhost opt]# rpm -ivh mysql-community-common-8.0.18-1.el8.x86_64.rpm

警告：mysql-community-common-8.0.18-1.el8.x86_64.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
Verifying...                          ################################# [100%]
准备中...                          ################################# [100%]
正在升级/安装...
   1:mysql-community-common-8.0.18-1.e################################# [100%]
   
#第二步安装lib 依赖于common，确保已经卸载mariadb() 
[root@localhost opt]# rpm -ivh mysql-community-libs-8.0.18-1.el8.x86_64.rpm
警告：mysql-community-libs-8.0.18-1.el8.x86_64.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
Verifying...                          ################################# [100%]
准备中...                          ################################# [100%]
正在升级/安装...
   1:mysql-community-libs-8.0.18-1.el8################################# [100%]
   
#第三步安装client，依赖于libs
[root@localhost opt]# rpm -ivh mysql-community-client-8.0.18-1.el8.x86_64.rpm
警告：mysql-community-client-8.0.18-1.el8.x86_64.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
Verifying...                          ################################# [100%]
准备中...                          ################################# [100%]
正在升级/安装...
   1:mysql-community-client-8.0.18-1.e################################# [100%]
   
#第四步安装server,依赖于client、common
[root@localhost opt]# rpm -ivh mysql-community-server-8.0.18-1.el8.x86_64.rpm
警告：mysql-community-server-8.0.18-1.el8.x86_64.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
Verifying...                          ################################# [100%]
准备中...                          ################################# [100%]
正在升级/安装...
   1:mysql-community-server-8.0.18-1.e################################# [100%]
[/usr/lib/tmpfiles.d/libgpod.conf:1] Line references path below legacy directory /var/run/, updating /var/run/libgpod → /run/libgpod; please update the tmpfiles.d/ drop-in file accordingly.
[/usr/lib/tmpfiles.d/libstoragemgmt.conf:1] Line references path below legacy directory /var/run/, updating /var/run/lsm → /run/lsm; please update the tmpfiles.d/ drop-in file accordingly.
[/usr/lib/tmpfiles.d/libstoragemgmt.conf:2] Line references path below legacy directory /var/run/, updating /var/run/lsm/ipc → /run/lsm/ipc; please update the tmpfiles.d/ drop-in file accordingly.
[/usr/lib/tmpfiles.d/mdadm.conf:1] Line references path below legacy directory /var/run/, updating /var/run/mdadm → /run/mdadm; please update the tmpfiles.d/ drop-in file accordingly.
[/usr/lib/tmpfiles.d/mysql.conf:23] Line references path below legacy directory /var/run/, updating /var/run/mysqld → /run/mysqld; please update the tmpfiles.d/ drop-in file accordingly.
[/usr/lib/tmpfiles.d/pesign.conf:1] Line references path below legacy directory /var/run/, updating /var/run/pesign → /run/pesign; please update the tmpfiles.d/ drop-in file accordingly.
[/usr/lib/tmpfiles.d/spice-vdagentd.conf:2] Line references path below legacy directory /var/run/, updating /var/run/spice-vdagentd → /run/spice-vdagentd; please update the tmpfiles.d/ drop-in file accordingly.
#初始化数据库
[root@localhost opt]# mysqld --initialize --console
#目录授权
#mysql8自动创建mysql组和用户
[root@localhost opt]# chown -R mysql:mysql /var/lib/mysql/
#启动服务
[root@localhost opt]# systemctl start mysqld

[root@localhost opt]# mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root: 
Error: Access denied for user 'root'@'localhost' (using password: YES)
[root@localhost opt]# mysql_secure_installation

[root@localhost opt]# mysql -uroot -p123456
Enter password: 
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)




```

### 重置mysql登录密码

```powershell
重置mysql登录密码
#密码错误不知道，直接重置密码
# 停mysql服务
[root@localhost opt]# service mysqld stop
Redirecting to /bin/systemctl stop mysqld.service
#修改配置文件免密码登录：vi /etc/my.cnf
#在[mysqld]最后加上如下语句(skip-grant-tables)，并保存退出
[root@localhost opt]# vi /etc/my.cnf
#启动mysql服务
[root@localhost opt]# service mysqld start
Redirecting to /bin/systemctl start mysqld.service
#免密登录mysql，直接命令行输入：
[root@localhost opt]# mysql -u root -p
#直接按Enter键进入
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 7
Server version: 8.0.18 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use mysql;
Database changed

mysql> select host,user,authentication_string, plugin from user;
+-----------+------------------+------------------------------------------------------------------------+------------------+
| host      | user             | authentication_string                                                  | plugin           |
+-----------+------------------+------------------------------------------------------------------------+------------------+
| localhost | mysql.infoschema | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | caching_shssword |
| localhost | mysql.session    | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | caching_shssword |
| localhost | mysql.sys        | $A$005$THISISACOMBINATIONOFINVALIDSALTANDPASSWORDTHATMUSTNEVERBRBEUSED | caching_shssword |
{ localh.ad6jSHyKHKH1p1qVF6Iz5fElnrHfhkV.iIwksijT.9 | caching_sha2_password |
+-----------+------------------+------------------------------------------------------------------------+------------------+




4 rows in set (0.00 sec)
#如果当前root用户authentication_string字段下有内容，先将其设置为空；
mysql> update user set authentication_string='' where user='root';
Query OK, 1 row affected (0.02 sec)
Rows matched: 1  Changed: 1  Warnings: 0
#退出mysql
mysql>\q;
Bye


[root@localhost opt]# vi /etc/my.cnf
#退出mysql, 删除/etc/my.cnf文件最后的 skip-grant-tables 重启mysql服务；
#停止mysql服务
[root@localhost opt]# service mysqld stop
Redirecting to /bin/systemctl stop mysqld.service
#启动mysql服务
[root@localhost opt]# service mysqld start
Redirecting to /bin/systemctl start mysqld.service 

#重启mysql也可以用下面service mysqld restart
#[root@localhost opt]#  service mysqld restart
#Redirecting to /bin/systemctl restart mysqld.service

[root@localhost opt]# 
[root@localhost opt]# mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.18

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
#设置root用户的密码为123456
mysql> alter user 'root'@'localhost' IDENTIFIED BY '123456';
Query OK, 0 rows affected (0.04 sec)
#退出数据库
mysql>\q
Bye
[root@localhost opt]# mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.18 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```



### 安装MySql中遇到的问题

1、rpm -ivh ***.rpm 安装时，安装不上

  原因：与默认mariaDB冲突。

  解决方法：卸载maridb(通过rpm -qa | grep mariadb查找)

  卸载：rpm -e **** --force --nodeps

```powershell
#查看系统中是否以rpm包安装的mariadb
[root@localhost ~]# rpm -qa | grep mariadb
mariadb-connector-c-config-3.0.7-1.el8.noarch
mariadb-connector-c-3.0.7-1.el8.x86_64
#普通删除
[root@localhost ~]# rpm -e mariadb-connector-c-config-3.0.7-1.el8.noarch
[root@localhost ~]# rpm -e mariadb-connector-c-3.0.7-1.el8.x86_64
#卸载强制删除（rpm -e --nodeps ***）
[root@localhost mysql]# rpm -e --nodeps mariadb-connector-c-config-3.0.7-1.el8.noarch
[root@localhost mysql]# rpm -e --nodeps mariadb-connector-c-3.0.7-1.el8.x86_64


```

```powershell
[root@localhost mysql]# rpm -ivh mysql-community-server-8.0.18-1.el8.x86_64.rpm
警告：mysql-community-server-8.0.18-1.el8.x86_64.rpm: 头V3 DSA/SHA1 Signature, 密钥 ID 5072e1f5: NOKEY
错误：依赖检测失败：
	mariadb-connector-c-config 被 mysql-community-server-8.0.18-1.el8.x86_64 取代
#普通删除无效
[root@localhost mysql]# rpm -e mariadb-connector-c-3.0.7-1.el8.x86_64
错误：依赖检测失败：
	libmariadb.so.3()(64bit) 被 (已安裝) net-snmp-agent-libs-1:5.8-7.el8.x86_64 需要
	libmariadb.so.3()(64bit) 被 (已安裝) perl-DBD-MySQL-4.046-2.module_el8.0.0+72+668237d8.x86_64 需要
	libmariadb.so.3()(64bit) 被 (已安裝) net-snmp-1:5.8-7.el8.x86_64 需要
	libmariadb.so.3(libmysqlclient_18)(64bit) 被 (已安裝) net-snmp-agent-libs-1:5.8-7.el8.x86_64 需要
	libmariadb.so.3(libmysqlclient_18)(64bit) 被 (已安裝) perl-DBD-MySQL-4.046-2.module_el8.0.0+72+668237d8.x86_64 需要
[root@localhost ~]# rpm -qa | grep mariadb
mariadb-connector-c-config-3.0.7-1.el8.noarch
mariadb-connector-c-3.0.7-1.el8.x86_64
#普通删除无效
[root@localhost mysql]# rpm -e  mariadb-connector-c-config-3.0.7-1.el8.noarch
错误：依赖检测失败：
	/etc/my.cnf 被 (已安裝) mariadb-connector-c-3.0.7-1.el8.x86_64 需要
#卸载强制删除
[root@localhost mysql]# rpm -e --nodeps mariadb-connector-c-config-3.0.7-1.el8.noarch
[root@localhost mysql]# rpm -qa | grep mariadb
mariadb-connector-c-3.0.7-1.el8.x86_64
[root@localhost mysql]# rpm -e --nodeps mariadb-connector-c-3.0.7-1.el8.x86_64
[root@localhost mysql]# rpm -qa | grep mariadb
[root@localhost mysql]# 
```

2、缺少依赖包libaio

   直接实用yum包管理工具安装即可：yum install libaio

```powershell
[root@localhost ~]# yum install libaio
```

3、mysql -u root -p登录时报错

安装步骤有详细说明

##### [Linux CentOS7系统中mysql8安装配置](https://www.cnblogs.com/guanqiweb/p/10591921.html)

### maven环境配置

```powershell
Linux：

1、进入 etc目录 cd /etc

2、编辑 profile文件 vi profile

     export MAVEN_HOME=/opt/maven-3.5.0
     export PATH=$PATH:${MAVEN_HOME}/bin

3、查看是否安装成功

     source /etc/profile
     mvn -version
```



# 安装idea

```powershell
[root@localhost opt]# cd /opt
[root@localhost opt]# ls
apache-tomcat-9.0.30         ideaIU-2019.3.tar.gz             mysql
apache-tomcat-9.0.30.tar.gz  jdk-13.0.1                       settings.zip
idea-IU-193.5233.102         jdk-13.0.1_linux-x64_bin.tar.gz 
[root@localhost opt]# tar -zxvf ideaideaIU-2019.3.tar.gz
/opt/idea-IU-193.5233.102
[root@localhost ~]# cd /opt
[root@localhost opt]# ls
apache-tomcat-9.0.30         idea-IU-193.5233.102  jdk-13.0.1                       mysql
apache-tomcat-9.0.30.tar.gz  ideaIU-2019.3.tar.gz  jdk-13.0.1_linux-x64_bin.tar.gz  settings.zip
[root@localhost opt]# cd idea-IU-193.5233.102
[root@localhost idea-IU-193.5233.102]# cd bin
[root@localhost bin]# pwd

#在centos系统内进入
[root@localhost ~]# cd /opt/idea-IU-193.5233.102/bin/
./idea.sh 
#进入图形化界面按照提示安装
```



### [centos 8 中没有iptables 和service iptables save 指令使用失败问题解决方案](https://www.cnblogs.com/AmbitiousMice/p/8486049.html)

centos7中 取消了iptables ，只能使用firewall。

```
1.任意运行一条iptables防火墙规则配置命令：

iptables -P OUTPUT ACCEPT  

2.对iptables服务进行保存：

service iptables save  

如果上述命令执行失败报出：The service command supports only basic LSB actions (start, stop, restart, try-restart, reload, force-reload, status). For other actions, please try to use systemctl.

*解决方法：
systemctl stop firewalld 关闭防火墙
yum install iptables-services 安装或更新服务
再使用systemctl enable iptables 启动iptables
最后 systemctl start iptables 打开iptables*

*再执行service iptables save*

3.重启iptables服务：

service iptables restart

执行完毕之后/etc/syscofig/iptables文件就有了
```

### centos软件源问题

```shell
#会删除文件夹下的所有文件（不建议使用只是刚开始时有过尝试）
rm -rf /etc/yum.repos.d/*
#创建文件夹
mkdir /etc/yum.repos.d

wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo
#或者
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo

#清除缓存
yum clean all
#运行 yum makecache 生成缓存
yum makecache
```

### Centos8 安装软件 Failed to synchronize cache for repo 'AppStream', ignoring this repo.







# CentOS 镜像



[CentOS 镜像新安装的centos设置国内阿里云镜像](https://developer.aliyun.com/mirror/centos?spm=a2c6h.13651102.0.0.53322f701a0POJ)

## 简介

CentOS，是基于 Red Hat Linux 提供的可自由使用源代码的企业级 Linux 发行版本；是一个稳定，可预测，可管理和可复制的免费企业级计算平台。

## 配置方法

### 1. 备份

```powershell
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
```

### 2. 下载新的 CentOS-Base.repo 到 /etc/yum.repos.d/

**CentOS 6**

```powershell
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
```

或者

```powershell
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
```

**CentOS 7**

```powershell
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
```

或者

```powershell
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
```

**CentOS 8**

```powershell
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo
```

或者

```powershell
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo
```

### 3. 运行 yum makecache 生成缓存

### 4. 其他

非阿里云ECS用户会出现 Couldn't resolve host 'mirrors.cloud.aliyuncs.com' 信息，不影响使用。用户也可自行修改相关配置: eg:

```powershell
sed -i -e '/mirrors.cloud.aliyuncs.com/d' -e '/mirrors.aliyuncs.com/d' /etc/yum.repos.d/CentOS-Base.repo
```

###### [Ubuntu 镜像](https://developer.aliyun.com/mirror/ubuntu?spm=a2c6h.13651102.0.0.53322f701a0POJ)

###### [CentOS 8 安装笔记](https://blog.csdn.net/netgc/article/details/103242200)

问题：Centos8 安装软件出现

```powershell
CentOS-8 - AppStream 28 B/s | 38 B 00:01 
CentOS-8 - Base 111 B/s | 38 B 00:00 
CentOS-8 - Extras 114 B/s | 38 B 00:00 
Failed to synchronize cache for repo 'AppStream', ignoring this repo.
Failed to synchronize cache for repo 'BaseOS', ignoring this repo.
Failed to synchronize cache for repo 'extras', ignoring this repo.
No matches found.

```

解决方案：找到配置文件打开 /etc/yum.repos.d 修改baseurl 为阿里云的地址 。

修改软件源:

[CentOS 镜像新安装的centos设置国内阿里云镜像](https://developer.aliyun.com/mirror/centos?spm=a2c6h.13651102.0.0.53322f701a0POJ)





详细解释看下边连接：

[centos8 配置阿里源 最小化安装](https://my.oschina.net/u/930279/blog/3112152/print)

```shell
1. 备份原来的yum源
$sudo cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak 
2.设置aliyun的yum源
$sudo wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo 
3.添加EPEL源
$sudo wget -P /etc/yum.repos.d/ http://mirrors.aliyun.com/repo/epel-8.repo

4.清理缓存，生成新缓存，执行yum更新
$sudo yum clean all
$sudo yum makecache
$sudo yum update

yum clean all
yum makecache
```

######  

```shell
cd /etc/yum.repos.d
ls -l
#repo更多
-rw-r--r--. 1 root root  731 Aug 14 14:42 CentOS-AppStream.repo
-rw-r--r--. 1 root root  712 Aug 14 14:42 CentOS-Base.repo
-rw-r--r--. 1 root root  798 Aug 14 14:42 CentOS-centosplus.repo
-rw-r--r--. 1 root root 1320 Aug 14 14:42 CentOS-CR.repo
-rw-r--r--. 1 root root  668 Aug 14 14:42 CentOS-Debuginfo.repo
-rw-r--r--. 1 root root  756 Aug 14 14:42 CentOS-Extras.repo
-rw-r--r--. 1 root root  338 Aug 14 14:42 CentOS-fasttrack.repo
-rw-r--r--. 1 root root  928 Aug 14 14:42 CentOS-Media.repo
-rw-r--r--. 1 root root  736 Aug 14 14:42 CentOS-PowerTools.repo
-rw-r--r--. 1 root root 1382 Aug 14 14:42 CentOS-Sources.repo
-rw-r--r--. 1 root root   74 Aug 14 14:42 CentOS-Vault.repo
-rw-r--r--. 1 root root 1351 Aug  7 02:17 epel-playground.repo
-rw-r--r--. 1 root root 1206 Aug  7 02:17 epel.repo
-rw-r--r--. 1 root root 1305 Aug  7 02:17 epel-testing.repo

dnf repolist
#只需要修改以下4个
Last metadata expiration check: 0:00:45 ago on Sat 09 Nov 2019 01:26:02 PM CST.
repo id                 repo name                                                status
AppStream               CentOS-8 - AppStream                                     5,089
BaseOS                  CentOS-8 - Base                                          2,843
epel                    Extra Packages for Enterprise Linux 8 - x86_64           2,911
extras                  CentOS-8 - Extras                                        3

#备份
cp CentOS-Base.repo CentOS-Base.repo.bak
cp CentOS-AppStream.repo CentOS-AppStream.repo.bak
cp CentOS-Extras.repo CentOS-Extras.repo.bak

#替换
sed -i 's/mirrorlist=/#mirrorlist=/g' CentOS-Base.repo CentOS-AppStream.repo CentOS-Extras.repo
sed -i 's/#baseurl=/baseurl=/g' CentOS-Base.repo CentOS-AppStream.repo CentOS-Extras.repo
sed -i 's/http:\/\/mirror.centos.org/https:\/\/mirrors.aliyun.com/g' CentOS-Base.repo CentOS-AppStream.repo CentOS-Extras.repo

#修改epel(url不一样)
cp epel.repo epel.repo.bak

sed -i 's/metalink=/#metalink=/g' epel.repo
sed -i 's/#baseurl=/baseurl=/g' epel.repo
sed -i 's/https:\/\/download.fedoraproject.org\/pub/https:\/\/mirrors.aliyun.com/g' epel.repo


$sudo yum clean all
$sudo yum makecache
$sudo yum update
```



###### Cannot update read-only repo

```shell
[root@localhost ~]# clean all
bash: clean: 未找到命令...
文件搜索失败: Cannot update read-only repo
[root@localhost ~]# sudo yum clean all
0 文件已删除

```



### RHEL8/CentOS8的基础防火墙配置-用例



```shell
systemctl unmask firewalld                    #执行命令，即可实现取消服务的锁定
systemctl mask firewalld                      #下次需要锁定该服务时执行
systemctl start firewalld.service               #启动防火墙  
systemctl stop firewalld.service                #停止防火墙  
systemctl reloadt firewalld.service             #重载配置
systemctl restart firewalld.service             #重启服务
systemctl status firewalld.service              #显示服务的状态
systemctl enable firewalld.service              #在开机时启用服务
systemctl disable firewalld.service             #在开机时禁用服务
systemctl is-enabled firewalld.service          #查看服务是否开机启动
systemctl list-unit-files|grep enabled          #查看已启动的服务列表
systemctl --failed                              #查看启动失败的服务列表



firewall-cmd --state                         #查看防火墙状态  
firewall-cmd --reload                        #更新防火墙规则  
firewall-cmd --state                         #查看防火墙状态  
firewall-cmd --reload                        #重载防火墙规则  
firewall-cmd --list-ports                    #查看所有打开的端口  
firewall-cmd --list-services                 #查看所有允许的服务  
firewall-cmd --get-services                  #获取所有支持的服务  

#区域相关
firewall-cmd --list-all-zones                    #查看所有区域信息  
firewall-cmd --get-active-zones                  #查看活动区域信息  
firewall-cmd --set-default-zone=public           #设置public为默认区域  
firewall-cmd --get-default-zone                  #查看默认区域信息  
firewall-cmd --zone=public --add-interface=eth0  #将接口eth0加入区域public

#接口相关
firewall-cmd --zone=public --remove-interface=eth0       #从区域public中删除接口eth0  
firewall-cmd --zone=default --change-interface=eth0      #修改接口eth0所属区域为default  
firewall-cmd --get-zone-of-interface=eth0                #查看接口eth0所属区域  

#端口控制
firewall-cmd --add-port=8080/tcp --permanent               #永久添加80端口例外(全局)
firewall-cmd --remove-port=8080/tcp --permanent            #永久删除80端口例外(全局)
firewall-cmd --add-port=65001-65010/tcp --permanent      #永久增加65001-65010例外(全局)  
firewall-cmd  --zone=public --add-port=80/tcp --permanent            #永久添加80端口例外(区域public)
firewall-cmd  --zone=public --remove-port=80/tcp --permanent         #永久删除80端口例外(区域public)
firewall-cmd  --zone=public --add-port=65001-65010/tcp --permanent   #永久增加65001-65010例外(区域public) 
firewall-cmd --query-port=8080/tcp    # 查询端口是否开放
firewall-cmd --permanent --add-port=8080/tcp    # 开放8080端口
firewall-cmd --permanent --remove-port=8080/tcp    # 移除8080端口
firewall-cmd --reload    #重启防火墙(修改配置后要重启防火墙)



#开启紧急模式，相当于断网
[root@localhost ~]# firewall-cmd --panic-on 
#关闭紧急模式
[root@localhost ~]# firewall-cmd --panic-off 

#查看网站服务是否被放行
[root@localhost ~]# firewall-cmd --zone=public --query-service=http
no
[root@localhost ~]# firewall-cmd --zone=public --add-service=http
success
[root@localhost ~]# firewall-cmd --zone=public --query-service=http
yes
[root@localhost ~]# firewall-cmd --permanent --zone=public --add-service=https 
success
[root@localhost ~]# firewall-cmd --permanent --zone=public --query-service=https 
yes
[root@localhost ~]# firewall-cmd --reload 
success
#开放端口号
[root@localhost ~]# firewall-cmd --zone=public --add-port=8000-8888/tcp
success
#查询开放的端口号
[root@localhost ~]# firewall-cmd --list-ports 
8080/tcp 3306/tcp 80/tcp 4300/tcp 2375/tcp 6379/tcp 8000-8888/tcp


[root@localhost ~]# firewall-cmd  --zone=public --query-service=https
yes
[root@localhost ~]# firewall-cmd  --zone=public --remove-service=https
success
[root@localhost ~]# firewall-cmd  --zone=public --query-service=https
no

#设置8888重定向22号端口
[root@localhost ~]# firewall-cmd --permanent --zone=public --add-forward-port=port=8888:proto=tcp:toport=22:toaddr=192.168.40.128
success
#重载防火墙规则
[root@localhost ~]# firewall-cmd --reload 
success

[root@localhost ~]# firewall-cmd --permanent --zone=public --add-rich-rule="rule family="ipv4" source address="192.168.40.0/24" service name="ssh" reject"
success
[root@localhost ~]# firewall-cmd --reload 
success
[root@localhost ~]# firewall-cmd --permanent --zone=public --add-rich-rule="rule family="ipv4" source address="192.168.40.0/24" service name="ssh" accept"
[root@localhost ~]# firewall-cmd --reload 
success
oot@localhost ~]# firewall-cmd --permanent --zone=public --remove-rich-rule="rule family="ipv4" source address="192.168.40.0/24" service name="ssh" reject"
[root@localhost ~]# firewall-cmd --reload 
```

rmdir  /home/annimal

### centos7 防火墙 错误 （坑）

centos7中 防火墙以及端口号的 启动。

1、centos7中 取消了iptables ，只能使用firewall。

```powershell
#所以
service iptables status
Redirecting to /bin/systemctl status iptables.service
Unit iptables.service could not be found.
```

如果不习惯可以自己安装（其实也没必要）：

```powershell
yum install iptables-services           #安装iptables  
systemctl stop firewalld.service        #停止firewalld  
systemctl mask firewalld.service        #禁止自动和手动启动firewalld  
systemctl start iptables.service        #启动iptables
systemctl start ip6tables.service       #启动ip6tables  
systemctl enable iptables.service       #设置iptables自启动  
systemctl enable ip6tables.service      #设置ip6tables自启动
```

2、安装tomcat 后，并启动tomcat，发现访问不通，检查tomcat配置文件，端口号没错是8080，

确认端口号是否启用，telnet 182.169.*.*(虚拟机ip) 8080， 无法使用，

firewall-cmd --query-port=8080/

使用 firewall-cmd --query-port=8080 确认8080 是否开启

结果：bad port (most likely missing protocol), correct syntax is portid[-portid]/protocol

！！！ 这是什么鬼，端口坏掉了吗！！！

百度也没有找到找到有人碰到我这种情况，谷歌到是有人提出过这个问题，但是也只是提交BUG的页面，没有给出解决的方案。

原因是因为：

man firewall-cmd有那么困难么？ 你使用的命令

```shell
firewall-cmd --query-port=8080
```

是为了查询有没有添加这个端口的规则，而不是linux端口的查询命令。你了解什么是firewalld以后就会发现这个问题很简单，你换成 

```powershell
#查询8080端口号的状态
firewall-cmd --query-port=8080/tcp
firewall-cmd --query-port=9080/tcp
firewall-cmd --query-port=8063/tcp
#添加8080端口号
firewall-cmd --add-port=8080/tcp --permanent 
firewall-cmd --add-port=9080/tcp --permanent 
#重启防火墙(修改配置后要重启防火墙)
firewall-cmd --reload







/sbin/iptables -I INPUT -p tcp --dport 8063 -j ACCEPT
```

https://bugzilla.redhat.com/show_bug.cgi?id=1274961(翻墙。。)
原文链接：https://blog.csdn.net/wangqingqi20005/article/details/51018260



centos6开放端口命令

```powershell

1、第一步，修改端口号 
/sbin/iptables -I INPUT -p tcp --dport 8080 -j ACCEPT 
2、第二步，保存修改
/etc/init.d/iptables save 
3、第三步，重启防火墙
service iptables restart



```



### Windows下访问VMware中tomcat

```
很多人都可能和我一样，运行在虚拟机上，开发时在windows上进行。

在linux上运行tomcat，并且windows中能ping通虚拟机，但就不能通过虚拟机ip访问到8080端口上的tomcat，真是太扯淡了，其实不然，我们大家都犯了同一个问题，安装系统时我们都把防火墙打开了，所以关掉就可以了。不过这可能不是最好的办法据说有种NAT映射方式，本人目前还没研究过。

解决方案：

1. 禁用防火墙（永久）：

chkconfig iptables off

2. 当前会话生命周期内关掉防火墙（重启就会失效）：

service iptables stop

无论使用以上那种方式关掉防火墙后我们都可以访问tomcat了。

以上两种方案不推荐：

在防火墙开启的情况下在iptables文件中加规则：

1. 在/etc/sysconfig/iptables文件中加入如下端口访问规则

-A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT

后重启iptables

service iptables restart

现在访问就OK了。
```

### [centos8开放8080端口](https://www.jb51.net/os/Ubuntu/617627.html)

centos8已经开始使用firewall作为防火墙，而不是iptables了，所以，开放8080端口就和以往不一样了，那么该怎么开放8080端口呢？下面我们就来看看详细的教程。

~~~shell
#1.我们可以输入命令查看防火墙的状态；
firewall-cmd --state 
#3、如果上一步处于关闭状态，输入命令：
systemctl start firewalld.service
#4、开启8080端口，输入命令：
firewall-cmd --zone=public --add-port=8080/tcp --permanent

#5、让我们来解释一下上一个命令：

```
--zone=public：表示作用域为公共的；``--add-port=8080/tcp：添加tcp协议的端口8080；``--permanent：永久生效，如果没有此参数，则只能维持当前服务生命周期内，重新启动后失效；`

#6、输入命令**重启防火墙**；
systemctl restart firewalld.service
#7、输入命令重新载入配置(#重载防火墙规则 )
firewall-cmd --reload
#8查看所有打开的端口  
firewall-cmd --list-ports                    


~~~

### Failed to start firewalld.service: Unit firewalld.service is masked.

firewalld服务被锁定，不能添加对应端口

执行命令，即可实现取消服务的锁定

\# systemctl unmask firewalld

下次需要锁定该服务时执行

\# systemctl mask firewalld

```powershell
[root@localhost mysql8]# firewall-cmd --state
not running
[root@localhost mysql8]# systemctl start firewalld.service
Failed to start firewalld.service: Unit firewalld.service is masked.
#执行命令，即可实现取消服务的锁定
[root@localhost mysql8]# systemctl unmask firewalld
Removed /etc/systemd/system/firewalld.service.
[root@localhost mysql8]# systemctl start firewalld.service
[root@localhost mysql8]# firewall-cmd --state
running

```



# centos8开放8080端口的方法

```powershell
[root@localhost opt]# firewall-cmd --state 
running
[root@localhost opt]# firewall-cmd --zone=public --add-port=8088/tcp --permanent
success
[root@localhost opt]# systemctl restart firewalld.service

[root@localhost opt]# firewall-cmd --reload
success
#查看所有打开的端口  
[root@localhost opt]# firewall-cmd --list-ports 
```

### "Host '192.168.40.1' is not allowed to connect to this MySQL server"

**连接数据库服务器出现1130-host'192.168.2.137'is not allowed to connect to this mysql server错误，**

**这个问题是因为在数据库服务器中的mysql数据库中的user的表中没有记录权限

遇到这个问题首先到mysql所在的服务器上用连接进行处理

主要分为以下几步：

   

```powershell
#1、连接mysql服务器

#2、看当前所有数据库：
mysql> show databases;

#3、进入mysql数据库：
mysql> use mysql;

#4、查看mysql数据库中所有的表：
mysql> show tables;

#5、查看user表中的数据：
mysql> select Host,User from user;

#6、修改user表中的:
mysql> update user set Host='%' where User='root';

#7、刷新授权列表：
mysql> flush privileges;

#8、开放3306端口防火墙：
#查看防火墙的状态；
[root@localhost opt]# firewall-cmd --state 
running
#开启3306端口
[root@localhost opt]# firewall-cmd --zone=public --add-port=3306/tcp --permanent
success
#重启防火墙
[root@localhost opt]# systemctl restart firewalld.service
#重新载入配置
[root@localhost opt]# firewall-cmd --reload
success
#查看3306端口是否开启
[root@localhost opt]# firewall-cmd --list-ports 
```



```powershell
[root@localhost opt]# mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 26
Server version: 8.0.18 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.23 sec)

mysql> use mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> show tables;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| columns_priv              |
| component                 |
| db                        |
| default_roles             |
| engine_cost               |
| func                      |
| general_log               |
| global_grants             |
| gtid_executed             |
| help_category             |
| help_keyword              |
| help_relation             |
| help_topic                |
| innodb_index_stats        |
| innodb_table_stats        |
| password_history          |
| plugin                    |
| procs_priv                |
| proxies_priv              |
| role_edges                |
| server_cost               |
| servers                   |
| slave_master_info         |
| slave_relay_log_info      |
| slave_worker_info         |
| slow_log                  |
| tables_priv               |
| time_zone                 |
| time_zone_leap_second     |
| time_zone_name            |
| time_zone_transition      |
| time_zone_transition_type |
| user                      |
+---------------------------+
33 rows in set (0.01 sec)

mysql> select Host,User from user;
+-----------+------------------+
| Host      | User             |
+-----------+------------------+
| localhost | mysql.infoschema |
| localhost | mysql.session    |
| localhost | mysql.sys        |
| localhost | root             |
+-----------+------------------+
4 rows in set (0.01 sec)

mysql> update user set Host='%' where User='root';
Query OK, 1 row affected (0.13 sec)
Rows matched: 1  Changed: 1  Warnings: 0
#刷新授权列表
mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)

mysql> 

```

[参考解决1130-host'192.168.2.137'is not allowed to connect to this mysql server报错问题](https://www.cnblogs.com/CMX_Shmily/p/11656541.html)



### [CentOS 8命令lsb_release: command not found解决方案](https://blog.csdn.net/xufengzhu/article/details/73330741)

```powershell
#确认lsb_release是否安装
[root@localhost ~]#  lsb_release -a
bash: lsb_release: 未找到命令...
文件搜索失败: Cannot update read-only repo
#使用yum安装lsb
[root@localhost ~]# yum install -y redhat-lsb
[root@localhost ~]#  lsb_release -a
LSB Version:	:core-4.1-amd64:core-4.1-noarch:cxx-4.1-amd64:cxx-4.1-noarch:desktop-4.1-amd64:desktop-4.1-noarch:languages-4.1-amd64:languages-4.1-noarch:printing-4.1-amd64:printing-4.1-noarch
Distributor ID:	CentOS
Description:	CentOS Linux release 8.0.1905 (Core) 
Release:	8.0.1905
Codename:	Core


```



### [Vim 保存和退出命令](https://www.cnblogs.com/firstcsharp/p/10241132.html)

| 命令          | 简单说明                                                     |
| ------------- | ------------------------------------------------------------ |
| :w            | 保存编辑后的文件内容，但不退出vim编辑器。这个命令的作用是把内存缓冲区中的数据写到启动vim时指定的文件中。 |
| :w!           | 强制写文件，即强制覆盖原有文件。如果原有文件的访问权限不允许写入文件，例如，原有的文件为只读文件，则可使用这个命令强制写入。但是，这种命令用法仅当用户是文件的属主时才适用，而超级用户则不受此限制。 |
| :wq           | 保存文件内容后退出vim编辑器。这个命令的作用是把内存缓冲区中的数据写到启动vim时指定的文件中，然后退出vim编辑器。另外一种替代的方法是用ZZ命令。 |
| :wq!          | 强制保存文件内容后退出vim编辑器。这个命令的作用是把内存缓冲区中的数据强制写到启动vim时指定的文件中，然后退出vim编辑器。 |
| ZZ            | 使用ZZ命令时，如果文件已经做过编辑处理，则把内存缓冲区中的数据写到启动vim时指定的文件中，然后退出vim编辑器。否则只是退出vim而已。注意，ZZ命令前面无需加冒号“：”，也无需按Enter键。 |
| :q            | 在未做任何编辑处理而准备退出vim时，可以使用此命令。如果已做过编辑处理，则vim不允许用户使用“:q”命令退出，同时还会输出下列警告信息：No write since last change (:quit! overrides) |
| :q!           | 强制退出vim编辑器，放弃编辑处理的结果。如果确实不需要保存修改后的文件内容，可输入“:q!”命令，强行退出vim编辑器。 |
| :w filename   | 把编辑处理后的结果写到指定的文件中保存                       |
| :w! filename  | 把编辑处理后的结果强制保存到指定的文件中，如果文件已经存在，则覆盖现有的文件。 |
| :wq! filename | 把编辑处理后的结果强制保存到指定的文件中，如果文件已经存在，则覆盖现有文件，并退出vim编辑器。 |

### vi或者vim显示行号

只需要在vi或者vim的资源文件中加入我们的设置就行了。

只要在/etc/vimrc(/etc/virc)文件中修改一下就行了

vim /etc/vimrc

可以看到都是一些配置vim环境的数据，我们只需要在行首或者行尾新建一行然后写入

set number

## linux 查看文件系统类型

Linux 查看文件系统的方式有多种，列举如下：

```powershell
[root@localhost bin]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             2.9G     0  2.9G    0% /dev
tmpfs                2.9G     0  2.9G    0% /dev/shm
tmpfs                2.9G  9.7M  2.9G    1% /run
tmpfs                2.9G     0  2.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   13G  4.2G   76% /
/dev/sdb1            5.0G   84M  5.0G    2% /newdisk
/dev/sda1            976M  221M  688M   25% /boot
tmpfs                584M   28K  584M    1% /run/user/42
tmpfs                584M  4.0K  584M    1% /run/user/0

[root@localhost ~]# df -lhT
文件系统            类型      容量  已用  可用 已用% 挂载点
devtmpfs            devtmpfs  889M     0  889M    0% /dev
tmpfs               tmpfs     904M     0  904M    0% /dev/shm
tmpfs               tmpfs     904M  9.4M  894M    2% /run
tmpfs               tmpfs     904M     0  904M    0% /sys/fs/cgroup
/dev/mapper/cl-root xfs        17G  9.7G  7.3G   58% /
/dev/sda1           ext4      976M  219M  690M   25% /boot
tmpfs               tmpfs     181M   28K  181M    1% /run/user/42
tmpfs               tmpfs     181M  4.0K  181M    1% /run/user/0



[root@localhost ~]# parted
GNU Parted 3.2
使用 /dev/sda
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) p                                                                
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sda: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End     Size    Type     File system  标志
 1      1049kB  1075MB  1074MB  primary  ext4         启动
 2      1075MB  21.5GB  20.4GB  primary               lvm

(parted)  


#df -T 只可以查看已经挂载的分区和文件系统类型。
[root@localhost ~]# df -T
文件系统            类型        1K-块     已用    可用 已用% 挂载点
devtmpfs            devtmpfs   909988        0  909988    0% /dev
tmpfs               tmpfs      924712        0  924712    0% /dev/shm
tmpfs               tmpfs      924712     9540  915172    2% /run
tmpfs               tmpfs      924712        0  924712    0% /sys/fs/cgroup
/dev/mapper/cl-root xfs      17811456 10095204 7716252   57% /
/dev/sda1           ext4       999320   224256  706252   25% /boot
tmpfs               tmpfs      184940       28  184912    1% /run/user/42
tmpfs               tmpfs      184940        4  184936    1% /run/user/0
[root@localhost ~]# 

# fdisk -l 可以显示出所有挂载和未挂载的分区，但不显示文件系统类型。
[root@localhost ~]# fdisk -l
Disk /dev/sda：20 GiB，21474836480 字节，41943040 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xe82b74c8

设备       启动    起点     末尾     扇区 大小 Id 类型
/dev/sda1  *       2048  2099199  2097152   1G 83 Linux
/dev/sda2       2099200 41943039 39843840  19G 8e Linux LVM




Disk /dev/mapper/cl-root：17 GiB，18249416704 字节，35643392 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节


Disk /dev/mapper/cl-swap：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
[root@localhost ~]# 

[root@localhost ~]# lsblk -f 
NAME        FSTYPE      LABEL UUID                                   MOUNTPOINT
sda                                                                  
├─sda1      ext4              ff037b8a-c85b-4d17-a919-7ab0b5ffb7e6   /boot
└─sda2      LVM2_member       wVXcRD-XcjB-A0V7-xf4R-IU4s-S7Ew-Pom5Jf 
  ├─cl-root xfs               546b9807-e6f9-44fd-8174-5a8edfd127bf   /
  └─cl-swap swap              f5bb0909-dd7d-4ee4-b437-bfc3d27e60b7   [SWAP]
sr0                     


```

```shell
#查看文件夹内容
ls -ltr
#检查是否安装过mysql
[root@localhost mysql8]#  rpm -qa|grep mysql
[root@localhost mysql8]# rpm -ivh mysql-community-server-8.0.18-1.el8.x86_64.rpm
#安装mysql客户端
[root@localhost mysql8]# rpm -ivh mysql-community-client-8.0.18-1.el8.x86_64.rpm

[root@localhost mysql8]# ps -ef|grep mysql
mysql      5164      1  0 10:04 ?        00:00:15 /usr/sbin/mysqld
root       8748   7954  0 10:39 pts/0    00:00:00 grep --color=auto mysql
#显示数据库的字符集
mysql> show variables like '%char%';
+--------------------------+--------------------------------+
| Variable_name            | Value                          |
+--------------------------+--------------------------------+
| character_set_client     | utf8mb4                        |
| character_set_connection | utf8mb4                        |
| character_set_database   | utf8mb4                        |
| character_set_filesystem | binary                         |
| character_set_results    | utf8mb4                        |
| character_set_server     | utf8mb4                        |
| character_set_system     | utf8                           |
| character_sets_dir       | /usr/share/mysql-8.0/charsets/ |
+--------------------------+--------------------------------+
8 rows in set (0.01 sec)
#mysql配置文件的地址Linux中 /etc/my.cnf
#关闭mysql服务
systemctl stop mysqld
#开启mysql服务
[root@localhost mysql]# systemctl start mysqld
#查询mysql状态
[root@localhost mysql]# systemctl status mysqld
#设置开机启动
[root@localhost mysql]# systemctl enable mysqld
#禁止开机启动
[root@localhost mysql]# systemctl disenable mysqld
```

# [CentOS7中systemctl的使用](http://blog.csdn.net/u012486840/article/details/53161574)

 

 

CentOS 7.x开始，CentOS开始使用systemd服务来代替daemon，原来管理系统启动和管理系统服务的相关命令全部由systemctl命令来代替。

## 1、原来的 service 命令与 systemctl 命令对比

| daemon命令             | systemctl命令                 | 说明     |
| ---------------------- | ----------------------------- | -------- |
| service [服务] start   | systemctl start [unit type]   | 启动服务 |
| service [服务] stop    | systemctl stop [unit type]    | 停止服务 |
| service [服务] restart | systemctl restart [unit type] | 重启服务 |

此外还是二个systemctl参数没有与service命令参数对应

- status：参数来查看服务运行情况
- reload：重新加载服务，加载更新后的配置文件（并不是所有服务都支持这个参数，比如network.service）

**应用举例：**

```powershell
#启动网络服务
systemctl start network.service

#停止网络服务
systemctl stop network.service

#重启网络服务
systemctl restart network.service

#查看网络服务状态
systemctl status network.service
```

## 2、原来的chkconfig 命令与 systemctl 命令对比

### 2.1、设置开机启动/不启动

| daemon命令           | systemctl命令                 | 说明                 |
| -------------------- | ----------------------------- | -------------------- |
| chkconfig [服务] on  | systemctl enable [unit type]  | 设置服务开机启动     |
| chkconfig [服务] off | systemctl disable [unit type] | 设备服务禁止开机启动 |

**应用举例：**

```powershell
#停止cup电源管理服务
systemctl stop cups.service

#禁止cups服务开机启动
systemctl disable cups.service

#查看cups服务状态
systemctl status cups.service

#重新设置cups服务开机启动
systemctl enable cups.service
```

### 2.2、查看系统上上所有的服务

命令格式：

```
systemctl [command] [–type=TYPE] [–all]
```

参数详解：

`command` - list-units：依据unit列出所有启动的unit。加上 –all 才会列出没启动的unit; - list-unit-files:依据/usr/lib/systemd/system/ 内的启动文件，列出启动文件列表

`–type=TYPE` - 为unit type, 主要有service, socket, target

**应用举例：**

| systemctl命令                                    | 说明                       |
| ------------------------------------------------ | -------------------------- |
| systemctl                                        | 列出所有的系统服务         |
| systemctl list-units                             | 列出所有启动unit           |
| systemctl list-unit-files                        | 列出所有启动文件           |
| systemctl list-units –type=service –all          | 列出所有service类型的unit  |
| systemctl list-units –type=service –all grep cpu | 列出 cpu电源管理机制的服务 |
| systemctl list-units –type=target –all           | 列出所有target             |

### 3、systemctl特殊的用法

| systemctl命令                   | 说明                       |
| ------------------------------- | -------------------------- |
| systemctl is-active [unit type] | 查看服务是否运行           |
| systemctl is-enable [unit type] | 查看服务是否设置为开机启动 |
| systemctl mask [unit type]      | 注销指定服务               |
| systemctl unmask [unit type]    | 取消注销指定服务           |

**应用举例：**

```powershell
#查看网络服务是否启动
systemctl is-active network.service

[root@localhost ~]# systemctl is-active network.service
inactive

#检查网络服务是否设置为开机启动
systemctl is-enable network.service

#停止cups服务
systemctl stop cups.service

#注销cups服务
systemctl mask cups.service

#查看cups服务状态
systemctl status cups.service

#取消注销cups服务
systemctl unmask cups.service
```

### 4、init 命令与systemctl命令对比

| init命令 | systemctl命令      | 说明     |
| -------- | ------------------ | -------- |
| init 0   | systemctl poweroff | 系统关机 |
| init 6   | systemctl reboot   | 重新启动 |

与开关机相关的其他命令：

| systemctl命令       | 说明                 |
| ------------------- | -------------------- |
| systemctl suspend   | 进入睡眠模式         |
| systemctl hibernate | 进入休眠模式         |
| systemctl rescue    | 强制进入救援模式     |
| systemctl emergency | 强制进入紧急救援模式 |

### 5、设置系统运行级别

#### 5.1、运行级别对应表

| init级别 | systemctl target  |
| -------- | ----------------- |
| 0        | shutdown.target   |
| 1        | emergency.target  |
| 2        | rescure.target    |
| 3        | multi-user.target |
| 4        | 无                |
| 5        | graphical.target  |
| 6        | 无                |

此外还是一个getty.target用来设置tty的数量。

#### 5.2、设置运行级别

命令格式：

```
systemctl [command] [unit.target]
```

参数详解：

```
command:
```

- get-default :取得当前的target
- set-default :设置指定的target为默认的运行级别
- isolate :切换到指定的运行级别
- unit.target :为5.1表中列出的运行级别

| systemctl命令                           | 说明                                         |
| --------------------------------------- | -------------------------------------------- |
| systemctl get-default                   | 获得当前的运行级别                           |
| systemctl set-default multi-user.target | 设置默认的运行级别为mulit-user               |
| systemctl isolate multi-user.target     | 在不重启的情况下，切换到运行级别mulit-user下 |
| systemctl isolate graphical.target      | 在不重启的情况下，切换到图形界面下           |

### 6、使用systemctl分析各服务之前的依赖关系

命令格式：

```
systemctl list-dependencies [unit] [–reverse]
```

`–reverse`是用来检查寻哪个unit使用了这个unit

**应用举例：**

```
#获得当前运行级别的target
[root@www ~]# systemctl get-default
multi-user.target

#查看当前运行级别target(mult-user)启动了哪些服务
[root@www ~]# systemctl list-dependencies
default.target
├─abrt-ccpp.service
├─abrt-oops.service
├─vsftpd.service
├─basic.target
│ ├─alsa-restore.service
│ ├─alsa-state.service
.....(中间省略).....
│ ├─sockets.target
│ │ ├─avahi-daemon.socket
│ │ ├─dbus.socket
.....(中间省略).....
│ ├─sysinit.target
│ │ ├─dev-hugepages.mount
│ │ ├─dev-mqueue.mount
.....(中间省略).....
│ └─timers.target
│   └─systemd-tmpfiles-clean.timer
├─getty.target
│ └─getty@tty1.service
└─remote-fs.target

#查看哪些target引用了当前运行级别的target
[root@www ~]# systemctl list-dependencies --reverse
default.target
└─graphical.target
```

### 7、关闭网络服务

在使用systemctl关闭网络服务时有一些特殊 需要同时关闭unit.servce和unit.socket

使用systemctl查看开启的sshd服务

```
[root@www system]#  systemctl list-units --all | grep sshd
sshd-keygen.service loaded inactive dead        OpenSSH Server Key Generation
sshd.service        loaded active   running     OpenSSH server daemon
sshd.socket         loaded inactive dead        OpenSSH Server Socket
```

可以看到系统同时开启了 `sshd.service` 和 `sshd.socket` , 如果只闭关了 `sshd.service` 那么 `sshd.socket`还在监听网络，在网络上有要求连接 sshd 时就会启动 `sshd.service` 。因此如果想完全关闭sshd服务的话，需要同时停用 `sshd.service` 和 `sshd.socket` 。

```
systemctl stop sshd.service
systemctl stop sshd.socket
systemctl disable sshd.service sshd.socket
```

由于centos 7.x默认没有安装net-tools，因此无法使用netstat 来查看主机开发的商品。需要通过yum安装来获得该工具包：

```
yum -y install net-tools
```

查看是否关闭22端口

```powershell
netstat -lnp |grep sshd
```

### 8、关闭防火墙firewall

Centos 7.x 中取消了iptables, 用firewall取而代之。要关闭防火墙并禁止开机启动服务使用下面的命令:

```powershell
systemctl stop firewalld.service
systemctl disable firewalld.
```



```powershell
#显示所有的环境变量
[root@server1 ~]# declare -x
```

# Linux自学篇——linux命令英文全称及解释

```powershell
man: Manual   					意思是手册，可以用这个命令查询其他命令的用法。
pwd：Print working directory   	显示当前目录
su：Swith user  				切换用户，切换到root用户
cd：Change directory 			切换目录
ls：List files  				列出目录下的文件
ps：Process Status  			进程状态
mkdir：Make directory  			建立目录
rmdir：Remove directory  		移动目录
mkfs: Make file system  		建立文件系统
fsck：File system check  		文件系统检查
cat: Concatenate  	串联
uname: Unix name  	系统名称
df: Disk free  		空余硬盘
du: Disk usage 		硬盘使用率
lsmod: List modules 列表模块
mv: Move file  		移动文件
rm: Remove file  	删除文件
cp: Copy file  		复制文件
ln: Link files  	链接文件
fg: Foreground 		前景
bg: Background 		背景
chown: Change owner 改变所有者
chgrp: Change group 改变用户组
chmod: Change mode 	改变模式
umount: Unmount 	卸载
dd:本应根据功能描述“Convert an copy”命名为“cc”，但“cc”已经代表“C Complier”，所以命名为“dd”
tar：Tape archive 				解压文件
ldd：List dynamic dependencies 	列出动态相依
insmod：Install module 			安装模块
rmmod：Remove module 			删除模块
lsmod：List module 				列表模块
Unix: 			Linux操作系统的前身 
Linux: 			一种开源，多用户操作系统 
Linus torvalds: linux系统作者
Kenel: 			内核 
GNU: GNU is Not Unix  GNU不是unix 
OSS: open source software 开放源代码软件 
License: 许可证 Red hat: 红帽子公司 
Text mode: 		字符模式 
Graphic mode: 	图形界面 
Root : 		linux默认系统管理员账号 
Command: 	命令
Option: 	选项，参数 
cd: change directory 	改变目录
mkdir: make directory 	创建目录 
rmdir: remove directory 删除目录
rm: remove 	删除文件 
cp: copy 	复制 
mv: move 	移动或重命名 
ISO：	   	光盘镜像文件
mount: 		挂载 
umount: undo mount 		卸载 
useradd: add a user 	添加用户 
userdel: delete a user 	删除用户 
groupadd: add a group 	添加组
groupdel: delete a group 删除组 
rwx: read write execute 读取，写入，执行 
chmod: change the permission mode of the files or the directories 改变文件或目录的权限 
chown: 		 改变文件或目录的宿主属性 
Application: 应用 
Rpm: redhat packages manager 包管理器
Version: 	 版本 
Build date:  创建日期
Summary: 	 概括描述
Description: 详细描述
level: 		 级别 
runlevel: 	 运行级别 
chkconfig: check config 检查系统服务启动状态
log: 		 日志 
quota: 		 配额 
NFS: network file system 网络文件系统 
export: 	 输出 
service: 	 服务 
opensource 	 开源
localhost 	 本地主机 
directory 	 目录 
total 		 总共 
forward 	 转发 
search 		 查询 
media 		 媒体 
autorun		 自动运行 
track 		 跟踪 
authentication 认证
successfully 成功地
deny 		 拒绝 
access 		 接入 
aplication	 应用  
block 		 模块 
protect 	 保护 
device 		 设备 
Double-click 双击 
mouse 		 鼠标 
init 		 初始化 
specific 	 精细的，细节
tag 		 标记 
normal 		 正规的 
accessories  附件  
login 		 登陆 
export 	 	 出口 输出 
specific 	 特定 特殊 
echo 		 输出
manager 	 管理器
environment  环境
source 		 源 
license 	 许可 
signature 	 签字，签名
interpreter  翻译器 
feature 	 特征 
modification 修改，修饰
summary 	 概要 
prepared	 准备 
faile 		 失败 
dependent 	 依赖，依靠
minimal		 最小的
variable 	 变量 
development  发展 
bracket		 归档
include 	 包括
action 		 行动
standard	 标准 
process 	 过程，进程
script 		 文本，剧本
execute 	 完成，执行 
status 	 	 状态
message 	 消息
console		 控制 
except 		 除了...外
private 	 私人的 
restricte 	 限定，制约
quota 		 限额，配额
inordinate 	 过度 
compress 	 压缩   
drop 		 丢弃
loopback 	 回环 
collision 	 冲突
transmit 	 传送，传递
unreachable  不可达的 
parameter 	 参数 
various 	 不同的，各式各样的
require 	 需要，要求 
generate     生成，导致   
confirm      确认
session      会话
terminal     终端 
operational  操作的，运行的
flag         标志
offset       偏移量
invalid      无效 
acknowledge  确认
split        分裂
platform     平台
bandwidth    带宽

```

### 开启的是图形界面

```shell
#默认开启的是图形界面
[root@hadoop101 ~]# systemctl get-default
graphical.target
#设置开启界面
[root@hadoop101 ~]# systemctl set-default multi-user.target
systemctl set-default graphical.target
```



```powershell
#设置主机名
[root@localhost ~]# hostnamectl set-hostname hadoop100
[root@hadoop101 ~]# hostnamectl set-hostname localhost


#当前登陆的用户为
[root@localhost ~]# whoami
root
[root@localhost ~]# which echo
/usr/bin/echo

#查询java的环境变量
[root@localhost ~]# echo $JAVA_HOME


[root@localhost ~]# mkdir /root/aaa
#重复前一命令的参数
[root@localhost ~]# cd !$
cd /root/aaa
[root@localhost aaa]# pwd
/root/aaa

[root@localhost aaa]# history
#查找less进程的进程号
[root@localhost aaa]# pidof less
7970
[root@localhost aaa]# kill -9 7970
#定义ls /boot 的别名为ok
[root@localhost aaa]# alias ok="ls /boot"
[root@localhost aaa]# ok
#取消定义的别名ok
[root@localhost aaa]# unalias ok
[root@localhost aaa]# ok

#显示所有文件包括隐藏的
[root@localhost ~]# ls -a /root
#创建空文件
[root@localhost ~]# touch file1


[root@localhost ~]# du -s /root/
84720	/root/
#显示/root目录磁盘占用量
[root@localhost ~]# du -sh /root/
83M	/root/

[root@localhost ~]# date
2020年 02月 15日 星期六 15:19:04 CST
#显示主机名
[root@localhost ~]# uname -n
localhost
#操作系统内核发型号
[root@localhost ~]# uname -r
4.18.0-80.el8.x86_64
[root@localhost ~]# uname -a
Linux localhost 4.18.0-80.el8.x86_64 #1 SMP Tue Jun 4 09:19:46 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
[root@localhost ~]# hostname Linux
[root@localhost ~]# uname -n
Linux
#查看硬件时间
[root@localhost ~]# hwclock
2020-02-15 15:24:41.459638+08:00
#以硬件时间更新系统时间
[root@localhost ~]# hwclock -s

#向所有用户发出消息
[root@localhost ~]# wall '下班后请关闭计算机'
[root@localhost ~]# uptime

[root@localhost ~]# type -a cd
cd 是 shell 内建
cd 是 /usr/bin/cd

[root@localhost ~]# vi /root/date
#上一条命令的参数
[root@localhost ~]# cat !$
cat /root/date
#!/bin/bash
#filename:date
echo "Mr.$USER,Today is:"
echo 'date'
echo Wish you a lucky day!
#设置可执行权限
[root@localhost ~]# chmod u+x /root/date 
[root@localhost ~]# ll /root/date 
-rwxr--r--. 1 root root 92 2月  15 15:38 /root/date
[root@localhost ~]# /root/date 
Mr.root,Today is:
date
Wish you a lucky day!
[root@localhost ~]# 

#显示环境变量
[root@localhost ~]# echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/.dotnet/tools:/opt/module/jdk1.8.0_144/bin:/opt/module/hadoop-3.2.1/bin:/opt/module/hadoop-3.2.1/sbin:/root/bin
#显示所有环境变量
[root@localhost ~]# env

[root@localhost ~]# useradd zhangsan
[root@localhost ~]# passwd zhangsan
更改用户 zhangsan 的密码 。
新的 密码：
无效的密码： 密码少于 8 个字符
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。
#不删除home目录文件
[root@localhost ~]# userdel zhangsan
[root@localhost ~]# useradd ls
#删除home目录文件
[root@localhost ~]# userdel -r ls
#更改指定用户的密码
[root@localhost ~]# passwd root
更改用户 root 的密码 。
新的 密码：
无效的密码： 密码少于 8 个字符
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。
#更改当前用户密码
[root@localhost ~]# passwd
更改用户 root 的密码 。
新的 密码：
无效的密码： 密码少于 8 个字符
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。

#查看当前系统的端口使用：
netstat -an
[root@localhost ~]# netstat -an | more

```









### 为虚拟机添加容量

```powershell
#在虚拟机上添加硬盘操作
#查看系统的分区和挂载情况，已添加sdb
[root@localhost ~]# lsblk -f
NAME        FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                   
├─sda1      ext4                               8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2      LVM2_member                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root xfs                                1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap swap                               c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                                   
sr0         iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00       
[root@localhost ~]# fdisk /dev/sdb

欢迎使用 fdisk (util-linux 2.32.1)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。

设备不包含可识别的分区表。
创建了一个磁盘标识符为 0x70498da4 的新 DOS 磁盘标签。

命令(输入 m 获取帮助)：m

帮助：

  DOS (MBR)
   a   开关 可启动 标志
   b   编辑嵌套的 BSD 磁盘标签
   c   开关 dos 兼容性标志

  常规
   d   删除分区
   F   列出未分区的空闲区
   l   列出已知分区类型
   n   添加新分区
   p   打印分区表
   t   更改分区类型
   v   检查分区表
   i   打印某个分区的相关信息

  杂项
   m   打印此菜单
   u   更改 显示/记录 单位
   x   更多功能(仅限专业人员)

  脚本
   I   从 sfdisk 脚本文件加载磁盘布局
   O   将磁盘布局转储为 sfdisk 脚本文件

  保存并退出
   w   将分区表写入磁盘并退出
   q   退出而不保存更改

  新建空磁盘标签
   g   新建一份 GPT 分区表
   G   新建一份空 GPT (IRIX) 分区表
   o   新建一份的空 DOS 分区表
   s   新建一份空 Sun 分区表


命令(输入 m 获取帮助)：n
分区类型
   p   主分区 (0个主分区，0个扩展分区，4空闲)
   e   扩展分区 (逻辑分区容器)
选择 (默认 p)：p
分区号 (1-4, 默认  1): 1
第一个扇区 (2048-4194303, 默认 2048): 
上个扇区，+sectors 或 +size{K,M,G,T,P} (2048-4194303, 默认 4194303): 

创建了一个新分区 1，类型为“Linux”，大小为 2 GiB。

命令(输入 m 获取帮助)：w
分区表已调整。
将调用 ioctl() 来重新读分区表。
正在同步磁盘。

[root@localhost ~]# lsblk -f
NAME        FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                   
├─sda1      ext4                               8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2      LVM2_member                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root xfs                                1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap swap                               c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                                   
└─sdb1                                                                                
sr0         iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00                 
[root@localhost ~]# 
[root@localhost ~]# mkfs -t ext4 /dev/sdb1
mke2fs 1.44.3 (10-July-2018)
创建含有 524032 个块（每块 4k）和 131072 个inode的文件系统
文件系统UUID：eabc293a-823c-4cd4-b72d-4156beab93b6
超级块的备份存储于下列块： 
	32768, 98304, 163840, 229376, 294912

正在分配组表： 完成                            
正在写入inode表： 完成                            
创建日志（8192 个块）完成
写入超级块和文件系统账户统计信息： 已完成
[root@localhost ~]# lsblk -f
NAME        FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                   
├─sda1      ext4                               8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2      LVM2_member                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root xfs                                1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap swap                               c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                                   
└─sdb1      ext4                               eabc293a-823c-4cd4-b72d-4156beab93b6   
sr0         iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00   
[root@localhost ~]# mkdir /home/newdisk
[root@localhost ~]# cd /home/
[root@localhost home]# ll
总用量 4
drwx------. 16 atguigu     atguigu     4096 2月  15 15:28 atguigu
drwx------.  3 houyingchao houyingchao   92 1月  31 10:16 houyingchao
drwxr-xr-x.  2 root        root           6 2月  15 20:53 newdisk
drwx------.  3        1002        1002   92 2月  15 15:49 zhangsan
[root@localhost home]# cd newdisk/
[root@localhost newdisk]# ll
总用量 0
[root@localhost newdisk]# cd ..
#临时挂载
[root@localhost home]# mount /dev/sdb1 /home/newdisk/
[root@localhost home]# lsblk -f
NAME        FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                   
├─sda1      ext4                               8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2      LVM2_member                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root xfs                                1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap swap                               c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                                   
└─sdb1      ext4                               eabc293a-823c-4cd4-b72d-4156beab93b6   /home/newdisk
sr0         iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00      
[root@localhost home]# cd newdisk/
[root@localhost newdisk]# ll
总用量 16
drwx------. 2 root root 16384 2月  15 20:51 lost+found
#设置永久挂载
/dev/sdb1               /home/newdisk                             ext4    defaults        0 0 
[root@localhost home]# vim /etc/fstab 
[root@localhost home]# cat !$
cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Fri Jan 31 09:52:04 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/cl-root     /                       xfs     defaults        0 0
UUID=8fb29c11-6deb-4bfc-9412-5bbf5f70749c /boot                   ext4    defaults        1 2
/dev/mapper/cl-swap     swap                    swap    defaults        0 0
/dev/sdb1               /home/newdisk                             ext4    defaults        0 0  

[root@localhost home]# mount -a
[root@localhost home]# reboot
[root@localhost ~]# 
#卸载挂载
[root@localhost ~]# umount /dev/sdb1
[root@localhost ~]# lsblk -f
NAME        FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                   
├─sda1      ext4                               8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2      LVM2_member                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root xfs                                1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap swap                               c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                                   
└─sdb1      ext4                               eabc293a-823c-4cd4-b72d-4156beab93b6   /home/newdisk
sr0         iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00
```







```powershell
#man命令自身的帮助信息
man 
[root@localhost ~]# echo $SHELL
/bin/bash
[root@localhost ~]# date
2020年 04月 10日 星期五 17:07:52 CST
[root@localhost ~]# date "+%Y-%m-%d %H:%M:%S"
2020-04-10 17:09:26
#一年的第几天
[root@localhost ~]# date "+%j"
101
#重启
[root@localhost ~]# reboot
#关机
[root@localhost ~]# poweroff
#wget 下载地址
#查看进程
[root@localhost ~]# ps
#用户以及其他详细信息
[root@localhost ~]# ps -u
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root       7073  0.0  0.0  26920  5396 pts/0    Ss   14:09   0:00 -bash
root       7315  0.0  0.0  10088  2252 pts/0    T    14:19   0:00 more
root       7413  0.0  0.1  86092  7840 pts/0    T    14:29   0:00 /bin/s
root       7424  0.0  0.0   9884  2088 pts/0    T    14:29   0:00 less
root       7426  0.0  0.0  10088  2132 pts/0    T    14:29   0:00 more
root       7739  0.0  0.1  86092  7928 pts/0    T    14:48   0:00 /bin/s
root       7750  0.0  0.0   9884  2140 pts/0    T    14:48   0:00 less
root       7802  0.0  0.0  50228  4400 pts/0    T    14:54   0:00 parted
root       7834  0.0  0.1  86092  7960 pts/0    T    14:57   0:00 system
root       7835  0.0  0.0   9884  2156 pts/0    T    14:57   0:00 less
root       7925  0.0  0.0  57380  3976 pts/0    R+   15:05   0:00 ps -u

[root@localhost ~]# ps -aux
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root          1  0.5  1.2 244496 13884 ?        Ss   17:11   0:02 /usr/lib/systemd/systemd --switched-root --system --deserialize 18
root          2  0.0  0.0      0     0 ?        S    17:11   0:00 [kthreadd]
root          3  0.0  0.0      0     0 ?        I<   17:11   0:00 [rcu_gp]
root          4  0.0  0.0      0     0 ?        I<   17:11   0:00 [rcu_par_gp]
root          6  0.0  0.0      0     0 ?        I<   17:11   0:00 [kworker/0:0H-kblockd]
root          8  0.0  0.0      0     0 ?        I<   17:11   0:00 [mm_percpu_wq]
...
#动态监视进程活动与系统负载等信息
[root@localhost ~]# top
#查看指定进程号码值（PID）
[root@localhost ~]# pidof sshd
6870 6854 955
#查看进程mysqld号码值（PID）
[root@localhost ~]# pidof mysqld
4439
[root@localhost ~]# kill 4439
[root@localhost ~]# pidof mysqld
#关闭httpd所对应的全部进程
[root@localhost ~]# killall httpd
httpd: 未找到进程
#获取网卡配置与网络状态等信息
[root@localhost ~]# ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.40.128  netmask 255.255.255.0  broadcast 192.168.40.255
        inet6 fe80::48d9:b814:b7a1:48cf  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:37:ea:2f  txqueuelen 1000  (Ethernet)
        RX packets 607  bytes 45046 (43.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 716  bytes 165463 (161.5 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 311  bytes 27435 (26.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 311  bytes 27435 (26.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:41:5a:8e  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
[root@localhost ~]# uname -a
Linux localhost 4.18.0-80.11.2.el8_0.x86_64 #1 SMP Tue Sep 24 11:32:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
#查看当前系统版本的详细信息
[root@localhost etc]# cat /etc/redhat-release
CentOS Linux release 8.0.1905 (Core) 
#查看系统的负载信息
[root@localhost etc]# uptime 
 17:35:38 up 23 min,  1 user,  load average: 0.07, 0.02, 0.06
#系统内存的使用信息
[root@localhost etc]# free -h
              total        used        free      shared  buff/cache   available
Mem:          1.1Gi       291Mi       482Mi       6.0Mi       346Mi       668Mi
Swap:         2.0Gi       3.0Mi       2.0Gi

#当前登陆主机的用户信息
[root@localhost etc]# who
root     pts/0        2020-04-10 17:14 (192.168.40.1)
#查看所有系统的登记记录
[root@localhost etc]# last
#显示历史执行过的命令
[root@localhost etc]# history
#收集系统配置及架构信息并输出诊断文档
[root@localhost ~]# sosreport
#当前工作目录
[root@localhost ~]# pwd
/root

[root@localhost ~]# ls -al
#查看目录属性
[root@localhost ~]# ls -ld /etc
drwxr-xr-x. 137 root root 8192 2月  19 10:30 /etc
#用于查看纯文本文件
[root@localhost ~]# more initial-setup-ks.cfg 
#用于查看纯文本文件的前10行
[root@localhost ~]# head -n 10 initial-setup-ks.cfg 
#用于查看纯文本文件的后10行
[root@localhost ~]# tail -n 10 initial-setup-ks.cfg 

#实时查看最新日志文件
[root@localhost ~]# tail -f 文件名
#tr 替换文本文件中的字符
[root@localhost ~]# cat anaconda-ks.cfg |tr [a-z] [A-Z]
wc -l 文本 显示行数
wc -l 文本 显示单词书
wc -l 文本 显示字节数
#查看文件的存储信息和时间
[root@localhost ~]# stat anaconda-ks.cfg
  touch install.log
  ll
  cp install.log x.log
  mv x.log linux.log


[root@localhost ~]# rm linux.log 
rm：是否删除普通空文件 'linux.log'？^C
# rm -f 强制删除
[root@localhost ~]# rm -f linux.log 
# 查看文件的类型
[root@localhost ~]# file anaconda-ks.cfg 
anaconda-ks.cfg: ASCII text
[root@localhost ~]# file /dev/sda
/dev/sda: block special (8/0)
#输出SHELL变量值
[root@localhost ~]# echo $SHELL
/bin/bash
#
[root@localhost ~]# echo $PATH

[root@localhost ~]# mkdir shell
[root@localhost ~]# cd shell/
[root@localhost shell]# vim myshell.sh
#!/bin/bash
echo "hello,world!"
[root@localhost shell]# ll
总用量 4
-rw-r--r--. 1 root root 32 4月  11 15:04 myshell.sh
[root@localhost shell]# cat myshell.sh 
#!/bin/bash
echo "hello,world!"
#赋予可执行权限
[root@localhost shell]# chmod 744 myshell.sh 
[root@localhost shell]# ls
myshell.sh
#相对路径执行
[root@localhost shell]# ./myshell.sh 
hello,world!
绝对路径执行
[root@localhost shell]# /root/shell/myshell.sh 
hello,world!
#取消可执行权限
[root@localhost shell]# chmod 644 myshell.sh 
[root@localhost shell]# ls
myshell.sh
[root@localhost shell]# sh ./myshell.sh 
hello,world!
[root@localhost shell]# bash ./myshell.sh 
hello,world!

#添加环境
[root@localhost shell]# vim /etc/profile
#使环境变量立即生效
[root@localhost shell]# source /etc/profile
#输出环境变量
[root@localhost shell]# echo $TOMCAT_HOME
/opt/apache-tomcat-9.0.30






[root@localhost ~]# type exec
exec 是 shell 内建
[root@localhost ~]# type ls
ls 是 `ls --color=auto' 的别名
[root@localhost ~]# type -a ls
ls 是 `ls --color=auto' 的别名
ls 是 /usr/bin/ls
[root@localhost ~]# type -a nc
nc 是 /usr/bin/nc





[root@localhost ~]# ifconfig
#网卡名称
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        #ip地址              #子网掩码
        inet 192.168.40.128  netmask 255.255.255.0  broadcast 192.168.40.255
              #网卡物理地址
        inet6 fe80::48d9:b814:b7a1:48cf  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:37:ea:2f  txqueuelen 1000  (Ethernet)
        #收到的数据包大小
        RX packets 100  bytes 11110 (10.8 KiB)        
        RX errors 0  dropped 0  overruns 0  frame 0
        #发送的数据包大小
        TX packets 126  bytes 12869 (12.5 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        
[root@localhost ~]# uname -a
                #内核版本
Linux localhost 4.18.0-80.11.2.el8_0.x86_64 #1 SMP Tue Sep 24 11:32:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
[root@localhost ~]# uptime
#当前时间   #运行时间  #运行终端数  #负载情况   1分钟  5分钟   #15分钟
 08:36:19 up 8 min,  1 user,  load average: 0.00, 0.45, 0.41
 #内存使用
[root@localhost ~]# free
              total        used        free      shared  buff/cache   available
Mem:        5976664     1044032     4212752       10028      719880     4660116
Swap:       2097148           0     2097148
#以G为单位 推荐
[root@localhost ~]# free -h

              total        used        free      shared  buff/cache   available
Mem:          5.7Gi       1.0Gi       4.0Gi       9.0Mi       703Mi       4.4Gi
Swap:         2.0Gi          0B       2.0Gi
#以M为单位
[root@localhost ~]# free -m
              total        used        free      shared  buff/cache   available
Mem:           5836        1021        4111           9         703        4548
Swap:          2047           0        2047

[root@localhost ~]# who
root     pts/0        2020-04-20 08:29 (192.168.40.1)
[root@localhost ~]# last
root     pts/0        192.168.40.1     Mon Apr 20 08:29   still logged in
reboot   system boot  4.18.0-80.11.2.e Mon Apr 20 08:27   still running
houyingc tty2         tty2             Sun Apr 19 21:57 - crash  (10:30)
reboot   system boot  4.18.0-80.11.2.e Sun Apr 19 21:45   still running
...
[root@localhost ~]# date
2020年 04月 20日 星期一 08:50:56 CST
#命令执行记录
[root@localhost ~]# history
...
  648  uptime
  649  free
  650  free -h
  651  free -m
  652  who
  653  last
  654  date
  655  history
[root@localhost ~]# !654
date
2020年 04月 20日 星期一 08:53:52 CST
[root@localhost ~]# !650
free -h
              total        used        free      shared  buff/cache   available
Mem:          5.7Gi       1.0Gi       4.0Gi       9.0Mi       703Mi       4.4Gi
Swap:         2.0Gi          0B       2.0Gi

[root@localhost ~]# sosreport

sosreport (version 3.6)

This command will collect diagnostic and configuration information from
this CentOS Linux system and installed applications.

An archive containing the collected information will be generated in
/var/tmp/sos.rf68p2q8 and may be provided to a CentOS support
representative.

Any information provided to CentOS will be treated in accordance with
the published support policies at:

  https://wiki.centos.org/

The generated archive may contain data considered sensitive and its
content should be reviewed by the originating organization before being
passed to any third party.

No changes will be made to system configuration.

按 ENTER 键继续，或者 CTRL-C 组合键退出。

Please enter the case id that you are generating this report for []: 

 Setting up archive ...
 Setting up plugins ...
 Running plugins. Please wait ...

  Finishing plugins              [Running: selinux]                                       
  Finished running plugins                                                               
生成压缩归档......

Your sosreport has been generated and saved in:
  /var/tmp/sosreport-localhost-2020-04-20-duuvwrd.tar.xz

The checksum is: 8a8a82d87583ef2b322975b2c11fc3cd

Please send this file to your support representative.


[root@localhost ~]# cd /var/tmp/
[root@localhost tmp]# ls
dracut.tXctnR
sosreport-localhost-2020-04-20-duuvwrd.tar.xz
sosreport-localhost-2020-04-20-duuvwrd.tar.xz.md5
systemd-private-3869d17b6df94b6882b5ed5a91b65377-bluetooth.service-Cepy1w
systemd-private-3869d17b6df94b6882b5ed5a91b65377-bolt.service-q4HYT0
systemd-private-3869d17b6df94b6882b5ed5a91b65377-chronyd.service-BVLFyG
systemd-private-3869d17b6df94b6882b5ed5a91b65377-colord.service-QIVOBo
systemd-private-3869d17b6df94b6882b5ed5a91b65377-ModemManager.service-9lcsM6
systemd-private-3869d17b6df94b6882b5ed5a91b65377-rtkit-daemon.service-F4yY10
[root@localhost tmp]# pwd
/var/tmp

[root@localhost /]# cd /etc/
[root@localhost etc]# pwd
/etc
#上一级目录
[root@localhost etc]# cd ..
[root@localhost /]# pwd
/
#上一次目录
[root@localhost /]# cd -
/etc

[root@localhost etc]# cd ~
#家目录
[root@localhost ~]# pwd
/root
#显示全部文件列表（以.号开头的是隐藏文件）
[root@localhost ~]# ls -a
#显示文件的详细信息
[root@localhost ~]# ls -al

Linux系统中的一切都是文件

#查看文件内容小文件
[root@localhost ~]# cat anaconda-ks.cfg 
#查看文件内容大/小文件
[root@localhost ~]# more anaconda-ks.cfg 
#查看前10 行
[root@localhost ~]# head -n 10 anaconda-ks.cfg 
#查看后5 行
[root@localhost ~]# tail -n 5 anaconda-ks.cfg 
#统计文件有多少行
[root@localhost ~]# wc -l anaconda-ks.cfg 
44 anaconda-ks.cfg
#统计文件有多少字节数
[root@localhost ~]# wc -c anaconda-ks.cfg 
1225 anaconda-ks.cfg
#统计文件有多少单词书
[root@localhost ~]# wc -w anaconda-ks.cfg 
115 anaconda-ks.cfg
[root@localhost ~]# stat anaconda-ks.cfg 
  文件：anaconda-ks.cfg
  大小：1225      	块：8          IO 块：4096   普通文件
设备：fd00h/64768d	Inode：34977045    硬链接：1
权限：(0600/-rw-------)  Uid：(    0/    root)   Gid：(    0/    root)
环境：system_u:object_r:admin_home_t:s0
最近访问：2020-04-20 08:57:48.628614427 +0800
最近更改：2019-12-12 23:52:11.741020940 +0800
最近改动：2019-12-12 23:52:11.741020940 +0800
创建时间：-





stat 查看
touch 修改
[root@localhost ~]# cat /etc/passwd
以：为间隔符提取第一列信息
[root@localhost ~]# cut -d : -f 1 /etc/passwd

[root@localhost ~]# mkdir -p a/b/c/d

cp     原始文件      目标文件
cp  -r  原始目录名称    目标目录名称
mv     原始文件      目标文件

#强制删除 -f(force)
rm -f 文件

rm -rf 目录文件

#查看香港的时区规则
[root@localhost ~]# zdump Hongkong
Hongkong  Tue Apr 21 18:57:10 2020 HKT
#查看中国PRC的时区规则
[root@localhost ~]# zdump -v PRC

#查看硬件时间
[root@localhost ~]# clock
2020-04-21 18:59:29.222193+08:00

#查找/root目录中为空的文件或子目录
[root@localhost ~]# find /root -empty

#只显示最近登录系统的5 个用户
[root@localhost ~]# last -n 5|awk '{print $1}'

#数据同步写入磁盘
[root@localhost ~]# sync

[root@localhost ~]# uptime
 19:25:34 up 2 min,  1 user,  load average: 0.21, 0.25, 0.10
#查看系统已登录用户
[root@localhost ~]# who -a
           系统引导 2020-04-21 19:23
           运行级别 5 2020-04-21 19:23
root     + pts/0        2020-04-21 19:25   .          2053 (192.168.40.1)
#查看系统版本相关信息
[root@localhost ~]# uname -a
Linux localhost 4.18.0-80.11.2.el8_0.x86_64 #1 SMP Tue Sep 24 11:32:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
#查看当前环境变量
[root@localhost ~]# export

#查看目录和文件占用空间
[root@localhost ~]# du -h --max-depth=1

#查看各挂载点空间
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             2.9G     0  2.9G    0% /dev
tmpfs                2.9G     0  2.9G    0% /dev/shm
tmpfs                2.9G  9.6M  2.9G    1% /run
tmpfs                2.9G     0  2.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   13G  4.3G   75% /
/dev/sda1            976M  221M  688M   25% /boot
tmpfs                584M   28K  584M    1% /run/user/42
tmpfs                584M  4.0K  584M    1% /run/user/0

#查看内存可用情况
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:          5.7Gi       977Mi       4.1Gi       9.0Mi       646Mi       4.5Gi
Swap:         2.0Gi          0B       2.0Gi


#查看进程的内存使用情况
[root@localhost ~]# ps
   PID TTY          TIME CMD
  2078 pts/0    00:00:00 bash
  2200 pts/0    00:00:00 ps
[root@localhost ~]# pmap 2078
2078:   -bash
000055640653c000   1056K r-x-- bash
0000556406843000     16K r---- bash
0000556406847000     36K rw--- bash
0000556406850000     40K rw---   [ anon ]
0000556407243000   1444K rw---   [ anon ]
00007f542ed30000   2736K r---- LC_COLLATE
00007f542efdc000     44K r-x-- libnss_files-2.28.so
00007f542efe7000   2048K ----- libnss_files-2.28.so
00007f542f1e7000      4K r---- libnss_files-2.28.so
00007f542f1e8000      4K rw--- libnss_files-2.28.so
00007f542f1e9000     24K rw---   [ anon ]
00007f542f1ef000   8212K r--s- passwd
00007f542f9f4000     32K r-x-- libnss_sss.so.2
00007f542f9fc000   2044K ----- libnss_sss.so.2
00007f542fbfb000      4K r---- libnss_sss.so.2
00007f542fbfc000      4K rw--- libnss_sss.so.2
00007f542fbfd000   1768K r-x-- libc-2.28.so
00007f542fdb7000   2048K ----- libc-2.28.so
00007f542ffb7000     16K r---- libc-2.28.so
00007f542ffbb000      8K rw--- libc-2.28.so
00007f542ffbd000     16K rw---   [ anon ]
00007f542ffc1000     12K r-x-- libdl-2.28.so
00007f542ffc4000   2044K ----- libdl-2.28.so
00007f54301c3000      4K r---- libdl-2.28.so
00007f54301c4000      4K rw--- libdl-2.28.so
00007f54301c5000    164K r-x-- libtinfo.so.6.1
00007f54301ee000   2044K ----- libtinfo.so.6.1
00007f54303ed000     16K r---- libtinfo.so.6.1
00007f54303f1000      4K rw--- libtinfo.so.6.1
00007f54303f2000    160K r-x-- ld-2.28.so
00007f54305b1000    332K r---- LC_CTYPE
00007f5430604000     20K rw---   [ anon ]
00007f5430609000      4K r---- LC_NUMERIC
00007f543060a000      4K r---- LC_TIME
00007f543060b000      4K r---- LC_MONETARY
00007f543060c000      4K r---- SYS_LC_MESSAGES
00007f543060d000      4K r---- LC_PAPER
00007f543060e000      4K r---- LC_NAME
00007f543060f000      4K r---- LC_ADDRESS
00007f5430610000      4K r---- LC_TELEPHONE
00007f5430611000      4K r---- LC_MEASUREMENT
00007f5430612000     28K r--s- gconv-modules.cache
00007f5430619000      4K r---- LC_IDENTIFICATION
00007f543061a000      4K r---- ld-2.28.so
00007f543061b000      4K rw--- ld-2.28.so
00007f543061c000      4K rw---   [ anon ]
00007ffdcd9fa000    132K rw---   [ stack ]
00007ffdcdb8b000     12K r----   [ anon ]
00007ffdcdb8e000      8K r-x--   [ anon ]
ffffffffff600000      4K r-x--   [ anon ]
 total            26644K

#查看虚拟内存统计信息
[root@localhost ~]# vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  0      0 4295404   2224 675836    0    0   205    30  108  173  1  2 97  0  0


[root@localhost ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:0c:29:37:ea:2f brd ff:ff:ff:ff:ff:ff
    inet 192.168.40.128/24 brd 192.168.40.255 scope global noprefixroute ens33
       valid_lft forever preferred_lft forever
    inet6 fe80::48d9:b814:b7a1:48cf/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:41:5a:8e brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
4: virbr0-nic: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel master virbr0 state DOWN group default qlen 1000
    link/ether 52:54:00:41:5a:8e brd ff:ff:ff:ff:ff:ff

#查看网络连接状态
[root@localhost ~]# netstat

#查看进程间关系
[root@localhost ~]# pstree

#显示所有进程信息
[root@localhost ~]# ps -A
PID TTY          TIME CMD
     1 ?        00:00:03 systemd
     2 ?        00:00:00 kthreadd
     3 ?        00:00:00 rcu_gp
     4 ?        00:00:00 rcu_par_gp
     6 ?        00:00:00 kworker/0:0H-kblockd
...


[root@localhost ~]# ps -aux

[root@localhost ~]# ps -aux|more
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root          1  0.3  0.2 244488 13708 ?        Ss   19:23   0:03 /usr/lib/systemd/systemd --switched-root --system --deserialize 18
root          2  0.0  0.0      0     0 ?        S    19:23   0:00 [kthreadd]
root          3  0.0  0.0      0     0 ?        I<   19:23   0:00 [rcu_gp]
root          4  0.0  0.0      0     0 ?        I<   19:23   0:00 [rcu_par_gp]
root          6  0.0  0.0      0     0 ?        I<   19:23   0:00 [kworker/0:0H-kblockd]
root          8  0.0  0.0      0     0 ?        I<   19:23   0:00 [mm_percpu_wq]
root          9  0.0  0.0      0     0 ?        S    19:23   0:00 [ksoftirqd/0]
root         10  0.0  0.0      0     0 ?        I    19:23   0:00 [rcu_sched]
root         11  0.0  0.0      0     0 ?        S    19:23   0:00 [migration/0]
root         12  0.0  0.0      0     0 ?        S    19:23   0:00 [watchdog/0]
root         13  0.0  0.0      0     0 ?        S    19:23   0:00 [cpuhp/0]
root         14  0.0  0.0      0     0 ?        S    19:23   0:00 [cpuhp/1]
root         15  0.0  0.0      0     0 ?        S    19:23   0:00 [watchdog/1]
root         16  0.0  0.0      0     0 ?        S    19:23   0:00 [migration/1]
root         17  0.0  0.0      0     0 ?        S    19:23   0:00 [ksoftirqd/1]
root         19  0.0  0.0      0     0 ?        I<   19:23   0:00 [kworker/1:0H-kblockd]
root         20  0.0  0.0      0     0 ?        S    19:23   0:00 [cpuhp/2]
root         21  0.0  0.0      0     0 ?        S    19:23   0:00 [watchdog/2]
root         22  0.0  0.0      0     0 ?        S    19:23   0:00 [migration/2]
root         23  0.0  0.0      0     0 ?        S    19:23   0:00 [ksoftirqd/2]
root         25  0.0  0.0      0     0 ?        I<   19:23   0:00 [kworker/2:0H-kblockd]
root         26  0.0  0.0      0     0 ?        S    19:23   0:00 [cpuhp/3]
root         27  0.0  0.0      0     0 ?        S    19:23   0:00 [watchdog/3]
root         28  0.0  0.0      0     0 ?        S    19:23   0:00 [migration/3]
--更多--




[root@localhost ~]# nmtui

[root@localhost ~]# uname
Linux
[root@localhost ~]# uname -a
Linux localhost 4.18.0-80.11.2.el8_0.x86_64 #1 SMP Tue Sep 24 11:32:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
[root@localhost ~]# cat /etc/redhat-release 
CentOS Linux release 8.0.1905 (Core)

[root@localhost ~]# uptime
 20:06:54 up 43 min,  1 user,  load average: 0.00, 0.00, 0.00
当前系统时间、系统已运行时间、启用终端数量以
及平均负载值等信息。平均负载值指的是系统在最近 1 分钟、5 分钟、15 分钟内的压力情
况（下面加粗的信息部分）；负载值越低越好，尽量不要长期超过 1，在生产环境中不要
超过 5。

[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:          5.7Gi       987Mi       4.1Gi       9.0Mi       672Mi       4.5Gi
Swap:         2.0Gi          0B       2.0Gi

#cut -d 间隔符 -f 列号 文件名称
#grep 关键词 文件名称
[root@localhost ~]# grep password initial-setup-ks.cfg 
# Root password
# -n 在第几行出现的
[root@localhost ~]# grep -n  password initial-setup-ks.cfg 
24:# Root password



[root@localhost ~]# ls -l /etc/ | more


[root@localhost ~]# ls -l /dev/sda*
brw-rw----. 1 root disk 8, 0 4月  21 19:23 /dev/sda
brw-rw----. 1 root disk 8, 1 4月  21 19:23 /dev/sda1
brw-rw----. 1 root disk 8, 2 4月  21 19:23 /dev/sda2

[root@localhost ~]# ls -l /dev/sda?
brw-rw----. 1 root disk 8, 1 4月  21 19:23 /dev/sda1
brw-rw----. 1 root disk 8, 2 4月  21 19:23 /dev/sda2
[root@localhost ~]# ls -l /dev/sda[0-9]
brw-rw----. 1 root disk 8, 1 4月  21 19:23 /dev/sda1
brw-rw----. 1 root disk 8, 2 4月  21 19:23 /dev/sda2

[root@localhost ~]# price=5
[root@localhost ~]# echo "Price is $price"
Price is 5
#\ 是转义字符
[root@localhost ~]# echo "Price is \$$price"
Price is $5



[root@localhost ~]# echo `uname -a`
Linux localhost 4.18.0-80.11.2.el8_0.x86_64 #1 SMP Tue Sep 24 11:32:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux


[root@localhost ~]# find / -name fstab
/etc/fstab
/var/lib/docker/overlay2/dcdb6ab119eb6bfd7b711c69ee8ae03bc10b0ae6d9fe8b6cc4537ffca8b5f1bc/diff/etc/fstab

#查找归属于root用户的文件
[root@localhost ~]# find / -user root





[root@localhost ~]# echo $PATH
//opt/apache-maven-3.6.3/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/module/jdk1.8.0_144/bin:/usr/local/mycat/bin:/root/bin

[root@localhost ~]# echo $HOME
/root
#历史命令记录条数
[root@localhost ~]# echo $HISTSIZE
1000
[root@localhost ~]# echo $HISTFILESIZE
1000
#当前用户邮件保存位置
[root@localhost ~]# echo $MAIL
/var/spool/mail/root
#系统语言语系名称
[root@localhost ~]# echo $LANG
zh_CN.UTF-8
#生成一个随机数字
[root@localhost ~]# echo $RANDOM
18888

[root@localhost ~]# echo $PS1
[\u@\h \W]\$


[root@localhost ~]# CHOUCHOU=/home/houyingchao/
[root@localhost ~]# cd $CHOUCHOU
[root@localhost houyingchao]# pwd
/home/houyingchao
#使用export提升到全局变量
[root@localhost houyingchao]# export CHOUCHOU
[houyingchao@localhost ~]$ echo $CHOUCHOU
/home/houyingchao/

                                                      
#进入vim自带的联系教程
[root@localhost ~]# vimtutor

#设置主机名称
[root@localhost ~]# vim /etc/hostname 
[root@localhost ~]# cat /etc/hostname 
linuxprobe.com
重启

#网卡的配置文件
[root@linuxprobe ~]# vim /etc/sysconfig/network-scripts/ifcfg-ens33 

  1 TYPE=Ethernet
  2 PROXY_METHOD=none
  3 BROWSER_ONLY=no
  4 BOOTPROTO=static
  5 DEFROUTE=yes
  6 IPV4_FAILURE_FATAL=no
  7 IPV6INIT=yes
  8 IPV6_AUTOCONF=yes
  9 IPV6_DEFROUTE=yes
 10 IPV6_FAILURE_FATAL=no
 11 IPV6_ADDR_GEN_MODE=stable-privacy
 12 NAME=ens33
 13 UUID=5aed5d2c-abf6-4c1c-b4fa-9393d18a3d9b
 14 DEVICE=ens33
 15 ONBOOT=yes
 16  
 17 IPADDR=192.168.40.128
 18 NETMASK=255.255.255.0
 19 GATEWAY=192.168.40.2




[root@localhost ~]# cat xiaoqing

#!/bin/bash
#测试的

#输出脚本名称
echo "$0"
echo "$1,$3,$5"

echo "$#,$*"

[root@localhost ~]# bash xiaoqing 
xiaoqing
,,
0,
[root@localhost ~]# bash xiaoqing 78 9 34
xiaoqing
78,34,
3,78 9 34



#判断语句
[root@localhost ~]# [ -e /etc/fstab ]
#$?上一条语句是否执行成功
[root@localhost ~]# echo $?
0

#-d 判断是否是目录
[root@localhost ~]# [ -d /etc/fstab ]
[root@localhost ~]# echo $?
1

# -f 判断是否是文本文件
[root@localhost ~]# [ -f /etc/fstab ]
[root@localhost ~]# echo $?
0

[root@localhost ~]# [ -f /etc/fstab ] && echo "ok"
ok

#当前用户的名称
[root@localhost ~]# echo $USER
root




[root@localhost ~]# [ $USER = root ] && echo "是超级管理员"
是超级管理员
[root@localhost ~]# su houyingchao
[houyingchao@localhost root]$ [ $USER = root ] && echo "是超级管理员"
[houyingchao@localhost root]$ [ $USER = root ] && echo "是超级管理员" || echo "是一个普通用户"
是一个普通用户




[root@localhost ~]# [ 10 = 10 ]
[root@localhost ~]# echo $?
0

[root@localhost ~]# [ 10 > 5 ]
[root@localhost ~]# echo $?
0
# -ge 大于等于
[root@localhost ~]# [ 10 -ge 5 ]
[root@localhost ~]# echo $?
0
[root@localhost ~]# [ 10 -ge 15 ]
[root@localhost ~]# echo $?
1
[root@localhost ~]# [ $? -eq 0 ]
[root@localhost ~]# echo $?
1

[root@localhost ~]# KXNC=`free -m | grep Mem: | awk '{print $4}'`
[root@localhost ~]# echo $KXNC
4151

[root@localhost ~]# [ $KXNC -lt 1024 ] && echo "内存不足" || echo "内存充足"
内存充足

[root@localhost ~]# [ `free -m | grep Mem: | awk '{print $4}'` -lt 1024 ] && echo "内存不足" || echo ‘"内存充足"
‘内存充足


[ `free -m | grep Mem: | awk '{print $4}'` -lt 1024 ] && echo "内存不足" || echo ‘"内存充足"


#-z 判断变量HAHA 有没被使用
[root@localhost ~]# [ -z $HAHA ]
[root@localhost ~]# echo $?
0
[root@localhost ~]# echo $HAHA

[root@localhost ~]# HAHA=5
[root@localhost ~]# HAHA=15
[root@localhost ~]# HAHA=20
[root@localhost ~]# echo $HAHA
20
[root@localhost ~]# [ -z $HAHA ]
[root@localhost ~]# echo $HAHA
20
[root@localhost ~]# [ -z $HAHA ]
[root@localhost ~]# echo $?
1
[root@localhost ~]# echo $HAHA
20



[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
#如果不存在haha文件则创建一个
if [ ! -e /media/haha ]
then 
	mkdir -p /media/haha
fi




#开启192.168.40.100虚拟机
[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
#    3次  间隔0.2秒 
ping -c 3 -i 0.2 -W 3  $1 &> /dev/null
if [ $? -eq 0 ]
then echo "$1 is On-line"
else
	echo "$1 is Off-line"
fi

[root@localhost ~]# bash xiaoguo.sh 192.168.40.100
192.168.40.100 is On-line
[root@localhost ~]# bash xiaoguo.sh 192.168.40.102
192.168.40.102 is Off-line
[root@localhost ~]# 




[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
read -p "Enter:" GRADE
            #大于等于           小于等于  
if [ $GRADE -ge 85 ] && [ $GRADE -le 100 ]
then
	echo " $GRADE is Excellent"
elif  [ $GRADE -ge 70 ] && [ $GRADE -le 84 ]
then echo " $GRADE is Pass"
else 
echo "$GRADE is 不及格"
fi
[root@localhost ~]# bash xiaoguo.sh 
Enter:89
 89 is Excellent
[root@localhost ~]# bash xiaoguo.sh 
Enter:76
 76 is Pass
[root@localhost ~]# bash xiaoguo.sh 
Enter:35
35 is 不及格



[root@localhost ~]# id houyingchao
uid=1000(houyingchao) gid=1000(houyingchao) 组=1000(houyingchao),10(wheel)
[root@localhost ~]# id root
uid=0(root) gid=0(root) 组=0(root)

[root@localhost ~]# id zhangsan
id: “zhangsan”：无此用户


#批量创建用户
[root@localhost ~]# vim user.txt
[root@localhost ~]# cat user.txt
andy
barry
carl
duke
eric
george

[root@localhost ~]# vim xiaoguo.sh 
[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
read -p "Enter:" PASSWD
for UNAME in `cat user.txt`
do 
	id $UNAME &> /dev/null 
	if [ $? -eq 0 ]
	then 
		echo "Already exist"
	else
		useradd $UNAME &> /dev/null
		echo "$PASSWD" | passwd --stdin $UNAME &> /dev/null
		if [ $? -eq 0 ]
		then 
			echo "$UNAME ,Create success"
		else 
			echo "$UNAME ,Create failure"
		fi

	fi
done


[root@localhost ~]# bash xiaoguo.sh 
Enter:
andy ,Create failure
barry ,Create failure
carl ,Create failure
duke ,Create failure
eric ,Create failure
george ,Create failure
[root@localhost ~]# bash xiaoguo.sh 
Enter:123456
Already exist
Already exist
Already exist
Already exist
Already exist
Already exist
[root@localhost ~]# vim user.txt 
[root@localhost ~]# bash xiaoguo.sh 
Enter:123456
Already exist
Already exist
Already exist
Already exist
Already exist
Already exist
hyc ,Create success
tl ,Create success
yt ,Create success




[root@localhost ~]# vim xiaoguo.sh 
[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
for IP in `cat ip.txt`
do
   #ping3次 间隔0.2秒  等待时间3秒  输出内容到黑洞文件
	ping -c 3 -i 0.2 -W 3 $IP &> /dev/null
if [ $? -eq 0 ]
then 
	echo "Host $IP is On-line"
else
	echo "Host $IP is Off-line"
fi
done

[root@localhost ~]# bash xiaoguo.sh 
Host 192.168.40.128 is On-line
Host 192.168.40.129 is Off-line
Host 192.168.40.130 is Off-line
Host 192.168.40.131 is Off-line




[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
#产生一个随机数并1000取余
PRICE=$(expr $RANDOM % 1000)
TIMES=0
while true
do 
    #输入一个数字
	read -p "Enter:" INT
	let TIMES++
if [ $INT -eq $PRICE ]
then 
       echo "OKOKOK"
       echo "$TIMES"
exit 0

elif [ $INT -gt $PRICE ]
then
	echo "太高了"
else
	echo "太低了"
fi
done
[root@localhost ~]# bash xiaoguo.sh 
Enter:500
太低了
Enter:700
太低了
Enter:850
太高了
Enter:830
太高了
Enter:820
太高了
Enter:800
太高了
Enter:750
太高了
Enter:730
太高了
Enter:710
OKOKOK
9



[root@localhost ~]# cat xiaoguo.sh 
#!/bin/bash
#注释说明
#提示用户输入一个值
read -p "Enter:" KEY
case "$KEY" in      
       [a-z]|[A-Z])
	       	echo "字母"
		;;
	[0-9])
		echo "数字"
		;;
	*)
	       	echo "错误"
esac
[root@localhost ~]# bash xiaoguo.sh 
Enter:8
数字
[root@localhost ~]# bash xiaoguo.sh 
Enter:h
字母
[root@localhost ~]# bash xiaoguo.sh 
Enter:156
错误
[root@localhost ~]# bash xiaoguo.sh 
Enter:uuu
错误





[root@linuxprobe ~]# at 23:30 
at > systemctl restart httpd 
at > 此处请同时按下 Ctrl + D 组合键来结束编写计划任务
job 3 at Mon Apr 27 23:30:00 2017 
[root@linuxprobe ~]# at -l 
3 Mon Apr 27 23:30:00 2017 a root


[root@localhost ~]# at 17:10
warning: commands will be executed using /bin/sh
at> ls 
at> reboot
at> <EOT>
job 3 at Wed Apr 22 17:10:00 2020
[root@localhost ~]# 
[root@localhost ~]# at -l
1	Wed Apr 22 17:10:00 2020 a root
4	Thu Apr 23 17:10:00 2020 a root
5	Wed Apr 22 17:15:00 2020 a root
[root@localhost ~]# at -c 5
#删除定时任务  atrm + 编号
[root@localhost ~]# atrm 4




crond  服务名称
crontab 配置工具
分   时   日   月   星期    命令
[root@localhost ~]# whereis reboot
reboot: /usr/sbin/reboot /usr/share/man/man8/reboot.8.gz /usr/share/man/man2/reboot.2.gz
[root@localhost ~]# whereis tar
tar: /usr/bin/tar /usr/include/tar.h /usr/share/man/man5/tar.5.gz /usr/share/man/man1/tar.1.gz /usr/share/info/tar.info-1.gz /usr/share/info/tar.info-2.gz /usr/share/info/tar.info.gz

[root@localhost ~]# crontab -e

no crontab for root - using an empty one
crontab: installing new crontab
[root@localhost ~]# crontab -l
* * * * * /usr/sbin/reboot

每天30 分钟 凌晨2点 每个月的8号 5月 周三（日期和星期不要同时写）
30  2  8  5  3  /usr/bin/tar czvf xxx
30 2-4 * * * 命令 



[root@localhost home]# ls
houyingchao  hyc  mycat  yt
[root@localhost home]# id yt
uid=1010(yt) gid=1010(yt) 组=1010(yt)
[root@localhost home]# userdel yt
[root@localhost home]# ls
houyingchao  hyc  mycat  yt
[root@localhost home]# rm -rf yt
[root@localhost home]# ls
houyingchao  hyc  mycat
[root@localhost home]# 



Linux系统三种用户身份：
1：管理员 root UID：0
[root@localhost ~]# id root
uid=0(root) gid=0(root) 组=0(root)
2：系统用户     UID：1~999
           RHEL5/6 UID：1~499
管理一个一个服务
[root@localhost home]# id mysql
uid=27(mysql) gid=27(mysql) 组=27(mysql)

3：普通用户      UID：1000~
		   RHEL5/6 UID：500~65535
		   
[root@localhost ~]# id houyingchao
uid=1000(houyingchao) gid=1000(houyingchao) 组=1000(houyingchao),10(wheel)
[root@localhost ~]# id hyc
uid=1008(hyc) gid=1008(hyc) 组=1008(hyc)


[root@localhost home]# useradd xiaoguo
[root@localhost home]# id xiaoguo
uid=1009(xiaoguo) gid=1009(xiaoguo) 组=1009(xiaoguo)
# -u 指定UID号码
[root@localhost home]# useradd -u 6666 yaotao
[root@localhost home]# id yaotao 
uid=6666(yaotao) gid=6666(yaotao) 组=6666(yaotao)


#-s /sbin/nologin不能登录终端
[root@localhost home]# useradd -u 8888 -s /sbin/nologin xiaozheng 
[root@localhost home]# id xiaozheng
uid=8888(xiaozheng) gid=8888(xiaozheng) 组=8888(xiaozheng)
[root@localhost home]# su - xiaozheng
This account is currently not available.

基本组     扩展组

[root@localhost home]# groupadd xiaozhai
[root@localhost home]# cat /etc/group


[root@localhost home]# id xiaoguo
uid=1009(xiaoguo) gid=1009(xiaoguo) 组=1009(xiaoguo)
# -G 加入扩展组
[root@localhost home]# usermod -G xiaozhai xiaoguo
[root@localhost home]# id xiaoguo
uid=1009(xiaoguo) gid=1009(xiaoguo) 组=1009(xiaoguo),8889(xiaozhai)



[root@localhost home]# id houyingchao
uid=1000(houyingchao) gid=1000(houyingchao) 组=1000(houyingchao),10(wheel)
[root@localhost home]# usermod -u 9999 houyingchao
[root@localhost home]# id houyingchao
uid=9999(houyingchao) gid=1000(houyingchao) 组=1000(houyingchao),10(wheel)
[root@localhost home]# vim /etc/group


[root@localhost home]# passwd
管理员是可以重置所有人的密码，普通用户只能重置自己的密码



[root@localhost home]# passwd hyc
更改用户 hyc 的密码 。
新的 密码：
无效的密码： 密码少于 8 个字符
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。
[root@localhost home]# passwd hyc
更改用户 hyc 的密码 。
新的 密码：
无效的密码： 密码少于 8 个字符
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。



[root@localhost home]# passwd -l hyc
锁定用户 hyc 的密码 。
passwd: 操作成功



[root@localhost home]# cd /home/
[root@localhost home]# ls
houyingchao  hyc  mycat  xiaoguo  xiaozheng  yaotao
[root@localhost home]# userdel -r yaotao
[root@localhost home]# ls
houyingchao  hyc  mycat  xiaoguo  xiaozheng

```

![image-20200422191041602](linux.assets/image-20200422191041602.png)

```powershell
rwxrw-rw-
766

rwxrw-r--
764


chmod 权限

chown 属性


[root@localhost ~]# ll
-rw-------. 1 root root     1225 12月 12 23:52 anaconda-ks.cfg
[root@localhost ~]# chmod 315 anaconda-ks.cfg 
[root@localhost ~]# ll
--wx--xr-x. 1 root root     1225 12月 12 23:52 anaconda-ks.cfg
[root@localhost ~]# chmod 657 anaconda-ks.cfg 
[root@localhost ~]# ll
-rw-r-xrwx. 1 root root     1225 12月 12 23:52 anaconda-ks.cfg
[root@localhost ~]# chown hyc:hyc anaconda-ks.cfg 
[root@localhost ~]# ll
-rw-r-xrwx. 1 hyc  hyc      1225 12月 12 23:52 anaconda-ks.cfg

[root@localhost ~]# su - hyc
[hyc@localhost ~]$ cat /etc/shadow
cat: /etc/shadow: 权限不够
[hyc@localhost ~]$ ls -l /etc/shadow
----------. 1 root root 1561 4月  22 19:03 /etc/shadow
[hyc@localhost ~]$ exit
注销
[root@localhost ~]# cat /etc/shadow


[root@localhost ~]# whereis passwd
passwd: /usr/bin/passwd /etc/passwd /usr/share/man/man5/passwd.5.gz /usr/share/man/man1/passwd.1.gz
[root@localhost ~]# ls -l /usr/bin/passwd
-rwsr-xr-x. 1 root root 34928 5月  11 2019 /usr/bin/passwd


SUID： rwsrwxrwx
rwx rws       rw- rwR
chmod  u+s 文件名

SGID：
1：文件   ------
2：目录   
chmod -R g+s  文件名


[root@localhost ~]# cd
[root@localhost home]# mkdir xiaoyun
[root@localhost home]# ll
drwxr-xr-x.  2 root        root           6 4月  23 07:03 xiaoyun
#让所有人都获得读写操作
[root@localhost home]# chmod -R 777 xiaoyun/
[root@localhost home]# ll
drwxrwxrwx.  2 root        root           6 4月  23 07:03 xiaoyun
#获得SGID权限
[root@localhost home]# chmod -R g+s xiaoyun/
[root@localhost home]# ls -ld xiaoyun
drwxrwsrwx. 2 root root 6 4月  23 07:03 xiaoyun





[houyingchao@localhost ~]$ touch haha
[houyingchao@localhost ~]$ ll haha
-rw-rw-r--. 1 houyingchao houyingchao 0 4月  23 07:20 haha

[houyingchao@localhost xiaoyun]$ cd /home/xiaoyun/
[houyingchao@localhost xiaoyun]$ touch haha
[houyingchao@localhost xiaoyun]$ ll
总用量 0
-rw-rw-r--. 1 houyingchao root 0 4月  23 07:22 haha



SBIT：保护位，粘滞位
chmod -R o+t 目录名
rwx  rwt
rw-  rwT


[houyingchao@localhost xiaoyun]$ cd /tmp
[houyingchao@localhost tmp]$ pwd
/tmp
[houyingchao@localhost tmp]$ ls -ld ./
drwxrwxrwt. 36 root root 4096 4月  23 07:22 ./



SUID 4  SGID 2  SBIT 1


rwSr-s-wT
rw-r-x-w-
7652
7654
rwSr-sr-T

文件rwx  r读取文件内容的权限，w修改文件内容的权限，x若文件是个脚本有执行该文件的权限
目录rwx  r读取目录内文件列表的权限，w对目录内文件进行新建删除重命名等操作，x进入一个目录的权限




#隐藏权限
chattr  设置
+ 添加
- 取消



lsattr  查看


[root@localhost ~]# ll wget-log
-rw-r--r--. 1 root root 346 12月 28 20:22 wget-log
[root@localhost ~]# chattr +i wget-log
[root@localhost ~]# rm -rf wget-log
rm: 无法删除'wget-log': 不允许的操作
[root@localhost ~]# lsattr wget-log
----i------------- wget-log
[root@localhost ~]# chattr -i wget-log
[root@localhost ~]# ll wget-log
-rw-r--r--. 1 root root 346 12月 28 20:22 wget-log
[root@localhost ~]# rm wget-log
rm：是否删除普通文件 'wget-log'？
[root@localhost ~]# 


[root@localhost ~]# 
#新建一个文件
[root@localhost ~]# touch yctest
#并添加隐藏权限
[root@localhost ~]# chattr +i yctest 
[root@localhost ~]# ll yctest 
-rw-r--r--. 1 root root 0 4月  23 08:13 yctest
#尝试删除，
[root@localhost ~]# rm -f yctest 
rm: 无法删除'yctest': 不允许的操作
#查看隐藏权限
[root@localhost ~]# lsattr yctest 
----i------------- yctest
#取消隐藏权限
[root@localhost ~]# chattr -i yctest 
[root@localhost ~]# rm -f yctest 



[root@localhost ~]# chattr +i anaconda-ks.cfg 
[root@localhost ~]# lsattr anaconda-ks.cfg 
----i------------- anaconda-ks.cfg
[root@localhost ~]# chattr -i anaconda-ks.cfg 
[root@localhost ~]# lsattr anaconda-ks.cfg 
------------------ anaconda-ks.cfg



[root@localhost ~]# lsattr anaconda-ks.cfg 
------------------ anaconda-ks.cfg
[root@localhost ~]# chattr +a anaconda-ks.cfg 
[root@localhost ~]# lsattr anaconda-ks.cfg 
-----a------------ anaconda-ks.cfg
[root@localhost ~]# chattr -a anaconda-ks.cfg 
[root@localhost ~]# lsattr anaconda-ks.cfg 
------------------ anaconda-ks.cfg


[root@localhost ~]# cd /home/
[root@localhost home]# ll
总用量 8
drwx------. 22 houyingchao houyingchao 4096 4月  23 07:20 houyingchao
drwx------. 15 hyc         hyc         4096 4月  22 19:44 hyc
drwx------.  3 lisi        lisi          99 4月  23 08:04 lisi
drwx------.  3 mycat       mycat         78 1月  31 17:27 mycat
drwx------.  3 xiaoguo     xiaoguo       78 4月  22 18:33 xiaoguo
drwxrwsrwx.  2 root        root          18 4月  23 07:22 xiaoyun
drwx------.  3 xiaozheng   xiaozheng     78 4月  22 18:37 xiaozheng
drwx------.  3 zhangsan    zhangsan      99 4月  23 07:36 zhangsan
[root@localhost home]# mkdir xiaoqian
[root@localhost home]# ll xiaoqian/
总用量 0
[root@localhost home]# chmod -R 000 xiaoqian/
[root@localhost home]# ls -ld xiaoqian/
d---------. 2 root root 6 4月  23 08:29 xiaoqian/
[root@localhost home]# id zhangsan 
uid=10000(zhangsan) gid=10000(zhangsan) 组=10000(zhangsan)
[root@localhost home]# id lisi 
uid=10001(lisi) gid=10001(lisi) 组=10001(lisi)
#  R对于目录设置 m设置facl权限   u对用户  g对组
[root@localhost home]# setfacl -Rm u:zhangsan:rwx xiaoqian
[root@localhost home]# ls -ld xiaoqian
d---rwx---+ 2 root root 6 4月  23 08:29 xiaoqian
[root@localhost home]# getfacl xiaoqian
# file: xiaoqian
# owner: root
# group: root
user::---
user:zhangsan:rwx
group::---
mask::rwx
other::---
[root@localhost home]# su - zhangsan 
[zhangsan@localhost ~]$ cd /home/xiaoqian/
[zhangsan@localhost xiaoqian]$ touch haha
[zhangsan@localhost xiaoqian]$ ls
haha
[zhangsan@localhost xiaoqian]$ mv haha hoho
[zhangsan@localhost xiaoqian]$ ls
hoho
[zhangsan@localhost xiaoqian]$ rm hoho 
[zhangsan@localhost xiaoqian]$ ll
总用量 0
[zhangsan@localhost xiaoqian]$ exit
注销

[root@localhost home]# su - lisi
[lisi@localhost ~]$ cd /home/xiaoqian/
-bash: cd: /home/xiaoqian/: 权限不够
[lisi@localhost ~]$ exit
注销



su - 用户


[root@localhost home]# 
[root@localhost home]# su - zhangsan
[zhangsan@localhost ~]$ su - lisi
密码：
[lisi@localhost ~]$ 
[lisi@localhost ~]$ exit
注销
[zhangsan@localhost ~]$ exit
注销



sudo
#查询关机命令
[root@localhost ~]# whereis poweroff
poweroff: /usr/sbin/poweroff /usr/share/man/man8/poweroff.8.gz
#为lisi用户添加关机权限
[root@localhost home]# visudo
100 root    ALL=(ALL)       ALL
101 lisi    ALL=(ALL)       /usr/sbin/poweroff
#切换zhangsan用户
[root@localhost home]# su - zhangsan
#执行关机命令
[zhangsan@localhost ~]$ poweroff
User root is logged in on sshd.
User houyingchao is logged in on sshd.
Please retry operation after closing inhibitors and logging out other users.
Alternatively, ignore inhibitors and users with 'systemctl poweroff -i'.
[zhangsan@localhost ~]$ exit
注销
[root@localhost home]# su - lisi
[lisi@localhost ~]$ poweroff
User root is logged in on sshd.
User houyingchao is logged in on sshd.
Please retry operation after closing inhibitors and logging out other users.
Alternatively, ignore inhibitors and users with 'systemctl poweroff -i'.
[lisi@localhost ~]$ sudo poweroff 

我们信任您已经从系统管理员那里了解了日常注意事项。
总结起来无外乎这三点：

    #1) 尊重别人的隐私。
    #2) 输入前要先考虑(后果和风险)。
    #3) 权力越大，责任越大。

[sudo] lisi 的密码：
Connection closing...Socket close.

Connection closed by foreign host.
```







### 给虚拟机添加硬盘

```powershell
1.添加步骤
[root@localhost ~]# ls -l /dev/sd*
brw-rw----. 1 root disk 8,  0 4月  23 10:58 /dev/sda
brw-rw----. 1 root disk 8,  1 4月  23 10:58 /dev/sda1
brw-rw----. 1 root disk 8,  2 4月  23 10:58 /dev/sda2
brw-rw----. 1 root disk 8, 16 4月  23 10:58 /dev/sdb
#为新添加的 /dev/sdb进行分区操作

欢迎使用 fdisk (util-linux 2.32.1)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。

设备不包含可识别的分区表。
创建了一个磁盘标识符为 0xfd65ef16 的新 DOS 磁盘标签。

命令(输入 m 获取帮助)：m

帮助：

  DOS (MBR)
   a   开关 可启动 标志
   b   编辑嵌套的 BSD 磁盘标签
   c   开关 dos 兼容性标志

  常规
   d   删除分区
   F   列出未分区的空闲区
   l   列出已知分区类型
   n   添加新分区
   p   打印分区表
   t   更改分区类型
   v   检查分区表
   i   打印某个分区的相关信息

  杂项
   m   打印此菜单
   u   更改 显示/记录 单位
   x   更多功能(仅限专业人员)

  脚本
   I   从 sfdisk 脚本文件加载磁盘布局
   O   将磁盘布局转储为 sfdisk 脚本文件

  保存并退出
   w   将分区表写入磁盘并退出
   q   退出而不保存更改

  新建空磁盘标签
   g   新建一份 GPT 分区表
   G   新建一份空 GPT (IRIX) 分区表
   o   新建一份的空 DOS 分区表
   s   新建一份空 Sun 分区表


命令(输入 m 获取帮助)：n
分区类型
   p   主分区 (0个主分区，0个扩展分区，4空闲)
   e   扩展分区 (逻辑分区容器)
选择 (默认 p)：p
分区号 (1-4, 默认  1): 
第一个扇区 (2048-10485759, 默认 2048): 
上个扇区，+sectors 或 +size{K,M,G,T,P} (2048-10485759, 默认 10485759): 

创建了一个新分区 1，类型为“Linux”，大小为 5 GiB。

命令(输入 m 获取帮助)：p
Disk /dev/sdb：5 GiB，5368709120 字节，10485760 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xfd65ef16

设备       启动  起点     末尾     扇区 大小 Id 类型
/dev/sdb1        2048 10485759 10483712   5G 83 Linux

命令(输入 m 获取帮助)：w
分区表已调整。
将调用 ioctl() 来重新读分区表。
正在同步磁盘。

[root@localhost ~]# ls -l /dev/sdb*
brw-rw----. 1 root disk 8, 16 4月  23 11:10 /dev/sdb
brw-rw----. 1 root disk 8, 17 4月  23 11:10 /dev/sdb1


#同步分区信息
[root@localhost ~]# partprobe
Warning: 无法以读写方式打开 /dev/sr0 (只读文件系统)。/dev/sr0 已按照只读方式打开。
[root@localhost ~]# ls -l /dev/sdb*
brw-rw----. 1 root disk 8, 16 4月  23 11:13 /dev/sdb
brw-rw----. 1 root disk 8, 17 4月  23 11:13 /dev/sdb1


#格式化

[root@localhost ~]# mkfs 按Tab键2下，即可列举出所有可以格式化的文件类型
mkfs         mkfs.ext2    mkfs.ext4    mkfs.minix   mkfs.vfat    
mkfs.cramfs  mkfs.ext3    mkfs.fat     mkfs.msdos   mkfs.xfs  

#将/dev/sdb1格式化成xfs文件系统
[root@localhost ~]# mkfs.xfs /dev/sdb1 
meta-data=/dev/sdb1              isize=512    agcount=4, agsize=327616 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=1310464, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0

#挂载（将设备与目录进行关联的动作叫做挂载）
#新建要挂载的目录
[root@localhost ~]# mkdir /newdisk
#挂载
[root@localhost ~]# mount /dev/sdb1 /newdisk
#查看所有的挂载信息
[root@localhost ~]# df -h 
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             2.9G     0  2.9G    0% /dev
tmpfs                2.9G     0  2.9G    0% /dev/shm
tmpfs                2.9G  9.6M  2.9G    1% /run
tmpfs                2.9G     0  2.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   13G  4.2G   76% /
/dev/sda1            976M  221M  688M   25% /boot
tmpfs                584M   28K  584M    1% /run/user/42
tmpfs                584M  4.0K  584M    1% /run/user/0
/dev/sdb1            5.0G   68M  5.0G    2% /newdisk

#到此为单次有效，重启失效

#修改 /etc/fstab 文件
[root@localhost ~]# vim /etc/fstab 
[root@localhost ~]# cat /etc/fstab 

#
# /etc/fstab
# Created by anaconda on Thu Dec 12 10:38:19 2019
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/cl-root     /                       xfs     defaults        0 0
UUID=ff037b8a-c85b-4d17-a919-7ab0b5ffb7e6 /boot                   ext4    defaults        1 2
/dev/mapper/cl-swap     swap                    swap    defaults        0 0


/dev/sdb1               /newdisk                xfs     defaults        0 0
#挂载fstab中所有的文件系统
[root@localhost ~]# mount -a
#查看所有的挂载信息
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             2.9G     0  2.9G    0% /dev
tmpfs                2.9G     0  2.9G    0% /dev/shm
tmpfs                2.9G  9.6M  2.9G    1% /run
tmpfs                2.9G     0  2.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   13G  4.2G   76% /
/dev/sda1            976M  221M  688M   25% /boot
tmpfs                584M   28K  584M    1% /run/user/42
tmpfs                584M  4.0K  584M    1% /run/user/0
/dev/sdb1            5.0G   68M  5.0G    2% /newdisk
[root@localhost ~]# reboot
#挂载成功
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             2.9G     0  2.9G    0% /dev
tmpfs                2.9G     0  2.9G    0% /dev/shm
tmpfs                2.9G  9.7M  2.9G    1% /run
tmpfs                2.9G     0  2.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   13G  4.1G   76% /
/dev/sdb1            5.0G   68M  5.0G    2% /newdisk
/dev/sda1            976M  221M  688M   25% /boot
tmpfs                584M   28K  584M    1% /run/user/42
tmpfs                584M  4.0K  584M    1% /run/user/0

#查看每个文件系统的格式
[root@localhost ~]# blkid
/dev/mapper/cl-root: UUID="546b9807-e6f9-44fd-8174-5a8edfd127bf" TYPE="xfs"
/dev/sda2: UUID="wVXcRD-XcjB-A0V7-xf4R-IU4s-S7Ew-Pom5Jf" TYPE="LVM2_member" PARTUUID="e82b74c8-02"
/dev/sda1: UUID="ff037b8a-c85b-4d17-a919-7ab0b5ffb7e6" TYPE="ext4" PARTUUID="e82b74c8-01"
/dev/sdb1: UUID="6781f1bd-8a7a-4675-8779-f4a7cf95937e" TYPE="xfs" PARTUUID="fd65ef16-01"
/dev/sr0: UUID="2019-08-15-21-52-52-00" LABEL="CentOS-8-BaseOS-x86_64" TYPE="iso9660" PTUUID="3e04f576" PTTYPE="dos"
/dev/mapper/cl-swap: UUID="f5bb0909-dd7d-4ee4-b437-bfc3d27e60b7" TYPE="swap"






[root@localhost ~]# xfs_quota -x -c 'limit bsoft=3m bhard=6m isoft=3 ihard=6 hyc' /boot
[root@localhost ~]# xfs_quota -x -c report /boot
report: foreign filesystem. Invoke xfs_quota with -f to enable.
[root@localhost ~]# chmod -Rf 777 /boot



#创建软连接  ln -s 原始文件 新文件  （相当于Windows中的快捷方式）
[root@localhost ~]# echo "Welcome to linux" >readme.txt
# -s 创建“符号链接”（如果不带-s 参数，则默认创建硬链接）
[root@localhost ~]# ln -s readme.txt readit.txt
[root@localhost ~]# cat readit.txt 
Welcome to linux
[root@localhost ~]# cat readme.txt 
Welcome to linux
[root@localhost ~]# ls -l readme.txt 
-rw-r--r--. 1 root root 17 4月  23 12:15 readme.txt
[root@localhost ~]# rm -rf readme.txt 
[root@localhost ~]# cat readit.txt 
cat: readit.txt: 没有那个文件或目录
[root@localhost ~]# cat readit.txt 
cat: readit.txt: 没有那个文件或目录
[root@localhost ~]# rm readit.txt 
rm：是否删除符号链接 'readit.txt'？


#硬链接
[root@localhost ~]# echo "你好，Linux" > readme.txt
[root@localhost ~]# ln readme.txt readit.txt
[root@localhost ~]# ln readme.txt readit.txt
[root@localhost ~]# cat readit.txt 
你好，Linux
[root@localhost ~]# cat readme.txt 
你好，Linux
[root@localhost ~]# ls -l readme.txt 
-rw-r--r--. 2 root root 15 4月  23 12:24 readme.txt
[root@localhost ~]# rm -f readme.txt 
[root@localhost ~]# cat readit.txt 
你好，Linux

[root@localhost ~]# ls -l logs/
总用量 0
drwxr-xr-x. 2 root root 25 1月  31 14:08 rocketmqlogs
[root@localhost ~]# ls -l user.txt 
-rw-r--r--. 1 root root 46 4月  22 16:12 user.txt






```



### 添加交换分区

```powershell
[root@localhost ~]# fdisk /dev/sdc

欢迎使用 fdisk (util-linux 2.32.1)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。


命令(输入 m 获取帮助)：m

帮助：

  DOS (MBR)
   a   开关 可启动 标志
   b   编辑嵌套的 BSD 磁盘标签
   c   开关 dos 兼容性标志

  常规
   d   删除分区
   F   列出未分区的空闲区
   l   列出已知分区类型
   n   添加新分区
   p   打印分区表
   t   更改分区类型
   v   检查分区表
   i   打印某个分区的相关信息

  杂项
   m   打印此菜单
   u   更改 显示/记录 单位
   x   更多功能(仅限专业人员)

  脚本
   I   从 sfdisk 脚本文件加载磁盘布局
   O   将磁盘布局转储为 sfdisk 脚本文件

  保存并退出
   w   将分区表写入磁盘并退出
   q   退出而不保存更改

  新建空磁盘标签
   g   新建一份 GPT 分区表
   G   新建一份空 GPT (IRIX) 分区表
   o   新建一份的空 DOS 分区表
   s   新建一份空 Sun 分区表



[root@localhost ~]# ls -l /dev/sd*
brw-rw----. 1 root disk 8,  0 4月  23 13:33 /dev/sda
brw-rw----. 1 root disk 8,  1 4月  23 13:33 /dev/sda1
brw-rw----. 1 root disk 8,  2 4月  23 13:33 /dev/sda2
brw-rw----. 1 root disk 8, 16 4月  23 13:33 /dev/sdb
brw-rw----. 1 root disk 8, 17 4月  23 13:33 /dev/sdb1
brw-rw----. 1 root disk 8, 32 4月  23 13:33 /dev/sdc
[root@localhost ~]# fdisk /dev/sdc

欢迎使用 fdisk (util-linux 2.32.1)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。

设备不包含可识别的分区表。
创建了一个磁盘标识符为 0x232f6b5a 的新 DOS 磁盘标签。

命令(输入 m 获取帮助)：n
分区类型
   p   主分区 (0个主分区，0个扩展分区，4空闲)
   e   扩展分区 (逻辑分区容器)
选择 (默认 p)：p
分区号 (1-4, 默认  1): 
第一个扇区 (2048-4194303, 默认 2048): 
上个扇区，+sectors 或 +size{K,M,G,T,P} (2048-4194303, 默认 4194303): 

创建了一个新分区 1，类型为“Linux”，大小为 2 GiB。

命令(输入 m 获取帮助)：p
Disk /dev/sdc：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x232f6b5a

设备       启动  起点    末尾    扇区 大小 Id 类型
/dev/sdc1        2048 4194303 4192256   2G 83 Linux

命令(输入 m 获取帮助)：w
分区表已调整。
将调用 ioctl() 来重新读分区表。
正在同步磁盘。

#使用 SWAP 分区专用的格式化命令 mkswap
[root@localhost ~]# mkswap /dev/sdc
mkswap: /dev/sdc：警告，不要擦除 bootbits 扇区
        (检测到 dos 分区表)。使用 -f 选项强制执行。
正在设置交换空间版本 1，大小 = 2 GiB (2147479552  个字节)
无标签，UUID=226342b9-a8e8-4c7d-860f-6473cf6e120f
[root@localhost ~]# free -m
              total        used        free      shared  buff/cache   available
Mem:           5836         981        4211           9         644        4600
Swap:          2047           0        2047
# swapon 命令把准备好的 SWAP 分区设备正式挂载到系统中
[root@localhost ~]# swapon /dev/sdc
#使用 free -m 命令查看交换分区的大小变化
[root@localhost ~]# free -m
              total        used        free      shared  buff/cache   available
Mem:           5836         981        4211           9         644        4600
Swap:          4095           0        4095
[root@localhost ~]# vim /etc/fstab 
[root@localhost ~]# cat /etc/fstab 

#
# /etc/fstab
# Created by anaconda on Thu Dec 12 10:38:19 2019
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/cl-root     /                       xfs     defaults        0 0
UUID=ff037b8a-c85b-4d17-a919-7ab0b5ffb7e6 /boot                   ext4    defaults        1 2
/dev/mapper/cl-swap     swap                    swap    defaults        0 0

/dev/sdc                swap                    swap    defaults        0 0

/dev/sdb1               /newdisk                xfs     defaults        0 0
[root@localhost ~]# reboot
[root@localhost ~]# free -m
              total        used        free      shared  buff/cache   available
Mem:           5836         996        4189           9         650        4573
Swap:          4095           0        4095

[root@localhost ~]# lsblk -f
NAME        FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                   
├─sda1      ext4                               ff037b8a-c85b-4d17-a919-7ab0b5ffb7e6   /boot
└─sda2      LVM2_member                        wVXcRD-XcjB-A0V7-xf4R-IU4s-S7Ew-Pom5Jf 
  ├─cl-root xfs                                546b9807-e6f9-44fd-8174-5a8edfd127bf   /
  └─cl-swap swap                               f5bb0909-dd7d-4ee4-b437-bfc3d27e60b7   [SWAP]
sdb                                                                                   
└─sdb1      xfs                                6781f1bd-8a7a-4675-8779-f4a7cf95937e   /newdisk
sdc         swap                               226342b9-a8e8-4c7d-860f-6473cf6e120f   [SWAP]
└─sdc1      swap                               226342b9-a8e8-4c7d-860f-6473cf6e120f   
sr0         iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00                 

```





### RAID10



```powershell
1、为虚拟机添加4块硬盘
[root@localhost ~]# ls -l /dev/sd*
brw-rw----. 1 root disk 8,  0 4月  23 15:19 /dev/sda
brw-rw----. 1 root disk 8,  1 4月  23 15:19 /dev/sda1
brw-rw----. 1 root disk 8,  2 4月  23 15:19 /dev/sda2
brw-rw----. 1 root disk 8, 16 4月  23 15:19 /dev/sdb
brw-rw----. 1 root disk 8, 32 4月  23 15:19 /dev/sdc
brw-rw----. 1 root disk 8, 48 4月  23 15:19 /dev/sdd
brw-rw----. 1 root disk 8, 64 4月  23 15:19 /dev/sde
[root@localhost ~]# lsblk -f
NAME   FSTYPE   LABEL                  UUID                                   MOUNTPOINT
sda                                                                           
├─sda1 ext4                            acfacd7f-af2c-4682-8e32-7cebf95fb3b8   /boot
└─sda2 LVM2_mem                        I7tXrN-TNdw-L2s1-WdJ8-ta5M-Qfrd-suaJDB 
  ├─cl-root
  │    xfs                             cd1af320-5206-414d-8e7a-d57605254f62   /
  └─cl-swap
       swap                            839460aa-05b3-472a-9be7-b497c712260f   [SWAP]
sdb                                                                           
sdc                                                                           
sdd                                                                           
sde                                                                           
sr0    iso9660  CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00             
[root@localhost ~]# mdadm -Cv /dev/md0 -a yes -n 4 -l 10 /dev/sd[b-e]
mdadm: layout defaults to n2
mdadm: layout defaults to n2
mdadm: chunk size defaults to 512K
mdadm: size set to 2094080K
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
[root@localhost ~]# mdadm -Q /dev/md0
/dev/md0: 3.99GiB raid10 4 devices, 0 spares. Use mdadm --detail for more detail.
#磁盘阵列组的详细信息
[root@localhost ~]# mdadm -D /dev/md0
/dev/md0:
           Version : 1.2
     Creation Time : Thu Apr 23 15:34:06 2020
        Raid Level : raid10
        Array Size : 4188160 (3.99 GiB 4.29 GB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 4
     Total Devices : 4
       Persistence : Superblock is persistent

       Update Time : Thu Apr 23 15:34:27 2020
             State : clean 
    Active Devices : 4
   Working Devices : 4
    Failed Devices : 0
     Spare Devices : 0

            Layout : near=2
        Chunk Size : 512K

Consistency Policy : resync

              Name : localhost.localdomain:0  (local to host localhost.localdomain)
              UUID : 22eb8d8a:590f443d:acabe8c7:0ade94bd
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       8       16        0      active sync set-A   /dev/sdb
       1       8       32        1      active sync set-B   /dev/sdc
       2       8       48        2      active sync set-A   /dev/sdd
       3       8       64        3      active sync set-B   /dev/sde
[root@localhost ~]# mkfs.xfs /dev/md0 
meta-data=/dev/md0               isize=512    agcount=8, agsize=130944 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=1047040, imaxpct=25
         =                       sunit=128    swidth=256 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
[root@localhost ~]# mkdir /RAID10
[root@localhost ~]# mount /dev/md0 /RAID10
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                2.0G     0  2.0G    0% /dev/shm
tmpfs                2.0G   10M  2.0G    1% /run
tmpfs                2.0G     0  2.0G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   11G  6.2G   64% /
/dev/sda1            976M  118M  792M   13% /boot
tmpfs                392M   28K  392M    1% /run/user/42
tmpfs                392M  2.3M  390M    1% /run/user/0
/dev/sr0             6.7G  6.7G     0  100% /run/media/root/CentOS-8-BaseOS-x86_64
/dev/md0             4.0G   62M  4.0G    2% /RAID10
[root@localhost ~]# vim /etc/fstab 
[root@localhost ~]# cat /etc/fstab 

#
# /etc/fstab
# Created by anaconda on Mon Dec 23 16:13:51 2019
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/cl-root     /                       xfs     defaults        0 0
UUID=acfacd7f-af2c-4682-8e32-7cebf95fb3b8 /boot                   ext4    defaults        1 2
/dev/mapper/cl-swap     swap                    swap    defaults        0 0
	
/dev/md0               /RAID10                  xfs     defaults        0 0

[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                2.0G     0  2.0G    0% /dev/shm
tmpfs                2.0G   10M  2.0G    1% /run
tmpfs                2.0G     0  2.0G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   11G  6.2G   64% /
/dev/sda1            976M  118M  792M   13% /boot
tmpfs                392M   28K  392M    1% /run/user/42
tmpfs                392M  2.3M  390M    1% /run/user/0
/dev/sr0             6.7G  6.7G     0  100% /run/media/root/CentOS-8-BaseOS-x86_64
/dev/md0             4.0G   62M  4.0G    2% /RAID10
[root@localhost ~]# reboot
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                2.0G     0  2.0G    0% /dev/shm
tmpfs                2.0G  9.7M  2.0G    1% /run
tmpfs                2.0G     0  2.0G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   11G  6.2G   64% /
/dev/md0             4.0G   62M  4.0G    2% /RAID10
/dev/sda1            976M  118M  792M   13% /boot
tmpfs                392M   28K  392M    1% /run/user/42
tmpfs                392M  4.0K  392M    1% /run/user/0

[root@localhost ~]# cd /RAID10/
[root@localhost RAID10]# ls
[root@localhost RAID10]# cp -rf /etc/* ./


#删除虚拟机其中一块硬盘

[root@localhost ~]# mdadm -D /dev/md0
/dev/md0:
           Version : 1.2
     Creation Time : Thu Apr 23 15:34:06 2020
        Raid Level : raid10
        Array Size : 4188160 (3.99 GiB 4.29 GB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 4
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Thu Apr 23 16:01:19 2020
             State : clean, degraded 
    Active Devices : 3
   Working Devices : 3
    Failed Devices : 0
     Spare Devices : 0

            Layout : near=2
        Chunk Size : 512K

Consistency Policy : resync

              Name : localhost.localdomain:0  (local to host localhost.localdomain)
              UUID : 22eb8d8a:590f443d:acabe8c7:0ade94bd
            Events : 19

    Number   Major   Minor   RaidDevice State
       -       0        0        0      removed
       1       8       16        1      active sync set-B   /dev/sdb
       2       8       32        2      active sync set-A   /dev/sdc
       3       8       48        3      active sync set-B   /dev/sdd

#在虚拟机中新建一个硬盘

#将删除的 /dev/sde 设置一下损坏
[root@localhost ~]# mdadm /dev/mdo -f /dev/sde
mdadm: error opening /dev/mdo: No such file or directory
#移除挂载信息
[root@localhost ~]# umount /dev/md0 
[root@localhost ~]# lsblk -f
NAME        FSTYPE            LABEL                   UUID                                   MOUNTPOINT
sda                                                                                          
├─sda1      ext4                                      acfacd7f-af2c-4682-8e32-7cebf95fb3b8   /boot
└─sda2      LVM2_member                               I7tXrN-TNdw-L2s1-WdJ8-ta5M-Qfrd-suaJDB 
  ├─cl-root xfs                                       cd1af320-5206-414d-8e7a-d57605254f62   /
  └─cl-swap swap                                      839460aa-05b3-472a-9be7-b497c712260f   [SWAP]
sdb                                                                                          
sdc         linux_raid_member localhost.localdomain:0 22eb8d8a-590f-443d-acab-e8c70ade94bd   
└─md0       xfs                                       356dc391-2760-44fb-a5a6-31be8e27969f   
sdd         linux_raid_member localhost.localdomain:0 22eb8d8a-590f-443d-acab-e8c70ade94bd   
└─md0       xfs                                       356dc391-2760-44fb-a5a6-31be8e27969f   
sde         linux_raid_member localhost.localdomain:0 22eb8d8a-590f-443d-acab-e8c70ade94bd   
└─md0       xfs                                       356dc391-2760-44fb-a5a6-31be8e27969f   
sr0         iso9660           CentOS-8-BaseOS-x86_64  2019-08-15-21-52-52-00              
[root@localhost ~]# mdadm /dev/md0 -a /dev/sdb
mdadm: added /dev/sdb
#查看磁盘阵列组的状态
[root@localhost ~]# mdadm -D /dev/md0
/dev/md0:
           Version : 1.2
     Creation Time : Thu Apr 23 15:34:06 2020
        Raid Level : raid10
        Array Size : 4188160 (3.99 GiB 4.29 GB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 4
     Total Devices : 4
       Persistence : Superblock is persistent

       Update Time : Thu Apr 23 16:20:07 2020
             State : clean 
    Active Devices : 4
   Working Devices : 4
    Failed Devices : 0
     Spare Devices : 0

            Layout : near=2
        Chunk Size : 512K

Consistency Policy : resync

              Name : localhost.localdomain:0  (local to host localhost.localdomain)
              UUID : 22eb8d8a:590f443d:acabe8c7:0ade94bd
            Events : 42

    Number   Major   Minor   RaidDevice State
       4       8       16        0      active sync set-A   /dev/sdb
       1       8       32        1      active sync set-B   /dev/sdc
       2       8       48        2      active sync set-A   /dev/sdd
       3       8       64        3      active sync set-B   /dev/sde
#重新挂载
[root@localhost ~]# mount -a
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                2.0G     0  2.0G    0% /dev/shm
tmpfs                2.0G  9.7M  2.0G    1% /run
tmpfs                2.0G     0  2.0G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   11G  6.2G   64% /
/dev/sda1            976M  118M  792M   13% /boot
tmpfs                392M   28K  392M    1% /run/user/42
tmpfs                392M  4.0K  392M    1% /run/user/0
/dev/md0             4.0G   93M  3.9G    3% /RAID10


```



RAID5+热备盘

```powershell
[root@localhost ~]# ls -l /dev/sd*
brw-rw----. 1 root disk 8,  0 4月  23 16:34 /dev/sda
brw-rw----. 1 root disk 8,  1 4月  23 16:34 /dev/sda1
brw-rw----. 1 root disk 8,  2 4月  23 16:34 /dev/sda2
brw-rw----. 1 root disk 8, 16 4月  23 16:34 /dev/sdb
brw-rw----. 1 root disk 8, 32 4月  23 16:34 /dev/sdc
brw-rw----. 1 root disk 8, 48 4月  23 16:34 /dev/sdd
brw-rw----. 1 root disk 8, 64 4月  23 16:34 /dev/sde

[root@localhost ~]# mdadm -Cv /dev/md0 -n 3 -l 5 -x 1 /dev/sd[b-e]
mdadm: layout defaults to left-symmetric
mdadm: layout defaults to left-symmetric
mdadm: chunk size defaults to 512K
mdadm: size set to 2094080K
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.
[root@localhost ~]# mdadm -D /dev/md0 
/dev/md0:
           Version : 1.2
     Creation Time : Thu Apr 23 16:40:02 2020
        Raid Level : raid5
        Array Size : 4188160 (3.99 GiB 4.29 GB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 3
     Total Devices : 4
       Persistence : Superblock is persistent

       Update Time : Thu Apr 23 16:40:14 2020
             State : clean 
    Active Devices : 3
   Working Devices : 4
    Failed Devices : 0
     Spare Devices : 1

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : localhost.localdomain:0  (local to host localhost.localdomain)
              UUID : 6101e84e:f5b03358:9dd9894c:4a609f8e
            Events : 18

    Number   Major   Minor   RaidDevice State
       0       8       16        0      active sync   /dev/sdb
       1       8       32        1      active sync   /dev/sdc
       4       8       48        2      active sync   /dev/sdd

       3       8       64        -      spare   /dev/sde
[root@localhost ~]# mkfs.ext4 /dev/md0 
mke2fs 1.44.3 (10-July-2018)
创建含有 1047040 个块（每块 4k）和 262144 个inode的文件系统
文件系统UUID：32f989fd-1a13-48f5-ad1b-92cd04344e50
超级块的备份存储于下列块： 
	32768, 98304, 163840, 229376, 294912, 819200, 884736

正在分配组表： 完成                            
正在写入inode表： 完成                            
创建日志（16384 个块）完成
写入超级块和文件系统账户统计信息： 已完成
[root@localhost ~]# mkdir /RAID5
[root@localhost ~]# mount /dev/md0 /RAID5
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             545M     0  545M    0% /dev
tmpfs                561M     0  561M    0% /dev/shm
tmpfs                561M  8.8M  552M    2% /run
tmpfs                561M     0  561M    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  5.7G   12G   34% /
/dev/sda1            976M  134M  775M   15% /boot
tmpfs                113M   28K  112M    1% /run/user/42
tmpfs                113M  2.4M  110M    3% /run/user/1000
/dev/sr0             6.7G  6.7G     0  100% /run/media/houyingchao/CentOS-8-BaseOS-x86_64
tmpfs                113M  4.0K  113M    1% /run/user/0
/dev/md0             3.9G   16M  3.7G    1% /RAID5
[root@localhost ~]# vim /etc/fstab 
[root@localhost ~]# cat /etc/fstab 

#
# /etc/fstab
# Created by anaconda on Fri Jan 31 09:52:04 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/cl-root     /                       xfs     defaults        0 0
UUID=8fb29c11-6deb-4bfc-9412-5bbf5f70749c /boot                   ext4    defaults        1 2
/dev/mapper/cl-swap     swap                    swap    defaults        0 0

/dev/md0             /RAID5                    ext4     defaults        0 0

#模拟破坏
[root@localhost ~]# cd /RAID5/
[root@localhost RAID5]# ls
lost+found
[root@localhost RAID5]# cp -rf /etc/* ./
#删除虚拟机磁盘
[root@localhost ~]#  mdadm -D /dev/md0 
/dev/md0:
           Version : 1.2
     Creation Time : Thu Apr 23 16:40:02 2020
        Raid Level : raid5
        Array Size : 4188160 (3.99 GiB 4.29 GB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 3
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Thu Apr 23 17:03:11 2020
             State : clean 
    Active Devices : 3
   Working Devices : 3
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : localhost.localdomain:0  (local to host localhost.localdomain)
              UUID : 6101e84e:f5b03358:9dd9894c:4a609f8e
            Events : 40

    Number   Major   Minor   RaidDevice State
       0       8       16        0      active sync   /dev/sdb
       1       8       32        1      active sync   /dev/sdc
       3       8       48        2      active sync   /dev/sdd

```

### LVM技术

```powershell
#1.虚拟机添加2块硬盘
[root@hadoop102 atguigu]# ls -l /dev/sd*
brw-rw----. 1 root disk 8,  0 4月  23 17:51 /dev/sda
brw-rw----. 1 root disk 8,  1 4月  23 17:51 /dev/sda1
brw-rw----. 1 root disk 8,  2 4月  23 17:51 /dev/sda2
brw-rw----. 1 root disk 8, 16 4月  23 17:51 /dev/sdb
brw-rw----. 1 root disk 8, 32 4月  23 17:51 /dev/sdc
[root@hadoop102 atguigu]# pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[root@hadoop102 atguigu]# pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[root@hadoop102 atguigu]# vgcreate storage /dev/sdb /dev/sdc
  Volume group "storage" successfully created
[root@hadoop102 atguigu]# vgdisplay
  --- Volume group ---
  VG Name               storage
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               3.99 GiB
  PE Size               4.00 MiB
  Total PE              1022
  Alloc PE / Size       0 / 0   
  Free  PE / Size       1022 / 3.99 GiB
  VG UUID               13Btex-qrwy-k5w2-W8bL-2InD-tIzU-ZRuB0c
   
  --- Volume group ---
  VG Name               cl
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <19.00 GiB
  PE Size               4.00 MiB
  Total PE              4863
  Alloc PE / Size       4863 / <19.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               nZyknJ-KJ9U-Bpm6-65de-Yj1y-bHFT-Muj4Dq
[root@hadoop102 atguigu]# lvcreate -n vo -l 100 storage
  Logical volume "vo" created.

[root@hadoop102 dev]# mkfs.ext4 /dev/storage/vo 
mke2fs 1.44.3 (10-July-2018)
创建含有 409600 个块（每块 1k）和 102400 个inode的文件系统
文件系统UUID：1bac48b6-251b-42a8-b9db-ba87af08d9d1
超级块的备份存储于下列块： 
	8193, 24577, 40961, 57345, 73729, 204801, 221185, 401409

正在分配组表： 完成                            
正在写入inode表： 完成                            
创建日志（8192 个块）完成
写入超级块和文件系统账户统计信息： 已完成

[root@hadoop102 dev]# mkdir /LVMTST
[root@hadoop102 dev]# vim /etc/fstab 
/dev/storage/vo        /LVMTST                  ext4    defaults        0 0
[root@hadoop102 dev]# mount -a
[root@hadoop102 dev]# df -h
文件系统                容量  已用  可用 已用% 挂载点
devtmpfs                384M     0  384M    0% /dev
tmpfs                   400M     0  400M    0% /dev/shm
tmpfs                   400M  6.4M  393M    2% /run
tmpfs                   400M     0  400M    0% /sys/fs/cgroup
/dev/mapper/cl-root      17G  7.0G   11G   41% /
/dev/sda1               976M  134M  775M   15% /boot
tmpfs                    80M   28K   80M    1% /run/user/42
tmpfs                   285M  4.0K  285M    1% /run/user/1001
/dev/mapper/storage-vo  380M  2.3M  354M    1% /LVMTST


#扩容逻辑卷
[root@hadoop102 dev]# umount /LVMTST 
[root@hadoop102 dev]# lvextend -L 800M /dev/storage/vo 
  Size of logical volume storage/vo changed from 400.00 MiB (100 extents) to 800.00 MiB (200 extents).
  Logical volume storage/vo successfully resized.
#检查文件系统是否造成丢失
[root@hadoop102 dev]# e2fsck -f /dev/storage/vo 
e2fsck 1.44.3 (10-July-2018)
第 1 步：检查inode、块和大小
第 2 步：检查目录结构
第 3 步：检查目录连接性
第 4 步：检查引用计数
第 5 步：检查组概要信息
/dev/storage/vo：11/102400 文件（0.0% 为非连续的）， 23456/409600 块

[root@hadoop102 dev]# resize2fs /dev/storage/vo 
resize2fs 1.44.3 (10-July-2018)
将 /dev/storage/vo 上的文件系统调整为 819200 个块（每块 1k）。
/dev/storage/vo 上的文件系统现在为 819200 个块（每块 1k）。
[root@hadoop102 dev]# mount -a
[root@hadoop102 dev]# df -h
文件系统                容量  已用  可用 已用% 挂载点
devtmpfs                384M     0  384M    0% /dev
tmpfs                   400M     0  400M    0% /dev/shm
tmpfs                   400M  6.4M  393M    2% /run
tmpfs                   400M     0  400M    0% /sys/fs/cgroup
/dev/mapper/cl-root      17G  7.0G   11G   41% /
/dev/sda1               976M  134M  775M   15% /boot
tmpfs                    80M   28K   80M    1% /run/user/42
tmpfs                   285M  4.0K  285M    1% /run/user/1001
/dev/mapper/storage-vo  767M  2.5M  721M    1% /LVMTST


#缩小逻辑卷
[root@hadoop102 ~]# umount /LVMTST 
#检查文件系统的完整性
[root@hadoop102 ~]# e2fsck -f /dev/storage/vo 
e2fsck 1.44.3 (10-July-2018)
第 1 步：检查inode、块和大小
第 2 步：检查目录结构
第 3 步：检查目录连接性
第 4 步：检查引用计数
第 5 步：检查组概要信息
/dev/storage/vo：11/204800 文件（0.0% 为非连续的）， 36617/819200 块
把逻辑卷 vo 的容量减小到 300MB
[root@hadoop102 ~]# resize2fs /dev/storage/vo 300M
resize2fs 1.44.3 (10-July-2018)
将 /dev/storage/vo 上的文件系统调整为 307200 个块（每块 1k）。
/dev/storage/vo 上的文件系统现在为 307200 个块（每块 1k）。
[root@hadoop102 ~]# lvreduce -L 300 /dev/storage/vo 
  WARNING: Reducing active logical volume to 300.00 MiB.
  THIS MAY DESTROY YOUR DATA (filesystem etc.)
Do you really want to reduce storage/vo? [y/n]: y
  Size of logical volume storage/vo changed from 800.00 MiB (200 extents) to 300.00 MiB (75 extents).
  Logical volume storage/vo successfully resized.

#重新挂载文件系统并查看系统状态。
[root@hadoop102 ~]# mount -a
[root@hadoop102 ~]# df -h
文件系统                容量  已用  可用 已用% 挂载点
devtmpfs                384M     0  384M    0% /dev
tmpfs                   400M     0  400M    0% /dev/shm
tmpfs                   400M  6.4M  393M    2% /run
tmpfs                   400M     0  400M    0% /sys/fs/cgroup
/dev/mapper/cl-root      17G  7.0G   11G   41% /
/dev/sda1               976M  134M  775M   15% /boot
tmpfs                    80M   28K   80M    1% /run/user/42
tmpfs                   285M  4.0K  285M    1% /run/user/1001
/dev/mapper/storage-vo  283M  2.1M  262M    1% /LVMTST


[root@hadoop102 LVMTST]# cd /LVMTST
[root@hadoop102 LVMTST]# ls
lost+found
[root@hadoop102 LVMTST]# echo "Welcome to Linuxprobe.com" > ./readme.txt
[root@hadoop102 LVMTST]# ll
总用量 14
drwx------. 2 root root 12288 4月  23 18:04 lost+found
-rw-r--r--. 1 root root    26 4月  23 18:38 readme.txt
[root@hadoop102 LVMTST]# lvcreate -L 300M -s -n SNAP /dev/storage/vo
  Logical volume "SNAP" created.

[root@hadoop102 LVMTST]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/storage/vo
  LV Name                vo
  VG Name                storage
  LV UUID                lb91cS-cHsA-g4JP-hSzx-BZxI-X5cc-d7q3fn
  LV Write Access        read/write
  LV Creation host, time hadoop102, 2020-04-23 18:00:30 +0800
  LV snapshot status     source of
                         SNAP [active]
  LV Status              available
  # open                 1
  LV Size                300.00 MiB
  Current LE             75
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
   
  --- Logical volume ---
  LV Path                /dev/storage/SNAP
  LV Name                SNAP
  VG Name                storage
  LV UUID                2QarzA-dmLP-fjV2-hmKp-1Prm-nIVM-EcSsyq
  LV Write Access        read/write
  LV Creation host, time hadoop102, 2020-04-23 18:41:41 +0800
  LV snapshot status     active destination for vo
  LV Status              available
  # open                 0
  LV Size                300.00 MiB
  Current LE             75
  COW-table size         300.00 MiB
  COW-table LE           75
  Allocated to snapshot  0.01%
  Snapshot chunk size    4.00 KiB
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:5
   
  --- Logical volume ---
  LV Path                /dev/cl/swap
  LV Name                swap
  VG Name                cl
  LV UUID                NQXsfN-ze33-0UnQ-QA5u-IYlU-GjRp-sscc3B
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-01-31 09:51:58 +0800
  LV Status              available
  # open                 2
  LV Size                2.00 GiB
  Current LE             512
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1
   
  --- Logical volume ---
  LV Path                /dev/cl/root
  LV Name                root
  VG Name                cl
  LV UUID                OeTfT4-z6lu-29Jv-ePhL-xKnQ-z9dx-uvUct3
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-01-31 09:51:59 +0800
  LV Status              available
  # open                 1
  LV Size                <17.00 GiB
  Current LE             4351
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
   
[root@hadoop102 LVMTST]# rm -rf readme.txt 
[root@hadoop102 LVMTST]# ls
lost+found
[root@hadoop102 LVMTST]# cd 
[root@hadoop102 ~]# umount /LVMTST 
[root@hadoop102 ~]# lvconvert --merge /dev/storage/SNAP 
  Merging of volume storage/SNAP started.
  storage/vo: Merged: 100.00%
[root@hadoop102 ~]# mount -a
[root@hadoop102 ~]# cd /LVMTST/
[root@hadoop102 LVMTST]# ls
lost+found  readme.txt
[root@hadoop102 LVMTST]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/storage/vo
  LV Name                vo
  VG Name                storage
  LV UUID                lb91cS-cHsA-g4JP-hSzx-BZxI-X5cc-d7q3fn
  LV Write Access        read/write
  LV Creation host, time hadoop102, 2020-04-23 18:00:30 +0800
  LV Status              available
  # open                 1
  LV Size                300.00 MiB
  Current LE             75
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
   
  --- Logical volume ---
  LV Path                /dev/cl/swap
  LV Name                swap
  VG Name                cl
  LV UUID                NQXsfN-ze33-0UnQ-QA5u-IYlU-GjRp-sscc3B
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-01-31 09:51:58 +0800
  LV Status              available
  # open                 2
  LV Size                2.00 GiB
  Current LE             512
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1
   
  --- Logical volume ---
  LV Path                /dev/cl/root
  LV Name                root
  VG Name                cl
  LV UUID                OeTfT4-z6lu-29Jv-ePhL-xKnQ-z9dx-uvUct3
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-01-31 09:51:59 +0800
  LV Status              available
  # open                 1
  LV Size                <17.00 GiB
  Current LE             4351
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
   
快照卷只能使用一次


#删除逻辑卷
[root@hadoop102 ~]# umount /LVMTST 
#删除挂载信息
[root@hadoop102 ~]# vim /etc/fstab 
#删除逻辑卷设备
[root@hadoop102 ~]# lvremove /dev/storage/vo 
Do you really want to remove active logical volume storage/vo? [y/n]: y
  Logical volume "vo" successfully removed
[root@hadoop102 ~]# vgremove storage 
  Volume group "storage" successfully removed
[root@hadoop102 ~]# pvremove /dev/sd[b-c]
  Labels on physical volume "/dev/sdb" successfully wiped.
  Labels on physical volume "/dev/sdc" successfully wiped.

[root@hadoop102 ~]# lsblk -f
NAME     FSTYPE      LABEL                  UUID                                   MOUNTPOINT
sda                                                                                
├─sda1   ext4                               8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2   LVM2_member                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root
  │      xfs                                1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap
         swap                               c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                                
sdc                                                                                
sr0      iso9660     CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00                
```



```powershell
1.编写配置文件
[root@localhost ~]# vim /etc/sysconfig/network-scripts/ifcfg-ens33 

  1 TYPE=Ethernet
  2 PROXY_METHOD=none
  3 BROWSER_ONLY=no
  4 BOOTPROTO=static
  5 DEFROUTE=yes
  6 IPV4_FAILURE_FATAL=no
  7 IPV6INIT=yes
  8 IPV6_AUTOCONF=yes
  9 IPV6_DEFROUTE=yes
 10 IPV6_FAILURE_FATAL=no
 11 IPV6_ADDR_GEN_MODE=stable-privacy
 12 NAME=ens33
 13 UUID=5aed5d2c-abf6-4c1c-b4fa-9393d18a3d9b
 14 DEVICE=ens33
 15 ONBOOT=yes
 16 
 17 IPADDR=192.168.40.128
 18 NETMASK=255.255.255.0
 19 GATEWAY=192.168.40.2
~
#centos7使用该命令重启网络服务
[root@localhost ~]# systemctl restart network
Failed to restart network.service: Unit network.service not found.
[root@localhost ~]# service network-manager restart
Redirecting to /bin/systemctl restart network-manager.service
Failed to restart network-manager.service: Unit network-manager.service not found.
#centos8使用下面命令重启网络服务
[root@localhost ~]# nmcli c reload
[root@localhost ~]# nmcli c up ens33 
连接已成功激活（D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/4）


#第二种
[root@localhost ~]# nmtui
[root@localhost ~]# nmcli c reload
[root@localhost ~]# nmcli c up ens33 
连接已成功激活（D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/4）

#第三种
[root@localhost ~]# nm-connection-editor
[root@localhost ~]# nmcli c reload
[root@localhost ~]# nmcli c up ens33 
连接已成功激活（D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/4）

#第四种图形化界面网络配置

#使用nmcli重新回载网络配置
[root@localhost ~]# nmcli c reload
[root@localhost ~]# nmcli c up ens33 
连接已成功激活（D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/4）



```



### nmcli管理网络

```powershell
#查看网卡信息
[root@localhost ~]# nmcli connection
NAME    UUID                                  TYPE      DEVICE 
ens33   5aed5d2c-abf6-4c1c-b4fa-9393d18a3d9b  ethernet  ens33  
virbr0  61da43b6-6e0e-4f0a-810a-0791032e4f53  bridge    virbr0 
#显示具体的网络接口信息
[root@localhost ~]# nmcli connection show ens33

[root@localhost ~]# nmcli connection show --active
NAME    UUID                                  TYPE      DEVICE 
ens33   5aed5d2c-abf6-4c1c-b4fa-9393d18a3d9b  ethernet  ens33  
virbr0  61da43b6-6e0e-4f0a-810a-0791032e4f53  bridge    virbr0 

```

### iptables7.2以后已经取消了

```powershell
[root@localhost ~]# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:domain
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:domain
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootps
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:bootps
ACCEPT     all  --  anywhere             anywhere             state RELATED,ESTABLISHED
ACCEPT     icmp --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     tcp  --  anywhere             anywhere             state NEW tcp dpt:ssh
ACCEPT     tcp  --  anywhere             anywhere             state NEW tcp dpt:webcache
REJECT     all  --  anywhere             anywhere             reject-with icmp-host-prohibited

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             192.168.122.0/24     ctstate RELATED,ESTABLISHED
ACCEPT     all  --  192.168.122.0/24     anywhere            
ACCEPT     all  --  anywhere             anywhere            
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable
REJECT     all  --  anywhere             anywhere             reject-with icmp-host-prohibited

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootpc
#清空防火墙的策略
[root@localhost ~]# iptables -F
[root@localhost ~]# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination  
#拒绝所任人从外网访问内网
[root@localhost ~]# iptables -P INPUT DROP
[root@localhost ~]# iptables -L
Chain INPUT (policy DROP)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
#开放指定ip地址链接
[root@localhost ~]# iptables -I INPUT -s 192.168.40.1 -p icmp -j ACCEPT
#开放ssh 22号端口访问
[root@localhost ~]# iptables -I INPUT -s 192.168.40.1 -p tcp --dport 22 -j ACCEPT
[root@localhost ~]# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     tcp  --  192.168.40.1         anywhere             tcp dpt:ssh
ACCEPT     icmp --  192.168.40.1         anywhere            

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         

[root@localhost ~]# iptables -P INPUT ACCEPT


C:\Users\Administrator>ping 192.168.40.128 -t
#拒绝指定ip地址访问
[root@localhost ~]# iptables -I INPUT -s 192.168.40.1 -p icmp -j REJECT
[root@localhost ~]# iptables -F
[root@localhost ~]# iptables -I INPUT -s 192.168.40.1 -p icmp -j DROP
[root@localhost ~]# iptables -F

#禁止端口号
[root@localhost ~]# iptables -I INPUT -s192.168.40.0/24 -p tcp --dport 22 -j DROP
[root@localhost ~]# iptables -F
#禁止端口1000:1500
[root@localhost ~]# iptables -I INPUT -s192.168.40.0/24 -p tcp --dport 1000:1500 -j DROP




[root@localhost ~]# iptables -F
[root@localhost ~]# service iptables save
iptables: Saving firewall rules to /etc/sysconfig/iptables:[  OK  ]
```



### firewalld

```powershell
#firewalld-cmd
[root@localhost ~]# firewall-cmd --get-default-zone 
FirewallD is not running



```

没有安装图形化用户界面

firewall-config 图形化界面













# 使用DHCP给ens33网卡分配一个IP地址，

从刚才配置的网段中分配
[yanc@localhost etc]$ sudo dhclient ens33

再次查看网卡，发现IP获取成功，然后尝试连接外网也可以成功

```powershell
[root@localhost ~]# ping www.baidu.com
```



VMware安装centos 8无法连接外网处理过程_运维_yanchao963852741的博客-CSDN博客  https://blog.csdn.net/yanchao963852741/article/details/105250107/

