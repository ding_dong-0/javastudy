

### Java集合容器

```java
import org.junit.Test;
import java.util.*;
public class Study {
    @Test//Collection接口示例
    public void IteratorDemo() {
        //创建集合对象
        Collection c1 = new ArrayList();
        //集合中添加元素
        c1.add("HI");
        c1.add(123);
        //获得迭代器
        Iterator iterator = c1.iterator();
        //遍历集合中的所有元素
        while (iterator.hasNext()) {
            Object obj = iterator.next();
            System.out.println(obj);
        }
        //通过泛型参数String限制集合c2中加入内容的类型只能是String型
        Collection<String> c2 = new ArrayList<>();
        c2.add("朝阳区");
        c2.add("海淀区");
        c2.add("大兴区");
        c2.add("丰台区");
        Iterator iterator1 = c2.iterator();
        while (iterator1.hasNext()) {
            Object obj = iterator1.next();
            System.out.println(obj);
        }
        System.out.println("集合c2中元素个数为：" + c2.size());
        //判断是否包含字符串丰台区
        boolean isContains = c2.contains("丰台区");
        System.out.println("是否包含字符串丰台区：" + isContains);
        boolean isEmpty = c2.isEmpty();
        System.out.println("集合是否为空：" + isEmpty);
        c2.remove("丰台区");
        System.out.println("移除后，集合c2中元素个数为：" + c2.size());
        c2.clear();
        System.out.println("清空后集合c2中元素个数为：" + c2.size());
    }

    @Test//增强型for循环
    public void foreachDemo() {
        Collection<String> c = new ArrayList<>();
        c.add("朝阳区");
        c.add("海淀区");
        c.add("大兴区");
        c.add("丰台区");
        for (Object obj : c
        ) {
            System.out.println(obj);
        }
    }

    @Test
    public void ListDemo() {
        List list = new LinkedList();
        list.add("李明");
        list.add("刘丽");
        System.out.println("List中的元素有：");
        for (int i = 0; i < list.size(); i++) {
            String str = (String) list.get(i);
            System.out.println(str);
        }//List中的元素是由次序的，可以用普通for循环遍历
        list.add(0, "张三");//指定位置添加元素
        list.add(123);
        System.out.println("加入2个元素后，List中有：");
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Object obj = iterator.next();
            System.out.println(obj);
        }
    }

    @Test//Set接口示例
    public void SetDemo() {
        Set set = new HashSet();
        set.add("李明");
        set.add("刘丽");
        System.out.println("Set中的元素有：");
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            System.out.println(obj);
        }//Set中的元素只能用iterator遍历
        set.add("刘丽");
        set.add(123);
        it = set.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            System.out.println(obj);
        }
    }

    @Test//Map接口示例
    public void MapDemo() {
        Map<Integer, String> map = new HashMap<>();
        //以工号为key,姓名为value,将信息保存到map中；
        map.put(1001, "李明");
        map.put(1002, "刘丽");
        map.put(1003, "张三");
        map.put(1003, "李四");//新值将覆盖原有的旧值
//        containsKey：查看是否存在相应的键
        boolean iscontains = map.containsKey(1001);
        System.out.println(iscontains);
        boolean iscontainsValue = map.containsValue("张三");
        System.out.println(iscontainsValue);
        Object Employee = map.get(1001);
        System.out.println(Employee);
        //entrySet返回此映射所包含的“键-值”对应关系的Set视图
        Set entry = map.entrySet();
        Iterator it = entry.iterator();
        System.out.println("“key-value”对应关系");
        while (it.hasNext()) {
            Object obj = it.next();
            System.out.println(obj);
        }
    }
}

```

### 反射

```java
package com.lean.ssm.chapter2.reflect;

public class ReflectServiceImpl {
    private String name;
    public ReflectServiceImpl(String name) {
        this.name = name;
    }
    public void sayHello(String name) {
        System.err.println("hello " + name);
    }
}

public Object reflect() {
        ReflectServiceImpl object = null;
        try {
            //利用反射的方法生成对象
            object = (ReflectServiceImpl) Class.forName("com.lean.ssm.chapter2.reflect.ReflectServiceImpl").newInstance();
            //反射调度方法
            Method method = object.getClass().getMethod("sayhello", String.class);
            method.invoke(object, "张三");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return object;
    }

```

### MyBatis

MyBatis 的核心组件分为 4 个部分:
● SqlSessionFactoryBuilder （ 构 造 器 ）： 它 会 根 据 配 置 或 者 代 码 来 生 成
SqlSessionFactory，采用的是分步构建的 Builder 模式。
● SqlSessionFactory（工厂接口）：依靠它来生成 SqlSession，使用的是工厂模式。
● SqlSession（会话）：一个既可以发送 SQL 执行返回结果，也可以获取 Mapper 的接
口。在现有的技术中，一般我们会让其在业务逻辑代码中“消失”，而使用的是
MyBatis 提供的 SQL Mapper 接口编程技术，它能提高代码的可读性和可维护性。
● SQL Mapper（映射器）： MyBatis 新设计存在的组件，它由一个 Java 接口和 XML
文件（或注解）构成，需要给出对应的 SQL 和映射规则。它负责发送 SQL 去执行，
并返回结果。  

![image-20191226142002216](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20191226142002216.png)











