### spring:整合MyBatis

maven导入依赖：

```xml
<dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.17</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.3</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.1.RELEASE</version>
        </dependency>
        <!--Spring操作数据库的话，还需要一个spring-jdbc-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.2.1.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.9.5</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>2.0.3</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.10</version>
            <scope>provided</scope>
        </dependency>

    </dependencies>
```

##### MyBatis

1. 编写实体类
2. 编写核心配置文件
3. 编写接口
4. 编写Mapper.xml
5. 测试

##### spring:整合MyBatis配置事务

```xml
 <!--配置声明式事务-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <constructor-arg ref="dataSource"/>
    </bean>

    <!--结合aop实现事务的织入-->
    <!--配置事务的类(通知)-->
    <tx:advice id="txadvice" transaction-manager="transactionManager">
        <!--给那些方法配置事务-->
        <tx:attributes>
            <tx:method name="add" propagation="REQUIRED"/>
            <tx:method name="delete" propagation="REQUIRED"/>
            <tx:method name="update" propagation="REQUIRED"/>
            <!--read-only="true"只读-->
            <tx:method name="query" read-only="true"/>
            <!--//所有方法-->
            <tx:method name="*" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>
    <!--配置事务切-->
    <aop:config>
        <aop:pointcut id="txPointCut" expression="execution(* com.kuang.mapper.*.*(..))"/>
        <aop:advisor advice-ref="txadvice" pointcut-ref="txPointCut"/>
    </aop:config>

```





##### 使用MyBatis实现分页

1. 接口

```java
//分页查询
List<User> getUserByLimit(HashMap<String, Integer> map);
```

2.Mapper.xml

```java
 <!--此处的UserMap指向9行的id,结果集映射-->
    <select id="getUserByLimit" parameterType="map" resultMap="UserMap">
        select * from mybatis.user limit #{startIndex},#{pageSize}
    </select>
```

3.测试

```java
@Test
    public void getUserByLimit() {
        SqlSession session = MybatisUtils.getSqlSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", 1);
        map.put("pageSize", 2);
        List<User> userList = mapper.getUserByLimit(map);
        for (User user : userList) {
            System.out.println(user);
        }
        session.close();
    }
```







BIO为每个连接创建一个线程

NIO:一个线程服务多个连接，有弊端，当连接数太多，性能下降

Netty:封装了NIO,在它 的基础上，添加主从处理组，BossGroup,WorkerGroup

编程模型：Reactor

ConcurrentHashMap:一个线程安全且性能兼顾的map(Hashtable线程安全，但性能一般)

```

```

### 分布式锁：

保证锁是共享的第三方资源

1.上锁   2.解锁

#### 1.1 数据库的方式

t_lock

id lock(0)

上锁：将数值修改为1

解锁：将数值修改为0

#### 1.2 Redis

setnx:

不存在，就设置成功，否则，设置失败

上锁：成功执行setnx key value

解锁：delete key

避免死锁：需要设置过期时间expire key timeout

注意：需要将两个操作变成一个原子操纵

4.0之前Redis+lua脚本 ，lua脚本 帮助我们扩充Redis指令

4.0 之后直接使用线程原子指令，在设置数据的时候同时设置时间

避免无锁：

释放锁的时候，检查这把锁是不是我的

#### 1.3 zookeeper

以节点作为锁，创建成功，表示获取锁成功

上锁：创建节点lock

解锁：删除节点lock

避免死锁：节点，为临时节点类型，客户端如果挂掉，会自动删除





```java
 //截取指定的字符串，
    public static void main(String[] args) {
        String st1 = "5a088000a14000085100ba01";
        String st = "5a088000a14000085100ba01";
        String shebeibianhao = st.substring(10, 18);
        String kgzt = st.substring(19, 20);
        System.out.println(kgzt);
        if (kgzt.equals("0")) {
            System.out.println("shebeibianhao:" + shebeibianhao + "关灯成功");
        } else if (kgzt.equals("1")) {
            System.out.println("shebeibianhao:" + shebeibianhao + "开灯成功");
        }
        System.out.println("");
        System.out.println(shebeibianhao);
    }
```





### Java中常见的对象类型简述(DO、BO、DTO、VO、AO、PO)

![img](images/original.png)

[summer_sunrise](https://blog.csdn.net/uestcyms) 2018-05-08 19:21:05 ![img](images/articleReadEyes.png) 90421 ![img](images/tobarCollect.png) 收藏 189

分类专栏： [Java](https://blog.csdn.net/uestcyms/category_7644365.html) [Java开发手册阅读知识点总结](https://blog.csdn.net/uestcyms/category_9274636.html)

版权

#### 题记

编写本篇文章的缘由是阿狸JAVA开发手册多处提到DO、BO、DTO、VO、PO等概念；
内容多引用于网络帖子上的回答，如下：

> 知乎：[PO BO VO DTO POJO DAO DO这些Java中的概念分别指一些什么？](https://www.zhihu.com/question/39651928)
> CNblogs：[PO BO VO DTO POJO DAO概念及其作用（附转换图）](http://www.blogjava.net/vip01/archive/2013/05/25/92430.html)

#### 概念及理解

这些概念用于描述对象的类型；由于java是面向对象的语言；程序的世界就是各个对象之间的“交互”；在交互的工程中会存在多个层次，每个层次中所拥有（关注）的内容都是不一样的；

- PO(Persistant Object) 持久对象
  用于表示数据库中的一条记录映射成的 java 对象。PO 仅仅用于表示数据，没有任何数据操作。通常遵守 Java Bean 的规范，拥有 getter/setter 方法。

  可以理解是**一个PO就是数据库中的一条记录**；可以理解某个事务依赖的原始数据；好处是可以将一条记录最为一个对象处理，可以方便转化为其他对象

- BO(Business Object) 业务对象
  封装对象、复杂对象，里面可能包含多个类
  主要作用是把业务逻辑封装为一个对象。这个对象可以包括一个或多个其它的对象。

  用于表示一个业务对象。BO 包括了业务逻辑，常常封装了对 DAO、RPC 等的调用，可以进行 PO 与 VO/DTO 之间的转换。BO 通常位于业务层，要区别于直接对外提供服务的服务层：BO 提供了基本业务单元的基本业务操作，在设计上属于被服务层业务流程调用的对象，一个业务流程可能需要调用多个 BO 来完成。

  比如一个简历，有教育经历、工作经历、社会关系等等。
  我们可以把教育经历对应一个PO，工作经历对应一个PO，社会关系对应一个PO。
  建立一个对应简历的BO对象处理简历，每个BO包含这些PO。
  这样处理业务逻辑时，我们就可以针对BO去处理。

- VO(Value Object) 表现对象
  前端界面展示；value object值对象；ViewObject表现层对象；主要对应界面显示的数据对象。对于一个WEB页面，或者SWT、SWING的一个界面，用一个VO对象对应整个界面的值；对于Android而言即是activity或view中的数据元素。

  用于表示一个与前端进行交互的 java 对象。有的朋友也许有疑问，这里可不可以使用 PO 传递数据？实际上，这里的 VO 只包含前端需要展示的数据即可，对于前端不需要的数据，比如数据创建和修改的时间等字段，出于减少传输数据量大小和保护数据库结构不外泄的目的，不应该在 VO 中体现出来。通常遵守 Java Bean 的规范，拥有 getter/setter 方法。

- DTO(Data Transfer Object) 数据传输对象
  前端调用时传输；也可理解成“上层”调用时传输;
  比如我们一张表有100个字段，那么对应的PO就有100个属性。但是我们界面上只要显示10个字段，客户端用WEB service来获取数据，没有必要把整个PO对象传递到客户端，这时我们就可以用只有这10个属性的DTO来传递结果到客户端，这样也不会暴露服务端表结构.到达客户端以后，如果用这个对象来对应界面显示，那此时它的身份就转为VO.

  用于表示一个数据传输对象。DTO 通常用于不同服务或服务不同分层之间的数据传输。DTO 与 VO 概念相似，并且通常情况下字段也基本一致。但 DTO 与 VO 又有一些不同，这个不同主要是设计理念上的，比如 API 服务需要使用的 DTO 就可能与 VO 存在差异。通常遵守 Java Bean 的规范，拥有 getter/setter 方法

- DAO(Data access object) 数据访问对象
  这个大家最熟悉，和上面几个O区别最大，基本没有互相转化的可能性和必要.，主要用来封装对数据库的访问。通过它可以把POJO持久化为PO，用PO组装出来VO、DTO；

  用于表示一个数据访问对象。使用 DAO 访问数据库，包括插入、更新、删除、查询等操作，与 PO 一起使用。DAO 一般在持久层，完全封装数据库操作，对外暴露的方法使得上层应用不需要关注数据库相关的任何信息。

- POJO(Plain ordinary java object) 简单java对象

一个POJO持久化以后就是PO；直接用它传递、传递过程中就是DTO；直接用来对应表示层就是VO。

举个例子：
事情：统计研发部门中的季度绩效（暂定以工程师填写的为准，当然实际上大部分不是）
过程：CTO发布统计绩效请求（附带要求：每个人对应的绩效等级）->各个组（也可以是子部门）负责人发布统计绩效请求（每个对应的绩效等级，并将绩效分为了3个方面）->每位开发工程师统计自己绩效（自身各个方面）；
可以从例子中看到：每个责任人要求都不同；
对于CTO，他需要知道的是该季度所用员工的绩效等级；这里可以认为VO：员工姓名、绩效等级；
开发工程师：需将本人这个季度的各个方面的表现都列出来：员工姓名、绩效等级、Ａ方面表现内容及等级、B方面表现内容及等级、C方面表现内容及等级、D方面表现内容及等级、E方面表现内容及等级、F方面表现内容及等级、E方面表现内容及等级；此处可认为是PO：员工姓名、绩效等级、A方面表现内容、A方面等级….E方面表现内容、E方面等级；
然后开发工程师将员工姓名、绩效等级、Ａ方面表现内容及等级、B方面表现内容及等级、C方面表现内容及等级内容传递给小组负责人；此处传递的对象就是DTO
小组负责人：从开发工程师中获取到数据后，经过评定，然后得出员工姓名、绩效等级、原因；此处的评定，可以理解为ＢＯ；

例子是根据当前正在进行的绩效评估事件总结，简单的例子可以参照上述知乎上面大神的回答。