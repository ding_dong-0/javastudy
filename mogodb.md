# [在Docker中安装MongoDB](https://www.cnblogs.com/peyshine/p/12876471.html)

## 1.版本选取

访问mongodb的镜像仓库地址：https://hub.docker.com/_/mongo?tab=tags&page=1

这里选取最新版本进行安装，如果想安装其他的可用版本，可以使用命令“docker search mongo”来查看

```powershell
[root@localhost ~]# docker search mongo
```

 

## 2.拉取最新版本镜像

这里执行命令"sudo docker pull mongo:latest" 拉取最新版的mongodb镜像文件

```powershell
sudo docker pull mongo
```

等待镜像拉取完成后，通过命令"sudo docker images"查看下拉取的镜像，可以看到，已经成功拉取到了本地

```shell
sudo docker images
```



## 3.开始运行

创建一个文件夹，用作mongodb的数据目录挂载

 ```powershell
[root@localhost ~]# mkdir -p /data/mongo
[root@localhost ~]# docker run -p 27017:27017 -v /data/mongo:/data/db --name mongodb -d mongo
 ```

```powershell

```



运行启动命令“docker run -p 27017:27017 -v /data/mongo:/data/db --name mongodb -d mongo”

![img](images/645287-20200512162016892-1127181293.png)

在上面的命令中，几个命令参数的详细解释如下：

-p 映射容器服务的 27017 端口到宿主机的 27017 端口。外部可以直接通过 宿主机 ip:27017 访问到 mongo 的服务

-v 为设置容器的挂载目录，这里是将本机的“/data/mongo”目录挂载到容器中的/data/db中，作为 mongodb 的存储目录

--name 为设置该容器的名称

-d 设置容器以守护进程方式运行

 

通过命令“docker ps”查看容器启动运行情况



可以看到mongo容器已经成功运行起来了

 

## 4.使用客户端工具Studio 3T连接mongo

studiot 3T下载地址:https://studio3t.com/download/

 

## 5.创建mongo用户

可以看到一个空的mongo数据库，有一个不太完美的地方在于我们的mongo没有任何账户密码，在裸奔，下面我们执行以下语句，创建一个管理员用户，

```

```

```powershell
db.createUser({
user: 'admin',
pwd: '123456',
roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
});　
```

 

## 6.指定auth重新运行服务

创建完用户我们就要重新启动mongo服务，并且指定auth

删除已经运行的mongo容器：

```powershell
docker rm -f mongodb
```

指定验证启动mongo容器：

```powershell
docker run -p 27017:27017 -v /data/mongo:/data/db --name mongodb -d mongo --auth
```

也就是在之前的启动命令后面加上 --auth：需要密码才能访问容器服务

 

此时强行访问，可以看到提示，没有授权

这里我们在连接的地方指定一下账户密码



再来查询，已经正确查询出来之前添加的用户信息



## 7.创建一个业务数据库和对应的读写账户

```powershell
use demo_db;
db.createUser({
  user: 'dev',
  pwd: 'Aa123456',
  roles: [ { role: "readWrite", db: "demo_db" } ]
  });
```

　　

然后用这个dev账号来登录



## 完整流程

```powershell
[root@localhost ~]# docker search mongo
[root@localhost ~]# sudo docker pull mongo
[root@localhost ~]# sudo docker images
[root@localhost ~]# mkdir -p /mydata/mongo
[root@localhost ~]# docker run -p 27017:27017 -v /mydata/mongo:/data/db --name mongodb -d mongo
[root@localhost ~]# docker ps
[root@localhost ~]# docker rm -f mongodb
[root@localhost ~]# docker run -p 27017:27017 -v /mydata/mongo:/data/db --name mongodb -d mongo --auth




```

