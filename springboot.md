



### 第一个SpringBoot

1. jdk
2. maven
3. springboot
4. idea

官方快速生成网站！IDEA集成





maven中package将springboot项目打成 .jar包

##### 怎样在wondows文件夹内进入命令窗口

进入指定文件夹内，空白处按**shift**及**鼠标右击**

选择**Powershell**

```powershell
PS F:\狂神学习相关\springboot\helloworld\target> java -jar .\helloworld-0.0.1-SNAPSHOT.jar

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::  (v2.3.0.BUILD-SNAPSHOT)

2020-02-06 22:11:11.170  INFO 22376 --- [           main] c.k.helloworld.HelloworldApplication     : Starting HelloworldApplication v0.0.1-SNAPSHOT on PC-20190710YZJT with PID 22376 (F:\狂神学习相关\springboot\helloworld\target\helloworld-0.0.1-SNAPSHOT.jar started by Administrator in F:\狂神学习相关\springboot\helloworld\target)
2020-02-06 22:11:11.170  INFO 22376 --- [           main] c.k.helloworld.HelloworldApplication     : No active profile set, falling back to default profiles: default
2020-02-06 22:11:13.643  INFO 22376 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2020-02-06 22:11:13.674  INFO 22376 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2020-02-06 22:11:13.674  INFO 22376 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.30]
2020-02-06 22:11:13.908  INFO 22376 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2020-02-06 22:11:13.908  INFO 22376 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 2629 ms
2020-02-06 22:11:14.236  INFO 22376 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2020-02-06 22:11:14.705  INFO 22376 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2020-02-06 22:11:14.721  INFO 22376 --- [           main] c.k.helloworld.HelloworldApplication     : Started HelloworldApplication in 4.381 seconds (JVM running for 5.186)

```



##### springboot热部署maven依赖

[SpringBoot热部署]: https://www.jianshu.com/p/a77636c86ede	"IDEA还需要配置一下"



```xml
<dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
        <optional>true</optional>
</dependency>

```

##### Spring Boot笔记之自定义启动banner

[Spring Boot笔记之自定义启动banner]: https://www.cnblogs.com/cc11001100/p/7456145.html

自定义banner只需要在resource下新建一个banner.txt文件，将我们需要的banner字样放进去即可。



##### 原理初探

###### 自动配置：

pom.xml

- spring-boot-dependencies:核心依赖在父工程中
- 我们在写或者引入一些springboot依赖时，不需要指定版本就是因为有这些版本仓库

###### 启动器

```xml
<!--启动器-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
</dependency>
```

如：spring-boot-starter-web，可帮我们自动导入web环境的所有依赖！

springboot会将所有的功能场景，都变成一个个的启动器

我们要使用什么功能，只需找到对应的启动器就可以starter

```xml
<!--Maven插件可以将项目打包为可执行jar包-->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

```

###### 主程序

```java
package com.kuang;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//标注这个类是一个springboot的应用
@SpringBootApplication
public class Springboot01HelloworldApplication {
    public static void main(String[] args) {
        //将springboot应用启动
        SpringApplication.run(Springboot01HelloworldApplication.class, args);
    }
}
```

- 注解

```
@SpringBootConfiguration
	@Configuration：spring的配置类
		@Component：说明是一个spring组件
@EnableAutoConfiguration：自动配置
	@AutoConfigurationPackage：自动配置包
		@Import(AutoConfigurationPackages.Registrar.class)：自动配置包导入选择器
    @Import(AutoConfigurationImportSelector.class)：自动导入选择
```



[SpringBoot：快速入门]: https://blog.kuangstudy.com/index.php/archives/630/

结论：所以，自动配置真正实现是从classpath中搜寻所有的META-INF/spring.factories配置文件 ，并将其中对应的 org.springframework.boot.autoconfigure. 包下的配置项（spring.factories的配置类不一定生效，要排断条件是否成立，只有导入了对应的start 就有对应的启动器了，自动装配才能生效，配置成功），通过反射实例化为对应标注了 @Configuration的JavaConfig形式的IOC容器配置类 ， 然后将这些都汇总成为一个实例并加载到IOC容器中。

1. SpringBoot在启动的时候从类路径下的META-INF/spring.factories中获取EnableAutoConfiguration指定的值
2. 将这些值作为自动配置类导入容器 ， 自动配置类就生效 ， 帮我们进行自动配置工作；
3. 以前我们需要自己配置的东西 ， 自动配置类都帮我们解决了
4. 整个J2EE的整体解决方案和自动配置都在springboot-autoconfigure的jar包中；
5. 它将所有需要导入的组件以全类名的方式返回 ， 这些组件就会被添加到容器中 ；
6. 它会给容器中导入非常多的自动配置类 （xxxAutoConfiguration）, 就是给容器中导入这个场景需要的所有组件 ， 并配置好这些组件 ；
7. 有了自动配置类 ， 免去了我们手动编写配置注入功能组件等的工作；

<img src="https://blog.kuangstudy.com/usr/uploads/2019/10/700745206.png" alt="img" style="zoom:200%;" />



##### yaml基础语法

```yaml
#k=v
#普通的key-value
name: qingjiang
#对象：
student:
  name: qinjiang
  age: 3
#行内写法
student1: {name: qinjiang,age: 3}
#数组
pets:
  -cat
  -dog
  -pig
pets1: [dog,cat,pig]
```

​       



yaml可以直接给实体类赋值:

[SpringBoot：配置文件及自动配置原理]: https://www.cnblogs.com/hellokuangshen/p/11259029.html

##### JSR303数据校验



![image-20191204201021915](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20191204201021915.png)

##### springboot配置

###### springboot的多环境配置：

application-test.properties
application-dev.properties
application.properties

```properties
#springboot的多环境配置：可以选择激活哪一个配置文件
#选择test环境
spring.profiles.active=test

```

###### 配置文件的4 个位置及其生效次序

![image-20191205100719236](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20191205100719236.png)



###### springboot中yaml多环境的配置

```yaml
server:
  port: 8081
#选择dev环境
spring:
  profiles:
    active: dev

---
server:
  port: 8082
spring:
  profiles: dev

---
server:
  port: 8083
spring:
  profiles: test

```



[JAVAEE——SpringBoot配置篇：配置文件、YAML语法、文件值注入、加载位置与顺序、自动配置原理]: https://www.cnblogs.com/xieyupeng/archive/2018/09/17/9664104.html

**这就是自动装配的原理！**

**精髓：**

**1）、SpringBoot启动会加载大量的自动配置类**

**2）、我们看我们需要的功能有没有在SpringBoot默认写好的自动配置类当中；**

**3）、我们再来看这个自动配置类中到底配置了哪些组件；（只要我们要用的组件存在在其中，我们就不需要再手动配置了）**

**4）、给容器中自动配置类添加组件的时候，会从properties类中获取某些属性。我们只需要在配置文件中指定这些属性的值即可；**

**xxxxAutoConfigurartion：自动配置类；**给容器中添加组件

**xxxxProperties:封装配置文件中相关属性；**



#### SpringBoot Web开发

jar:webapp

自动装配

springboot到底帮我们配置了什么？我们能不能修改？能修改哪些东西？能不能扩展？

xxxxAutoConfuguration  向容器中自动配置组件

xxxxProperties: 自动配置类，装配配置文件中自定义的一些内容！



解决的问题：

- 导入静态资源。。。
- 首页
- jsp,模板引擎Thymeleaf
- 装配扩展SpringMVC
- 增删改查
- 国际化！



总结：

1.在springboot,

```
"classpath:/META-INF/resources/", 
"classpath:/resources/",
 "classpath:/static/", 
"classpath:/public/",
"/" ：当前项目的根目录
```

优先级自上而下

### 



启动类中的@ComponentScan注解， 除了扫描@Service、@Repository、 @Component、 @Controller和@RestController等之外， 也
会扫描@Configuration注解的类





### 项目构建与部署构建JAR

```powershell
#将springboot生成的thymeleaf-0.0.1-SNAPSHOT.jar上传至centos根目录/
最后面的&表示让项目在后台运行。 由于在生产环境中，
Linux大多数情况下都是远程服务器， 开发者通过远程工具连接Linux，
如果使用上面的命令启动JAR， 一旦窗口关闭， JAR也就停止运行了，
因此一般通过如下命令启动JAR：
[root@localhost /]# java -jar thymeleaf-0.0.1-SNAPSHOT.jar &
#多了nohup， 表示当窗口关闭时服务不挂起， 继续在后台运行。
[root@localhost /]# nohup java -jar do_iptable.jar >/dev/null 2>&1 &
```

