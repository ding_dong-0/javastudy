```powershell
#df -T 只可以查看已经挂载的分区和文件系统类型。
[root@localhost ~]# df -T
文件系统            类型        1K-块     已用    可用 已用% 挂载点
devtmpfs            devtmpfs   909988        0  909988    0% /dev
tmpfs               tmpfs      924712        0  924712    0% /dev/shm
tmpfs               tmpfs      924712     9540  915172    2% /run
tmpfs               tmpfs      924712        0  924712    0% /sys/fs/cgroup
/dev/mapper/cl-root xfs      17811456 10095744 7715712   57% /
/dev/sda1           ext4       999320   224256  706252   25% /boot
tmpfs               tmpfs      184940       28  184912    1% /run/user/42
tmpfs               tmpfs      184940        4  184936    1% /run/user/0



#fdisk -l 可以显示出所有挂载和未挂载的分区，但不显示文件系统类型。
[root@localhost ~]# fdisk -l
Disk /dev/sda：20 GiB，21474836480 字节，41943040 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xe82b74c8

设备       启动    起点     末尾     扇区 大小 Id 类型
/dev/sda1  *       2048  2099199  2097152   1G 83 Linux
/dev/sda2       2099200 41943039 39843840  19G 8e Linux LVM




Disk /dev/mapper/cl-root：17 GiB，18249416704 字节，35643392 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节


Disk /dev/mapper/cl-swap：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节

#parted -l 可以查看未挂载的文件系统类型，以及哪些分区尚未格式化。
[root@localhost ~]# parted -l
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sda: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End     Size    Type     File system  标志
 1      1049kB  1075MB  1074MB  primary  ext4         启动
 2      1075MB  21.5GB  20.4GB  primary               lvm

#lsblk -f 也可以查看未挂载的文件系统类型。
[root@localhost ~]# lsblk -f
NAME        FSTYPE      LABEL UUID                                   MOUNTPOINT
sda                                                                  
├─sda1      ext4              ff037b8a-c85b-4d17-a919-7ab0b5ffb7e6   /boot
└─sda2      LVM2_member       wVXcRD-XcjB-A0V7-xf4R-IU4s-S7Ew-Pom5Jf 
  ├─cl-root xfs               546b9807-e6f9-44fd-8174-5a8edfd127bf   /
  └─cl-swap swap              f5bb0909-dd7d-4ee4-b437-bfc3d27e60b7   [SWAP]
sr0            

#file -s /dev/sda3
[root@localhost ~]# file -s /dev/sda2
/dev/sda2: LVM2 PV (Linux Logical Volume Manager), UUID: wVXcRD-XcjB-A0V7-xf4R-IU4s-S7Ew-Pom5Jf, size: 20400046080
[root@localhost ~]# 

[root@localhost ~]# df -T
文件系统            类型        1K-块    已用     可用 已用% 挂载点
devtmpfs            devtmpfs   909052       0   909052    0% /dev
tmpfs               tmpfs      924732       0   924732    0% /dev/shm
tmpfs               tmpfs      924732    9304   915428    2% /run
tmpfs               tmpfs      924732       0   924732    0% /sys/fs/cgroup
/dev/mapper/cl-root xfs      17811456 7736956 10074500   44% /
/dev/sda1           ext4       999320  137084   793424   15% /boot
tmpfs               tmpfs      184944       4   184940    1% /run/user/0



/dev/sdb1               /                             ext4    defaults        0 0 
[root@localhost ~]# vim /etc/fstab
[root@localhost ~]# mount -a

#磁盘情况查询
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             888M     0  888M    0% /dev
tmpfs                904M     0  904M    0% /dev/shm
tmpfs                904M  9.1M  894M    2% /run
tmpfs                904M     0  904M    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  7.4G  9.6G   44% /
/dev/sdb1            2.0G  6.0M  1.9G    1% /boot
tmpfs                181M  4.0K  181M    1% /run/user/0
[root@localhost ~]# df -lh
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             888M     0  888M    0% /dev
tmpfs                904M     0  904M    0% /dev/shm
tmpfs                904M  9.1M  894M    2% /run
tmpfs                904M     0  904M    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  7.4G  9.6G   44% /
/dev/sdb1            2.0G  6.0M  1.9G    1% /boot
tmpfs                181M  4.0K  181M    1% /run/user/0
#查询每个目录的磁盘占用情况
[root@localhost ~]# du -h

[root@localhost ~]# du -ach --max-depth=1 /opt
1.3G	/opt/module
520M	/opt/software
4.0K	/opt/test
1.8G	/opt
1.8G	总用量


[root@localhost opt]# rm -rf software/
#统计某个目录下有多少个文件
[root@localhost home]# ls -l /home|grep "^-" |wc -l
#统计某个目录下个数
[root@localhost home]# ls -l /home|grep "^d" |wc -l
-R包括子目录下文件及文件夹
[root@localhost home]# ls -lR /home|grep "^-" |wc -l
[root@localhost home]# ls -lR /home|grep "^-" |wc -l
#以树状图显示目录
[root@localhost home]# tree
.
├── atguigu
│   ├── 公共
│   ├── 模板
│   ├── 视频
│   ├── 图片
│   ├── 文档
│   ├── 下载
│   ├── 音乐
│   └── 桌面
├── houyingchao
├── newdisk
│   └── lost+found
└── zhangsan

13 directories, 0 files


  IPv4 地址 . . . . . . . . . . . . : 192.168.189.1
```





### 使用 iSCSI 服务部署网络存储

iscsi=internet+scsi

#### RAID10

```powershell
1.为虚拟机添加4块硬盘
[root@hadoop100 ~]# ll /dev/sd*
brw-rw----. 1 root disk 8,  0 4月  24 18:56 /dev/sda
brw-rw----. 1 root disk 8,  1 4月  24 18:56 /dev/sda1
brw-rw----. 1 root disk 8,  2 4月  24 18:56 /dev/sda2
brw-rw----. 1 root disk 8, 16 4月  24 18:56 /dev/sdb
brw-rw----. 1 root disk 8, 32 4月  24 18:56 /dev/sdc
brw-rw----. 1 root disk 8, 48 4月  24 18:56 /dev/sdd
brw-rw----. 1 root disk 8, 64 4月  24 18:56 /dev/sde
[root@hadoop100 ~]# lsblk -f
NAME   FSTYPE     LABEL                  UUID                                   MOUNTPOINT
sda                                                                             
├─sda1 ext4                              8fb29c11-6deb-4bfc-9412-5bbf5f70749c   /boot
└─sda2 LVM2_membe                        eECR5H-m3xa-D48j-JaYu-1qFs-3miM-qEdPIz 
  ├─cl-root
  │    xfs                               1a39b8c3-0d95-4fef-a0ca-e4a71f066314   /
  └─cl-swap
       swap                              c3f742af-d529-455d-ab90-ad047bc32e2e   [SWAP]
sdb                                                                             
sdc                                                                             
sdd                                                                             
sde                                                                             
sr0    iso9660    CentOS-8-BaseOS-x86_64 2019-08-15-21-52-52-00                 /run/media/root/CentOS-8-BaseO
#通过上述明亮查询到添加的硬盘为sdb、sdc、sdd、sde
[root@hadoop100 ~]# mdadm -Cv /dev/md0 -n 4 -l 10 /dev/sd[b-e]
mdadm: layout defaults to n2
mdadm: layout defaults to n2
mdadm: chunk size defaults to 512K
mdadm: size set to 2094080K
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.

[root@hadoop100 ~]# mdadm -D /dev/md0 
/dev/md0:
           Version : 1.2
     Creation Time : Fri Apr 24 19:16:51 2020
        Raid Level : raid10
        Array Size : 4188160 (3.99 GiB 4.29 GB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 4
     Total Devices : 4
       Persistence : Superblock is persistent

       Update Time : Fri Apr 24 19:17:11 2020
             State : clean 
    Active Devices : 4
   Working Devices : 4
    Failed Devices : 0
     Spare Devices : 0

            Layout : near=2
        Chunk Size : 512K

Consistency Policy : resync

              Name : hadoop100:0  (local to host hadoop100)
              UUID : 17dcaf86:07099fad:1495421f:47c7d9a7
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       8       16        0      active sync set-A   /dev/sdb
       1       8       32        1      active sync set-B   /dev/sdc
       2       8       48        2      active sync set-A   /dev/sdd
       3       8       64        3      active sync set-B   /dev/sde

[root@hadoop100 ~]# 




[root@hadoop100 ~]# yum -y install targetd targetcli
Repository AppStream is listed more than once in the configuration
Repository extras is listed more than once in the configuration
Repository PowerTools is listed more than once in the configuration
Repository centosplus is listed more than once in the configuration
上次元数据过期检查：1 day, 9:15:20 前，执行于 2020年04月24日 星期五 22时54分46秒。
未找到匹配的参数： targetd
Package targetcli-2.1.fb49-1.el8.noarch is already installed.
错误：没有任何匹配
[root@hadoop100 ~]# systemctl restart iscsid
[root@hadoop100 ~]# systemctl enable iscsid
Created symlink /etc/systemd/system/multi-user.target.wants/iscsid.service → /usr/lib/systemd/system/iscsid.service.
[root@hadoop100 ~]# 
#交互式配置
[root@hadoop100 ~]# targetcli
Warning: Could not load preferences file /root/.targetcli/prefs.bin.
targetcli shell version 2.1.fb49
Copyright 2011-2013 by Datera, Inc and others.
For help on commands, type 'help'.

/> ls
o- / ......................................................................................................................... [...]
  o- backstores .............................................................................................................. [...]
  | o- block .................................................................................................. [Storage Objects: 0]
  | o- fileio ................................................................................................. [Storage Objects: 0]
  | o- pscsi .................................................................................................. [Storage Objects: 0]
  | o- ramdisk ................................................................................................ [Storage Objects: 0]
  o- iscsi ............................................................................................................ [Targets: 0]
  o- loopback ......................................................................................................... [Targets: 0]
/> 
/> cd backstores/block/
/backstores/block> create disk /dev/md0
Created block storage object disk using /dev/md0.
/backstores/block> cd /
/> ls
o- / ......................................................................................................................... [...]
  o- backstores .............................................................................................................. [...]
  | o- block .................................................................................................. [Storage Objects: 1]
  | | o- disk ........................................................................... [/dev/md0 (4.0GiB) write-thru deactivated]
  | |   o- alua ................................................................................................... [ALUA Groups: 1]
  | |     o- default_tg_pt_gp ....................................................................... [ALUA state: Active/optimized]
  | o- fileio ................................................................................................. [Storage Objects: 0]
  | o- pscsi .................................................................................................. [Storage Objects: 0]
  | o- ramdisk ................................................................................................ [Storage Objects: 0]
  o- iscsi ............................................................................................................ [Targets: 0]
  o- loopback ......................................................................................................... [Targets: 0]
/> 
cd iscsi 
/iscsi> 
/iscsi> create
Created target iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb.
Created TPG 1.
Global pref auto_add_default_portal=true
Created default portal listening on all IPs (0.0.0.0), port 3260.
/iscsi> ls
o- iscsi .............................................................................................................. [Targets: 1]
  o- iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb ......................................................... [TPGs: 1]
    o- tpg1 ................................................................................................. [no-gen-acls, no-auth]
      o- acls ............................................................................................................ [ACLs: 0]
      o- luns ............................................................................................................ [LUNs: 0]
      o- portals ...................................................................................................... [Portals: 1]
        o- 0.0.0.0:3260 ....................................................................................................... [OK]
/iscsi> cd iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb/
/iscsi/iqn.20....32f217e1daeb> ls
o- iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb ........................................................... [TPGs: 1]
  o- tpg1 ................................................................................................... [no-gen-acls, no-auth]
    o- acls .............................................................................................................. [ACLs: 0]
    o- luns .............................................................................................................. [LUNs: 0]
    o- portals ........................................................................................................ [Portals: 1]
      o- 0.0.0.0:3260 ......................................................................................................... [OK]
/iscsi/iqn.20....32f217e1daeb> cd tpg1/luns 
/iscsi/iqn.20...aeb/tpg1/luns> create /backstores/block/disk 
Created LUN 0.
/iscsi/iqn.20...aeb/tpg1/luns> cd ..
/iscsi/iqn.20...17e1daeb/tpg1> cd acls 
/iscsi/iqn.20...aeb/tpg1/acls> create iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb:client
Created Node ACL for iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb:client
Created mapped LUN 0.
/iscsi/iqn.20...aeb/tpg1/acls> 
/iscsi/iqn.20...17e1daeb/tpg1> cd portals/
/iscsi/iqn.20.../tpg1/portals> crrate 192.168.40.100
Command not found crrate
/iscsi/iqn.20.../tpg1/portals> create 192.168.40.100
Using default IP port 3260
Could not create NetworkPortal in configFS
iscsi/iqn.20.../tpg1/portals> delete 0.0.0.0 3260
Deleted network portal 0.0.0.0:3260
/iscsi/iqn.20.../tpg1/portals> create 192.168.40.100
Using default IP port 3260
Created network portal 192.168.40.100:3260.

/iscsi/iqn.20.../tpg1/portals>ls /
o- / ......................................................................................................................... [...]
  o- backstores .............................................................................................................. [...]
  | o- block .................................................................................................. [Storage Objects: 1]
  | | o- disk ............................................................................. [/dev/md0 (4.0GiB) write-thru activated]
  | |   o- alua ................................................................................................... [ALUA Groups: 1]
  | |     o- default_tg_pt_gp ....................................................................... [ALUA state: Active/optimized]
  | o- fileio ................................................................................................. [Storage Objects: 0]
  | o- pscsi .................................................................................................. [Storage Objects: 0]
  | o- ramdisk ................................................................................................ [Storage Objects: 0]
  o- iscsi ............................................................................................................ [Targets: 1]
  | o- iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb ....................................................... [TPGs: 1]
  |   o- tpg1 ............................................................................................... [no-gen-acls, no-auth]
  |     o- acls .......................................................................................................... [ACLs: 1]
  |     | o- iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb:client ................................... [Mapped LUNs: 1]
  |     |   o- mapped_lun0 .................................................................................. [lun0 block/disk (rw)]
  |     o- luns .......................................................................................................... [LUNs: 1]
  |     | o- lun0 ....................................................................... [block/disk (/dev/md0) (default_tg_pt_gp)]
  |     o- portals .................................................................................................... [Portals: 1]
  |       o- 192.168.40.100:3260 .............................................................................................. [OK]
  o- loopback ......................................................................................................... [Targets: 0]
/iscsi/iqn.20.../tpg1/portals> 
/iscsi/iqn.20.../tpg1/portals> exit
Global pref auto_save_on_exit=true
Configuration saved to /etc/target/saveconfig.json

出错了
[root@hadoop100 ~]# systemctl restart targetd
Failed to restart targetd.service: Unit targetd.service not found.


#记住
iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb


配置客户端
192.168.40.101

[root@hadoop101 ~]# yum install iscsi-initiator-utils
[root@hadoop101 ~]# vim /etc/iscsi/initiatorname.iscsi 
[root@hadoop101 ~]# cat /etc/iscsi/initiatorname.iscsi 
iqn.2003-01.org.linux-iscsi.hadoop100.x8664:sn.32f217e1daeb
[root@hadoop101 ~]# systemctl restart iscsid
[root@hadoop101 ~]# systemctl enable iscsid
Created symlink /etc/systemd/system/multi-user.target.wants/iscsid.service → /usr/lib/systemd/system/iscsid.service.
[root@hadoop101 ~]# 
```

客户端192.168.40.101

# ISCSI报错：Could not create NetworkPortal in configFShttps://blog.51cto.com/13587182/2087607



```powershell
[root@localhost ~]# yum install bind-chroot

```









### 使用 PXE+Kickstart 无人值守安装服务

```powershell
[root@localhost ~]# yum install dhcp




```







```powershell
[root@localhost ~]# hostnamectl set-hostname localhost
[root@localhost ~]# ulimit -u
14919
[root@localhost ~]# ulimit -u 10000
[root@localhost ~]# ulimit -u
10000
#列出当前执行过的命令。
[root@localhost ~]# fc -l

#查看内存使用情况
[root@localhost ~]# free 
              total        used        free      shared  buff/cache   available
Mem:        3848816     1178104     1756900       18256      913812     2399952
Swap:       4194296           0     4194296
[root@localhost ~]# free -g
              total        used        free      shared  buff/cache   available
Mem:              3           1           1           0           0           2
Swap:             3           0           3
[root@localhost ~]# free -m
              total        used        free      shared  buff/cache   available
Mem:           3758        1150        1716          17         892        2344
Swap:          4095           0        4095
[root@localhost ~]# uptime
21:41:18 up 9 min,  1 user,  load average: 0.06, 0.50, 0.47
 
 
 #按1显示CPU核数及其使用情况
[root@localhost ~]# top
top - 21:39:57 up 8 min,  1 user,  load average: 0.08, 0.64, 0.51
Tasks: 226 total,   1 running, 225 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.1 sy,  0.0 ni, 99.7 id,  0.0 wa,  0.2 hi,  0.1 si,  0.0 st
MiB Mem :   3758.6 total,   1787.3 free,   1131.5 used,    839.8 buff/cache
MiB Swap:   4096.0 total,   4096.0 free,      0.0 used.   2360.0 avail Mem 
 
   PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                       
  6906 mysql     20   0 2450596 421436  29924 S   0.7  10.9   0:05.63 mysqld                                                        
  7560 root      20   0   64212   4852   3984 R   0.3   0.1   0:00.31 top                                                           
     1 root      20   0  244660  13940   8972 S   0.0   0.4   0:04.73 systemd                                                       
     2 root      20   0       0      0      0 S   0.0   0.0   0:00.04 kthreadd                                                      
     3 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 rcu_gp                                                        
     4 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 rcu_par_gp                                                    
     5 root      20   0       0      0      0 I   0.0   0.0   0:02.23 kworker/0:0-events_power_efficient                            
     6 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 kworker/0:0H-kblockd                                          
     7 root      20   0       0      0      0 I   0.0   0.0   0:00.31 kworker/u256:0-flush-253:0                                    
     8 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 mm_percpu_wq                                                  
     9 root      20   0       0      0      0 S   0.0   0.0   0:00.04 ksoftirqd/0                                                   
    10 root      20   0       0      0      0 I   0.0   0.0   0:00.67 rcu_sched                                                     
    11 root      rt   0       0      0      0 S   0.0   0.0   0:00.03 migration/0                                                   
    12 root      rt   0       0      0      0 S   0.0   0.0   0:00.00 watchdog/0                                                    
    13 root      20   0       0      0      0 S   0.0   0.0   0:00.00 cpuhp/0                                                       
    14 root      20   0       0      0      0 S   0.0   0.0   0:00.00 cpuhp/1                                                       
    15 root      rt   0       0      0      0 S   0.0   0.0   0:00.00 watchdog/1                                                    
    16 root      rt   0       0      0      0 S   0.0   0.0   0:00.00 migration/1                                                   
    17 root      20   0       0      0      0 S   0.0   0.0   0:00.01 ksoftirqd/1               [root@localhost ~]# vmstat -n 2 3
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 0  0      0 1767840   4344 909108    0    0   146    29  131  158  1  2 94  4  0
 0  0      0 1767644   4344 909108    0    0     0     0  248  455  0  0 100  0  0
 0  0      0 1767644   4344 909108    0    0     0     0  231  441  0  0 100  0  0
 
 #磁盘空间
[root@localhost ~]# df
文件系统               1K-块     已用    可用 已用% 挂载点
devtmpfs             1909684        0 1909684    0% /dev
tmpfs                1924408        0 1924408    0% /dev/shm
tmpfs                1924408    18056 1906352    1% /run
tmpfs                1924408        0 1924408    0% /sys/fs/cgroup
/dev/mapper/cl-root 17811456 13430404 4381052   76% /
/dev/sdb1            5231616    87932 5143684    2% /newdisk
/dev/sda1             999320   226260  704248   25% /boot
tmpfs                 384880       28  384852    1% /run/user/42
tmpfs                 384880        4  384876    1% /run/user/0

[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                1.9G     0  1.9G    0% /dev/shm
tmpfs                1.9G   18M  1.9G    1% /run
tmpfs                1.9G     0  1.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   13G  4.2G   76% /
/dev/sdb1            5.0G   86M  5.0G    2% /newdisk
/dev/sda1            976M  221M  688M   25% /boot
tmpfs                376M   28K  376M    1% /run/user/42
tmpfs                376M  4.0K  376M    1% /run/user/0

[root@localhost ~]# ifstat
#kernel
Interface        RX Pkts/Rate    TX Pkts/Rate    RX Data/Rate    TX Data/Rate  
                 RX Errs/Drop    TX Errs/Drop    RX Over/Rate    TX Coll/Rate  
lo                   609 0           609 0         79988 0         79988 0      
                       0 0             0 0             0 0             0 0      
ens33               9306 0          6419 0        10377K 0         2957K 0      
                       0 0             0 0             0 0             0 0      
virbr0                 0 0             0 0             0 0             0 0      
                       0 0             0 0             0 0             0 0      
docker0                0 0             0 0             0 0             0 0      
                       0 0             0 0             0 0             0 0      

[root@localhost ~]# ps -mp 8652 -o THREAD,tid,time
USER     %CPU PRI SCNT WCHAN  USER SYSTEM    TID     TIME
root      0.0   -    - -         -      -      - 00:00:00
root      0.0  19    - -         -      -   8652 00:00:00
```





### shell学习

```powershell
[root@localhost ~]# mkdir shellstudy
[root@localhost ~]# cd shellstudy/
[root@localhost shellstudy]# vim myShell.h
[root@localhost shellstudy]# cat myShell.h 
#!/bin/bash
echo "hello world!"
[root@localhost shellstudy]# ll
总用量 4
-rw-r--r--. 1 root root 32 5月  28 07:45 myShell.h
[root@localhost shellstudy]# chmod 744 myShell.h 
[root@localhost shellstudy]# ll
总用量 4
-rwxr--r--. 1 root root 32 5月  28 07:45 myShell.h
[root@localhost shellstudy]# ./myShell.h 
hello world!
[root@localhost shellstudy]# /root/shellstudy/myShell.h 
hello world!
[root@localhost shellstudy]# chmod 644 myShell.h 
[root@localhost shellstudy]# bash ./myShell.h 
hello world!
[root@localhost shellstudy]# sh ./myShell.h
hello world!
```



```powershell
[root@localhost shellstudy]# vim myShell.sh 
[root@localhost shellstudy]# cat myShell.sh 
#!/bin/bash
echo "hello world!"

echo "PATH=$PATH"
echo "user=$USER"
[root@localhost shellstudy]# ./myShell.sh 
hello world!
PATH=//usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/module/jdk1.8.0_144/bin:/usr/local/mycat/bin:/opt/apache-maven-3.6.3/bin:/root/bin
user=root
[root@localhost shellstudy]# 

#分页显示
[root@localhost shellstudy]# set | more
BASH=/bin/bash
BASHOPTS=checkwinsize:cmdhist:complete_fullquote:expand_aliases:extglob:extquote:force_fignore:histappend:interactive_comments:login_s
hell:progcomp:promptvars:sourcepath
BASHRCSOURCED=Y
BASH_ALIASES=()
BASH_ARGC=()
BASH_ARGV=()
BASH_CMDS=()
BASH_COMPLETION_VERSINFO=([0]="2" [1]="7")
BASH_LINENO=()
BASH_REMATCH=()
BASH_SOURCE=()
BASH_VERSINFO=([0]="4" [1]="4" [2]="19" [3]="1" [4]="release" [5]="x86_64-redhat-linux-gnu")
BASH_VERSION='4.4.19(1)-release'
CATALINA_HOME=/opt/apache-tomcat-9.0.30
COLUMNS=134
COMP_WORDBREAKS=$' \t\n"\'><=;|&(:'
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/0/bus
DIRSTACK=()
DISPLAY=localhost:10.0


[root@localhost shellstudy]# vim myShell.sh 
[root@localhost shellstudy]# cat myShell.sh 
#!/bin/bash
A=100
echo "A=$A"
unset A
echo "A=$A"
[root@localhost shellstudy]# ./myShell.sh 
A=100
A=



[root@localhost shellstudy]# vim myShell.sh 
[root@localhost shellstudy]# cat myShell.sh 
#!/bin/bash
readonly A=99
echo "A=$A"
unset A
echo "A=$A"
[root@localhost shellstudy]# ./myShell.sh 
A=99
./myShell.sh: 第 14 行:unset: A: 无法取消设定: 只读 variable
A=99




[root@localhost shellstudy]# vim myShell.sh 
[root@localhost shellstudy]# cat myShell.sh 
#!/bin/bash
RESULT=`ls -l /home`
echo "$RESULT"
[root@localhost shellstudy]# ./myShell.sh 
总用量 12
drwx------. 22 houyingchao houyingchao 4096 5月  21 14:32 houyingchao
drwx------. 15 hyc         hyc         4096 4月  22 19:44 hyc
drwx------. 15 lisi        lisi        4096 5月  21 14:37 lisi
drwx------.  3 mycat       mycat         78 1月  31 17:27 mycat
drwx------.  3 xiaoguo     xiaoguo       78 4月  22 18:33 xiaoguo
d---rwx---+  2 root        root           6 4月  23 08:39 xiaoqian
drwxrwsrwx.  2 root        root          18 4月  23 07:22 xiaoyun
drwx------.  3 xiaozheng   xiaozheng     78 4月  22 18:37 xiaozheng
drwx------.  3 zhangsan    zhangsan      99 4月  23 07:36 zhangsan
[root@localhost shellstudy]# 


[root@localhost shellstudy]# vim myShell.sh 
[root@localhost shellstudy]# cat myShell.sh 
#!/bin/bash
RESULT=`ls -l /home`
echo "$RESULT"
echo ""
MY_DATE=$(date)
echo "date=$MY_DATE"
[root@localhost shellstudy]# ./myShell.sh 
总用量 12
drwx------. 22 houyingchao houyingchao 4096 5月  21 14:32 houyingchao
drwx------. 15 hyc         hyc         4096 4月  22 19:44 hyc
drwx------. 15 lisi        lisi        4096 5月  21 14:37 lisi
drwx------.  3 mycat       mycat         78 1月  31 17:27 mycat
drwx------.  3 xiaoguo     xiaoguo       78 4月  22 18:33 xiaoguo
d---rwx---+  2 root        root           6 4月  23 08:39 xiaoqian
drwxrwsrwx.  2 root        root          18 4月  23 07:22 xiaoyun
drwx------.  3 xiaozheng   xiaozheng     78 4月  22 18:37 xiaozheng
drwx------.  3 zhangsan    zhangsan      99 4月  23 07:36 zhangsan

date=2020年 05月 28日 星期四 08:34:00 CST
[root@localhost shellstudy]# 







```





### 环境变量

```powershell
#JAVA_HOME
export JAVA_HOME=/opt/module/jdk1.8.0_144
export PATH=$PATH:$JAVA_HOME/bin


#TOMCAT环境变量
export  TOMCAT_HOME=/opt/apache-tomcat-9.0.30
export CATALINA_HOME=/opt/apache-tomcat-9.0.30


#mycat配置环境变量
export MYCAT_HOME=/usr/local/mycat
export PATH=/$PATH:$MYCAT_HOME/bin


#maven 环境变量
export MAVEN_HOME=/opt/apache-maven-3.6.3
export PATH=$PATH:${MAVEN_HOME}/bin

```





```
source /etc/profile
```





```powershell
[root@localhost shellstudy]# cat myShell.sh 
#!/bin/bash
#多行注释 
:<<!
RESULT=`ls -l /home`
echo "$RESULT"
echo ""
MY_DATE=$(date)
echo "date=$MY_DATE"
!
echo $TOMCAT_HOME
[root@localhost shellstudy]# 
[root@localhost shellstudy]# ./myShell.sh 
/opt/apache-tomcat-9.0.30

```

```powershell
[root@localhost shellstudy]# vim positionPara.sh 
[root@localhost shellstudy]# chmod 744 positionPara.sh 
[root@localhost shellstudy]# cat positionPara.sh 
#!/bin/bash
#获取到各个参数
echo "$0 $1 $2"
echo "$*"
echo "$@"
echo "参数个数=$#"
[root@localhost shellstudy]# ./positionPara.sh 
./positionPara.sh  


参数个数=0
[root@localhost shellstudy]# ./positionPara.sh 45 7
./positionPara.sh 45 7
45 7
45 7
参数个数=2
[root@localhost shellstudy]# 

```



$$ （功能描述：当前进程的进程号（PID））

$!	（功能描述：后台运行的最后一个进程的进程号（PID））

$？ （功能描述：最后一次执行的命令的返回状态。如果这个变量的值为 0，证明上一个命令正确执行；如果这个变量的值为非 0（具体是哪个数，由命令自己来决定），则证明上一个命令执行不正确了。

```powershell
[root@localhost shellstudy]# vim preVar.sh
[root@localhost shellstudy]# ll
总用量 12
-rwxr--r--. 1 root root 323 5月  28 08:45 myShell.sh
-rwxr--r--. 1 root root  94 5月  28 09:01 positionPara.sh
-rw-r--r--. 1 root root 166 5月  28 09:13 preVar.sh
[root@localhost shellstudy]# chmod 744 preVar.sh 
[root@localhost shellstudy]# cat preVar.sh 
#!/bin/bash

echo "当前的进程号=$$"
#后台的方式运行myShell  & 表示后台运行
./myShell.sh &
echo "最后的进程号=$!"
echo "执行的值=$?"

[root@localhost shellstudy]# ./preVar.sh 
当前的进程号=9088
最后的进程号=9089
执行的值=0
[root@localhost shellstudy]# /opt/apache-tomcat-9.0.30
^C
[root@localhost shellstudy]#
```





```powershell
[root@localhost ~]# free
              total        used        free      shared  buff/cache   available
Mem:         817272      544220       63888        1400      209164      125616
Swap:       7340024      612096     6727928
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:          798Mi       531Mi        51Mi       1.0Mi       214Mi       122Mi
Swap:         7.0Gi       598Mi       6.4Gi
[root@localhost ~]# top

top - 05:24:18 up 15:19,  1 user,  load average: 0.23, 0.32, 0.25
Tasks: 222 total,   1 running, 221 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.8 us,  1.8 sy,  0.0 ni, 96.8 id,  0.0 wa,  0.5 hi,  0.1 si,  0.0 st
MiB Mem :    798.1 total,     64.6 free,    530.8 used,    202.7 buff/cache
MiB Swap:   7168.0 total,   6569.7 free,    598.2 used.    123.5 avail Mem 

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                       
    60 root      20   0       0      0      0 S   0.7   0.0   1:34.62 kswapd0                                                       
 20177 root      20   0   64204   1660   1212 R   0.7   0.2   0:01.58 top                                                           
     1 root      20   0  245156   5784   2728 S   0.3   0.7   1:19.72 systemd                                                       
  1444 root      20   0  248644   2636   1616 S   0.3   0.3   0:15.44 rsyslogd                                                      
 13737 systemd+  20   0 3259256  10312    932 S   0.3   1.3   4:31.61 beam.smp 
 
 [root@localhost ~]# uptime
 05:28:06 up 15:22,  1 user,  load average: 0.09, 0.18, 0.20
#查询每个目录的磁盘占用情况
[root@localhost ~]# du -ach --max-depth=1 /opt
1.4G	/opt/module
36M	/opt/cni
0	/opt/containerd
143M	/opt/soft
0	/opt/downloads
1.5G	/opt
1.5G	总用量
[root@localhost ~]# df -h
[root@localhost ~]# df -T

[root@localhost ~]# top -c
[root@localhost ~]# top -Hp 14235

```



ps aux

当一个进程kiss不掉时

ps -ef |grep 僵尸进程id ，找到父进程id ,kiss调即可



du -h 查看各个目录占用的磁盘空间大小，看看不是不是那个目录有大量文件

du -h>hyceshi.log   把结果输入到hyceshi.log文件中