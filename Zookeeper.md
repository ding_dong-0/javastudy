# Zookeeper安装

## 本地模式安装部署

```powershell
1．安装前准备

（1）安装Jdk

（2）拷贝Zookeeper安装包到Linux系统下

（3）解压到指定目录

[atguigu@hadoop102 software]$ tar -zxvf zookeeper-3.4.10.tar.gz -C /opt/module/

2．配置修改

（1）将/opt/module/zookeeper-3.4.10/conf这个路径下的zoo_sample.cfg修改为zoo.cfg；

[atguigu@hadoop102 conf]$ mv zoo_sample.cfg zoo.cfg

（2）打开zoo.cfg文件，修改dataDir路径：

[atguigu@hadoop102 zookeeper-3.4.10]$ vim zoo.cfg

修改如下内容：

dataDir=/opt/module/zookeeper-3.4.10/zkData

（3）在/opt/module/zookeeper-3.4.10/这个目录上创建zkData文件夹

[atguigu@hadoop102 zookeeper-3.4.10]$ mkdir zkData

3．操作Zookeeper

（1）启动Zookeeper

[atguigu@hadoop102 zookeeper-3.4.10]$ bin/zkServer.sh start

（2）查看进程是否启动

[atguigu@hadoop102 zookeeper-3.4.10]$ jps

4020 Jps

4001 QuorumPeerMain

（3）查看状态：

[atguigu@hadoop102 zookeeper-3.4.10]$ bin/zkServer.sh status

ZooKeeper JMX enabled by default

Using config: /opt/module/zookeeper-3.4.10/bin/../conf/zoo.cfg

Mode: standalone

（4）启动客户端：

[atguigu@hadoop102 zookeeper-3.4.10]$ bin/zkCli.sh

（5）退出客户端：

[zk: localhost:2181(CONNECTED) 0] quit

（6）停止Zookeeper

[atguigu@hadoop102 zookeeper-3.4.10]$ bin/zkServer.sh stop
```



```powershell
[root@server1 ~]# mkdir /opt/module
[root@server1 ~]# cd /opt/module
[root@server1 module]# pwd
/opt/module
#上传zookeeper-3.4.10.tar.gz至该目录
[root@server1 module]# tar -zxvf zookeeper-3.4.10.tar.gz 
[root@server1 module]# ll
总用量 34228
drwxr-xr-x. 10 mycat mycat     4096 3月  23 2017 zookeeper-3.4.10
-rw-r--r--.  1 root  root  35042811 2月   7 01:06 zookeeper-3.4.10.tar.gz

[root@server1 module]# cd zookeeper-3.4.10/
[root@server1 zookeeper-3.4.10]# ll
[root@server1 zookeeper-3.4.10]# cd conf/
[root@server1 conf]# pwd
/opt/module/zookeeper-3.4.10/conf
[root@server1 conf]# cp zoo_sample.cfg zoo.cfg
[root@server1 conf]# vim zoo.cfg

dataDir=/opt/module/zookeeper-3.4.10/zkData

[root@server1 zookeeper-3.4.10]# mkdir zkData
[root@server1 zookeeper-3.4.10]# cd zkData/
[root@server1 zkData]# pwd
/opt/module/zookeeper-3.4.10/zkData

#启动

[root@server1 zookeeper-3.4.10]# bin/zkServer.sh start
ZooKeeper JMX enabled by default
Using config: /opt/module/zookeeper-3.4.10/bin/../conf/zoo.cfg
Starting zookeeper ... STARTED
#查看进程是否启动
[root@server1 zookeeper-3.4.10]# jps
3700 QuorumPeerMain
3735 Jps
[root@server1 zookeeper-3.4.10]# bin/zkServer.sh status
ZooKeeper JMX enabled by default
Using config: /opt/module/zookeeper-3.4.10/bin/../conf/zoo.cfg
Mode: standalone
```





## *\*2.2\** *\*配置参数\***\*解读\**

Zookeeper中的配置文件zoo.cfg中参数含义解读如下：

1．tickTime =2000：通信心跳数，Zookeeper服务器与客户端心跳时间，单位毫秒

Zookeeper使用的基本时间，服务器之间或客户端与服务器之间维持心跳的时间间隔，也就是每个tickTime时间就会发送一个心跳，时间单位为毫秒。

它用于心跳机制，并且设置最小的session超时时间为两倍心跳时间。(session的最小超时时间是2*tickTime)

2．initLimit =10：LF初始通信时限

集群中的Follower跟随者服务器与Leader领导者服务器之间初始连接时能容忍的最多心跳数（tickTime的数量），用它来限定集群中的Zookeeper服务器连接到Leader的时限。

3．syncLimit =5：LF同步通信时限

集群中Leader与Follower之间的最大响应时间单位，假如响应超过syncLimit * tickTime，Leader认为Follwer死掉，从服务器列表中删除Follwer。

4．dataDir：数据文件目录+数据持久化路径

主要用于保存Zookeeper中的数据。

5．clientPort =2181：客户端连接端口

监听客户端连接的端口。



```powershell
[atguigu@hadoop201 conf]$  vim zoo.cfg

server.1=hadoop201:2888:3888
server.2=hadoop202:2888:3888
server.3=hadoop203:2888:3888

[atguigu@hadoop201 conf]$ xsync zoo.cfg

[atguigu@hadoop201 zookeeper-3.4.10]$ bin/zkServer.sh start
[atguigu@hadoop202 zookeeper-3.4.10]$ bin/zkServer.sh start
[atguigu@hadoop203 zookeeper-3.4.10]$ bin/zkServer.sh start

jps

[atguigu@hadoop201 zookeeper-3.4.10]$ bin/zkServer.sh status
[atguigu@hadoop202 zookeeper-3.4.10]$ bin/zkServer.sh status
[atguigu@hadoop203 zookeeper-3.4.10]$ bin/zkServer.sh status

[atguigu@hadoop201 zookeeper-3.4.10]$  bin/zkCli.sh

[zk: localhost:2181(CONNECTED) 0] help
ZooKeeper -server host:port cmd args
	stat path [watch]
	set path data [version]
	ls path [watch]
	delquota [-n|-b] path
	ls2 path [watch]
	setAcl path acl
	setquota -n|-b val path
	history 
	redo cmdno
	printwatches on|off
	delete path [version]
	sync path
	listquota path
	rmr path
	get path [watch]
	create [-s] [-e] path data acl
	addauth scheme auth
	quit 
	getAcl path
	close 
	connect host:port
[zk: localhost:2181(CONNECTED) 1] ls
[zk: localhost:2181(CONNECTED) 2] ls2
[zk: localhost:2181(CONNECTED) 5] create /sanguo "jinlian"
Created /sanguo
[zk: localhost:2181(CONNECTED) 6] ls /
[zookeeper, sanguo]
[zk: localhost:2181(CONNECTED) 7] create /sanguo/shuguo "liubei"
Created /sanguo/shuguo
[zk: localhost:2181(CONNECTED) 8] ls /
[zookeeper, sanguo]
[zk: localhost:2181(CONNECTED) 9] ls /sanguo
[shuguo]
[zk: localhost:2181(CONNECTED) 10] get /sanguo/shuguo
liubei
cZxid = 0x100000003
ctime = Fri Feb 14 12:06:40 CST 2020
mZxid = 0x100000003
mtime = Fri Feb 14 12:06:40 CST 2020
pZxid = 0x100000003
cversion = 0
dataVersion = 0
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 6
numChildren = 0

[zk: localhost:2181(CONNECTED) 11] create -e /sanguo/wuguo "zhouyu"
Created /sanguo/wuguo
[zk: localhost:2181(CONNECTED) 12] ls /sanguo/wuguo
[]
[zk: localhost:2181(CONNECTED) 13] ls /sanguo
[wuguo, shuguo]
[zk: localhost:2181(CONNECTED) 16] quit
[atguigu@hadoop201 zookeeper-3.4.10]$ bin/zkCli.sh
[zk: localhost:2181(CONNECTED) 0] ls /sanguo
[shuguo]
[zk: localhost:2181(CONNECTED) 1] create /sanguo/weiguo "caocao4~"
Created /sanguo/weiguo
[zk: localhost:2181(CONNECTED) 2] create -s /sanguo/weiguo "cao"  
Created /sanguo/weiguo0000000003
[zk: localhost:2181(CONNECTED) 3] ls /sanguo
[weiguo0000000003, shuguo, weiguo]
[zk: localhost:2181(CONNECTED) 4] create -s /sanguo/weiguo "cao"
Created /sanguo/weiguo0000000004
[zk: localhost:2181(CONNECTED) 5] create -s /sanguo/weiguo "cao"
Created /sanguo/weiguo0000000005
[zk: localhost:2181(CONNECTED) 6] set /sanguo/shuguo "diaochan"
cZxid = 0x100000003
ctime = Fri Feb 14 12:06:40 CST 2020
mZxid = 0x10000000b
mtime = Fri Feb 14 12:21:24 CST 2020
pZxid = 0x100000003
cversion = 0
dataVersion = 1
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 8
numChildren = 0
[zk: localhost:2181(CONNECTED) 7] get /sanguo/shuguo
diaochan
cZxid = 0x100000003
ctime = Fri Feb 14 12:06:40 CST 2020
mZxid = 0x10000000b
mtime = Fri Feb 14 12:21:24 CST 2020
pZxid = 0x100000003
cversion = 0
dataVersion = 1
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 8
numChildren = 0
```

