### linux 下配置 redis开机自动启动

## 1. 新建一个文件

```powershell
vim /etc/init.d/redis
```

## 2. 将下面内容粘贴到文件中

```powershell
#!/bin/sh
#chkconfig:   2345 90 10
#description:  Redis is a persistent key-value database
PATH=/usr/local/bin:/sbin:/usr/bin:/bin

REDISPORT=6379                             #redis的默认端口， 要和下文中的
EXEC=/usr/local/redis/bin/redis-server     #redis服务端的命令
REDIS_CLI=/usr/local/redis/bin/redis-cli   #redis客户端的命令  这两个一般都在 
PIDFILE=/var/run/redis_6379.pid            #reids的进程文件生成的位置
CONF="/usr/local/redis/bin/redis.conf"     #redis的配置文件所在的目录 

case "$1" in  
    start)  
        if [ -f $PIDFILE ]  
        then  
                echo "$PIDFILE exists, process is already running or crashed"  
        else  
                echo "Starting Redis server..."  
                $EXEC $CONF  
        fi  
        if [ "$?"="0" ]   
        then  
              echo "Redis is running..."  
        fi  
        ;;  
    stop)  
        if [ ! -f $PIDFILE ]  
        then  
                echo "$PIDFILE does not exist, process is not running"  
        else  
                PID=$(cat $PIDFILE)  
                echo "Stopping ..."  
                $REDIS_CLI -p $REDISPORT SHUTDOWN  
                while [ -x ${PIDFILE} ]  
               do  
                    echo "Waiting for Redis to shutdown ..."  
                    sleep 1  
                done  
                echo "Redis stopped"  
        fi  
        ;;  
   restart|force-reload)  
        ${0} stop  
        ${0} start  
        ;;  
  *)  
    echo "Usage: /etc/init.d/redis {start|stop|restart|force-reload}" >&2  
        exit 1  
esac
```

## 3. 修改下面配置为自己对应的信息

```powershell
REDISPORT=6379 # 端口号
EXEC=/usr/local/bin/redis-server # 执行脚本的地址
REDIS_CLI=/usr/local/bin/redis-cli # 客户端执行脚本的地址
PIDFILE=/var/run/redis_6379.pid # 进程id文件地址，启动redis后才能看见
CONF="/myredis/redis.conf" #配置文件地址
```

## 4. 设置权限

```powershell
chmod 755 /etc/init.d/redis
```

## 5. 启动测试

```powershell
/etc/init.d/redis start
```

## 6. 启动成功会提示如下信息

```powershell
Starting Redis server...
Redis is running...
```

## 7. 设置开机自启动

```powershell
chkconfig --add /etc/init.d/redis
chkconfig redis on
```

## * 如果启动失败

查看第一步的配置文件信息，是否和第二步的文件信息一致，vim粘贴会出现粘贴不全的问题



Redis设置为开机自启动 - 农夫三拳有点疼 - OSCHINA  https://my.oschina.net/songjilong/blog/3212371