##### MySQl数据库时区的设置：      

```xml
?serverTimezone=GMT
```

##### MySQL中外键约束的设置：

```mysql
constraint fk_stuinfo_major  foreign key   (majorId) references  major (id)
foreign key  (majorId) references major (id)
```

constraint 约束名 foreign key(字段名) references 主表（被引用列）

fk_stuinfo_major：是外键约束的名字。命名规则stuinfo是本表的名字，major是主表的名字；

整型的每一种都有无符号（unsigned）和有符号（signed）两种类型（float和double总是带符号的），在默认情况下声明的整型变量都是有符号的类型（char有点特别），如果需声明无符号类型的话就需要在类型前加上unsigned。无符号版本和有符号版本的区别就是无符号类型能保存2倍于有符号类型的正整数数据，比如16位系统中一个int能存储的数据的范围为-32768~32767，而unsigned能存储的数据范围则是0~65535。由于在计算机中,整数是以补码形式存放的。根据最高位的不同，如果是1,有符号数的话就是负数；如果是无符号数,则都解释为正数。

```mysql
create database db_example;
#username=localhost
#password=ThePassword
create user 'springuser'@'localhost' identified by 'ThePassword';
grant all on db_example.* to 'springuser'@'localhost';

```





事务：

##### 开启事务

```mysql
set autocommit =0;
insert into book(bid, bname, price, btypeId) VALUES (1,'小李飞刀',100,5);
#(回滚了不插入)
rollback ;
```



```mysql
CREATE TABLE `students`.`stu` (
  `stuID` VARCHAR(12) NOT NULL,
  `stuName` VARCHAR(20)  DEFAULT NULL,
  `stuSex` VARCHAR(4)  DEFAULT NULL,
  `stuAge` VARCHAR(11)  DEFAULT NULL,
  `stuJG` VARCHAR(10)  DEFAULT NULL,
  `stuDept` VARCHAR(30)  DEFAULT NULL,
  PRIMARY KEY (`stuID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'stuID';

CREATE TABLE `students`.`t_student` (
  `studID` VARCHAR(15) NOT NULL,
  `stuName` VARCHAR(10) NOT NULL,
  `age` INT NOT NULL,
  `sex` VARCHAR(2) NOT NULL,
  `birth` DATETIME NULL,
  PRIMARY KEY (`studID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `students`.`t_result` (
  `stuID` VARCHAR(15) NOT NULL,
  `curID` VARCHAR(15) NOT NULL,
  `result` DOUBLE NULL,
  PRIMARY KEY (`stuID`, `curID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



```

##### 数据库、表的创建以及插入数据

```mysql
create database crmdb;
use crmdb;
CREATE TABLE `crmdb`.`customer` (
  `CNO` VARCHAR(20) NOT NULL COMMENT '编号',
  `Name` VARCHAR(20) NOT NULL COMMENT '姓名',
  `Sex` VARCHAR(4) NOT NULL COMMENT '性别',
  `Principalship` VARCHAR(10) NULL COMMENT '职务',
  `Company` VARCHAR(40) NULL COMMENT '公司/单位',
  `Telephone` VARCHAR(20) NULL COMMENT '公司电话',
  `Address` VARCHAR(40) NULL COMMENT '公司地址',
  `Background` VARCHAR(80) NULL COMMENT '公司背景',
  PRIMARY KEY (`CNO`))	
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
INSERT INTO `crmdb`.`customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2008002',
'郭华',
'女',
'董事长',
'光华集团',
'8221089',
'深圳',
'上市公司');

INSERT INTO `crmdb`.`customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2008001',
'江中',
'女',
'董事长',
'时代在线',
'8372168',
'长沙',
'上市公司');

INSERT INTO `crmdb`.`customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2007002',
'李兴',
'男',
'经理',
'九州方圆',
'6182755',
'北京',
'上市公司');

INSERT INTO `crmdb`.`customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2007001',
'胡振',
'男',
'总经理',
'华夏大邦',
'8226858',
'上海',
'上市公司');

select * from customer;

DROP DATABASE `crmdb`;

UPDATE `crmdb`.`customer`
SET
`Telephone` = '021-8226859'
WHERE `Name` = '胡振';
select * from customer where Company='时代在线';
select * from customer where Sex='男';
DELETE FROM `crmdb`.`customer`
WHERE CNO='2007002';

CREATE TABLE `crmdb`.`dbphoto` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(200) NULL,
  `photo image` VARCHAR(45) NOT NULL,
  `photo mediumblob` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

```

------



###### db.properties

```properties
org.hibernate.dialect.MySQLDialect

driver=com.mysql.cj.jdbc.Driver
url=jdbc:mysql://localhost:3306/crmdb?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT
username=root
password=123456
```



## MYSQL主从复制

#### 环境

centos8

mysql8

192.168.40.128（主）

192.168.40.129（从）

### 配置主从同步

#### 1.创建数据库

192.168.40.128（主）

```mysql
[root@localhost ~]#  mysql -uroot -p123456
mysql> create database yidiankt;
Query OK, 1 row affected (0.65 sec)
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| yidiankt           |
+--------------------+
5 rows in set (0.26 sec)
mysql> 

```

 192.168.40.129（从）

```
[root@localhost ~]#  mysql -uroot -p123456
mysql> create database yidiankt;
Query OK, 1 row affected (0.65 sec)
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| yidiankt           |
+--------------------+
5 rows in set (0.26 sec)
mysql> 

```

2.修改主服务器的my.cnf配置文件

```powershell
[root@localhost etc]#  vi /etc/my.cnf
 
 #主服务器配置文件 my.cnf的配置（添加以下内容）
 log-bin=master-a-bin       #日志文件名称
 binlog-format=ROW          #二进制日志的格式有row，statement和mixed三种类型
 server-id=1                #要求各个服务器的这个id必须不一样
 binlog-do-db=yidiankt      #同步的数据库名称
```

```powershell
#重新启动mysql服务
shell> systemctl restart mysqld
```

[MySQL日志格式 binlog_format](https://blog.csdn.net/mycwq/article/details/17136997)

4.从服务器的配置

```powershell
[root@localhost etc]#  vi /etc/my.cnf

#从服务器配置文件 my.cnf的配置（添加以下内容）
 log-bin=master-a-bin       #日志文件名称和主服务器一一致
 binlog-format=ROW          #二进制日志的格式，和主服务器一致
 server-id=2                #要求各个服务器的这个id必须不一样
#中继日志执行之后，这些变化是否需要计入自己的binarylog。当你的服务器需要作为另一个服务器的主服务器的时候需要
#打开。就是双主互相备份，或者多主循环备份。
log-slave-updates=true
```

```powershell
#重新启动mysql服务
shell>systemctl restart mysqld
```

3.在主服务器的MySQL中配置从服务器登录主服务器的账号授权

```powershell
#1.在主服务器中新建一个可以让从机登录的用户并且给该用户授权
CREATE USER 'yaotao'@'192.168.40.129' IDENTIFIED WITH mysql_native_password BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'yaotao'@'192.168.40.129';
flush privileges;
#2.查看主机的状态这时可以看到log-bin文件的名还有position需要记录下来一会从机配置需要用
SHOW MASTER STATUS;
#  **记下position+file**

#在从服务器中执行下列命令
CHANGE MASTER TO
MASTER_HOST='192.168.40.128',      #主服务器的IP地址
MASTER_USER='yaotao',              #被授权的账号
MASTER_PASSWORD='123456',          #被授权的密码
MASTER_LOG_FILE='master-a-bin.000001', 
MASTER_LOG_POS=10265;

start slave;
show slave status\G;
```

详细解释参考

```powershell
#在主服务器中新建一个可以让从机登录的用户并且给该用户授权
CREATE USER '用户名'@'从机ip' IDENTIFIED WITH mysql_native_password BY '登陆密码';
GRANT REPLICATION SLAVE ON *.* TO '用户名'@'从机ip';
flush privileges;
SHOW MASTER STATUS;

#在从服务器中执行下列命令
CHANGE MASTER TO
MASTER_HOST='主机ip',
MASTER_USER='用户名',
MASTER_PASSWORD='密码',
MASTER_LOG_FILE='记下的file',
MASTER_LOG_POS=记下的position;
mysql> start slave;
mysql> stop slave;
```

在主服务器中数据库执行

```powershell
mysql> CREATE USER 'yaotao'@'192.168.40.129' IDENTIFIED WITH mysql_native_password BY '123456';
Query OK, 0 rows affected (0.07 sec)
mysql> GRANT REPLICATION SLAVE ON *.* TO 'yaotao'@'192.168.40.129';
Query OK, 0 rows affected (0.00 sec)
mysql> flush privileges;
Query OK, 0 rows affected (0.03 sec)
mysql> SHOW MASTER STATUS;
+---------------------+----------+--------------+------------------+-------------------+
| File                | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------------+----------+--------------+------------------+-------------------+
| master-a-bin.000001 |     1354 | yidiankt     |                  |                   |
+---------------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)

```

在主服务器中数据库执行

```powershell
mysql> CHANGE MASTER TO
    -> MASTER_HOST='192.168.40.128',
    -> MASTER_USER='yaotao',
    -> MASTER_PASSWORD='123456',
    -> MASTER_LOG_FILE='master-a-bin.000001',
    -> MASTER_LOG_POS=1354;
Query OK, 0 rows affected, 2 warnings (0.16 sec)
mysql> start slave;
Query OK, 0 rows affected (0.06 sec)

mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.40.128
                  Master_User: yaotao
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master-a-bin.000001
          Read_Master_Log_Pos: 1354
               Relay_Log_File: localhost-relay-bin.000002
                Relay_Log_Pos: 325
        Relay_Master_Log_File: master-a-bin.000001
                     此处为yes即正确
             Slave_IO_Running: Yes
                     此处为yes即正确
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 1354
              Relay_Log_Space: 537
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: 24b2a333-2a02-11ea-a94e-000c2937ea2f
             Master_Info_File: mysql.slave_master_info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
       Master_public_key_path: 
        Get_master_public_key: 0
            Network_Namespace: 
1 row in set (0.00 sec)

```

### mysql8.0数据库添加用户和授权

mysql8有新的安全要求，不能像之前的版本那样一次性创建用户并授权需要先创建用户，再进行授权操作

创建新用户：

```powershell
create user 'username'@'host' identified by 'password';
CREATE USER 'username'@'host' IDENTIFIED WITH mysql_native_password BY 'password';
```

其中username为自定义的用户名；host为登录域名（即授权用户的远程IP地址），host为’%'时表示为 任意IP，为localhost时表示本机，或者填写指定的IP地址；password为密码
为用户授权：

```powershell
grant all privileges on *.* to 'username'@'%';
```

 其中第一个* 表示所有数据库，第二个 * 表示所有数据表，如果不想授权全部那就把对应的写成相应数据库或者数据表；username为指定的用户；%为该用户登录的域名
授权之后刷新权限：

```powershell
flush privileges;
```

完整示例



```powershell
#mysql之前版本的授提示意思是不能用grant创建用户，mysql8.0以前的版本可以使用grant在授权的时候隐式的创建用户，8.0以后已经不支持，所以必须先创建用户，然后再授权，命令如下：权方式
grant replication slave on *.* to 'slave'@'%' identified by '123456';
#方式一
CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
GRANT all privileges ON *.* TO 'slave'@'%';
#方式二
create user 'username'@'host' identified by 'password';
grant all privileges on *.* to 'username'@'%' ;


GRANT all privileges ON *.* TO 'root'@'%';
```



```powershell
#这是一种可行的方法
CREATE USER 'yaotao'@'192.168.40.129' IDENTIFIED WITH mysql_native_password BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'yaotao'@'192.168.40.129';
flush privileges;
```

[mysql中grant all privileges on赋给用户远程权限](https://blog.csdn.net/qq_31725371/article/details/83019856)

```powershell
CREATE USER 'user01'@'192.168.40.132' IDENTIFIED WITH mysql_native_password BY '123456';
grant all privileges on *.* to 'user01'@'192.168.40.132' with grant option;
flush privileges;

CHANGE MASTER TO
MASTER_HOST='192.168.40.128',
MASTER_USER='user01',
MASTER_PASSWORD='123456',
MASTER_LOG_FILE='master-a-bin.000001',
MASTER_LOG_POS=7609;
```

```
CREATE USER 'user'@'192.168.40.128' IDENTIFIED WITH mysql_native_password BY '123456';
grant all privileges on *.* to 'user'@'192.168.40.128' with grant option;
flush privileges;
```

#### 测试

在主服务器数据库yidiankt 中添加表以及插入数据，接着在从服务器的数据库能否查询到相应的数据和表

```powershell

CREATE TABLE `customer` (
  `CNO` VARCHAR(20) NOT NULL COMMENT '编号',
  `Name` VARCHAR(20) NOT NULL COMMENT '姓名',
  `Sex` VARCHAR(4) NOT NULL COMMENT '性别',
  `Principalship` VARCHAR(10) NULL COMMENT '职务',
  `Company` VARCHAR(40) NULL COMMENT '公司/单位',
  `Telephone` VARCHAR(20) NULL COMMENT '公司电话',
  `Address` VARCHAR(40) NULL COMMENT '公司地址',
  `Background` VARCHAR(80) NULL COMMENT '公司背景',
  PRIMARY KEY (`CNO`))	
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2008002',
'郭华',
'女',
'董事长',
'光华集团',
'8221089',
'深圳',
'上市公司');

INSERT INTO `customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2007001',
'胡振',
'男',
'总经理',
'华夏大邦',
'8226858',
'上海',
'上市公司');
INSERT INTO `customer`
(`CNO`,
`Name`,
`Sex`,
`Principalship`,
`Company`,
`Telephone`,
`Address`,
`Background`)
VALUES
('2007002',
'李兴',
'男',
'经理',
'九州方圆',
'6182755',
'北京',
'上市公司');
```

遇到的问题

[Slave_IO_Running，Slave_SQL_Running问题的解决方法](https://www.cnblogs.com/byfboke/articles/9120672.html)

```powershell
 错误实例：
 show slave status\G 
 Slave_IO_Running: Yes 
 Slave_SQL_Running: No 
 
#在MySQL中执行
stop slave;
SET GLOBAL SQL_SLAVE_SKIP_COUNTER=1; 
START SLAVE;  
```









# mysql8.0.18用户root登录开启远程访问权限

```mysql
mysql> use mysql ;
mysql> select host,user from user;
+-----------+------------------+
| host      | user             |
+-----------+------------------+
| localhost | mysql.infoschema |
| localhost | mysql.session    |
| localhost | mysql.sys        |
| localhost | root             |
+-----------+------------------+
4 rows in set (0.00 sec)
mysql> update user set host='%' where user='root';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select host,user from user;
+-----------+------------------+
| host      | user             |
+-----------+------------------+
| %         | root             |
| localhost | mysql.infoschema |
| localhost | mysql.session    |
| localhost | mysql.sys        |
+-----------+------------------+
4 rows in set (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.01 sec)
```































```
docker run -d -p 4300:3306 --name=mysql-300 -v /datavol/mysql-300/conf:/etc/mysql/conf.d -v /datavol/mysql-300/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
```









```
docker run -d -p 4400:3306 --name=mysql-400 -v /datavol/mysql-400/conf:/etc/mysql/conf.d -v /datavol/mysql-400/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
```







```
docker run -d -p 4500:3306 --name=mysql-500 -v /datavol/mysql-500/conf:/etc/mysql/conf.d -v /datavol/mysql-500/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql
```





```
docker run -d -p 4345:3306 --name=mysql-10345 -v /datavol/mysql-10345/conf:/etc/mysql/conf.d -v /datavol/mysql-10345/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql

```

error connecting to master 'repl@192.168.1.107:3306' - retry-time: 60 retries: 86400 - 金翅大鹏吃唐僧 - 博客园  https://www.cnblogs.com/allen-zhang/p/9410464.html



MySQL5.7多主一从（多源复制）同步配置 - 魔流剑 - 博客园  https://www.cnblogs.com/lr393993507/p/9998962.html



```mysql
#在Windows控制台登录远程数据库
mysql -uroot -p -h192.168.40.128 -P3306
#查询数据库引擎
mysql> show variables like '%storage_engine%';
+---------------------------------+-----------+
| Variable_name                   | Value     |
+---------------------------------+-----------+
| default_storage_engine          | InnoDB    |
| default_tmp_storage_engine      | InnoDB    |
| disabled_storage_engines        |           |
| internal_tmp_mem_storage_engine | TempTable |
+---------------------------------+-----------+
4 rows in set (0.15 sec)


```

索引是排好序的快速查找的数据结构。

存储引擎是形容数据库表的。

```powershell
mysql> show global status like 'innodb_page_size';
+------------------+-------+
| Variable_name    | Value |
+------------------+-------+
| Innodb_page_size | 16384 |
+------------------+-------+
1 row in set (0.11 sec)

mysql> select version();
+-----------+
| version() |
+-----------+
| 8.0.18    |
+-----------+
1 row in set (0.00 sec)


mysql> use school;
Database changed
mysql> show tables;
+------------------+
| Tables_in_school |
+------------------+
| student          |
+------------------+
1 row in set (0.04 sec)

mysql> desc student;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(10)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(60) | NO   |     | NULL    |                |
| age   | int(3)      | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
3 rows in set (0.05 sec)

mysql> create database westos;
Query OK, 1 row affected (0.03 sec)

exit  或者  \q   #退出mysql数据库
```

```sql
create database [if not exists] westos;

drop database [if exists] westos;




```



```powershell
查看当前 mysqladmin的版本
[root@localhost ~]# mysqladmin -v
mysqladmin  Ver 8.0.18 for Linux on x86_64 (MySQL Community Server - GPL)
使用mysqladmin工具连接到MySQL并执行version命令查看MySQL数据库版本
[root@localhost ~]# mysqladmin -u root -p version
Enter password: 
mysqladmin  Ver 8.0.18 for Linux on x86_64 (MySQL Community Server - GPL)
Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Server version		8.0.18
Protocol version	10
Connection		Localhost via UNIX socket
UNIX socket		/var/lib/mysql/mysql.sock
#表示MySQL服务器已经运行的描述
Uptime:			1 hour 15 min 10 sec
#活动线程的数量
Threads: 4  
#从MySQL启动以来客户问题的数量（查询）
Questions: 119  
#执行时间超过long_query_time秒的查询的数量
Slow queries: 0  
#服务器已打开的数据库的数量
Opens: 158  
#服务器已执行的flush...、refresh和reload命令的数量
Flush tables: 3  
#目前大开的表的数量
Open tables: 78  
#执行平均用时秒数
Queries per second avg: 0.026

[root@localhost ~]# mysqladmin -u root -p processlist
Enter password: 

#mysqlshow工具连接数据库，并显示MySQL上所有数据库
[root@localhost ~]# mysqlshow -u root -p
Enter password: 
+--------------------+
|     Databases      |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| school             |
| sys                |
| testdb             |
| yidiankt           |
+--------------------+
#mysqlshow工具连接数据库，并显示school数据库下的所有数据表名称
[root@localhost ~]# mysqlshow school -u root -p
Enter password: 
Database: school
+---------+
| Tables  |
+---------+
| student |
+---------+
# -v显示行数
[root@localhost ~]# mysqlshow school -v -u root -p
Enter password: 
Database: school
+---------+----------+
| Tables  | Columns  |
+---------+----------+
| student |        3 |
+---------+----------+
1 row in set.

#查看所有的存储引擎  \G可以让显示结果更加美观
mysql> show engines\G
*************************** 1. row ***************************
     #数据库存储引擎的名称
      Engine: FEDERATED
      #表示MySQL是否支持该类引擎，YES表示支持，NO表示不支持
     Support: NO
     #表示对该引擎的注释信息
     Comment: Federated MySQL storage engine
     #表示是否支持事务处理，YES表示支持，NO表示不支持
Transactions: NULL
#表示是否分布式交易处理的XA规范，YES表示支持，NO表示不支持
          XA: NULL
          #表示是否支持保存点，以便事务回滚到保存点，YES表示支持，NO表示不支持
  Savepoints: NULL
*************************** 2. row ***************************
      Engine: MEMORY
     Support: YES
     Comment: Hash based, stored in memory, useful for temporary tables
Transactions: NO
          XA: NO
  Savepoints: NO
*************************** 3. row ***************************
      Engine: InnoDB
     Support: DEFAULT
     Comment: Supports transactions, row-level locking, and foreign keys
Transactions: YES
          XA: YES
  Savepoints: YES
*************************** 4. row ***************************
      Engine: PERFORMANCE_SCHEMA
     Support: YES
     Comment: Performance Schema
Transactions: NO
          XA: NO
  Savepoints: NO
*************************** 5. row ***************************
      Engine: MyISAM
     Support: YES
     Comment: MyISAM storage engine
Transactions: NO
          XA: NO
  Savepoints: NO
*************************** 6. row ***************************
      Engine: MRG_MYISAM
     Support: YES
     Comment: Collection of identical MyISAM tables
Transactions: NO
          XA: NO
  Savepoints: NO
*************************** 7. row ***************************
      Engine: BLACKHOLE
     Support: YES
     Comment: /dev/null storage engine (anything you write to it disappears)
Transactions: NO
          XA: NO
  Savepoints: NO
*************************** 8. row ***************************
      Engine: CSV
     Support: YES
     Comment: CSV storage engine
Transactions: NO
          XA: NO
  Savepoints: NO
*************************** 9. row ***************************
      Engine: ARCHIVE
     Support: YES
     Comment: Archive storage engine
Transactions: NO
          XA: NO
  Savepoints: NO
9 rows in set (0.00 sec)




```



```mysql
#查看错误日志文件所在的目录及文件名信息
mysql> show variables like 'log_error';
+---------------+---------------------+
| Variable_name | Value               |
+---------------+---------------------+
| log_error     | /var/log/mysqld.log |
+---------------+---------------------+
1 row in set (0.00 sec)
#查看错误日志文件
[root@localhost ~]# vim /var/log/mysqld.log 
#删除错误日志文件
mysql> flush logs;
Query OK, 0 rows affected (0.51 sec)
#也可在my.cnf文件中看
[root@localhost ~]# cat /etc/my.cnf


#查看通用查询日志，默认关闭
mysql> show variables like 'general%';
+------------------+------------------------------+
| Variable_name    | Value                        |
+------------------+------------------------------+
| general_log      | OFF                          |
| general_log_file | /var/lib/mysql/localhost.log |
+------------------+------------------------------+
2 rows in set (0.00 sec)
#启用查询日志
mysql> set global general_log=on;
Query OK, 0 rows affected (0.03 sec)
mysql> set global general_log=0;
Query OK, 0 rows affected (0.02 sec)
mysql> set global general_log=1;
Query OK, 0 rows affected (0.00 sec)


[root@localhost ~]# vim /var/lib/mysql/localhost.log 
mysql> flush logs;
Query OK, 0 rows affected (0.04 sec)

```

```mysql
#查看缓存
mysql> show variables like '%cache%';
+---------------------------------+----------------------+
| Variable_name                   | Value                |
+---------------------------------+----------------------+
| binlog_cache_size               | 32768                |
| binlog_stmt_cache_size          | 32768                |
| have_query_cache                | NO                   |
| host_cache_size                 | 279                  |
| innodb_disable_sort_file_cache  | OFF                  |
| innodb_ft_cache_size            | 8000000              |
| innodb_ft_result_cache_limit    | 2000000000           |
| innodb_ft_total_cache_size      | 640000000            |
| key_cache_age_threshold         | 300                  |
| key_cache_block_size            | 1024                 |
| key_cache_division_limit        | 100                  |
| max_binlog_cache_size           | 18446744073709547520 |
| max_binlog_stmt_cache_size      | 18446744073709547520 |
| schema_definition_cache         | 256                  |
| stored_program_cache            | 256                  |
| stored_program_definition_cache | 256                  |
| table_definition_cache          | 2000                 |
| table_open_cache                | 4000                 |
| table_open_cache_instances      | 16                   |
| tablespace_definition_cache     | 256                  |
| thread_cache_size               | 9                    |
+---------------------------------+----------------------+
21 rows in set (0.00 sec)


```

### 启动二进制日志

```powershell
#查询mysql数据库配置文件的地址
[root@localhost ~]# whereis my.cnf
my: /etc/my.cnf
[root@localhost ~]# vim /etc/my.cnf
[root@localhost ~]# cat /etc/my.cnf
# For advice on how to change settings please see
# http://dev.mysql.com/doc/refman/8.0/en/server-configuration-defaults.html

[mysqld]
#
# Remove leading # and set to the amount of RAM for the most important data
# cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
# innodb_buffer_pool_size = 128M
#
# Remove the leading "# " to disable binary logging
# Binary logging captures changes between backups and is enabled by
# default. It's default setting is log_bin=binlog
# disable_log_bin
#
# Remove leading # to set options mainly useful for reporting servers.
# The server defaults are faster for transactions and fast SELECTs.
# Adjust sizes as needed, experiment to find the optimal values.
# join_buffer_size = 128M
# sort_buffer_size = 2M
# read_rnd_buffer_size = 2M
#
# Remove leading # to revert to previous value for default_authentication_plugin,
# this will increase compatibility with older clients. For background, see:
# https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_default_authentication_plugin
# default-authentication-plugin=mysql_native_password

datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid







 
 #主从复制，主配置文件 my.cnf的配置
 log-bin=master-a-bin       #日志文件名称
 binlog-format=ROW          #二进制日志的格式有row，statement和mixed三种类型
 server-id=1                #要求各个服务器的这个id必须不一样
 binlog-do-db=yidiankt      #同步的数据库名称（紧同步指定名称的数据库）
 
 
 #忽略指定名称的数据库
 #Binlog-ignore-db=db_name（数据库名称）
 
 
 #查看二进制日志的上限
 mysql> show variables like 'max_binlog_size';
+-----------------+------------+
| Variable_name   | Value      |
+-----------------+------------+
| max_binlog_size | 1073741824 |
+-----------------+------------+
1 row in set (0.00 sec)
#查看二进制日志相关信息（log_bin的值为ON，表名二进制日志打开成功）
mysql> show variables like 'log_bin%'\G
*************************** 1. row ***************************
Variable_name: log_bin
        Value: ON
*************************** 2. row ***************************
Variable_name: log_bin_basename
        Value: /var/lib/mysql/master-a-bin
*************************** 3. row ***************************
Variable_name: log_bin_index
        Value: /var/lib/mysql/master-a-bin.index
*************************** 4. row ***************************
Variable_name: log_bin_trust_function_creators
        Value: OFF
*************************** 5. row ***************************
Variable_name: log_bin_use_v1_row_events
        Value: OFF
5 rows in set (0.01 sec)

#查看二进制日志
 mysql> show variables like 'log_bin';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_bin       | ON    |
+---------------+-------+
1 row in set (0.00 sec)
mysql> show binary logs;
+---------------------+-----------+-----------+
| Log_name            | File_size | Encrypted |
+---------------------+-----------+-----------+
| master-a-bin.000047 |       604 | No        |
| master-a-bin.000048 |       178 | No        |
| master-a-bin.000049 |       178 | No        |
| master-a-bin.000050 |       155 | No        |
| master-a-bin.000051 |       155 | No        |
| master-a-bin.000052 |       178 | No        |
| master-a-bin.000053 |       155 | No        |
| master-a-bin.000054 |       178 | No        |
| master-a-bin.000055 |       155 | No        |
| master-a-bin.000056 |       155 | No        |
| master-a-bin.000057 |       178 | No        |
| master-a-bin.000058 |       178 | No        |
| master-a-bin.000059 |       178 | No        |
| master-a-bin.000060 |       178 | No        |
| master-a-bin.000061 |       178 | No        |
| master-a-bin.000062 |       178 | No        |
| master-a-bin.000063 |       178 | No        |
| master-a-bin.000064 |       178 | No        |
| master-a-bin.000065 |       178 | No        |
| master-a-bin.000066 |       178 | No        |
| master-a-bin.000067 |       178 | No        |
| master-a-bin.000068 |       178 | No        |
| master-a-bin.000069 |       178 | No        |
| master-a-bin.000070 |       178 | No        |
| master-a-bin.000071 |       155 | No        |
| master-a-bin.000072 |       178 | No        |
| master-a-bin.000073 |       205 | No        |
| master-a-bin.000074 |       205 | No        |
| master-a-bin.000075 |       205 | No        |
| master-a-bin.000076 |       205 | No        |
| master-a-bin.000077 |       205 | No        |
| master-a-bin.000078 |       205 | No        |
| master-a-bin.000079 |       205 | No        |
| master-a-bin.000080 |       205 | No        |
| master-a-bin.000081 |       205 | No        |
| master-a-bin.000082 |       155 | No        |
| master-a-bin.000083 |       155 | No        |
+---------------------+-----------+-----------+
37 rows in set (0.27 sec)

#查看指定的二进制文件的内容（对数据库操作的记录），作为对数据库进行管理和数据恢复的依据
mysql> show binlog events in 'master-a-bin.000083'\G
*************************** 1. row ***************************
   Log_name: master-a-bin.000083
        Pos: 4
 Event_type: Format_desc
  Server_id: 1
End_log_pos: 124
       Info: Server ver: 8.0.18, Binlog ver: 4
*************************** 2. row ***************************
   Log_name: master-a-bin.000083
        Pos: 124
 Event_type: Previous_gtids
  Server_id: 1
End_log_pos: 155
       Info: 
2 rows in set (0.00 sec)

#删除指定编号前的所有日志
mysql> purge master logs to 'master-a-bin.000066';
Query OK, 0 rows affected (0.01 sec)
#查看二进制文件
mysql> show binary logs;
+---------------------+-----------+-----------+
| Log_name            | File_size | Encrypted |
+---------------------+-----------+-----------+
| master-a-bin.000066 |       178 | No        |
| master-a-bin.000067 |       178 | No        |
| master-a-bin.000068 |       178 | No        |
| master-a-bin.000069 |       178 | No        |
| master-a-bin.000070 |       178 | No        |
| master-a-bin.000071 |       155 | No        |
| master-a-bin.000072 |       178 | No        |
| master-a-bin.000073 |       205 | No        |
| master-a-bin.000074 |       205 | No        |
| master-a-bin.000075 |       205 | No        |
| master-a-bin.000076 |       205 | No        |
| master-a-bin.000077 |       205 | No        |
| master-a-bin.000078 |       205 | No        |
| master-a-bin.000079 |       205 | No        |
| master-a-bin.000080 |       205 | No        |
| master-a-bin.000081 |       205 | No        |
| master-a-bin.000082 |       155 | No        |
| master-a-bin.000083 |       155 | No        |
+---------------------+-----------+-----------+
18 rows in set (0.00 sec)

#删除指定日期前的日志
mysql> purge master logs before '20180808';
Query OK, 0 rows affected (0.00 sec)

#删除所有日志
mysql> reset master;








```



```mysql

#查询数据库的存储地址
mysql> show variables like '%datadir%';
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| datadir       | /var/lib/mysql/ |
+---------------+-----------------+
1 row in set (0.01 sec)
#也可在my.cnf文件中看
[root@localhost ~]# cat /etc/my.cnf





#查看二进制日志是否开启（log_bin的值为ON，说明二进制日志已成功开启）
mysql> show variables like '%log_bin%'\G
*************************** 1. row ***************************
Variable_name: log_bin
        Value: ON
*************************** 2. row ***************************
Variable_name: log_bin_basename
        Value: /var/lib/mysql/master-a-bin
*************************** 3. row ***************************
Variable_name: log_bin_index
        Value: /var/lib/mysql/master-a-bin.index
*************************** 4. row ***************************
Variable_name: log_bin_trust_function_creators
        Value: OFF
*************************** 5. row ***************************
Variable_name: log_bin_use_v1_row_events
        Value: OFF
*************************** 6. row ***************************
Variable_name: sql_log_bin
        Value: ON
6 rows in set (0.00 sec)



```





```mysql
#当前用户的连接信息的操作
mysql> show processlist\G
*************************** 1. row ***************************
     Id: 4
   User: system user
   Host: connecting host
     db: NULL
Command: Connect
   Time: 33450
  State: Connecting to master
   Info: NULL
*************************** 2. row ***************************
     Id: 5
   User: system user
   Host: 
     db: NULL
Command: Query
   Time: 33450
  State: Slave has read all relay log; waiting for more updates
   Info: NULL
*************************** 3. row ***************************
     Id: 6
   User: event_scheduler
   Host: localhost
     db: NULL
Command: Daemon
   Time: 33450
  State: Waiting on empty queue
   Info: NULL
*************************** 4. row ***************************
     Id: 458
   User: root
   Host: localhost
     db: school
Command: Sleep
   Time: 5869
  State: 
   Info: NULL
*************************** 5. row ***************************
     Id: 529
   User: root
   Host: 192.168.40.1:58178
     db: school
Command: Sleep
   Time: 3128
  State: 
   Info: NULL
*************************** 6. row ***************************
     Id: 530
   User: root
   Host: localhost
     db: NULL
Command: Query
   Time: 0
  State: starting
   Info: show processlist
*************************** 7. row ***************************
     Id: 580
   User: root
   Host: 192.168.40.1:59815
     db: school
Command: Sleep
   Time: 199
  State: 
   Info: NULL
7 rows in set (0.00 sec)

#charset返回指定参数使用的字符集
mysql> select charset('test'),charset(convert('test' USING latin1)),charset(version())\G
*************************** 1. row ***************************
                      charset('test'): utf8mb4
charset(convert('test' USING latin1)): latin1
                   charset(version()): utf8
1 row in set (0.00 sec)

show databases ;
use example;
create table student(
    ID int auto_increment not null primary key ,
    NAME varchar(40)
);
insert into student values (NULL,'张三');
insert into student values (NULL,'李四');
select * from student;
#查询最后插入的一条记录的id
select LAST_INSERT_ID();
insert into student values (NULL,'王五'),(null,'赵六'),(null,'周三');
select last_insert_id();

#使用MD5（str）函数返回加密字符串的操作
mysql> select md5('test');
+----------------------------------+
| md5('test')                      |
+----------------------------------+
| 098f6bcd4621d373cade4e832627b4f6 |
+----------------------------------+
1 row in set (0.00 sec)




```



### 用户授权

```mysql
C:\Users\Administrator>mysql -uroot -p123456
#创建一个新用户
mysql> create user hyc@localhost identified by '123456';
#给用户授权（只是把mysql数据库中的user表的增删改查授权给新添加的用户）
mysql> grant select,update,delete,insert on mysql.user to hyc@localhost;
Query OK, 0 rows affected (0.10 sec)
mysql> flush privileges;
Query OK, 0 rows affected (0.05 sec)
mysql> show grants for hyc@localhost;
+-----------------------------------------------------------------------------+
| Grants for hyc@localhost                                                    |
+-----------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `hyc`@`localhost`                                     |
| GRANT SELECT, INSERT, UPDATE, DELETE ON `mysql`.`user` TO `hyc`@`localhost` |
+-----------------------------------------------------------------------------+
2 rows in set (0.00 sec)

#与之对应的取消授权
mysql> revoke select,update,delete,insert on mysql.user from hyc@localhost;
Query OK, 0 rows affected (1.74 sec)


#
C:\Users\Administrator>mysql -uhyc -p123456
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
+--------------------+
2 rows in set (0.00 sec)
mysql> use mysql ;
Database changed
mysql> show tables;
+-----------------+
| Tables_in_mysql |
+-----------------+
| user            |
+-----------------+
1 row in set (0.06 sec)
```

### 数据库的逻辑备份还原

```mysql
[root@localhost ~]# mysql -uroot -p123456
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| Market             |
| aa                 |
| example            |
| information_schema |
| mysql              |
| performance_schema |
| school             |
| student            |
| sys                |
| test_db            |
| yidiankt           |
+--------------------+
11 rows in set (0.01 sec)

mysql> use school;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> show tables;
+------------------+
| Tables_in_school |
+------------------+
| student          |
+------------------+
1 row in set (0.00 sec)

mysql> select * from student;
+----+-----------+-----+
| id | name      | age |
+----+-----------+-----+
|  1 | kuangshen |   3 |
+----+-----------+-----+
1 row in set (0.00 sec)

mysql> 
#备份数据库
[root@localhost ~]# mysqldump -uroot -p123456  school > /newdisk/schoolDB.dump
mysqldump: [Warning] Using a password on the command line interface can be insecure.
#删除数据库
mysql> drop database school;
Query OK, 1 row affected (0.05 sec)
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| Market             |
| aa                 |
| example            |
| information_schema |
| mysql              |
| performance_schema |
| student            |
| sys                |
| test_db            |
| yidiankt           |
+--------------------+
10 rows in set (0.00 sec)
mysql> create database school;
Query OK, 1 row affected (0.00 sec)
mysql> use school;
Database changed
mysql> show tables;
Empty set (0.00 sec)

#还原数据库
[root@localhost ~]# mysql -uroot -p123456  school < /newdisk/schoolDB.dump
mysql: [Warning] Using a password on the command line interface can be insecure.
mysql> show tables;
+------------------+
| Tables_in_school |
+------------------+
| student          |
+------------------+
1 row in set (0.01 sec)
mysql> select * from student;
+----+-----------+-----+
| id | name      | age |
+----+-----------+-----+
|  1 | kuangshen |   3 |
+----+-----------+-----+
1 row in set (0.00 sec)
```





```powershell
[root@localhost ~]# mysqldump
Usage: mysqldump [OPTIONS] database [tables]
OR     mysqldump [OPTIONS] --databases [OPTIONS] DB1 [DB2 DB3...]
OR     mysqldump [OPTIONS] --all-databases [OPTIONS]
For more options, use mysqldump --help
#备份所有的数据库
[root@localhost newdisk]# mysqldump -uroot -p123456  --all-databases >/newdisk/alldb.sql
mysqldump: [Warning] Using a password on the command line interface can be insecure.
#备份某几个数据库
[root@localhost newdisk]# mysqldump -uroot -p123456  --databases school  yidiankt girls   >/newdisk/sch_yi_gir.sql
mysqldump: [Warning] Using a password on the command line interface can be insecure.
#备份一个数据库
[root@localhost newdisk]# mysqldump -uroot -p123456 student >/newdisk/studb.sql
mysqldump: [Warning] Using a password on the command line interface can be insecure.
#备份指定数据库的一个表
[root@localhost newdisk]# mysqldump -uroot -p123456 student major >/newdisk/student_major.sql
mysqldump: [Warning] Using a password on the command line interface can be insecure.
[root@localhost ~]# cd /newdisk/
[root@localhost newdisk]# ll
总用量 18316
-rw-r--r--. 1 root root 16635040 12月 13 10:04 1421418.PDF
-rw-r--r--. 1 root root  1052967 4月  25 08:02 alldb.sql
-rw-r--r--. 1 root root  1034515 4月  25 08:13 create
-rw-r--r--. 1 root root     2938 4月  25 08:07 girls.sql
-rw-r--r--. 1 root root     6713 4月  25 08:14 sch_yi_gir.sql
-rw-r--r--. 1 root root     4911 4月  25 08:15 studb.sql
-rw-r--r--. 1 root root     1951 4月  25 08:16 student_major.sql
#查看备份的数据库
[root@localhost newdisk]# more sch_yi_gir.sql 
-- MySQL dump 10.13  Distrib 8.0.18, for Linux (x86_64)
--
-- Host: localhost    Database: school
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `school`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `school` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*
!80016 DEFAULT ENCRYPTION='N' */;

USE `school`;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '学院id',
  `name` varchar(60) NOT NULL COMMENT '学员姓名',
  `age` int(3) NOT NULL COMMENT '学员年龄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'kuangshen',3);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `yidiankt`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `yidiankt` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ 
/*!80016 DEFAULT ENCRYPTION='N' */;

USE `yidiankt`;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `CNO` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(20) NOT NULL COMMENT '姓名',
  `Sex` varchar(4) NOT NULL COMMENT '性别',
  `Principalship` varchar(10) DEFAULT NULL COMMENT '职务',
  `Company` varchar(40) DEFAULT NULL COMMENT '公司/单位',
  `Telephone` varchar(20) DEFAULT NULL COMMENT '公司电话',
  `Address` varchar(40) DEFAULT NULL COMMENT '公司地址',
  `Background` varchar(80) DEFAULT NULL COMMENT '公司背景',
  PRIMARY KEY (`CNO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES ('2008002','郭华','女','董事长','光华集团','8221089','深圳','上市公司');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `girls`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `girls` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!
80016 DEFAULT ENCRYPTION='N' */;

USE `girls`;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'john','8888'),(2,'lyt','6666');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beauty`
--

DROP TABLE IF EXISTS `beauty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beauty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sex` char(1) DEFAULT '女',
  `borndate` datetime DEFAULT '1987-01-01 00:00:00',
  `phone` varchar(11) NOT NULL,
  `photo` blob,
  `boyfriend_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beauty`
--

LOCK TABLES `beauty` WRITE;
/*!40000 ALTER TABLE `beauty` DISABLE KEYS */;
INSERT INTO `beauty` VALUES (1,'柳岩','女','1988-02-03 00:00:00','18209876577',NULL,8),(2,'苍老师','女','1987-12-30 00:00
:00','18219876577',NULL,9),(3,'Angelababy','女','1989-02-03 00:00:00','18209876567',NULL,3),(4,'热巴','女','1993-02-03 00
:00:00','18209876579',NULL,2),(5,'周冬雨','女','1992-02-03 00:00:00','18209179577',NULL,9),(6,'周芷若','女','1988-02-03 0
0:00:00','18209876577',NULL,1),(7,'岳灵珊','女','1987-12-30 00:00:00','18219876577',NULL,9),(8,'小昭','女','1989-02-03 00
:00:00','18209876567',NULL,1),(9,'双儿','女','1993-02-03 00:00:00','18209876579',NULL,9),(10,'王语嫣','女','1992-02-03 00
:00:00','18209179577',NULL,4),(11,'夏雪','女','1993-02-03 00:00:00','18209876579',NULL,9),(12,'赵敏','女','1992-02-03 00:
00:00','18209179577',NULL,1);
/*!40000 ALTER TABLE `beauty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boys`
--

DROP TABLE IF EXISTS `boys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `boys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `boyName` varchar(20) DEFAULT NULL,
  `userCP` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boys`
--

LOCK TABLES `boys` WRITE;
/*!40000 ALTER TABLE `boys` DISABLE KEYS */;
INSERT INTO `boys` VALUES (1,'张无忌',100),(2,'鹿晗',800),(3,'黄晓明',50),(4,'段誉',300);
/*!40000 ALTER TABLE `boys` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-25  8:14:12



#还原数据库
mysql> create database girls;
Query OK, 1 row affected (0.02 sec)
[root@localhost ~]# mysql -uroot -p123456 girls < /newdisk/girls.sql 
mysql: [Warning] Using a password on the command line interface can be insecure.

```



### 触发器

```powershell
#创建一个单执行语句的触发器
mysql> use school;
Database changed
mysql> show tables;
+------------------+
| Tables_in_school |
+------------------+
| student          |
+------------------+
1 row in set (0.01 sec)
mysql> create table account (acct_num int,amount decimal(10,2));
Query OK, 0 rows affected (0.02 sec)
mysql> create trigger ins_sum before insert on account for each row set @sum=@sum+new.amount;
Query OK, 0 rows affected (0.01 sec)
mysql> set @sum=0;
Query OK, 0 rows affected (0.00 sec)
mysql> insert into account values(1,100),(2,45.3),(3,9777);
Query OK, 3 rows affected (0.01 sec)
Records: 3  Duplicates: 0  Warnings: 0
mysql> insert into account values(4,39.36907);
Query OK, 1 row affected, 1 warning (0.00 sec)
mysql> select @sum;
+---------+
| @sum    |
+---------+
| 9961.67 |
+---------+
1 row in set (0.00 sec)
#查看触发器
mysql> show triggers\G
*************************** 1. row ***************************
             Trigger: ins_sum
               Event: INSERT
               Table: account
           Statement: set @sum=@sum+new.amount
              Timing: BEFORE
             Created: 2020-04-25 09:27:15.21
            sql_mode: ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
             Definer: root@%
character_set_client: utf8mb4
collation_connection: utf8mb4_0900_ai_ci
  Database Collation: utf8mb4_0900_ai_ci
1 row in set (0.00 sec)

mysql> create table myevent(id int default null,evt_name char(20) default null);
Query OK, 0 rows affected (0.02 sec)
#创建触发器
mysql> create trigger trig_update
    ->     after update
    ->     on account
    ->     for each row insert into myevent
    ->                  values (1, 'after update');
Query OK, 0 rows affected (0.01 sec)

mysql> show triggers \G
*************************** 1. row ***************************
             Trigger: ins_sum
               Event: INSERT
               Table: account
           Statement: set @sum=@sum+new.amount
              Timing: BEFORE
             Created: 2020-04-25 09:27:15.21
            sql_mode: ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
             Definer: root@%
character_set_client: utf8mb4
collation_connection: utf8mb4_0900_ai_ci
  Database Collation: utf8mb4_0900_ai_ci
*************************** 2. row ***************************
             Trigger: trig_update
               Event: UPDATE
               Table: account
           Statement: insert into myevent
                 values (1, 'after update')
              Timing: AFTER
             Created: 2020-04-25 09:56:32.85
            sql_mode: ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
             Definer: root@%
character_set_client: utf8mb4
collation_connection: utf8mb4_0900_ai_ci
  Database Collation: utf8mb4_0900_ai_ci
2 rows in set (0.00 sec)

#触发器保存在information_schema.TRIGGERS
mysql> select * from information_schema.TRIGGERS;
mysql> select * from information_schema.TRIGGERS where TRIGGER_NAME='trig_update'\G
*************************** 1. row ***************************
           TRIGGER_CATALOG: def
            TRIGGER_SCHEMA: school
              TRIGGER_NAME: trig_update
        EVENT_MANIPULATION: UPDATE
      EVENT_OBJECT_CATALOG: def
       EVENT_OBJECT_SCHEMA: school
        EVENT_OBJECT_TABLE: account
              ACTION_ORDER: 1
          ACTION_CONDITION: NULL
          ACTION_STATEMENT: insert into myevent
                 values (1, 'after update')
        ACTION_ORIENTATION: ROW
             ACTION_TIMING: AFTER
ACTION_REFERENCE_OLD_TABLE: NULL
ACTION_REFERENCE_NEW_TABLE: NULL
  ACTION_REFERENCE_OLD_ROW: OLD
  ACTION_REFERENCE_NEW_ROW: NEW
                   CREATED: 2020-04-25 09:56:32.85
                  SQL_MODE: ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
                   DEFINER: root@%
      CHARACTER_SET_CLIENT: utf8mb4
      COLLATION_CONNECTION: utf8mb4_0900_ai_ci
        DATABASE_COLLATION: utf8mb4_0900_ai_ci
1 row in set (0.00 sec)

mysql> insert into account values(5,67);
Query OK, 1 row affected (0.00 sec)

mysql> update account set amount=8888 where acct_num=5;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> update account set amount=6666 where acct_num=4;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
#删除触发器
mysql> drop trigger school.tri_insert;
Query OK, 0 rows affected (0.00 sec)


```





### 完整的创建触发器的过程

```mysql
create database chufaqi;
use chufaqi;
create table persons
(
    name varchar(40),
    num  int
);
create table sales
(
    name varchar(40),
    sum  int
);
create trigger num_sum
    after insert
    on persons
    for each row insert into sales
                 values (NEW.name,7*NEW.num);
insert into persons values ('小华',20) ,('小明',88);
select * from persons;
select * from sales;
drop trigger chufaqi.num_sum;
```





## MySQL Cluster 集群搭建



| 节点          | 对应的ip地址和端口  |
| :------------ | ------------------- |
| 管理节点（1） | 192.168.40.201      |
| SQL节点（2）  | 192.168.40.202:3331 |
|               | 192.168.40.203:3331 |
| 数据节点（2） | 192.168.40.202      |
|               | 192.168.40.203      |

### 1、配置网卡信息

### 2、删除安装过的mysql数据库

### 3、关闭防火墙或者设置防火墙

#### 4、管理节点配置步骤

```powershell
[root@hadoop201 ~]$ cd /usr/local/
[root@hadoop201 local]$ ll
总用量 0
drwxr-xr-x. 2 root root  6 5月  11 2019 bin
drwxr-xr-x. 2 root root  6 5月  11 2019 etc
drwxr-xr-x. 2 root root  6 5月  11 2019 games
drwxr-xr-x. 2 root root  6 5月  11 2019 include
drwxr-xr-x. 2 root root  6 5月  11 2019 lib
drwxr-xr-x. 2 root root  6 5月  11 2019 lib64
drwxr-xr-x. 2 root root  6 5月  11 2019 libexec
drwxr-xr-x. 2 root root  6 5月  11 2019 sbin
drwxr-xr-x. 5 root root 49 1月  31 09:56 share
drwxr-xr-x. 2 root root  6 5月  11 2019 src
[root@hadoop201 local]$ rm -rf mysql
[root@hadoop201 software]# cd /opt/software/
[root@hadoop201 software]# tar -zxvf mysql-cluster-8.0.19-linux-glibc2.12-x86_64.tar.gz 
[root@hadoop201 software]# groupadd mysql
[root@hadoop201 software]# useradd mysql -g mysql
[root@hadoop201 software]# cd
[root@hadoop201 ~]# mkdir /usr/local/mysql-cluster
[root@hadoop201 ~]# mkdir /usr/local/mysql-cluster
[root@hadoop201 ~]# mv /opt/software/mysql-cluster-8.0.19-linux-glibc2.12-x86_64/* /usr/local/mysql-cluster/
[root@hadoop201 ~]# cd /usr/local/mysql-cluster/
[root@hadoop201 mysql-cluster]# ll
总用量 492
drwxr-xr-x  2 mysql 31415   4096 12月 11 16:41 bin
drwxr-xr-x  2 mysql 31415     55 12月 11 16:41 docs
drwxr-xr-x  4 mysql 31415    297 12月 11 16:41 include
drwxr-xr-x  6 mysql 31415    303 12月 11 16:41 lib
-rw-r--r--  1 mysql 31415 489954 12月 10 18:41 LICENSE
drwxr-xr-x  4 mysql 31415     30 12月 11 16:41 man
drwxr-xr-x  3 mysql 31415     17 12月 11 16:41 mysql-test
-rw-r--r--  1 mysql 31415    761 12月 10 18:41 README
drwxr-xr-x 31 mysql 31415   4096 12月 11 16:41 share
drwxr-xr-x  2 mysql 31415     77 12月 11 16:41 support-files
#进入到/uar/local/mysql-cluster目录，将管理程序拷贝到 bin目录：
[root@hadoop201 mysql-cluster]# cp bin/ndb_mgm* /usr/local/bin/
[root@hadoop201 mysql-cluster]# cd /usr/local/bin/
#授权指定文件可执行权限
[root@hadoop201 bin]# chmod +X ndb_mgm*
#初始化管理节点
[root@hadoop201 ~]# mkdir /var/lib/mysql-cluster
[root@hadoop201 ~]# mkdir /usr/local/mysql
[root@hadoop201 ~]# vi /var/lib/mysql-cluster/config.ini
[root@hadoop201 ~]# cat /var/lib/mysql-cluster/config.ini
[root@hadoop201 mysql-cluster]# cat /var/lib/mysql-cluster/config.ini


[ndbd default]
NoOfReplicas=2
DataMemory=512M
IndexMemory=20M

[ndb_mgmd]
NodeId=1
HostName=192.168.40.201          
DataDir=/var/lib/mysql-cluster

[ndbd]
NodeId=2
HostName=192.168.40.202
DataDir=/var/lib/mysql-cluster

[ndbd]
NodeId=3
HostName=192.168.40.203
DataDir=/var/lib/mysql-cluster

[mysqld]
HostName=192.168.40.202

[mysqld]
HostName=192.168.40.203

[mysqld]

# 使用配置文件初始化管理节点
[root@hadoop201 ~]# /usr/local/bin/ndb_mgmd -f /var/lib/mysql-cluster/config.ini --initial
/usr/local/bin/ndb_mgmd: error while loading shared libraries: libtinfo.so.5: cannot open shared object file: No such file or directory
#缺少libaio-0.3.112-1.el8.x86_64.rpm 
[root@hadoop201 ~]# sudo ln -s /usr/lib64/libtinfo.so.6.1 /usr/lib64/libtinfo.so.5
[root@hadoop201 ~]# /usr/local/bin/ndb_mgmd -f /var/lib/mysql-cluster/config.ini --initial
MySQL Cluster Management Server mysql-8.0.19 ndb-8.0.19
2020-04-25 22:19:23 [MgmtSrvr] WARNING  -- at line 6: [DB] IndexMemory is deprecated, will use Number bytes on each ndbd(DB) node allocated for storing indexes instead

[root@hadoop201 mysql-cluster]# netstat -ntulp |grep 1186
tcp        0      0 0.0.0.0:1186            0.0.0.0:*               LISTEN      19410/ndb_mgmd 
[root@hadoop201 mysql-cluster]# ps -ef |grep ndb
root      19410      1  0 21:52 ?        00:00:09 /usr/local/bin/ndb_mgmd -f /var/lib/mysql-cluster/config.ini --initial
root      20376  14549  0 22:13 pts/1    00:00:00 grep --color=auto ndb
[root@hadoop201 ~]# ndb_mgm
-- NDB Cluster -- Management Client --
ndb_mgm> show 
Connected to Management Server at: localhost:1186
Cluster Configuration
---------------------
[ndbd(NDB)]	2 node(s)
id=2 (not connected, accepting connect from 192.168.40.202)
id=3 (not connected, accepting connect from 192.168.40.203)

[ndb_mgmd(MGM)]	1 node(s)
id=1	@192.168.40.201  (mysql-8.0.19 ndb-8.0.19)

[mysqld(API)]	3 node(s)
id=4 (not connected, accepting connect from 192.168.40.202)
id=5 (not connected, accepting connect from 192.168.40.203)
id=6 (not connected, accepting connect from any host)


#安装配置 存储节点 与  sql节点

[root@hadoop201 mysql-cluster]# chown -R root .
[root@hadoop201 mysql-cluster]# chown -R mysql .




```

[mysql 报 error while loading shared libraries: libtinfo.so.5 解决办法](https://www.cnblogs.com/kaishirenshi/p/12667004.html)

### 192.168.40.202配置

```powershell
[root@hadoop202 ~]# groupadd mysql
[root@hadoop202 ~]# useradd -r -g mysql mysql
[root@hadoop202 ~]# mkdir /var/lib/mysql-cluster
#解压（解压到根目录了）
[root@hadoop202 ~]# tar -zxvf /opt/software/mysql-cluster-8.0.19-linux-glibc2.12-x86_64.tar.gz 
[root@hadoop202 ~]# mkdir /usr/local/mysql
[root@hadoop202 ~]# mv mysql-cluster-8.0.19-linux-glibc2.12-x86_64/* /usr/local/mysql


#mysql数据库的初始化及密码的重置
[root@hadoop202 ~]# cd /usr/local/mysql/
[root@hadoop202 mysql]# ./bin/mysqld --initialize
2020-04-25T15:04:35.195612Z 0 [System] [MY-013169] [Server] /usr/local/mysql/bin/mysqld (mysqld 8.0.19-cluster) initializing of server in progress as process 14621
2020-04-25T15:04:37.556877Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: /ukotis<t0rD
#使用生成的密码登录
[root@hadoop202 ~]# cd /usr/local/mysql/bin
[root@hadoop202 bin]# ./mysql -u root -p
./mysql: error while loading shared libraries: libtinfo.so.5: cannot open shared object file: No such file or directory
[root@hadoop202 bin]# sudo ln -s /usr/lib64/libtinfo.so.6.1 /usr/lib64/libtinfo.so.5
[root@hadoop202 bin]# ./mysql -u root -p
Enter password: 
#重置root密码为123456
[root@hadoop202 bin]# ./mysqladmin -u root -p password "123456"
Enter password: 
mysqladmin: [Warning] Using a password on the command line interface can be insecure.
Warning: Since password will be sent to server in plain text, use ssl connection to ensure password safety.


[root@hadoop202 mysql]# cd /usr/local/mysql
[root@hadoop202 mysql]# chown -R mysql:mysql /usr/local/mysql
[root@hadoop202 mysql]# chown -R mysql:mysql data
#将/usr/local/mysql/support-files文件夹下的mysql.server复制到/etc/rc.d/init.d/下
[root@hadoop202 mysql]#  cp support-files/mysql.server /etc/rc.d/init.d/
#授权服务
[root@hadoop202 mysql]# chmod +x /etc/rc.d/init.d/mysql.server
#使用 chkconfig 增加新的一项服务，系统从其之后服务自动运行。
[root@hadoop202 mysql]# chkconfig --add mysql.server
#配置数据节点
[root@hadoop202 ~]# vi /etc/my.cnf
[root@hadoop202 ~]# cat /etc/my.cnf
[mysqld]
ndbcluster
#数据节点定位管理节点的 IP 地址
ndb-connectstring=192.168.40.201
[mysql_cluster]
#定位管理节点
ndb-connectstring=192.168.40.201

[root@hadoop202 ~]# /usr/local/mysql/bin/ndbd --initial
2020-04-25 23:30:39 [ndbd] INFO     -- Angel connected to '192.168.40.201:1186'
2020-04-25 23:30:39 [ndbd] INFO     -- Angel allocated nodeid: 2



#开启 MySQL 服务停止
[root@hadoop202 ~]# cd /etc/init.d/
[root@hadoop202 init.d]# ./mysql.server start
Starting MySQL................................. SUCCESS! 
#关闭 MySQL 服务停止
[root@hadoop202 init.d]# ./mysql.server stop
Shutting down MySQL............................. SUCCESS! 
[root@hadoop202 init.d]# 
[root@hadoop202 ~]# cd /usr/local/mysql/
[root@hadoop202 mysql]# bin/mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.19-cluster MySQL Cluster Community Server - GPL

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```

### 192.168.40.203配置

```powershell
[root@hadoop203 ~]# groupadd mysql
[root@hadoop203 ~]# useradd -r -g mysql mysql
[root@hadoop203 ~]# mkdir /var/lib/mysql-cluster
#解压（解压到根目录了）
[root@hadoop203 ~]# tar -zxvf /opt/software/mysql-cluster-8.0.19-linux-glibc2.12-x86_64.tar.gz 
[root@hadoop203 ~]# mkdir /usr/local/mysql
[root@hadoop203 ~]#  mv mysql-cluster-8.0.19-linux-glibc2.12-x86_64/* /usr/local/mysql


#mysql数据库的初始化及密码的重置
[root@hadoop203 ~]# cd /usr/local/mysql/bin
[root@hadoop203 bin]# ./mysqld --initialize
2020-04-25T19:05:55.776801Z 0 [System] [MY-013169] [Server] /usr/local/mysql/bin/mysqld (mysqld 8.0.19-cluster) initializing of server in progress as process 10429
2020-04-25T19:05:57.827158Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: rZh)HsuWu1s*
#使用生成的密码登录
[root@hadoop203 bin]# ./mysql -u root -p 
./mysql: error while loading shared libraries: libtinfo.so.5: cannot open shared object file: No such file or directory
[root@hadoop203 bin]# sudo ln -s /usr/lib64/libtinfo.so.6.1 /usr/lib64/libtinfo.so.5
[root@hadoop203 bin]# ./mysql -u root -p 
Enter password: 
#重置root密码为123456
[root@hadoop203 bin]# ./mysqladmin -u root -p password "123456"
Enter password: 
mysqladmin: [Warning] Using a password on the command line interface can be insecure.
Warning: Since password will be sent to server in plain text, use ssl connection to ensure password safety.


[root@hadoop203 ~]#  cd /usr/local/mysql
[root@hadoop203 mysql]# chown -R mysql:mysql /usr/local/mysql
[root@hadoop203 mysql]# chown -R mysql:mysql data
#将/usr/local/mysql/support-files文件夹下的mysql.server复制到/etc/rc.d/init.d/下
[root@hadoop203 mysql]#  cp support-files/mysql.server /etc/rc.d/init.d/
#授予可执行权限
[root@hadoop203 mysql]# chmod +x /etc/rc.d/init.d/mysql.server
#使用 chkconfig 增加新的一项服务，系统从其之后服务自动运行。
[root@hadoop203 mysql]# chkconfig --add mysql.server
[root@hadoop203 mysql]# vi /etc/my.cnf

[mysqld]
ndbcluster
#数据节点定位管理节点的 IP 地址
ndb-connectstring=192.168.40.201
#定位管理节点
[mysql_cluster]
ndb-connectstring=192.168.40.201


[root@hadoop203 mysql]# 
[root@hadoop203 ~]# /etc/init.d/mysql.server start
Starting MySQL.Logging to '/usr/local/mysql/data/hadoop203.err'.
................................... SUCCESS! 
[root@hadoop203 init.d]# /etc/init.d/mysql.server stop
Shutting down MySQL........................ SUCCESS!


[root@hadoop203 ~]# /usr/local/mysql/bin/ndbd --initial
2020-04-25 23:31:32 [ndbd] INFO     -- Angel connected to '192.168.40.201:1186'
2020-04-25 23:31:32 [ndbd] INFO     -- Angel allocated nodeid: 3



#mysql服务的启动和关闭
[root@hadoop203 ~]# cd /etc/init.d
[root@hadoop203 init.d]# ./mysql.server start
Starting MySQL................................ SUCCESS! 
[root@hadoop203 init.d]# ./mysql.server stop
Shutting down MySQL.............................. SUCCESS! 


#mysql数据库的启动
[root@hadoop203 ~]# cd /usr/local/mysql/
[root@hadoop203 mysql]# bin/mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 8.0.19-cluster MySQL Cluster Community Server - GPL

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```



```powershell
# ln -s /usr/local/mysql-cluster-gpl-7.5.4-linux-glibc2.5-x86_64 /usr/local/mysql

或者直接将 目录 mysql-cluster-gpl-7.5.4-linux-glibc2.5-x86_64 改名为 mysql
```



```powershell
[root@hadoop201 ~]# ndb_mgm
-- NDB Cluster -- Management Client --
ndb_mgm> show
Connected to Management Server at: localhost:1186
Cluster Configuration
---------------------
[ndbd(NDB)]	2 node(s)
id=2	@192.168.40.202  (mysql-8.0.19 ndb-8.0.19, Nodegroup: 0, *)
id=3	@192.168.40.203  (mysql-8.0.19 ndb-8.0.19, Nodegroup: 0)

[ndb_mgmd(MGM)]	1 node(s)
id=1	@192.168.40.201  (mysql-8.0.19 ndb-8.0.19)

[mysqld(API)]	3 node(s)
id=4	@192.168.40.202  (mysql-8.0.19 ndb-8.0.19)
id=5	@192.168.40.203  (mysql-8.0.19 ndb-8.0.19)
id=6 (not connected, accepting connect from any host)

ndb_mgm> 

```

```powershell
#
[root@hadoop201 ~]# ps -ef | grep ndb
root      14167   3008  0 01:58 pts/0    00:00:00 grep --color=auto ndb
[root@hadoop201 ~]# cd /var/lib/mysql-cluster/
[root@hadoop201 mysql-cluster]# cd 
#使用 ndb_mgmd 命令启动管理节点，并指定配置文件
[root@hadoop201 ~]# ndb_mgmd -f ./config.ini
MySQL Cluster Management Server mysql-8.0.19 ndb-8.0.19
[root@hadoop201 ~]# ps -ef | grep ndb
root      14177      1  0 01:59 ?        00:00:00 ndb_mgmd -f ./config.ini
root      14192   3008  0 01:59 pts/0    00:00:00 grep --color=auto ndb
[root@hadoop201 ~]# 

[root@hadoop201 ~]# ndb_mgm
-- NDB Cluster -- Management Client --
ndb_mgm> show
Connected to Management Server at: localhost:1186
Cluster Configuration
---------------------
[ndbd(NDB)]	2 node(s)
id=2 (not connected, accepting connect from 192.168.40.202)
id=3 (not connected, accepting connect from 192.168.40.203)

[ndb_mgmd(MGM)]	1 node(s)
id=1	@192.168.40.201  (mysql-8.0.19 ndb-8.0.19)

[mysqld(API)]	3 node(s)
id=4 (not connected, accepting connect from 192.168.40.202)
id=5 (not connected, accepting connect from 192.168.40.203)
id=6 (not connected, accepting connect from any host)

ndb_mgm> 


```



[手把手教你用Mysql-Cluster-7.5搭建数据库集群](https://www.cnblogs.com/linkstar/p/6510713.html)



```powershell
[root@hadoop201 ~]# cd /var/lib/mysql-cluster/
[root@hadoop201 mysql-cluster]# ll
总用量 548
-rw-r--r-- 1 root root    681 4月  25 21:51 1config.ini
-rw-r--r-- 1 root root    370 4月  26 00:23 config.ini
-rw-r--r-- 1 root root 545906 4月  26 04:26 ndb_1_cluster.log
-rw-r--r-- 1 root root     80 4月  26 03:19 ndb_1_out.log


[root@hadoop201 mysql-cluster]# ndb_mgmd -f ./config.ini 
MySQL Cluster Management Server mysql-8.0.19 ndb-8.0.19
[root@hadoop201 mysql-cluster]# ndb_mgm


[root@hadoop202 ~]# cd /usr/local/mysql/
[root@hadoop202 mysql]# bin/ndbd


[root@hadoop203 ~]# cd /usr/local/mysql/
[root@hadoop203 mysql]# bin/ndbd


[root@hadoop202 ~]# cd /usr/local/mysql/
[root@hadoop202 mysql]# bin/mysql -uroot -p

[root@hadoop203 ~]# cd /usr/local/mysql/
[root@hadoop203 mysql]# bin/mysql -uroot -p

```



配置数据节点的环境变量

```powershell

[root@hadoop202 ~]# vim /etc/profile
#MySQL Cluster环境变量
PATH=$PATH:/usr/local/mysql/bin
export PATH
[root@hadoop202 mysql]# source /etc/profile


[root@hadoop203 ~]# vim /etc/profile
#MySQL Cluster环境变量
PATH=$PATH:/usr/local/mysql/bin
export PATH
[root@hadoop203 ~]# source /etc/profile
```





### Cluster 的启动

```powershell

[root@hadoop202 mysql]# cd /etc/init.d
[root@hadoop202 init.d]# ./mysql.server start
Starting MySQL.................................. SUCCESS! 

[root@hadoop203 mysql]# cd /etc/init.d
[root@hadoop203 init.d]# ./mysql.server start
Starting MySQL.................................. SUCCESS! 


[root@hadoop202 ~]# mysql -uroot -p
[root@hadoop203 ~]# mysql -uroot -p
[root@hadoop202 ~]# ndbd
2020-04-28 01:10:38 [ndbd] INFO     -- Angel connected to '192.168.40.201:1186'
2020-04-28 01:10:38 [ndbd] INFO     -- Angel allocated nodeid: 2
[root@hadoop203 ~]# ndbd
2020-04-28 01:10:30 [ndbd] INFO     -- Angel connected to '192.168.40.201:1186'
2020-04-28 01:10:30 [ndbd] INFO     -- Angel allocated nodeid: 3

[root@hadoop201 ~]# ndb_mgm
-- NDB Cluster -- Management Client --
ndb_mgm> show
Connected to Management Server at: localhost:1186
Cluster Configuration
---------------------
[ndbd(NDB)]	2 node(s)
id=2	@192.168.40.202  (mysql-8.0.19 ndb-8.0.19, Nodegroup: 0, *)
id=3	@192.168.40.203  (mysql-8.0.19 ndb-8.0.19, Nodegroup: 0)

[ndb_mgmd(MGM)]	1 node(s)
id=1	@192.168.40.201  (mysql-8.0.19 ndb-8.0.19)

[mysqld(API)]	3 node(s)
id=4	@192.168.40.202  (mysql-8.0.19 ndb-8.0.19)
id=5	@192.168.40.203  (mysql-8.0.19 ndb-8.0.19)
id=6 (not connected, accepting connect from any host)
```











### Cluster 的关闭

MySQL Cluster 关闭只需要使用 ndb_mgm 命令



```powershell
[root@hadoop201 ~]# ndb_mgm -e shutdown
```

































































































































































































































































































































#### mysql cluster root用户密码修改

发表于 2019-11-03 | 分类于 [DevOps](https://www.dazhuanlan.com/devops/) | 没有评论

在mysqld节点上登录root时，发现密码总是不正确，或者忘记密码，需要在无密码登录情况下修改密码。 首先修改mysqld上配置文件/etc/my.cnf，允许无密码登录。 配置内容为：

```powershell
[root@hadoop203 ~]# vim /etc/my.cnf
[mysqld]
ndbcluster
socket=/var/run/mysqld/mysqld.sock
[mysql_cluster]
ndb-connectstring=192.168.40.201
[client]
socket=/var/run/mysqld/mysqld.sock

skip-grant-tables
```

在myqld下面增加一句：skip-grant-tables 然后重启mysqld服务：

```powershell
#sudo /opt/mysql/server-5.7/support-files/mysql.server restart

[root@hadoop203 bin]# /etc/init.d/mysql.server restart
```

之后，无密码登录mysql，并修改root密码

```mysql
[root@hadoop203 bin]# /etc/init.d/mysql.server restart

#sudo /opt/mysql/server-5.7/bin/mysql
[root@hadoop203 bin]# ./mysql
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.19-cluster MySQL Cluster Community Server - GPL

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 

use mysql
flush privileges;
#mysql8弃用
#set password for 'root'@'localhost' = PASSWORD('2LRZ9LmT5GDFHAs3');

mysql> update user set authentication_string="123456" where user='root';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
flush privileges;
[root@hadoop203 bin]# /etc/init.d/mysql.server restart
```

然后再进入/etc/my.cnf把刚才加的那句删掉，再重启mysqld服务：

```
sudo /opt/mysql/server-5.7/support-files/mysql.server restart
```

现在密码应该生效了。

authentication_string为mysql数据库中user表的用户密码选项！



### 查看数据库的最大连接数

```mysql
mysql> show variables like 'max_connections';
+-----------------+-------+
| Variable_name   | Value |
+-----------------+-------+
| max_connections | 151   |
+-----------------+-------+
1 row in set, 1 warning (0.05 sec)

mysql>
```







### MySQL BLOB 类型介绍

MySQL中，BLOB是一个**二进制大型对象**，是一个可以存储大量数据的容器，它能容纳不同大小的数据。BLOB类型实际是个类型系列（TinyBlob、Blob、MediumBlob、LongBlob），除了在存储的最大信息量上不同外，他们是等同的。 

MySQL的四种BLOB类型 
 类型 大小(单位：字节) 
 TinyBlob 最大 255B
 Blob 最大 65K 
 MediumBlob 最大 16M 
 LongBlob 最大 4G 

实际使用中根据需要存入的数据大小定义不同的BLOB类型。 
需要注意的是：如果你存储的文件过大，数据库的性能会下降很多。 

 

### mysql 数据库字段和 Java 类型的对应关系

#### 背景

   小编最近接触了Mysql数据库表反射生成java实体类（POJO）和映射文件（xml文件），使用Netbeans上的jeddict插件生成JPA(JPA是Java Persistence API的简称，中文名Java持久层API，是JDK 5.0注解或XML描述对象－关系表的映射关系，并将运行期的实体[对象持久化](https://baike.baidu.com/item/对象持久化)到数据库中)的图表，在敲代码的过程中发现了entity层属性的数据类型悄悄发生了改变，原来Mysql和java的数据类型是有对应关系的。

#### 正文

#####  一、类型对应表

​       ![img](images/20170930200408361.png)

​       

  注意：Mysql中int 和 integer 同义，他们都对应着java中的long类型。

#####  二 、int、bigint、mediumint、smallint、tinyint的比较

​       ![img](images/20171001195726888.png)

​       

#####  三、易错点

​     在做机房的时候，会以为int(4)只能存储4个长度的数字，int（11）就会存储11个长度的数字，这是错误的。

​    实际上int(n)所占多少存储空间并无任何关系。int(3)、int(4)、int(8) 在磁盘上都是占用 4 btyes 的存储空间。说白了，除了显示给用户的方式有点不同外，int(M) 跟 int 数据类型是相同的。

  举例：

​      int 的值是10 （指定zerofill）

​      int（8）的显示结果为 00000010 （左边填充8个0）

​      int（5）的显示结果为 00010（左边填充3个0）

​      只是显示的长度不一样而已，都是占用四个字节的空间。

​      对于其他类型（n）中的n也同样适用，n 代表的并不是存储在数据库中的具体长度，如果设定了zerofill属性，当数据的宽度小于设定的宽度时（2<8），则自动填充0，这只是最后显示的结果（一种格式化输出），但是在Mysql中实际存储的还是10。

总结

​    在了解数据类型的知识时，发现各种ORM对应的数据类型还是有一些区别的；同时觉得我总是会把我不熟悉的东西想的很难很难，其实都是自己给自己设的坎儿~~迈过去就好了！

​    感谢您的阅读，如有不完善之处，还望指教！





### 实体类中不要使用基本类型

由于 Java 中的基本类型会有默认值，例如当某个类中存在 private int age；字段时，
创建这个类时， age 会有默认值 0。当使用 age 属性时，它总会有值。因此在某些情况下，
便无法实现使 age 为 null。并且在动态 SQL 的部分，如果使用 age != null 进行判断，
结果总会为 true，因而会导致很多隐藏的问题。
所以， 在实体类中不要使用基本类型。 基本类型包括 byte、 int、 short、 long、 float、
double、 char、 boolean。

### 基础业务字段

```

```

```
[id, create_time, create_user, update_time, update_user, status, is_deleted]
```





### 数据库有重复数据，并且删除重复数据

```mysql
#建立表
create table student
(
    id   int auto_increment
        primary key,
    name varchar(20) null comment '姓名',
    age  int         null comment '年龄',
    sex  varchar(10) null comment '性别'
)
    comment '学生表';
#插入数据
INSERT INTO sm.student (name, age, sex)
VALUES ('李四', 17, '男');
INSERT INTO sm.student (name, age, sex)
VALUES ('李四', 17, '男');
INSERT INTO sm.student (name, age, sex)
VALUES ('王花', 18, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('王五', 19, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('张三', 16, '男');
INSERT INTO sm.student (name, age, sex)
VALUES ('张三', 16, '男');
INSERT INTO sm.student (name, age, sex)
VALUES ('张三', 17, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('王五', 19, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('张三', 17, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('王花', 18, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('王花', 18, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('王花', 18, '女');
INSERT INTO sm.student (name, age, sex)
VALUES ('小明', 20, '男');
INSERT INTO sm.student (name, age, sex)
VALUES ('姚瑶', 21, '女');

# 查询数据表里有没有除了id之外其他内容都一样的数据
select *, count(*)
from student
group by name, age, sex
having count(*) >= 2;

# 删除除了id值最小的数据之外的数据（也就是只保留id值最小的数据）
# 先查出id最小的数据（下面的两条语句都可以）
select min(id)
from student2
group by name, age, sex;

select id
from student2
group by name, age, sex;

#删除重复数据，保留最小id的数据
delete
from student
where id not in
      (select a.id
       from (select min(id) as id
             from student
             group by name, age, sex) a);

delete
from student
where id not in
      (select a.id
       from (select id
             from student
             group by name, age, sex) a);
             
#删除重复数据，保留最大id的数据  
delete
from student
where id not in
      (select a.id
       from (select max(id) as id
             from student
             group by name, age, sex) a);


```

ERROR 1093 (HY000): You can't specify target table 'student2' for update in FROM clause

原因：在mysql中，不能通过嵌套子查询来直接删除或者修改记录，需要通过别名来指定嵌套子查询作为一个临时表。

解决办法：给嵌套子查询的结果取一个别名，然后从这个表中再次查询出记录,然后再做删除或者修改操作。





# [for update的作用和用法](https://www.cnblogs.com/banma/p/11797560.html)

**一、for update定义**

for update是一种行级锁，又叫排它锁，一旦用户对某个行施加了行级加锁，则该用户可以查询也可以更新被加锁的数据行，其它用户只能查询但不能更新被加锁的数据行．如果其它用户想更新该表中的数据行，则也必须对该表施加行级锁．即使多个用户对一个表均使用了共享更新，但也不允许两个事务同时对一个表进行更新，真正对表进行更新时，是以独占方式锁表，一直到提交或复原该事务为止。行锁永远是独占方式锁。

只有当出现如下之一的条件，才会释放共享更新锁：
1、执行提交（COMMIT）语句
2、退出数据库（LOG　OFF）
3、程序停止运行

 

**二、概念和用法**

通常情况下，select语句是不会对数据加锁，妨碍影响其他的DML和DDL操作。同时，在多版本一致读机制的支持下，select语句也不会被其他类型语句所阻碍。

而select … for update 语句是我们经常使用手工加锁语句。在数据库中执行select … for update ,大家会发现会对数据库中的表或某些行数据进行锁表，在mysql中，如果查询条件带有主键，会锁行数据，如果没有，会锁表。

 由于InnoDB预设是Row-Level Lock，所以只有「明确」的指定主键，MySQL才会执行Row lock (只锁住被选取的资料例) ，否则MySQL将会执行Table Lock (将整个资料表单给锁住)。

举个例子: 假设有张表user ，里面有 id 和 name 两列，id是主键。

 

例1: (明确指定主键，并且数据真实存在，row lock)

```mysql
SELECT * FROM user WHERE id=3 FOR UPDATE;

SELECT * FROM user WHERE id=3 and name='Tom' FOR UPDATE;
```

 

例2: (明确指定主键，但数据不存在，无lock)

```mysql
SELECT * FROM user WHERE id=0 FOR UPDATE;
```

 

例3: (主键不明确，table lock)

```mysql
SELECT * FROM user WHERE id<>3 FOR UPDATE;

SELECT * FROM user WHERE id LIKE '%3%' FOR UPDATE;
```

 

例4: (无主键，table lock)

```mysql
SELECT * FROM user WHERE name='Tom' FOR UPDATE;
```

 

注意：
1、FOR UPDATE仅适用于InnoDB，且必须在事务处理模块(BEGIN/COMMIT)中才能生效。

2、要测试锁定的状况，可以利用MySQL的Command Mode(命令模式) ，开两个视窗来做测试。

3、Myisam 只支持表级锁，InnerDB支持行级锁 添加了(行级锁/表级锁)锁的数据不能被其它事务再锁定，也不被其它事务修改。是表级锁时，不管是否查询到记录，都会锁定表。

 

**三、什么时候需要使用for update？**

借助for update语句，我们可以在应用程序的层面手工实现数据加锁保护操作。就是那些需要业务层面数据独占时，可以考虑使用for update。

场景上，比如火车票订票，在屏幕上显示有票，而真正进行出票时，需要重新确定一下这个数据没有被其他客户端修改。所以，在这个确认过程中，可以使用for update。

 

**四、for update悲观锁**

悲观锁：总是假设最坏的情况，每次去拿数据的时候都认为别人会修改，所以每次在拿数据的时候都会上锁，这样别人想拿这个数据就会阻塞直到它解锁。传统的关系型数据库里边就用到了很多这种锁机制，比如行锁，表锁等，读锁，写锁等，都是在做操作之前先上锁。就像for update，再比如Java里面的同步原语synchronized关键字的实现也是悲观锁。

乐观锁：顾名思义，就是很乐观，每次去拿数据的时候都认为别人不会修改，所以不会上锁，但是在更新的时候会判断一下在此期间别人有没有去更新这个数据，可以使用版本号等机制。乐观锁适用于多读的应用类型，这样可以提高吞吐量，像数据库提供的类似于write_condition机制，其实都是提供的乐观锁。