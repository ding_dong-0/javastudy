drop database if exists school; #如果存在SCHOOL则删除
create database school;         #建立库SCHOOL
use school;                     #打开库SCHOOL
create table teacher             #建立表TEACHER
(
    id int(3) auto_increment not null primary key,
    name char(10) not null,
    address varchar(50) default "深圳",
    year date
);                                #建表结束

#以下为插入字段
insert into teacher ( name, address, year) values('allen','大连一中','1976-10-10');
insert into teacher ( name, address, year) values('jack','大连二中','1975-12-23');