zshjs1226790210 zshjs1226790210



![image-20220329101401864](images/image-20220329101401864.png)



### [虚拟机扩容](https://blog.csdn.net/young_0609/article/details/120869420?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~default-2.no_search_link&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~default-2.no_search_link&utm_relevant_index=4)

```powershell
[root@localhost ~]# fdisk -l
Disk /dev/sdc：5 GiB，5368709120 字节，10485760 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xee725385

设备       启动  起点     末尾     扇区 大小 Id 类型
/dev/sdc1        2048 10485759 10483712   5G 83 Linux


Disk /dev/sda：50 GiB，53687091200 字节，104857600 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x42d3004e

设备       启动    起点     末尾     扇区 大小 Id 类型
/dev/sda1  *       2048  2099199  2097152   1G 83 Linux
/dev/sda2       2099200 41943039 39843840  19G 8e Linux LVM


Disk /dev/sdb：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x70498da4

设备       启动  起点    末尾    扇区 大小 Id 类型
/dev/sdb1        2048 4194303 4192256   2G 83 Linux




Disk /dev/mapper/cl-root：17 GiB，18249416704 字节，35643392 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节


Disk /dev/mapper/cl-swap：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
[root@localhost ~]# fdisk /dev/sda

欢迎使用 fdisk (util-linux 2.32.1)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。


命令(输入 m 获取帮助)：n
分区类型
   p   主分区 (2个主分区，0个扩展分区，2空闲)
   e   扩展分区 (逻辑分区容器)
选择 (默认 p)：p
分区号 (3,4, 默认  3): 3
第一个扇区 (41943040-104857599, 默认 41943040): 
上个扇区，+sectors 或 +size{K,M,G,T,P} (41943040-104857599, 默认 104857599): 

创建了一个新分区 3，类型为“Linux”，大小为 30 GiB。

命令(输入 m 获取帮助)：t
分区号 (1-3, 默认  3): 
Hex 代码(输入 L 列出所有代码)：8e

已将分区“Linux”的类型更改为“Linux LVM”。

命令(输入 m 获取帮助)：w
分区表已调整。
正在同步磁盘。

[root@localhost ~]# fdisk -l
Disk /dev/sdc：5 GiB，5368709120 字节，10485760 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xee725385

设备       启动  起点     末尾     扇区 大小 Id 类型
/dev/sdc1        2048 10485759 10483712   5G 83 Linux


Disk /dev/sda：50 GiB，53687091200 字节，104857600 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x42d3004e

设备       启动     起点      末尾     扇区 大小 Id 类型
/dev/sda1  *        2048   2099199  2097152   1G 83 Linux
/dev/sda2        2099200  41943039 39843840  19G 8e Linux LVM
/dev/sda3       41943040 104857599 62914560  30G 8e Linux LVM


Disk /dev/sdb：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x70498da4

设备       启动  起点    末尾    扇区 大小 Id 类型
/dev/sdb1        2048 4194303 4192256   2G 83 Linux




Disk /dev/mapper/cl-root：17 GiB，18249416704 字节，35643392 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节


Disk /dev/mapper/cl-swap：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
[root@localhost ~]#  reboot




fdisk -l
Disk /dev/sdb：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x70498da4

设备       启动  起点    末尾    扇区 大小 Id 类型
/dev/sdb1        2048 4194303 4192256   2G 83 Linux


Disk /dev/sdc：5 GiB，5368709120 字节，10485760 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xee725385

设备       启动  起点     末尾     扇区 大小 Id 类型
/dev/sdc1        2048 10485759 10483712   5G 83 Linux


Disk /dev/sda：50 GiB，53687091200 字节，104857600 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x42d3004e

设备       启动     起点      末尾     扇区 大小 Id 类型
/dev/sda1  *        2048   2099199  2097152   1G 83 Linux
/dev/sda2        2099200  41943039 39843840  19G 8e Linux LVM
/dev/sda3       41943040 104857599 62914560  30G 8e Linux LVM


Disk /dev/mapper/cl-root：17 GiB，18249416704 字节，35643392 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节


Disk /dev/mapper/cl-swap：2 GiB，2147483648 字节，4194304 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
[root@localhost ~]# pvcreate /dev/sda3
  Physical volume "/dev/sda3" successfully created.
[root@localhost ~]# vgdisplay
  --- Volume group ---
  VG Name               cl
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <19.00 GiB
  PE Size               4.00 MiB
  Total PE              4863
  Alloc PE / Size       4863 / <19.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               nZyknJ-KJ9U-Bpm6-65de-Yj1y-bHFT-Muj4Dq
   
[root@localhost ~]# vgextend centos /dev/sda3
  Volume group "centos" not found
  Cannot process volume group centos
[root@localhost ~]# vgextend cl /dev/sda3
  Couldn't create temporary archive name.
  #剩余空间不足
[root@localhost ~]# vgextend cl /dev/sda3
  Volume group "cl" successfully extended
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                1.9G     0  1.9G    0% /dev/shm
tmpfs                1.9G  183M  1.7G   10% /run
tmpfs                1.9G     0  1.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   17G  171M  100% /
/dev/sdb1            2.0G  6.0M  1.9G    1% /home/newdisk
/dev/sda1            976M  134M  775M   15% /boot
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/71e0b304d4d343ae7c1e3e2367290ec1af647768006002dfccf919d5fb9479df/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/bc6f6fdd3f535e3e605faa258d81b43daada264af0014f577be1b26831e307de/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/8d45320eb78c524afc4705e2e4f2aa6acf45f86bd969c749aac52e71ce946640/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/323adaae0024f7ab62e5751287f33dbf74ad3fdbb788bbd274ac09e9cb1fef57/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/383dbd826953c6fcedbdcba123b1d0753fadf1317f468c613cb5685328d53da7/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/c64ab2c91215511f6f3791bc1a84460402f1de91d4d0390b64ee6b308ab4f799/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/3aea902b63b0867b9a679cca5fb1501733987f52abd1f8b75fc2f1eab0b06f41/merged
overlay               17G   17G  171M  100% /var/lib/docker/overlay2/c1eb294517393517f15c2223cfd8d2b281faa689009e0badb7e14c35967a6ef6/merged
tmpfs                376M  4.0K  376M    1% /run/user/0
[root@localhost ~]# lvextend -L +50G /dev/mapper/cl-root
  Insufficient free space: 12800 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +50G /dev/mapper/cl-root
  Insufficient free space: 12800 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +45G /dev/mapper/cl-root
  Insufficient free space: 11520 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +40G /dev/mapper/cl-root
  Insufficient free space: 10240 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +35G /dev/mapper/cl-root
  Insufficient free space: 8960 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +32G /dev/mapper/cl-root
  Insufficient free space: 8192 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +30G /dev/mapper/cl-root
  Insufficient free space: 7680 extents needed, but only 7679 available
[root@localhost ~]# lvextend -L +29.9G /dev/mapper/cl-root
  Rounding size to boundary between physical extents: 29.90 GiB.
  Size of logical volume cl/root changed from <17.00 GiB (4351 extents) to <46.90 GiB (12006 extents).
  Logical volume cl/root successfully resized.
[root@localhost ~]# resize2fs /dev/mapper/cl-root
resize2fs 1.44.3 (10-July-2018)
resize2fs: 超级块中的幻数有错 尝试打开 /dev/mapper/cl-root 时
找不到有效的文件系统超级块。
[root@localhost ~]# xfs_growfs /dev/mapper/cl-root
xfs_growfs: /dev/mapper/cl-root is not a mounted XFS filesystem
[root@localhost ~]# xfs_growfs /dev/mapper/cl-root
xfs_growfs: /dev/mapper/cl-root is not a mounted XFS filesystem
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                1.9G     0  1.9G    0% /dev/shm
tmpfs                1.9G  183M  1.7G   10% /run
tmpfs                1.9G     0  1.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G   17G  274M   99% /
/dev/sdb1            2.0G  6.0M  1.9G    1% /home/newdisk
/dev/sda1            976M  134M  775M   15% /boot
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/71e0b304d4d343ae7c1e3e2367290ec1af647768006002dfccf919d5fb9479df/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/bc6f6fdd3f535e3e605faa258d81b43daada264af0014f577be1b26831e307de/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/8d45320eb78c524afc4705e2e4f2aa6acf45f86bd969c749aac52e71ce946640/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/323adaae0024f7ab62e5751287f33dbf74ad3fdbb788bbd274ac09e9cb1fef57/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/383dbd826953c6fcedbdcba123b1d0753fadf1317f468c613cb5685328d53da7/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/c64ab2c91215511f6f3791bc1a84460402f1de91d4d0390b64ee6b308ab4f799/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/3aea902b63b0867b9a679cca5fb1501733987f52abd1f8b75fc2f1eab0b06f41/merged
overlay               17G   17G  274M   99% /var/lib/docker/overlay2/c1eb294517393517f15c2223cfd8d2b281faa689009e0badb7e14c35967a6ef6/merged
tmpfs                376M  4.0K  376M    1% /run/user/0
[root@localhost ~]# xfs_growfs  /
meta-data=/dev/mapper/cl-root    isize=512    agcount=4, agsize=1113856 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=4455424, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 4455424 to 12294144
[root@localhost ~]# df 
文件系统               1K-块     已用     可用 已用% 挂载点
devtmpfs             1908728        0  1908728    0% /dev
tmpfs                1924408        0  1924408    0% /dev/shm
tmpfs                1924408   186676  1737732   10% /run
tmpfs                1924408        0  1924408    0% /sys/fs/cgroup
/dev/mapper/cl-root 49166336 17761340 31404996   37% /
/dev/sdb1            2030416     6144  1903084    1% /home/newdisk
/dev/sda1             999320   137084   793424   15% /boot
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/71e0b304d4d343ae7c1e3e2367290ec1af647768006002dfccf919d5fb9479df/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/bc6f6fdd3f535e3e605faa258d81b43daada264af0014f577be1b26831e307de/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/8d45320eb78c524afc4705e2e4f2aa6acf45f86bd969c749aac52e71ce946640/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/323adaae0024f7ab62e5751287f33dbf74ad3fdbb788bbd274ac09e9cb1fef57/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/383dbd826953c6fcedbdcba123b1d0753fadf1317f468c613cb5685328d53da7/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/c64ab2c91215511f6f3791bc1a84460402f1de91d4d0390b64ee6b308ab4f799/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/3aea902b63b0867b9a679cca5fb1501733987f52abd1f8b75fc2f1eab0b06f41/merged
overlay             49166336 17761340 31404996   37% /var/lib/docker/overlay2/c1eb294517393517f15c2223cfd8d2b281faa689009e0badb7e14c35967a6ef6/merged
tmpfs                 384880        4   384876    1% /run/user/0
[root@localhost ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             1.9G     0  1.9G    0% /dev
tmpfs                1.9G     0  1.9G    0% /dev/shm
tmpfs                1.9G  183M  1.7G   10% /run
tmpfs                1.9G     0  1.9G    0% /sys/fs/cgroup
/dev/mapper/cl-root   47G   17G   30G   37% /
/dev/sdb1            2.0G  6.0M  1.9G    1% /home/newdisk
/dev/sda1            976M  134M  775M   15% /boot
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/71e0b304d4d343ae7c1e3e2367290ec1af647768006002dfccf919d5fb9479df/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/bc6f6fdd3f535e3e605faa258d81b43daada264af0014f577be1b26831e307de/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/8d45320eb78c524afc4705e2e4f2aa6acf45f86bd969c749aac52e71ce946640/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/323adaae0024f7ab62e5751287f33dbf74ad3fdbb788bbd274ac09e9cb1fef57/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/383dbd826953c6fcedbdcba123b1d0753fadf1317f468c613cb5685328d53da7/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/c64ab2c91215511f6f3791bc1a84460402f1de91d4d0390b64ee6b308ab4f799/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/3aea902b63b0867b9a679cca5fb1501733987f52abd1f8b75fc2f1eab0b06f41/merged
overlay               47G   17G   30G   37% /var/lib/docker/overlay2/c1eb294517393517f15c2223cfd8d2b281faa689009e0badb7e14c35967a6ef6/merged
tmpfs                376M  4.0K  376M    1% /run/user/0


```





### 扩展根分区报错，xfs_growfs 提示is not a mounted XFS filesystem

挂载点通过命令df -h查询

我的/dev/mapper/cl-root 挂载点 /

同步文件系统命令：
\# xfs_growfs /









### Linux查询文件所在路径

```powershell
[root@GAT-NET-BAK-5540-C-X64-ZHFW opt]# find / -name 'xqxx.jsp'
/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-BAK-5540-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/ryzs_xsjb/zsrk/xqxx.jsp

```



```powershell
[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# ll
total 20
-rwxrwxrwx 1 user user 3092 Dec 25  2019 jsonservice_consolebf.jsp
-rw-r--r-- 1 root root 4484 Nov 24 14:57 jsonservice_console.jsp
-rwxrwxrwx 1 user user 4365 Dec 25  2019 tch_downwximage.jsp
[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# chown -R user jsonservice_console.jsp 
[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# ll
total 20
-rwxrwxrwx 1 user user 3092 Dec 25  2019 jsonservice_consolebf.jsp
-rw-r--r-- 1 user root 4484 Nov 24 14:57 jsonservice_console.jsp
-rwxrwxrwx 1 user user 4365 Dec 25  2019 tch_downwximage.jsp
[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# chown -R user:user jsonservice_console.jsp 
[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# ll
total 20
-rwxrwxrwx 1 user user 3092 Dec 25  2019 jsonservice_consolebf.jsp
-rw-r--r-- 1 user user 4484 Nov 24 14:57 jsonservice_console.jsp
-rwxrwxrwx 1 user user 4365 Dec 25  2019 tch_downwximage.jsp
[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# 




[root@GAT-NET-BAK-5540-C-X64-ZHFW server]# chmod a+x jsonservice_console.jsp 


```









curl -H "Content-Type:application/json" -X POST -d '{"data":"2354325235"}' 'localhost:8063/receveldrk'





简单且朴素的原则：不要在for循环中操作DB，包括关系型数据库和NoSql。**

我们应该根据自己的业务场景，在for循环之前批量拿到数据，用尽量少的sql查询批量查到结果。 在for循环中进行数据的匹配组装。







2021/11/3 旧的评价视图

```sql
create view V_ONLINEEVAL as
select s.sdono as projectNo,---办件编号
--        s.csource ,
       decode(s.csource, '0', '1', '2', '2', '5') as pf,--评价渠道（pc端=1，移动端=2，二维码=3，政务大厅平板电脑=4，政务大厅其他终端=5，”12345”热线主动评价=6，短信=7，12345”热线回访评价=8）
       s.sopinion as writingEvaluation ,    ---评价内容
--        s.sefficiency,
--        s.sserattitude,
--        s.ssatisfaction ssatisfaction,
       ceil((s.sefficiency + s.sserattitude) / 4) as satisfaction, --整体满意度得分（1-5)
       g.sdotime assessTime,
decode(sserattitude,
              '10',
              '517',--服务态度非常满意
              '9',
              '517',
              '8',
              '413',--满意
              '7',
              '413',
              '6',
              '314',--基本满意
              '5',
              '314',
              '4',
              '214',--不满意
              '3',
              '214',
              '2',
              '115',--非常不满意
              '1',
              '115') as evalDetail --好差评评价详情
  from tc_webjj.t_dobus s, tc_webjj.t_dobus_log g
 where s.sdono = g.sdono
   and s.sbusdotype in ('1', '2','3')
   and s.state in ('43','71')
   and s.ssatisfaction='满意'
   and g.soperationno = '4301001000000050'
/


```









```powershell
^.*prompt.*$  正则表达式查找包含指定字符串的行
```





```powershell

a123456HYC




docker import minio.tar registry.cn-beijing.aliyuncs.com/kubesphereio/minio:RELEASE.2019-08-07T01-59-21Z
```

### docker动态修改--restart参数

```powershell
--restart参数

no 不自动重启
on-failure:重启次数 指定自动重启的失败次数,到达失败次数后不再重启
always 自动重启
修改线上容器--restart参数值

docker update --restart=no [容器名] 
docker update --restart=always [容器名]
docker update --restart=on-failure:3 [容器名]
```







### KubeSphere安装

```powershell
#####################################################
###              Welcome to KubeSphere!           ###
#####################################################

Console: http://192.168.40.145:30880
Account: admin
Password: P@88w0rd

NOTES：
  1. After you log into the console, please check the
     monitoring status of service components in
     "Cluster Management". If any service is not
     ready, please wait patiently until all components 
     are up and running.
  2. Please change the default password after login.

#####################################################
https://kubesphere.io             2021-10-09 10:32:21
#####################################################

```

```powershell
# 安装环境需要的依赖
yum install conntrack-tools -y
yum install socat -y
 
export KKZONE=cn
curl -sfL https://get-kk.kubesphere.io | VERSION=v2.2.1 sh -

[root@localhost ~]# curl -sfL https://get-kk.kubesphere.io | VERSION=v2.2.1 sh -

Downloading kubekey v2.2.1 from https://kubernetes.pek3b.qingstor.com/kubekey/releases/download/v2.2.1/kubekey-v2.2.1-linux-amd64.tar.gz ...


Kubekey v2.2.1 Download Complete!

[root@localhost ~]# export KKZONE=cn
[root@localhost ~]# chmod +x kk
[root@localhost ~]# ll
总用量 70088
-rw-------. 1 root root       1327 7月  15 09:13 anaconda-ks.cfg
-rwxr-xr-x. 1 1001 docker 54767616 6月  23 14:05 kk
-rw-r--r--. 1 root root   16997207 7月  20 11:36 kubekey-v2.2.1-linux-amd64.tar.gz



./kk create cluster --with-kubernetes v1.22.10 --with-kubesphere v3.3.0

kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l 'app in (ks-install, ks-installer)' -o jsonpath='{.items[0].metadata.name}') -f



kubectl get pods --all-namespaces
kubectl get pod -n <namespace>
kubectl logs <pod_name> -n <namespace>

kubectl delete pod <pod_name> -n <namespace>

# 查询systemd日志
journalctl -xefu kubelet

kubectl get services
kubectl get deployments

kubectl cluster-info
kubectl get pods -ALL-namespace


kubectl describe deployment nginx-cc -n kube-public
kubectl describe pod nginx-cc-5d7d5c6b54 -n kube-public

kubectl describe deployment ks-installer -n kubesphere-system
kubectl describe pod ks-installer-c9655d997-npp9c -n kube-public


kubectl logs cattle-cluster-agent-5586fdfd64-phlkb -n cattle-system
kubectl logs cattle-cluster-agent-5586fdfd64-2d2lf -n cattle-system

kubectl describe pod ks-installer-c9655d997-npp9c -n kubesphere-system



##验证安装结果
kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l 'app in (ks-install, ks-installer)' -o jsonpath='{.items[0].metadata.name}') -f


```



```powershell
WARNING! The remote SSH server rejected X11 forwarding request.
Last login: Sat Oct  9 09:41:13 2021 from 192.168.40.1
[root@localhost ~]# export KKZONE=cn
[root@localhost ~]# ./kk create cluster --with-kubernetes v1.20.4 --with-kubesphere v3.1.1
+-----------------------+------+------+---------+----------+-------+-------+-----------+--------+------------+-------------+------------------+--------------+
| name                  | sudo | curl | openssl | ebtables | socat | ipset | conntrack | docker | nfs client | ceph client | glusterfs client | time         |
+-----------------------+------+------+---------+----------+-------+-------+-----------+--------+------------+-------------+------------------+--------------+
| localhost.localdomain | y    | y    | y       | y        |       | y     | y         |        |            |             |                  | CST 09:45:50 |
+-----------------------+------+------+---------+----------+-------+-------+-----------+--------+------------+-------------+------------------+--------------+

This is a simple check of your environment.
Before installation, you should ensure that your machines meet all requirements specified at
https://github.com/kubesphere/kubekey#requirements-and-recommendations

Continue this installation? [yes/no]: yes
INFO[09:45:53 CST] Downloading Installation Files               
INFO[09:45:53 CST] Downloading kubeadm ...                      
INFO[09:45:54 CST] Downloading kubelet ...                      
INFO[09:50:44 CST] Downloading kubectl ...                      
INFO[09:51:48 CST] Downloading helm ...                         
INFO[09:53:05 CST] Downloading kubecni ...                      
INFO[09:54:06 CST] Configuring operating system ...             
[localhost.localdomain 192.168.40.145] MSG:
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-arptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_local_reserved_ports = 30000-32767
vm.max_map_count = 262144
vm.swappiness = 1
fs.inotify.max_user_instances = 524288
no crontab for root
INFO[09:54:11 CST] Installing docker ...                        
INFO[09:57:59 CST] Start to download images on all nodes        
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/etcd:v3.4.13
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/pause:3.2
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/kube-apiserver:v1.20.4
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/kube-controller-manager:v1.20.4
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/kube-scheduler:v1.20.4
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/kube-proxy:v1.20.4
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/coredns:1.6.9
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/k8s-dns-node-cache:1.15.12
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/kube-controllers:v3.16.3
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/cni:v3.16.3
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/node:v3.16.3
[localhost.localdomain] Downloading image: registry.cn-beijing.aliyuncs.com/kubesphereio/pod2daemon-flexvol:v3.16.3
INFO[10:12:16 CST] Generating etcd certs                        
INFO[10:12:17 CST] Synchronizing etcd certs                     
INFO[10:12:17 CST] Creating etcd service                        
[localhost.localdomain 192.168.40.145] MSG:
etcd will be installed
INFO[10:12:19 CST] Starting etcd cluster                        
[localhost.localdomain 192.168.40.145] MSG:
Configuration file will be created
INFO[10:12:19 CST] Refreshing etcd configuration                
[localhost.localdomain 192.168.40.145] MSG:
Created symlink from /etc/systemd/system/multi-user.target.wants/etcd.service to /etc/systemd/system/etcd.service.
Waiting for etcd to start
INFO[10:12:26 CST] Backup etcd data regularly                   
INFO[10:12:32 CST] Get cluster status                           
[localhost.localdomain 192.168.40.145] MSG:
Cluster will be created.
INFO[10:12:33 CST] Installing kube binaries                     
Push /root/kubekey/v1.20.4/amd64/kubeadm to 192.168.40.145:/tmp/kubekey/kubeadm   Done
Push /root/kubekey/v1.20.4/amd64/kubelet to 192.168.40.145:/tmp/kubekey/kubelet   Done
Push /root/kubekey/v1.20.4/amd64/kubectl to 192.168.40.145:/tmp/kubekey/kubectl   Done
Push /root/kubekey/v1.20.4/amd64/helm to 192.168.40.145:/tmp/kubekey/helm   Done
Push /root/kubekey/v1.20.4/amd64/cni-plugins-linux-amd64-v0.8.6.tgz to 192.168.40.145:/tmp/kubekey/cni-plugins-linux-amd64-v0.8.6.tgz   Done
INFO[10:12:40 CST] Initializing kubernetes cluster              
[localhost.localdomain 192.168.40.145] MSG:
W1009 10:12:41.260711    7600 utils.go:69] The recommended value for "clusterDNS" in "KubeletConfiguration" is: [10.233.0.10]; the provided value is: [169.254.25.10]
[init] Using Kubernetes version: v1.20.4
[preflight] Running pre-flight checks
	[WARNING FileExisting-socat]: socat not found in system path
	[WARNING SystemVerification]: this Docker version is not on the list of validated versions: 20.10.9. Latest validated version: 19.03
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local lb.kubesphere.local localhost localhost.localdomain localhost.localdomain.cluster.local] and IPs [10.233.0.1 192.168.40.145 127.0.0.1]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] External etcd mode: Skipping etcd/ca certificate authority generation
[certs] External etcd mode: Skipping etcd/server certificate generation
[certs] External etcd mode: Skipping etcd/peer certificate generation
[certs] External etcd mode: Skipping etcd/healthcheck-client certificate generation
[certs] External etcd mode: Skipping apiserver-etcd-client certificate generation
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[kubelet-check] Initial timeout of 40s passed.
[apiclient] All control plane components are healthy after 52.504075 seconds
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.20" in namespace kube-system with the configuration for the kubelets in the cluster
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node localhost.localdomain as control-plane by adding the labels "node-role.kubernetes.io/master=''" and "node-role.kubernetes.io/control-plane='' (deprecated)"
[mark-control-plane] Marking the node localhost.localdomain as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[bootstrap-token] Using token: tjiakt.w0upk7grgrpq0kjs
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join lb.kubesphere.local:6443 --token tjiakt.w0upk7grgrpq0kjs \
    --discovery-token-ca-cert-hash sha256:7a2010788d8b9db30e494dadc4fad3b2f87231b8fa249761f448d4a689ce3566 \
    --control-plane 

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join lb.kubesphere.local:6443 --token tjiakt.w0upk7grgrpq0kjs \
    --discovery-token-ca-cert-hash sha256:7a2010788d8b9db30e494dadc4fad3b2f87231b8fa249761f448d4a689ce3566
[localhost.localdomain 192.168.40.145] MSG:
node/localhost.localdomain untainted
[localhost.localdomain 192.168.40.145] MSG:
node/localhost.localdomain labeled
[localhost.localdomain 192.168.40.145] MSG:
service "kube-dns" deleted
[localhost.localdomain 192.168.40.145] MSG:
service/coredns created
[localhost.localdomain 192.168.40.145] MSG:
serviceaccount/nodelocaldns created
daemonset.apps/nodelocaldns created
[localhost.localdomain 192.168.40.145] MSG:
configmap/nodelocaldns created
[localhost.localdomain 192.168.40.145] MSG:
I1009 10:14:11.483235    9980 version.go:254] remote version is much newer: v1.22.2; falling back to: stable-1.20
[upload-certs] Storing the certificates in Secret "kubeadm-certs" in the "kube-system" Namespace
[upload-certs] Using certificate key:
6a0a3083d065cdd8ab9a898daefabdb58dec20e1f9af9daf3e5c2b5ec8317f00
[localhost.localdomain 192.168.40.145] MSG:
secret/kubeadm-certs patched
[localhost.localdomain 192.168.40.145] MSG:
secret/kubeadm-certs patched
[localhost.localdomain 192.168.40.145] MSG:
secret/kubeadm-certs patched
[localhost.localdomain 192.168.40.145] MSG:
kubeadm join lb.kubesphere.local:6443 --token fq0cmd.rtiin15vvt6fsx93     --discovery-token-ca-cert-hash sha256:7a2010788d8b9db30e494dadc4fad3b2f87231b8fa249761f448d4a689ce3566
[localhost.localdomain 192.168.40.145] MSG:
localhost.localdomain   v1.20.4   [map[address:192.168.40.145 type:InternalIP] map[address:localhost.localdomain type:Hostname]]
INFO[10:14:18 CST] Joining nodes to cluster                     
INFO[10:14:18 CST] Deploying network plugin ...                 
[localhost.localdomain 192.168.40.145] MSG:
configmap/calico-config created
customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/kubecontrollersconfigurations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/networksets.crd.projectcalico.org created
clusterrole.rbac.authorization.k8s.io/calico-kube-controllers created
clusterrolebinding.rbac.authorization.k8s.io/calico-kube-controllers created
clusterrole.rbac.authorization.k8s.io/calico-node created
clusterrolebinding.rbac.authorization.k8s.io/calico-node created
daemonset.apps/calico-node created
serviceaccount/calico-node created
deployment.apps/calico-kube-controllers created
serviceaccount/calico-kube-controllers created
[localhost.localdomain 192.168.40.145] MSG:
storageclass.storage.k8s.io/local created
serviceaccount/openebs-maya-operator created
Warning: rbac.authorization.k8s.io/v1beta1 ClusterRole is deprecated in v1.17+, unavailable in v1.22+; use rbac.authorization.k8s.io/v1 ClusterRole
clusterrole.rbac.authorization.k8s.io/openebs-maya-operator created
Warning: rbac.authorization.k8s.io/v1beta1 ClusterRoleBinding is deprecated in v1.17+, unavailable in v1.22+; use rbac.authorization.k8s.io/v1 ClusterRoleBinding
clusterrolebinding.rbac.authorization.k8s.io/openebs-maya-operator created
deployment.apps/openebs-localpv-provisioner created
INFO[10:14:22 CST] Deploying KubeSphere ...                     
v3.1.1
[localhost.localdomain 192.168.40.145] MSG:
namespace/kubesphere-system created
namespace/kubesphere-monitoring-system created
[localhost.localdomain 192.168.40.145] MSG:
secret/kube-etcd-client-certs created
[localhost.localdomain 192.168.40.145] MSG:
namespace/kubesphere-system unchanged
serviceaccount/ks-installer unchanged
customresourcedefinition.apiextensions.k8s.io/clusterconfigurations.installer.kubesphere.io unchanged
clusterrole.rbac.authorization.k8s.io/ks-installer unchanged
clusterrolebinding.rbac.authorization.k8s.io/ks-installer unchanged
deployment.apps/ks-installer unchanged
clusterconfiguration.installer.kubesphere.io/ks-installer created
#####################################################
###              Welcome to KubeSphere!           ###
#####################################################

Console: http://192.168.40.145:30880
Account: admin
Password: P@88w0rd

NOTES：
  1. After you log into the console, please check the
     monitoring status of service components in
     "Cluster Management". If any service is not
     ready, please wait patiently until all components 
     are up and running.
  2. Please change the default password after login.

#####################################################
https://kubesphere.io             2021-10-09 10:32:21
#####################################################
INFO[10:32:24 CST] Installation is complete.

Please check the result using the command:

       kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l app=ks-install -o jsonpath='{.items[0].metadata.name}') -f
 
您在 /var/spool/mail/root 中有邮件

```





```
kubectl get po -A

kubectl describe po openebs-localpv-provisioner-6c9dcb5c54-j2ck2 -n kube-system

kubectl describe po ks-installer-57d7fd8859-s5646  -n kubesphere-system

kubectl taint nodes --all node-role.kubernetes.io/master-

kubectl taint nodes --all notification-manager-webhook



kubectl delete ks-controller-manager -n kubesphere-system

```





### kubesphere安装完成后下载的镜像

```powershell
[root@localhost amd64]# docker images
REPOSITORY                                                              TAG       IMAGE ID       CREATED         SIZE
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-proxy                v1.20.4   c29e6c583067   7 months ago    118MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-apiserver            v1.20.4   ae5eb22e4a9d   7 months ago    122MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-controller-manager   v1.20.4   0a41a1414c53   7 months ago    116MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-scheduler            v1.20.4   5f8cb769bd73   7 months ago    47.3MB
registry.cn-beijing.aliyuncs.com/kubesphereio/node                      v3.16.3   f0d3b0d0e32c   12 months ago   164MB
registry.cn-beijing.aliyuncs.com/kubesphereio/pod2daemon-flexvol        v3.16.3   a0b97353aa18   12 months ago   22.9MB
registry.cn-beijing.aliyuncs.com/kubesphereio/cni                       v3.16.3   fe49caa20c30   12 months ago   133MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-controllers          v3.16.3   75c8849ca840   12 months ago   52.9MB
registry.cn-beijing.aliyuncs.com/kubesphereio/etcd                      v3.4.13   d1985d404385   13 months ago   83.8MB
registry.cn-beijing.aliyuncs.com/kubesphereio/k8s-dns-node-cache        1.15.12   5340ba194ec9   18 months ago   107MB
registry.cn-beijing.aliyuncs.com/kubesphereio/coredns                   1.6.9     faac9e62c0d6   18 months ago   43.2MB
registry.cn-beijing.aliyuncs.com/kubesphereio/pause                     3.2       80d28bedfe5d   20 months ago   683kB
[root@localhost amd64]# 

```





```powershell
[root@localhost amd64]# docker images
REPOSITORY                                                                    TAG       IMAGE ID       CREATED         SIZE
registry.cn-beijing.aliyuncs.com/kubesphereio/ks-installer                    v3.1.1    92fc13a9e908   2 months ago    503MB
registry.cn-beijing.aliyuncs.com/kubesphereio/ks-controller-manager           v3.1.1    acc607b5666a   3 months ago    141MB
registry.cn-beijing.aliyuncs.com/kubesphereio/ks-apiserver                    v3.1.1    905d6f5ae7d4   3 months ago    158MB
registry.cn-beijing.aliyuncs.com/kubesphereio/ks-console                      v3.1.1    0d4a1e7d9fef   3 months ago    110MB
registry.cn-beijing.aliyuncs.com/kubesphereio/provisioner-localpv             2.10.1    b5e01d57c971   3 months ago    62.7MB
registry.cn-beijing.aliyuncs.com/kubesphereio/notification-manager            v1.0.0    1b82628bbeb5   3 months ago    47.4MB
registry.cn-beijing.aliyuncs.com/kubesphereio/notification-manager-operator   v1.0.0    4d5c553eebcb   3 months ago    44.3MB
registry.cn-beijing.aliyuncs.com/kubesphereio/linux-utils                     2.10.0    feeb9aca65e7   3 months ago    11.7MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kubectl                         v1.20.0   4f1615bb5315   5 months ago    77.2MB
registry.cn-beijing.aliyuncs.com/kubesphereio/prometheus                      v2.26.0   6d6859d1a42a   6 months ago    169MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-proxy                      v1.20.4   c29e6c583067   7 months ago    118MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-apiserver                  v1.20.4   ae5eb22e4a9d   7 months ago    122MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-controller-manager         v1.20.4   0a41a1414c53   7 months ago    116MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-scheduler                  v1.20.4   5f8cb769bd73   7 months ago    47.3MB
registry.cn-beijing.aliyuncs.com/kubesphereio/snapshot-controller             v3.0.3    05897620e3b0   9 months ago    43MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-rbac-proxy                 v0.8.0    ad393d6a4d1b   11 months ago   49MB
registry.cn-beijing.aliyuncs.com/kubesphereio/node                            v3.16.3   f0d3b0d0e32c   12 months ago   164MB
registry.cn-beijing.aliyuncs.com/kubesphereio/pod2daemon-flexvol              v3.16.3   a0b97353aa18   12 months ago   22.9MB
registry.cn-beijing.aliyuncs.com/kubesphereio/cni                             v3.16.3   fe49caa20c30   12 months ago   133MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-controllers                v3.16.3   75c8849ca840   12 months ago   52.9MB
registry.cn-beijing.aliyuncs.com/kubesphereio/prometheus-config-reloader      v0.42.1   1c513305d7c7   12 months ago   10.1MB
registry.cn-beijing.aliyuncs.com/kubesphereio/prometheus-operator             v0.42.1   072763fd75f3   12 months ago   40MB
registry.cn-beijing.aliyuncs.com/kubesphereio/etcd                            v3.4.13   d1985d404385   13 months ago   83.8MB
registry.cn-beijing.aliyuncs.com/kubesphereio/alertmanager                    v0.21.0   c876f5897d7b   15 months ago   55.5MB
registry.cn-beijing.aliyuncs.com/kubesphereio/kube-state-metrics              v1.9.7    6497f02dbdad   16 months ago   32.8MB
registry.cn-beijing.aliyuncs.com/kubesphereio/k8s-dns-node-cache              1.15.12   5340ba194ec9   18 months ago   107MB
registry.cn-beijing.aliyuncs.com/kubesphereio/coredns                         1.6.9     faac9e62c0d6   18 months ago   43.2MB
registry.cn-beijing.aliyuncs.com/kubesphereio/pause                           3.2       80d28bedfe5d   20 months ago   683kB
registry.cn-beijing.aliyuncs.com/kubesphereio/configmap-reload                v0.3.0    7ec24a279487   2 years ago     9.7MB
registry.cn-beijing.aliyuncs.com/kubesphereio/node-exporter                   v0.18.1   e5a616e4b9cf   2 years ago     22.9MB
registry.cn-beijing.aliyuncs.com/kubesphereio/defaultbackend-amd64            1.4       846921f0fe0e   3 years ago     4.84MB

```



### ipvs rr udp 10.133.0.3 53 no destination available

如果遇到这样的显示：“ipvs rr udp 10.133.0.3 53 no destination available” ，这个不是影响环境的因素，要消除这个显示，在机器上执行：`dmesg -n 1`就不会显示。







### docker下gitlab安装配置使用(完整版)

https://www.jianshu.com/p/080a962c35b6

```sh
1.gitlab镜像拉取

# gitlab-ce为稳定版本，后面不填写版本则默认pull最新latest版本
$ docker pull gitlab/gitlab-ce


2.运行gitlab镜像

mkdir -p /home/gitlab/config
mkdir -p /home/gitlab/logs
mkdir -p /home/gitlab/data
cd /home/gitlab/
chmod -R 777 config  logs data


$ docker run -d  -p 443:443 -p 80:80 -p 222:22 --name gitlab --restart=always -v /home/gitlab/config:/etc/gitlab -v /home/gitlab/logs:/var/log/gitlab -v /home/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce
# -d：后台运行
# -p：将容器内部端口向外映射
# --name：命名容器名称
# -v：将容器内数据文件夹或者日志、配置等文件夹挂载到宿主机指定目录

docker run -d -p 443:443 -p 80:80 -p 222:22 --name gitlab --restart=always -v /home/gitlab/config:/etc/gitlab -v /home/gitlab/logs:/var/log/gitlab -v /home/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce



3.配置
按上面的方式，gitlab容器运行没问题，但在gitlab上创建项目的时候，生成项目的URL访问地址是按容器的hostname来生成的，也就是容器的id。作为gitlab服务器，我们需要一个固定的URL访问地址，于是需要配置gitlab.rb（宿主机路径：/home/gitlab/config/gitlab.rb）。

# gitlab.rb文件内容默认全是注释
$ vim /home/gitlab/config/gitlab.rb


# 配置http协议所使用的访问地址,不加端口号默认为80
external_url 'http://192.168.199.231'

# 配置ssh协议所使用的访问地址和端口
gitlab_rails['gitlab_ssh_host'] = '192.168.199.231'
gitlab_rails['gitlab_shell_ssh_port'] = 222 # 此端口是run时22端口映射的222端口
:wq #保存配置文件并退出

gitlab 修改初始密码



```

#### gitlab 修改初始密码

```powershell
docker exec -it 4f518602129e /bin/bash   // 01fb1dd86bb5是容器id
 
gitlab-rails console -e production   // 耐心等待
 
 
 user = User.where(id:1).first
# user = User.find_by(email: '284511796@qq.com')  // 记得换成自己住的的邮箱
# 刚安装没有上述密码
 
 #注意  这两个选项都得设置，  pass  为你要设置的密码
user.password ='12345678'
user.password_confirmation = '12345678'  // 确认密码

 
user.save!  // 保存修改
 
exit   // 最后依次输入退出容器

========================================================
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                   PORTS                                                           NAMES
4f518602129e        gitlab/gitlab-ce    "/assets/wrapper"   41 minutes ago      Up 7 minutes (healthy)   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 0.0.0.0:222->22/tcp   gitlab
[root@localhost ~]# docker exec -it 4f518602129e /bin/bash
root@4f518602129e:/# gitlab-rails console -e production
--------------------------------------------------------------------------------
 Ruby:         ruby 2.7.4p191 (2021-07-07 revision a21a3b7d23) [x86_64-linux]
 GitLab:       14.3.0 (ceec8accb09) FOSS
 GitLab Shell: 13.21.0
 PostgreSQL:   12.7
--------------------------------------------------------------------------------

Loading production environment (Rails 6.1.3.2)
irb(main):001:0> 
irb(main):002:0>  user = User.where(id:1).first
=> #<User id:1 @root>
irb(main):003:0> user.password ='12345678'
=> "12345678"
irb(main):004:0> user.password_confirmation = '12345678'
=> "12345678"
irb(main):005:0> user.save!
Enqueued ActionMailer::MailDeliveryJob (Job ID: abb8abb6-fb8e-4d06-a1f9-c93451de1276) to Sidekiq(mailers) with arguments: "DeviseMailer", "password_change", "deliver_now", {:args=>[#<GlobalID:0x00007fd6ecc11978 @uri=#<URI::GID gid://gitlab/User/1>>]}
=> true
irb(main):006:0> exit
root@4f518602129e:/# 




```

### [Docker 下安装gitlab 密码 重置](https://www.cnblogs.com/Cryan/p/15060207.html) 





### Oracle 插入数据添加 COMMIT 提交

**问题场景**: 往Oracle表中插入数据,仅在当前执行插入SQL的窗口可以查到数据,切换窗口后查不到最新插入的数据
**产生原因**: Oracle的自动提交默认是关闭的
**错误写法**: 这样的SQL, 在其他窗口或者关闭该窗口后查询不到新增的数据; 该数据仅在内存中, 并未提交

```sql
insert into table_a (a,b,c)
values ('xxx','xxxxxx','xxxxxxxx');
```

**解决方式**: 手动 COMMIT提交

```sql
insert into table_a (a,b,c)
values ('xxx','xxxxxx','xxxxxxxx');
commit;
```













### ORA-00054: 资源正忙 --锁表的解决方法

https://blog.csdn.net/wlf2601567/article/details/82623705

```mysql
-- 查询锁表并处理
select session_id from v$locked_object;
-- 1301 1421
SELECT sid, serial#, username, osuser FROM v$session where sid =1301;
-- 265
SELECT sid, serial#, username, osuser FROM v$session where sid =1421;
-- 5
ALTER SYSTEM KILL SESSION '1301,265';
ALTER SYSTEM KILL SESSION '1421,5';
```











```sql
SELECT column_name(s)
FROM table_name1
LEFT JOIN table_name2 
ON table_name1.column_name=table_name2.column_name
```

LEFT JOIN 关键字会从左表 (table_name1) 那里返回所有的行，即使在右表 (table_name2) 中没有匹配的行。

[SQL LEFT JOIN 关键字 (w3school.com.cn)](https://www.w3school.com.cn/sql/sql_join_left.asp)









### springboot 项目mybatis plus 设置 jdbcTypeForNull （oracle数据库需配置JdbcType.NULL, 默认是Other）...

方法1：
application.yml

```yaml
mybatis-plus:
  configuration:
    jdbc-type-for-null: 'null' #注意：单引号 
```

[(38条消息) springboot 项目mybatis plus 设置 jdbcTypeForNull （oracle数据库需配置JdbcType.NULL, 默认是Other）..._weixin_33964094的博客-CSDN博客](https://blog.csdn.net/weixin_33964094/article/details/85989528)



# 

java中有三种移位运算符

<<    :   左移运算符，num << 1,相当于num乘以2

\>>    :   右移运算符，num >> 1,相当于num除以2

\>>>   :   无符号右移，忽略符号位，空位都以0补齐

```java
@Test
void test12() {
    int num = 16;
    //右移几接除以几个2
    int i = num >> 2;
    //左移几接乘以几个2
    int j = num << 4;
    System.out.println(i);//4
    System.out.println(j);//256
}
```

## mybatis



### [Mybatis Map保存到数据库，Mybatis Map动态同步表，Mybatis Map Foreach插入数据库](https://www.cnblogs.com/fanshuyao/p/14343498.html)



# mybatis choose when 多条件_总结了 10 种 Mybatis 经常写错且必会的知识点



用来循环容器的标签forEach,查看例子
foreach元素的属性主要有item，index，collection，open，separator，close。

item：集合中元素迭代时的别名，

index：集合中元素迭代时的索引

open：常用语where语句中，表示以什么开始，比如以'('开始

separator：表示在每次进行迭代时的分隔符，

close 常用语where语句中，表示以什么结束，

在使用foreach的时候最关键的也是最容易出错的就是collection属性，该属性是必须指定的，但是在不同情况下，该属性的值是不一样的，主要有一下3种情况：

如果传入的是单参数且参数类型是一个List的时候，collection属性值为list .

如果传入的是单参数且参数类型是一个array数组的时候，collection的属性值为array .

如果传入的参数是多个的时候，我们就需要把它们封装成一个Map了，当然单参数也可以封装成map，实际上如果你在传入参数的时候，在MyBatis里面也是会把它封装成一个Map的，map的key就是参数名，所以这个时候collection属性值就是传入的List或array对象在自己封装的map里面的key.

针对最后一条，我们来看一下官方说法：


`注意 你可以将一个 List 实例或者数组作为参数对象传给 MyBatis，当你这么做的时候，MyBatis 会自动将它包装在一个 Map 中并以名称为键。List 实例将会以“list”作为键，而数组实例的键将是“array”。`

所以，不管是多参数还是单参数的list,array类型，都可以封装为map进行传递。如果传递的是一个List，则mybatis会封装为一个list为key，list值为object的map，如果是array，则封装成一个array为key，array的值为object的map，如果自己封装呢，则colloection里放的是自己封装的map里的key值

```xml
//mapper中我们要为这个方法传递的是一个容器,将容器中的元素一个一个的
//拼接到xml的方法中就要使用这个forEach这个标签了
public List queryById(List userids);

//对应的xml中如下
<select id="queryById" resultType="BaseReslutMap">
    select *
    FROM entity
    where id in
    <foreach collection="userids" item="userid" index="index" open="(" separator="," close=")">
        #{userid}
    </foreach>
</select>

```

### **concat模糊查询**

```xml
	//比如说我们想要进行条件查询,但是几个条件不是每次都要使用,那么我们就可以
	//通过判断是否拼接到sql中
<select id="queryById" resultType="BascResultMap" parameterType="entity">
    select *
    from entity
    <where>
        <if test="name!=null and name!=''">
            name like concat('%',concat(#{name},'%'))
        </if>
    </where>
</select>
```

**choose (when, otherwise)标签**
choose标签是按顺序判断其内部when标签中的test条件出否成立，如果有一个成立，则 choose 结束。当 choose 中所有 when 的条件都不满则时，则执行 otherwise 中的sql。类似于Java 的 switch 语句，choose 为 switch，when 为 case，otherwise 则为 default。

例如下面例子，同样把所有可以限制的条件都写上，方面使用。choose会从上到下选择一个when标签的test为true的sql执行。安全考虑，我们使用where将choose包起来，放置关键字多于错误。

```xml
<select id="getUserList_choose" resultMap="resultMap_user" parameterType="com.yiibai.pojo.User">
    select *
    from User u 
    <where>
        <choose>
            <when test="username !=null and username !=''">
                and u.username LIKE CONCAT(CONCAT('%', #{username, jdbcType=VARCHAR}),'%')  
            </when>
            <when test='sex=="男"'>
                and u.sex = #{sex, jdbcType=INTEGER}
            </when>
            <when test="birthday != null and ">
                and u.birthday = #{birthday, jdbcType=DATE}
            </when>
        </choose>
    </where>
</select>
```

### **selectKey 标签**

在insert语句中，在Oracle经常使用序列、在MySQL中使用函数来自动生成插入表的主键，而且需要方法能返回这个生成主键。使用myBatis的selectKey标签可以实现这个效果。

下面例子，使用mysql数据库自定义函数nextval('student')，用来生成一个key，并把他设置到传入的实体类中的studentId属性上。所以在执行完此方法后，边可以通过这个实体类获取生成的key。

```xml
<insert id="createStudentAutoKey" parameterType="liming.student.manager.data.model.StudentEntity"
			keyProperty="studentId">  
		    <selectKey keyProperty="studentId" resultType="String" order="BEFORE">  
			        select nextval('student')
			    </selectKey>  
		    INSERT INTO STUDENT_TBL(STUDENT_ID,  
		                            STUDENT_NAME,  
		                            STUDENT_SEX,  
		                            STUDENT_BIRTHDAY,  
		                            STUDENT_PHOTO,  
		                            CLASS_ID,  
		                            PLACE_ID)  
		    VALUES (#{studentId},  
		            #{studentName},  
		            #{studentSex},  
		            #{studentBirthday},  
		            #{studentPhoto, javaType=byte[], jdbcType=BLOB, typeHandler=org.apache.ibatis.type.BlobTypeHandler},  
		            #{classId},  
		            #{placeId})  
	</insert>
```



调用接口方法，和获取自动生成key

```java
StudentEntity entity = new StudentEntity();  
entity.setStudentName("黎明你好");  
entity.setStudentSex(1);  
entity.setStudentBirthday(DateUtil.parse("1985-05-28"));  
entity.setClassId("20000001");  
entity.setPlaceId("70000001");  
this.dynamicSqlMapper.createStudentAutoKey(entity);  
System.out.println("新增学生ID: " + entity.getStudentId());  

```



### **if标签**

if标签可用在许多类型的sql语句中，我们以查询为例。首先看一个很普通的查询：

```xml
<select id="getStudentListLikeName" parameterType="StudentEntity" resultMap="studentResultMap">
    select * from STUDENT_TBL ST   
    where ST.STUDENT_NAME 
    like CONCAT('%', #{studentName}, '%')
</select>
```

但是此时如果studentName为null，此语句很可能报错或查询结果为空。此时我们使用if动态sql语句先进行判断，如果值为null或等于空字符串，我们就不进行此条件的判断，增加灵活性。

参数为实体类StudentEntity。将实体类中所有的属性均进行判断，如果不为空则执行判断条件。

```

```








版权声明：本文为CSDN博主「weixin_39637370」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/weixin_39637370/article/details/111275640



### MyBatis choose(when, otherwise)标签

choose (when, otherwise)标签
有时候我们并不想应用所有的条件，而只是想从多个选项中选择一个。而使用if标签时，只要test中的表达式为 true，就会执行 if 标签中的条件。MyBatis 提供了 choose 元素。if标签是与(and)的关系，而 choose 是或(or)的关系。

choose标签是按顺序判断其内部when标签中的test条件出否成立，如果有一个成立，则 choose 结束。当 choose 中所有 when 的条件都不满则时，则执行 otherwise 中的sql。类似于Java 的 switch 语句，choose 为 switch，when 为 case，otherwise 则为 default。

例如下面例子，同样把所有可以限制的条件都写上，方面使用。choose会从上到下选择一个when标签的test为true的sql执行。安全考虑，我们使用where将choose包起来，放置关键字多于错误。

```xml
<!--  choose(判断参数) - 按顺序将实体类第一个不为空的属性作为：where条件 -->  
<choose>
    <when test="entity.city1 != null and entity.city1  != '' and entity.city2 != null and entity.city2  != '' and entity.city3 != null and entity.city3  != ''  ">
        AND (FIND_IN_SET(#{entity.city1},t3.`city`)  OR FIND_IN_SET(#{entity.city2},t3.`city`) OR FIND_IN_SET(#{entity.city3},t3.`city`) )
    </when>
    <when test="entity.city1 != null and entity.city1  != '' and entity.city2 != null and entity.city2  != '' ">
        AND (FIND_IN_SET(#{entity.city1},t3.`city`) OR FIND_IN_SET(#{entity.city2},t3.`city`)
    </when>
    <when test="entity.city1 != null and entity.city1  != '' ">
        AND FIND_IN_SET(#{entity.city1},t3.`city`)
    </when>
</choose>

<!--when & otherwise-->
<select id="dynamicChooseTest" parameterType="Blog" resultType="Blog">
    select * from t_blog where 1 = 1 
    <choose>
        <when test="title != null">
            and title = #{title}
        </when>
        <when test="content != null">
            and content = #{content}
        </when>
        <otherwise>
            and owner = "owner1"
        </otherwise>
    </choose>
```

`when元素表示当 when 中的条件满足的时候就输出其中的内容，跟 JAVA 中的 switch 效果差不多的是按照条件的顺序，当 when 中有条件满足的时候，就会跳出 choose，即所有的 when 和 otherwise 条件中，只有一个会输出，当所有的我很条件都不满足的时候就输出 otherwise 中的内容。所以上述语句的意思非常简单， 当 title!=null 的时候就输出 and titlte = #{title}，不再往下判断条件，当title为空且 content!=null 的时候就输出 and content = #{content}，当所有条件都不满足的时候就输出 otherwise 中的内容。`



————————————————
版权声明：本文为CSDN博主「码农张晓」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/qq_35661734/article/details/52608234



### xshell和xftp下载

xshell下载地址：
链接：https://pan.baidu.com/s/1uQxGxG31PxSf2-coZnzCTA
提取码：6jx2

xftp下载地址：
链接：https://pan.baidu.com/s/1vUcPX83ULf1k5zSktFQ0JA
提取码：lqlw






### 农行对接认证接口

https://openbank.abchina.com/Portal/index/index.html























 日期格式定义

![image-20210619175302627](images/image-20210619175302627.png)

日期时间格式定义

![image-20210619175329723](images/image-20210619175329723.png)





### OA系统登录

http://192.168.16.233:9089/index/index.jsp

hyc  a

公司名称：福建天创信息科技有限公司
统一社会信用代码：913501002601951900

## 在 centos 中安装 jdk8

```powershell

将JDK导入到opt目录下面的software文件夹

[root@hadoop101 software]# ll
总用量 531948
-rw-r--r--. 1 root root 185515842 2月  11 12:30 jdk-8u144-linux-x64.tar.gz

[root@hadoop101 software]# tar -zxvf jdk-8u144-linux-x64.tar.gz -C /opt/module/
[root@hadoop101 jdk1.8.0_144]# pwd
/opt/module/jdk1.8.0_144
[root@hadoop101 jdk1.8.0_144]# vi /etc/profile


#JAVA_HOME
export JAVA_HOME=/opt/module/jdk1.8.0_144
export PATH=$PATH:$JAVA_HOME/bin
 
[root@hadoop101 jdk1.8.0_144]# source /etc/profile
[root@hadoop101 jdk1.8.0_144]# java
[root@hadoop101 jdk1.8.0_144]# java -version
```







### java 抛出异常

```java
            throw new RuntimeException("身份证已经注册过");
```

### 

113.140.74.222:3006  root Xian$201748.com

# RabbitMQ：@RabbitListener 与 @RabbitHandler 及 消息序列化

[![img](images/7f1fa14b-ae38-4637-8ede-77a3e379720d.webp)](https://www.jianshu.com/u/50d83346eaff)

[林塬](https://www.jianshu.com/u/50d83346eaff)关注

22018.02.05 10:50:39字数 865阅读 54,222

- 添加 @RabbitListener 注解来指定某方法作为消息消费的方法，例如监听某 Queue 里面的消息

## MessageConvert

- 涉及网络传输的应用序列化不可避免，发送端以某种规则将消息转成 byte 数组进行发送，接收端则以约定的规则进行 byte[] 数组的解析
- RabbitMQ 的序列化是指 Message 的 body 属性，即我们真正需要传输的内容，**RabbitMQ 抽象出一个 MessageConvert 接口处理消息的序列化**，其实现有 SimpleMessageConverter（默认）、Jackson2JsonMessageConverter 等
- 当调用了 convertAndSend 方法时会使用 MessageConvert 进行消息的序列化
- **SimpleMessageConverter 对于要发送的消息体 body 为 byte[] 时不进行处理，如果是 String 则转成字节数组,如果是 Java 对象，则使用 jdk 序列化将消息转成字节数组，转出来的结果较大，含class类名，类相应方法等信息。因此性能较差**
- 当使用 RabbitMQ 作为中间件时，数据量比较大，此时就要考虑使用类似 Jackson2JsonMessageConverter 等序列化形式以此提高性能

## @RabbitListener 用法

- 使用 @RabbitListener 注解标记方法，当监听到队列 debug 中有消息时则会进行接收并处理



```csharp
@RabbitListener(queues = "debug")
public void processMessage1(Message bytes) {
    System.out.println(new String(bytes));
}
```

### 注意

- 消息处理方法参数是由 MessageConverter 转化，若使用自定义 MessageConverter 则需要在 RabbitListenerContainerFactory 实例中去设置（默认 Spring 使用的实现是 SimpleRabbitListenerContainerFactory）
- 消息的 content_type 属性表示消息 body 数据以什么数据格式存储，接收消息除了使用 Message 对象接收消息（包含消息属性等信息）之外，还可直接使用对应类型接收消息 body 内容，但若方法参数类型不正确会抛异常：
  - **application/octet-stream**：二进制字节数组存储，使用 byte[]
  - **application/x-java-serialized-object**：java 对象序列化格式存储，使用 Object、相应类型（反序列化时类型应该同包同名，否者会抛出找不到类异常）
  - **text/plain**：文本数据类型存储，使用 String
  - **application/json**：JSON 格式，使用 Object、相应类型

![img](images/9434708-77b9d3222ae376cd.webp)

ZiVg2.png

## @Payload 与 @Headers

- 使用 @Payload 和 @Headers 注解可以消息中的 body 与 headers 信息



```dart
@RabbitListener(queues = "debug")
public void processMessage1(@Payload String body, @Headers Map<String,Object> headers) {
    System.out.println("body："+body);
    System.out.println("Headers："+headers);
}
```

- 也可以获取单个 Header 属性



```kotlin
@RabbitListener(queues = "debug")
public void processMessage1(@Payload String body, @Header String token) {
    System.out.println("body："+body);
    System.out.println("token："+token);
}
```

### 通过 @RabbitListener 注解声明 Binding

- 通过 @RabbitListener 的 bindings 属性声明 Binding（若 RabbitMQ 中不存在该绑定所需要的 Queue、Exchange、RouteKey 则自动创建，若存在则抛出异常）



```csharp
@RabbitListener(bindings = @QueueBinding(
        exchange = @Exchange(value = "topic.exchange",durable = "true",type = "topic"),
        value = @Queue(value = "consumer_queue",durable = "true"),
        key = "key.#"
))
public void processMessage1(Message message) {
    System.out.println(message);
}
```

## @RabbitListener 和 @RabbitHandler 搭配使用

- @RabbitListener 可以标注在类上面，需配合 @RabbitHandler 注解一起使用
- @RabbitListener 标注在类上面表示当有收到消息的时候，就交给 @RabbitHandler 的方法处理，具体使用哪个方法处理，根据 MessageConverter 转换后的参数类型



```java
@Component
@RabbitListener(queues = "consumer_queue")
public class Receiver {

    @RabbitHandler
    public void processMessage1(String message) {
        System.out.println(message);
    }

    @RabbitHandler
    public void processMessage2(byte[] message) {
        System.out.println(new String(message));
    }
    
}
```

## Message 内容对象序列化与反序列化

### 使用 Java 序列化与反序列化

- 默认的 SimpleMessageConverter 在发送消息时会将对象序列化成字节数组，若要反序列化对象，需要自定义 MessageConverter



```java
@Configuration
public class RabbitMQConfig {

    @Bean
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new MessageConverter() {
            @Override
            public Message toMessage(Object object, MessageProperties messageProperties) throws MessageConversionException {
                return null;
            }

            @Override
            public Object fromMessage(Message message) throws MessageConversionException {
                try(ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(message.getBody()))){
                    return (User)ois.readObject();
                }catch (Exception e){
                    e.printStackTrace();
                    return null;
                }
            }
        });

        return factory;
    }

}
```



```java
@Component
@RabbitListener(queues = "consumer_queue")
public class Receiver {

    @RabbitHandler
    public void processMessage1(User user) {
        System.out.println(user.getName());
    }

}
```

### 使用 JSON 序列化与反序列化

- RabbitMQ 提供了 Jackson2JsonMessageConverter 来支持消息内容 JSON 序列化与反序列化
- 消息发送者在发送消息时应设置 MessageConverter 为 Jackson2JsonMessageConverter



```cpp
rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
```

- 发送消息



```cpp
User user = new User("linyuan");
rabbitTemplate.convertAndSend("topic.exchange","key.1",user);
```

- 消息消费者也应配置 MessageConverter 为 Jackson2JsonMessageConverter



```dart
@Configuration
public class RabbitMQConfig {
    
    @Bean
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        return factory;
    }

}
```

- 消费消息



```kotlin
@Component
@RabbitListener(queues = "consumer_queue")
public class Receiver {

    @RabbitHandler
    public void processMessage1(@Payload User user) {
        System.out.println(user.getName());
    }

}
```

- **注意**：被序列化对象应提供一个无参的构造函数，否则会抛出异常

# [Java对Redis的批量操作 - RedisTemplate](https://blog.csdn.net/qq_36189144/article/details/88350310?utm_term=multiget%E5%91%BD%E4%BB%A4redis&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-1-88350310&spm=3001.4430)

一、背景

需求：一次性获取redis缓存中多个key的value
潜在隐患：循环key，获取value，可能会造成连接池的连接数增多，连接的创建和摧毁，消耗性能
解决方法：根据项目中的缓存数据结构的实际情况，数据结构为string类型的，使用RedisTemplate的multiGet方法；数据结构为hash，使用Pipeline(管道)，组合命令，批量操作redis。
二、操作

RedisTemplate的multiGet的操作

针对数据结构为String类型

示例代码

```java
List<String> keys = new ArrayList<>();
for (Book e : booklist) {
   String key = generateKey.getKey(e);
   keys.add(key);
}
List<Serializable> resultStr = template.opsForValue().multiGet(keys);
```


此方法还是比较好用，使用者注意封装。

在实际代码中我是用的是以下代码。获取多个key的value值：

 ```java
@Autowired
    StringRedisTemplate stringRedisTemplate;

public List<String> findKeysForPage(List<String> keys) {
        List<String> resultStr =  stringRedisTemplate.opsForValue().multiGet(keys);
        return resultStr;
    }
 ```




RedisTemplate的Pipeline使用

1）方式一 ： 基础方式

使用类：StringRedisTemplate

使用方法

public executePipelined(RedisCallback<?> action) {...}
示例代码：批量获取value

```java
List<Object> redisResult = redisTemplate.executePipelined(new RedisCallback<String>() {
   @Override
    public String doInRedis(RedisConnection redisConnection) throws DataAccessException {  
        for (BooK e : booklist) {
       StringRedisConnection stringRedisConnection =(StringRedisConnection)redisConnection;
        stringRedisConnection.get(e.getId());
        }
       return null;
    }
});
```

方法二 : 使用自定义序列化方法
使用类：RedisTemplate

使用方法

```java
public List<Object> executePipelined(final RedisCallback<?> action, final RedisSerializer<?> resultSerializer) {...}
```


示例代码：批量获取hash数据结构value

```java
List<Object> redisResult = redisTemplate.executePipelined(
  new RedisCallback<String>() {
    // 自定义序列化
    RedisSerializer keyS = redisTemplate.getKeySerializer();
    @Override
    public String doInRedis(RedisConnection redisConnection) throws DataAccessException {
        for (BooK e : booklist) {
              redisConnection.hGet(keyS.serialize(e.getName()), keyS.serialize(e.getAuthor()));
        }
        return null;
    }
  }, redisTemplate.getValueSerializer()); // 自定义序列化
```

三、说明
本文简单的举了关于RedisTemplate的两个例子，但大家千万别以为只是批量取值的时候会用到，PipeLine其实是用来批量发送命令操作Redis。后来用Jedis也进行了实现。
————————————————
版权声明：本文为CSDN博主「l_瓶中精灵」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/qq_36189144/article/details/88350310











# [Request对象中getHeader、requestHeaders、request.getHeaderNames三者区别](https://blog.csdn.net/handsome_boy_wsq/article/details/82822186)



```java
package TwoDay.request;
 
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*
 * 获取头信息和请求数据
 */
public class RequestDemo2 extends HttpServlet {
 
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/**
		 * 获取头信息
		 */
		String headValue = request.getHeader("Accept-Encoding");//获取单个请求头name对应的value值
		System.out.println(headValue);
		
		System.out.println("--------------");
		
		Enumeration e =  request.getHeaders("Accept-Encoding");//获取多个同名请求头对应的一组value值，因此返回枚举类型数据
		/**
		 * 将数据遍历出来
		 */
		while(e.hasMoreElements()){
		//遍历枚举中存储的每一个元素
		String value = (String)e.nextElement();
		System.out.println(value);//将值输出
		}
		
		System.out.println("----------------");
		/**
		 * 获取所有请求头信息
		 */
		Enumeration er = request.getHeaderNames();//获取请求头的所有name值
		while(er.hasMoreElements()){
		String name	=(String) er.nextElement();
		String value = request.getHeader(name);
		System.out.println(name+"="+value);
		}
	}
 
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
 
}
```



[Java 对第三方提供通用接口设计](https://blog.csdn.net/ssyujay/article/details/104615283/)





# 开放API接口或URL链接给第三方使用

使用场景
场景1
应用做到一定程度后，会有一些第三方用户或机构要对接部分的功能进入他们自己的应用。
比如：要通过URL的方式提供一个含有加密视频文件的H5页面给第三方使用。
实现思路：

1. 后台管理系统给不同的第三方开权限，分别提供AppID和AppSecret
2. 服务端提供一个API接口(如：getAccessToken)，入参为：AppID和AppSecret，返回值为：token（token要设置有效期，可以短一点）
3. 使用URL时带上token这个参数
4. 服务端校验token成功后开放权限
PS：也可以用AppID（应用的唯一标识）、AppKey（公匙）、AppSecret（私匙）三个参数来实现

场景2
类似微信支付、微信公众号的第三方接入应用。
实现思路：
参考OAuth 2.0的设计

什么是OAuth 2.0
什么是OAuth ？
OAuth（开放授权）是一个开放标准，允许用户让第三方应用访问该用户在某一网站上存储的私密的资源（如照片，视频），而无需将用户名和密码提供给第三方应用。它是提供一个令牌来访问他们存放在特定服务提供者的数据，每一个令牌授权一个特定的网站在特定的时段内访问特定的资源。
什么是OAuth 2.0 ？
OAuth 2.0是OAuth协议的下一版本，但不向后兼容OAuth。但OAuth 2.0保留了与OAuth相同的整体架构 。
OAuth 2.0的优点：

支持的应用更广，支持pc、web、app等
有刷新令牌（OAuth的访问令牌失效后要重新授权获取，OAuth 2.0直接刷新获取新的访问令牌）
真正分离了服务器和用户
OAuth 2.0的运行流程如下图，摘自RFC 6749：

![在这里插入图片描述](images/20191209183939480.png)

————————————————
版权声明：本文为CSDN博主「joinclear」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/joinclear/article/details/103461184



# [开放API接口签名验证，让你的接口从此不再裸奔](https://blog.csdn.net/qq_18495465/article/details/79248608?utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control&dist_request_id=1330144.33054.16181947615915717&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control)

上文中的案例开源代码： https://github.com/Joker-Coder/awesome-pay.git







# [API接口签名生成算法和签名验证算法](https://blog.csdn.net/qq_33721382/article/details/85856093?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-2.control&dist_request_id=1330147.30969.16181951207665091&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-2.control)





# [RESTful登录(基于token鉴权)的设计实例](https://blog.csdn.net/pony_maggie/article/details/69401500?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-3.control&dist_request_id=&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-3.control)







# [Spring Boot接口如何设计防篡改、防重放攻击](https://www.cnblogs.com/tqlin/p/11251321.html)





# [SpringBoot之HandlerInterceptorAdapter](https://www.cnblogs.com/weianlai/p/11358768.html)



















http://www.nat123.com/Pages_2_32.jsp

fzdhyctl

fzdhyctl



# [经典权限系统设计（五张表）](https://my.oschina.net/rightemperor/blog/775759)

大致用到５张表：用户表（UserInfo）、角色表（RoleInfo）、菜单表（MenuInfo）、用户角色表（UserRole）、角色菜单表（RoleMenu）。

　　各表的大体表结构如下：

　　１、用户表（UserInfo）：Id、UserName、UserPwd

　　２、角色表（RoleInfo）：Id、RoleName

　　３、菜单表（MenuInfo）：Id、MenuName

　　４、用户角色表（UserRole）：Id、UserId、RoleId

　　５、角色菜单表（RoleMenu）：Id、RoleId、MenuId

 

　　最关键的地方是，某个用户登录时，如何查找该用户的菜单权限？其实一条语句即可搞定：

　　假如用户的用户名为zhangsan，则他的菜单权限查询如下：

　　

```mysql
Select m.Id,m.MenuName from MenuInfo m ,UserInfo u UserRole ur, RoleMenu rm Where m.Id = rm.MenuId and ur.RoleId = rm.RoleId and ur.UserId = u.Id and u.UserName = 'zhangsan'
```





# [RBAC（基于角色的访问控制权限的基本模型）](https://blog.csdn.net/weixin_41174072/article/details/83387223)





# 如何在项目中导入layui

如何使用layui
下载layui：[Layui官网](https://www.layui.com/)

使用Layui只需要在项目中引用就可以了

![在这里插入图片描述](images/20200409223855943.png)

在项目中导入你在官网下载的文件夹，然后在配置文件中 引用 就可以的
头部添加 一个 < link >< /link >标签引入layui.css 结束时添加一个 < script > < /script >标签 引入layui.js 标签具体内容如下：

```html

<head>
	<link rel="stylesheet" href="layui/css/layui.css" />
</head>
<body>
	<script type="text/javascript" src="layui/layui.js" ></script>
</body>
```











spring.http.multipart.max-file-size=1000Mb

spring.http.multipart.max-request-size=1000Mb

```powershell
[root@localhost /]# mkdir tccloud
mkdir -p /tccloud/mysql/conf
mkdir -p /tccloud/mysql/logs
mkdir -p /tccloud/mysql/data



touch /tccloud/mysql/conf/my.cnf

vim /tccloud/mysql/conf/my.cnf
[mysqld]
user=mysql
character-set-server=utf8
default_authentication_plugin=mysql_native_password
secure_file_priv=/var/lib/mysql
expire_logs_days=7
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
max_connections=1000
 
[client]
default-character-set=utf8
 
[mysql]
default-character-set=utf8

[mysqld]
secure_file_priv=/var/lib/mysql





docker run -p 3346:3306 --name mysqltcclod -v /tccloud/mysql/logs:/var/log/mysql -v /tccloud/mysql/data:/var/lib/mysql -v /tccloud/mysql/conf:/etc/mysql -e MYSQL_ROOT_PASSWORD=123456 -d b15310a99490


 docker update mysqltcclod --restart=always
 
 docker exec -it mysqltcclod bash 
 mysql -u root -p  

```



留言系统后台

http://10.5.55.40:9083/new_lygl/index.jsp#



https://github.com/pig-mesh 



# JqGrid之传参查询



带条件的查询：

```javascript
$("#search_btn").click(function(){  
    //此处可以添加对查询数据的合法验证  
    var orderId = $("#orderId").val();  
    $("#list4").jqGrid('setGridParam',{  
        datatype:'json',  
        postData:{'orderId':orderId},
        page:1  
    }).trigger("reloadGrid");
}); 
```

代码解析
① setGridParam用于设置jqGrid的options选项。返回jqGrid对象
② datatype为指定发送数据的格式；
③ postData为发送请求的数据，以key:value的形式发送，多个参数可以以逗号”,”间隔；
④ page为指定查询结果跳转到第一页；
⑤ trigger(“reloadGrid”);为重新载入jqGrid表格。









 





## [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#幂等概述) 幂等概述

- 幂等性原本是数学上的概念，即使公式：f(x)=f(f(x)) 能够成立的数学性质。用在编程领域，则意为对同一个系统，使用同样的条件，一次请求和重复的多次请求对系统资源的影响是一致的。
- 幂等性是分布式系统设计中十分重要的概念，具有这一性质的接口在设计时总是秉持这样的一种理念：**调用接口发生异常并且重复尝试时，总是会造成系统所无法承受的损失，所以必须阻止这种现象的发生**。
- 实现幂等的方式很多，目前基于请求令牌机制适用范围较广。其核心思想是为每一次操作生成一个唯一性的凭证，也就是 token。一个 token 在操作的每一个阶段只有一次执行权，一旦执行成功则保存执行结果。对重复的请求，返回同一个结果（报错）等。[参考《幂等性浅谈》](https://www.jianshu.com/p/475589f5cd7b)

## [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#幂等处理实现) 幂等处理实现

### [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#加入依赖) 加入依赖

```
<dependency>
    <groupId>com.pig4cloud.plugin</groupId>
    <artifactId>idempotent-spring-boot-starter</artifactId>
    <version>0.0.3</version>
</dependency>
```

### [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#配置-redis-链接) 配置 Redis 链接

- 默认情况下，可以不配置。理论是支持 [redisson-spring-boot-starter](https://github.com/redisson/redisson/tree/master/redisson-spring-boot-starter) 全部配置

```
spring:
  redis:
    host: 127.0.0.1
    port: 6379
```

### [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#接口) 接口

```
@Idempotent(key = "#key", expireTime = 10, info = "请勿重复查询")
@GetMapping("/test")
public String test(String key) {
    return "success";
}
```

### [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#测试) 测试

- 10 个独立线程请求

![img](images/20201105154723.png)

- 执行查看结果，10 个请求只会有一个成功

  ![img](images/20201105155405.png)

- 查看后台异常报错，9 个异常报错满足预期

![img](images/20201105155516.png)

## [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#idempotent-注解说明) idempotent 注解说明

- key: 幂等操作的唯一标识，使用 spring el 表达式 用#来引用方法参数 。 **可为空则取当前 url + args 做请求的唯一标识**
- expireTime: 有效期 默认：1 有效期要大于程序执行时间，否则请求还是可能会进来
- timeUnit: 时间单位 默认：s （秒）
- info: 幂等失败提示信息，可自定义
- delKey: 是否在业务完成后删除 key true:删除 false:不删除

## [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#幂等处理设计原理) 幂等处理设计原理

[流程设计参考](https://github.com/it4alla/idempotent)

- 1.请求开始前，根据 key 查询 查到结果：报错 未查到结果：存入 key-value-expireTime key=ip+url+args
- 2.请求结束后，直接删除 key 不管 key 是否存在，直接删除 是否删除，可配置
- 3.expireTime 过期时间，防止一个请求卡死，会一直阻塞，超过过期时间，自动删除 过期时间要大于业务执行时间，需要大概评估下;
- 4.此方案直接切的是接口请求层面。
- 5.过期时间需要大于业务执行时间，否则业务请求 1 进来还在执行中，前端未做遮罩，或者用户跳转页面后再回来做重复请求 2，在业务层面上看，结果依旧是不符合预期的。
- 6.建议 delKey = false。即使业务执行完，也不删除 key，强制锁 expireTime 的时间。预防 5 的情况发生。
- 7.实现思路：同一个请求 ip 和接口，相同参数的请求，在 expireTime 内多次请求，只允许成功一次。
- 8.页面做遮罩，数据库层面的唯一索引，先查询再添加，等处理方式应该都处理下。
- 9.此注解只用于幂等，不用于锁，100 个并发这种压测，会出现问题，在这种场景下也没有意义，实际中用户也不会出现 1s 或者 3s 内手动发送了 50 个或者 100 个重复请求,或者弱网下有 100 个重复请求；

## [#](https://www.pig4cloud.com/doc/pigx/pigx-idempotent#总结) 总结

- [pig-mesh/pig](https://github.com/pig-mesh/pig)
- [pig-mesh/idempotent-spring-boot-starter](https://github.com/pig-mesh/idempotent-spring-boot-starter)







# [postman测试springsecurity 登录鉴权，获取Cookie后进行其他接口测试](https://www.cnblogs.com/yangzhixue/p/12269160.html)

renrensecurity 后台管理系统通过postman测试

**1.获取登录后的Cookie**

在登录前点击F12进行抓包，获取登录后的Cookie

![image-20210220170330294](images/image-20210220170330294.png)



**2.找到Cookie 之后，在Headers添加Cookies的键值对，就可以在Postman测试其他的接口了**

![image-20210220170425250](images/image-20210220170425250.png)



**3.请求头添加后的结果**

![image-20210220170514034](images/image-20210220170514034.png)













# @SneakyThrows

这个注解用在方法上，可以将方法中的代码用`try-catch`语句包裹起来，捕获异常并在catch中用Lombok.sneakyThrow(e)把异常抛出，可以使用@SneakyThrows(Exception.class)的形式指定抛出哪种异常



```
        <!--尝试添加-->
        <div class="form-group">
            <div class="col-sm-2 control-label">紧急联系人</div>
            <div class="col-sm-10">
                <input type="text" class="form-control" v-model="missingPeople.conect"
                       placeholder="紧急联系人"/>
            </div>
        </div>


        <!--尝试添加-->
```



# 【Springboot】——@EnableAsync@Async

一直不太明白，线程池在实际应用当中到底扮演什么样的角色，有什么场景要用到，只有真正的项目设计的时候才能逐渐理解，实践出真知说的就是这么个道理。
使用多线程，往往是创建Thread，或者是实现runnable接口，用到线程池的时候还需要创建Executors，spring中有十分优秀的支持，就是注解@EnableAsync就可以使用多线程，@Async加在线程任务的方法上（需要异步执行的任务），定义一个线程任务，通过spring提供的ThreadPoolTaskExecutor就可以使用线程池

# 首先定义配置类

这个配置类需要实现AsyncConfiguer接口，并实现它的方法

- 异步线程的执行者，在里面配置自动执行的东西，比如线程池参数
- 线程异常处理

```java
package ds.watsons.app.label.config;
import java.util.concurrent.Executor;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
 
@Configuration
@EnableAsync
public class TreadPoolConfig implements AsyncConfigurer{
 
    @Override
    public Executor getAsyncExecutor() {
        // TODO Auto-generated method stub
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程池数量，方法: 返回可用处理器的Java虚拟机的数量。
        executor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        //最大线程数量
        executor.setMaxPoolSize(Runtime.getRuntime().availableProcessors()*5);
        //线程池的队列容量
        executor.setQueueCapacity(Runtime.getRuntime().availableProcessors()*2);
        //线程名称的前缀
        executor.setThreadNamePrefix("hyc-excutor-");
        // setRejectedExecutionHandler：当pool已经达到max size的时候，如何处理新任务
        // CallerRunsPolicy：不在新线程中执行任务，而是由调用者所在的线程来执行
        //executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
    /*异步任务中异常处理*/
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        // TODO Auto-generated method stub
        return new SimpleAsyncUncaughtExceptionHandler();
    }       
}
 
```

- 线程任务类

```java
package ds.watsons.app.label.service;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
 
@Component
public class TreadTasks {
    @Async
    public void startMyTreadTask() {
        System.out.println("this is my async task");
        System.out.println(Thread.currentThread().getName());
    }
}
 
```

- 调用异步线程任务

```java
package ds.watsons.app.label.controller;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
 
import ds.watsons.app.label.service.TreadTasks;
 
@Controller
public class AsyncTaskUse {
    @Autowired
    private TreadTasks treadTasks;
    @GetMapping("/startMysync")
    public void useMySyncTask() {
        treadTasks.startMyTreadTask();
    }
}
```

请求url

```
/startMysync
```

返回结果：

```powershell
this is my async task
hyc-excutor-1
this is my async task
hyc-excutor-2
this is my async task
hyc-excutor-3
this is my async task
hyc-excutor-4
this is my async task
hyc-excutor-1


```





# Spring Boot中@Autowired可以省略的情况

```java
@Repository
@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate jdbcTemplate;
	...
}
```

@AllArgsConstructor是Lombok中的一个注解，其功能是生成一个为类中所有**非static变量以及未初始化的final变量**进行赋值的构造函数，生成的class文件中的代码如下：

```java
@Repository
public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate jdbcTemplate;
 
	...
 
    public UserRepositoryImpl(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
```

这个生成的构造函数没有使用@Autowired注解，但却能够成功运行。

网上搜了一下原因，按照官方文档的说法，如果一个bean有一个构造器，就可以省略@Autowired。

> If a bean has one constructor, you can omit the @Autowired

省略@Autowired配合Lombok，能够有效减少代码长度，可能对于初学者有点不太直观，但是习惯了之后就会觉得很舒服。

使用@AllArgsConstructor为final变量自动生成构造器的话，idea中代码会一直飘红报错，idea中安装lombok plugin插件就能够避免这种情况，能够识别自动生成的代码。



​	

- @NonNull可以标注在方法、字段、参数之上，表示对应的值不可以为空
- @Nullable注解可以标注在方法、字段、参数之上，表示对应的值可以为空



@bindparam







{"method":"connect","id":"12345","jsonrpc":"2.0","params":{"data":{"appId":"08e955d0e030499ba77dc0639a0bb13a","version":"20190221","nonce":"vMksKOwd","timestamp":"20210126120547222"},"sign":""}}









![image-20210126112536942](images/image-20210126112536942.png)



#### Error:java: 无效的源发行版: 9

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <source>8</source>
                <target>8</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```





# [解决renren-security使用oracle主键问题](https://www.renren.io/detail/12236)

分享 未结[ 1](https://www.renren.io/detail/12236#comment) ** 655

[![LancCJ](images/adf16201809101230064655.jpg)](https://www.renren.io/home/1564018037)

[LancCJ ](https://www.renren.io/home/1564018037)2019-11-04

悬赏：20积分

下载项目，直接配置oracle数据库，在配置文件中使用 id-type: ID_WORKER 模式的主键生成策略，但是出现了错误

错误描述为：前端的Long类型主键最后位数精度不对，变成了00

解决方法在项目新增如下代码,方式是将返回的Long 类型的id以字符串的方式返回

```java
package io.renren.common.config;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class JacksonConfig {
    /**
     * Jackson全局转化long类型为String，解决jackson序列化时long类型缺失精度问题
     * @return Jackson2ObjectMapperBuilderCustomizer 注入的对象
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        Jackson2ObjectMapperBuilderCustomizer cunstomizer = new Jackson2ObjectMapperBuilderCustomizer() {

            @Override
            public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {


                jacksonObjectMapperBuilder.serializerByType(Long.class, ToStringSerializer.instance)
                        .serializerByType(Long.TYPE, ToStringSerializer.instance);

            }
        };

        return cunstomizer;
    }

}
```











# 最新的堡垒机账号密码（7/30）



Dzzwk2  

User!@#1231（原始密码）

User!@#12311（设置的密码）

User!@#123111（设置的密码）

User!@#123112（设置的密码）

User!@#123113

Dzzwk&5827

root密码

Dzzwk&5827



62687300305

## 公安网堡垒机账号密码

bgs_zwfu
123456



### linux系统之间传输文件

```
scp  /opt/tc_szf_hcp

scp local_file remote_username@remote_ip:remote_folder
```





http://wsbs.shxga.gov.cn/webjjcluster/cityindex.jsp?from=gab

## 服务器jar包位置

/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-BAK-5540-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/WEB-INF/lib

```powershell
nacos单节点启动
[root@GAT-NET-Clone-5541-C-X64-ZHFW bin]# sh startup.sh -m standalone
vim /etc/hosts


[root@GAT-NET-Clone-5541-C-X64-ZHFW opt]# tar -zxvf node-v14.19.0-linux-x64.tar.gz 




二、移动到指定目录
[root@GAT-NET-Clone-5541-C-X64-ZHFW opt]# mv node-v14.19.0-linux-x64 /usr/local/node

mv node-v8.9.0-linux-x64 /usr/local/node
三、建立软连接

cd  /usr/bin
ln -s /usr/local/node/bin/node node
ln -s /usr/local/node/bin/npm npm
四、安装cnpm

npm install -g cnpm --registry=https://registry.npm.taobao.org
cd /usr/bin
ln -s /usr/local/node/lib/node_modules/cnpm/bin/cnpm cnpm





chmod +x  pigx-auth.sh
chmod +x  pigx-gateway.sh
chmod +x  pigx-upms-biz.sh

[root@GAT-NET-Clone-5541-C-X64-ZHFW fuwu]# sh pigx-auth.sh stop
[root@GAT-NET-Clone-5541-C-X64-ZHFW fuwu]# sh pigx-gateway.sh stop
[root@GAT-NET-Clone-5541-C-X64-ZHFW fuwu]# sh pigx-upms-biz.sh stop


[root@GAT-NET-Clone-5541-C-X64-ZHFW fuwu]# sh pigx-upms-biz.sh start
[root@GAT-NET-Clone-5541-C-X64-ZHFW fuwu]# sh pigx-gateway.sh start
[root@GAT-NET-Clone-5541-C-X64-ZHFW fuwu]# sh pigx-auth.sh start


```

7OFzDa

```shell
http://10.5.55.41:3000/oauth/authorize?client_id=open&response_type=code&redirect_uri=http://wsbs.shxga.gov.cn

http://10.5.55.41:3000/oauth/token?grant_type=authorization_code&code=xDvmx5&client_id=open&client_secret=open&redirect_uri=http://wsbs.shxga.gov.cn


http://10.5.55.41:3000/oauth/authorize?client_id=open&response_type=code&redirect_uri=https://pig4cloud.com


http://10.5.55.41:3000/oauth/token?grant_type=authorization_code&code=xDvmx5&client_id=open&client_secret=open&redirect_uri=http://wsbs.shxga.gov.cn

```

## 公安网业务系统：账号：tch2008    密码：Dzzwk&5827







```powershell
ps -ef|grep nginx
```

nginx 位置

user(SSH)@10.5.55.41(10.5.55.41)



## 33服务器上文件位置

find /opt/IBM/WebSphere/ -name t_pay_order.jsp

```powershell
[user@GAT-NET-5533-C-X64-ZHFW ~]$ find /opt/IBM/WebSphere/ -name t_pay_order.jsp
/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-MAIN-5539-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/webjjbus/t_pay_order.jsp


find /opt/IBM/WebSphere/ -name hlw_zcfg_list.jsp

cd /opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-MAIN-5539-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/uploadfile

find /opt/IBM/WebSphere/ -name asksuggest.jsp

[root@GAT-NET-5533-C-X64-ZHFW up_down_load]# find /opt/IBM/WebSphere/ -name asksuggest.jsp
/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-MAIN-5539-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/webjjbus/asksuggest.jsp

```



### git 仓库搭建

```powershell
# 切换root账号
[user@GAT-NET-Clone-5541-C-X64-ZHFW /]$ su 
Password: 
[root@GAT-NET-Clone-5541-C-X64-ZHFW /]# 
# 查看 centos 版本号
[root@GAT-NET-Clone-5541-C-X64-ZHFW temp]# lsb_release -a
LSB Version:	:base-4.0-amd64:base-4.0-noarch:core-4.0-amd64:core-4.0-noarch:graphics-4.0-amd64:graphics-4.0-noarch:printing-4.0-amd64:printing-4.0-noarch
Distributor ID:	CentOS
Description:	CentOS release 6.10 (Final)
Release:	6.10
Codename:	Final
[root@GAT-NET-Clone-5541-C-X64-ZHFW temp]# cat /etc/redhat-release
CentOS release 6.10 (Final)

[root@GAT-NET-Clone-5541-C-X64-ZHFW temp]# wget https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-10.2.2-ce.0.el7.x86_64.rpm


wget mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-10.0.0-ce.0.el7.x86_64.rpm



```

# 

```




```



### nexus



https://blog.csdn.net/sinat_32230773/article/details/109224326

# Nexus搭建maven私仓

```powershell
#端口被占用
[root@GAT-NET-Clone-5541-C-X64-ZHFW bin]# netstat -lnp|grep 8099
tcp        0      0 0.0.0.0:8099                0.0.0.0:*                   LISTEN      15341/java    
[root@GAT-NET-Clone-5541-C-X64-ZHFW bin]# kill -9 15341
[root@GAT-NET-Clone-5541-C-X64-ZHFW bin]# netstat -lnp|grep 8099
[1]+  Killed                  ./nexus run

```

```






```



![image-20210908172622675](images/image-20210908172622675.png)



nexus

http://10.5.55.41:8099

admin

123456

https://blog.csdn.net/u011042133/article/details/108879449











# [免费内网穿透工具之HTTP穿透](https://blog.csdn.net/suprezheng/article/details/103443702?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-19.control&dist_request_id=1328656.11298.16158893054989381&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-19.control)



ding -config=./ding.cfg -subdomain=hjwtce 8081

回家网前台路径

```
/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-BAK-5540-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/hjwxt







/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-BAK-5540-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/hjwqt



//张康的表单提交
http://wsbs.shxga.gov.cn/crj/crjxxtb_sxga.html

/opt/IBM/WebSphere/AppServer/profiles/Custom01/installedApps/GAT-NET-MAIN-5539-C-X64-ZHFWCell01/tchwebstruts2_was_war.ear/tchwebstruts2_was.war/crj



voiceMai_type
voiceMai_theme
voiceMai_content
voiceMai_lyrname
voiceMai_lyrtelephone
voiceMai_email
voiceMai_qq
voiceMai_ip
voiceMai_lytime
voiceMai_ystatus
voiceMai_reply



保存的寻亲信息
missingPeople_type
missingPeople_missingName
missingPeople_sex
missingPeople_missingIdCard
missingPeople_birthday
missingPeople_missingHeight
missingPeople_missingAge
missingPeople_missingTime
missingPeople_missingPlace
missingPeople_homeTown
missingPeople_bloodCollection
missingPeople_putOnRecord
missingPeople_image
missingPeople_feature
missingPeople_disappeared
missingPeople_otherInformation
missingPeople_otherInstructions
missingPeople_releaseTime
missingPeople_status
missingPeople_retrieve
missingPeople_name
missingPeople_relationShip
missingPeople_telephone
missingPeople_phone
missingPeople_email
missingPeople_qq
missingPeople_address
missingPeople_otherContact



```



# layui获取table中的checkbox选中的值

```js
var checkStatus = layui.table.checkStatus('testReload').data;
		var ids = [];
 
		for(var i=0;i<checkStatus.length;i++){
			ids.push(checkStatus[i].id)
		}
		ids = ids.join(',');//必须要写，不然后台获取不到数据
```











# [关于线程池的工作队列及新线程的流程顺序](https://www.cnblogs.com/liumz0323/p/11287037.html)

new ThreadPoolExecutor(corePoolSize, maxPoolSize , keepAliveTime ,timeUnit, workQueue,threadFactory,rejectMethod )

新线程加入:

1. Running 的线程 小于 corePoolSize ，直接创建新的线程在Pool执行

2. Running 的线程 等于corePoolSize ，则任务加入工作队列

3. Running 的线程 等于corePoolSize，工作队列已满，则加入 大于corePoolSize 小于 maxPoolSize 线程

4. 全部满，执行拒绝策略



### 远程服务的下载及上传

（https://github.com/MAXIAODONGS/Remote-operation-of-static-resources）



# [Method annotated with @Bean is called directly. Use dependency injection instead.](https://www.cnblogs.com/king0207/p/13360820.html)

未添加@Configuration注解，导致@Bean之间相互调用出错



# [java.lang.ClassCastException: [Ljava.lang.Long； cannot be cast to java.util.List](https://blog.csdn.net/qq_27424223/article/details/108226930)

4. 解决方案加上这个list转换就可以了
 List list = Arrays.asList(jobIds);


```java
@Override
public int updateBatch(Long[] jobIds, int status){
	Map<String, Object> map = new HashMap<>(2);
	// 这里需要再次转换list
	List<Long> list = Arrays.asList(jobIds);
    //——————————————————————————————————-
	map.put("list", list);
	map.put("status", status);
	return baseMapper.updateBatch(map);
}
```

5. 这里很奇怪，如果不加自动填充就没有问题，加了自动填充必须转换一下，真是个大坑





# renren-fast 连接oracle 数据库的问题

修改配置文件已经连接成功，主键修改为ID_WORKER
但创建成功，编辑是报错：
数据库的ID为1355521651906756610；
但在VUE页面显示为1355521651906756600
最后的两位不对，就导致无法修改或删除记录；
求解，谢谢！
![img](images/03f59202101302247166992.png)





# 解决renren-security使用oracle主键问题

下载项目，直接配置oracle数据库，在配置文件中使用 id-type: ID_WORKER 模式的主键生成策略，但是出现了错误

错误描述为：前端的Long类型主键最后位数精度不对，变成了00

解决方法在项目新增如下代码,方式是将返回的Long 类型的id以字符串的方式返回



```java
package io.renren.config;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class JacksonConfig {
    /**
     * Jackson全局转化long类型为String，解决jackson序列化时long类型缺失精度问题
     *
     * @return Jackson2ObjectMapperBuilderCustomizer 注入的对象
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        Jackson2ObjectMapperBuilderCustomizer cunstomizer = new Jackson2ObjectMapperBuilderCustomizer() {
            @Override
            public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
                jacksonObjectMapperBuilder.serializerByType(Long.class, ToStringSerializer.instance)
                        .serializerByType(Long.TYPE, ToStringSerializer.instance);
            }
        };
        return cunstomizer;
    }
}
```









# 数据库迁移更换springboot jar包内容

# springboot项目jar包发布的，如何线上修改jar包。

1.jar xf xxx.jar解压这个jar包，
2.然后覆盖你要修改的代码或者线上修改你的配置文件或者静态页面。
3.然后使用jar cfM xxx.jar *来重新压缩。压缩后使用java -jar xxx.jar执行
4.如果出现：‘Exception in thread “main” java.lang.IllegalStateException: Unable to open nested entry’错误。
5.解决方案：
在压缩的时候添加-0这个参数。jar cf0M xxx.jar *压缩后可正常执行

```powershell

find / -name springboot-getcardinfo.jar
jar xf t_szrk.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M t_szrk.jar *






find / -name springboot-getcardinfo.jar
jar xf springboot-getcardinfo.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M springboot-getcardinfo.jar *






find / -name t_certification.jar
jar xf t_certification.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M t_certification.jar *





已完成
=========================================
find / -name t_kiosk.jar
jar xf t_kiosk.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M t_kiosk.jar *
=========================================
 find / -name tch_cer.jar
 [root@GAT-NET-5531-C-X64-ZHFW usr]#  find / -name tch_cer.jar
/usr/local/tc_kxsfrz_t/tch_cer.jar
^Z
[3]+  Stopped                 find / -name tch_cer.jar
[root@GAT-NET-5531-C-X64-ZHFW usr]# cd /usr/local/tc_kxsfrz_t/
[root@GAT-NET-5531-C-X64-ZHFW tc_kxsfrz_t]# ll
total 78204
-rw-r--r-- 1 root  root         0 Jun  8  2020 aaa.txt
drwxrwxr-x 3 35320 wheel     4096 Oct 14  2006 jai_imageio-1_1
drwxr-xr-x 5 root  root      4096 Jun  8  2020 jre
drwxr-xr-x 2 root  root      4096 Jan 23 00:00 logs
-rwxr-xr-x 1 root  root      1386 Jun  8  2020 spring-boot.sh
-rw-r--r-- 1 root  root  26684527 Jun  9  2020 tch_cer_2.jar
-rwxrwxrwx 1 user  user  26688511 Jun  9  2020 tch_cer_5.jar
-rw-r--r-- 1 root  root  26688511 Jun  9  2020 tch_cer.jar

ps -ef|grep java

 
 

tch_cer.jar
jar xf tch_cer.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M tch_cer.jar *



msgcall
=========================================
msgcall.jar
jar xf msgcall.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M msgcall.jar *

=========================================
tc-quartz.jar
jar xf tc-quartz.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M tc-quartz.jar *

=========================================
haochaping.jar
jar xf haochaping.jar
url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
jar cf0M haochaping.jar *





url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))



tail -f out.log

#只限制最后100条的日志，并持续更新日志显示
docker logs -f --tail=100 CONTAINER_ID
docker logs -f --tail 100 CONTAINER_ID
[root@localhost ~]# docker logs -f --tail=3 6c54aaeaeef7

```

```yml
     url: jdbc:oracle:thin:@(description=(FAILOVER=ON)(RETRIES=50)(DELAY=5)(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP) (HOST = 10.5.68.48)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = 10.5.68.48)(PORT = 1521)))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME =sxgafw)))
     
     
     
     url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))


url: jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.5.68.48)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sxgafw)))
```



```yaml
spring:
　　datasource:
　　　　driver-class-name: oracle.jdbc.driver.OracleDriver
　　　　jdbc-url: jdbc:oracle:thin:@10.124.0.42:1521:tact5
　　　　username: uop_act5
　　　　password: uop_act5_cb1cs
#数据源类型
type: com.alibaba.druid.pool.DruidDataSource
#jdbc:oracle:thin:@10.124.0.42:1521:tact5 其中tact5为数据库的实例名称可连接数据库之后通过在pl/sql中执行select INSTANCE_NAME from v$instance查询
#springboot2以后自动配置需要使用driver-class-name和jdbc-url不然会报错
# 数据源其他配置
initialSize: 5
minIdle: 5
maxActive: 20
maxWait: 60000
timeBetweenEvictionRunsMillis: 60000
minEvictableIdleTimeMillis: 300000
validationQuery: SELECT 1 FROM DUAL
testWhileIdle: true
testOnBorrow: false
testOnReturn: false
poolPreparedStatements: true
# 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
filters: stat,wall,log4j
maxPoolPreparedStatementPerConnectionSize: 20
useGlobalDataSourceStat: true
connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=500
webservices:
path: com.asiainfo.asiainfo_select_sh.service.UserService


```



# [Gitlab安装部署及基础操作](https://www.cnblogs.com/ssgeek/p/9328758.html)centos7的

```powershell
#卸载
[root@localhost soft]# rpm -q gitlab-ce
[root@localhost soft]# rpm -e gitlab-ce


[root@localhost soft]# mkdir -p /home/newdisk/gitlab
[root@localhost gitlab]# pwd
/home/newdisk/gitlab
[root@localhost soft]# rpm -ivh gitlab-ce-13.5.7-ce.0.el8.x86_64.rpm
#安装到指定的目录
[root@localhost gitlab]# rpm -ivh --prefix=/home/newdisk/gitlab gitlab-ce-13.5.7-ce.0.el8.x86_64.rpm
[root@localhost gitlab]# yum install policycoreutils-python-utils 
警告：gitlab-ce-13.5.7-ce.0.el8.x86_64.rpm: 头V4 RSA/SHA256 Signature, 密钥 ID f27eab47: NOKEY
Verifying...                          ################################# [100%]
准备中...                          ################################# [100%]
正在升级/安装...
   1:gitlab-ce-13.5.7-ce.0.el8        ################################# [100%]
It looks like GitLab has not been configured yet; skipping the upgrade script.

       *.                  *.
      ***                 ***
     *****               *****
    .******             *******
    ********            ********
   ,,,,,,,,,***********,,,,,,,,,
  ,,,,,,,,,,,*********,,,,,,,,,,,
  .,,,,,,,,,,,*******,,,,,,,,,,,,
      ,,,,,,,,,*****,,,,,,,,,.
         ,,,,,,,****,,,,,,
            .,,,***,,,,
                ,*,.
  


     _______ __  __          __
    / ____(_) /_/ /   ____ _/ /_
   / / __/ / __/ /   / __ `/ __ \
  / /_/ / / /_/ /___/ /_/ / /_/ /
  \____/_/\__/_____/\__,_/_.___/
  

Thank you for installing GitLab!
GitLab was unable to detect a valid hostname for your instance.
Please configure a URL for your GitLab instance by setting `external_url`
configuration in /etc/gitlab/gitlab.rb file.
Then, you can start your GitLab instance by running the following command:
  sudo gitlab-ctl reconfigure

For a comprehensive list of configuration options please see the Omnibus GitLab readme
https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md

[root@localhost soft]# 
[root@localhost soft]# vim /etc/gitlab/gitlab.rb
external_url 'http://192.168.40.128'
# #重新加载配置
[root@localhost soft]# gitlab-ctl reconfigure 
[root@localhost soft]#  gitlab-ctl stop
#查看版本号
[root@localhost soft]# rpm -qa gitlab-ce
gitlab-ce-13.5.7-ce.0.el8.x86_64
[root@localhost soft]#  gitlab-ctl start






```





# [基于Docker部署GitLab环境搭建](https://blog.csdn.net/yanglinna/article/details/104293436/)

docker 安装gitlab

```
docker pull gitlab/gitlab-ce:latest
mkdir -p /mnt/gitlab/etc
mkdir -p /mnt/gitlab/log
mkdir -p /mnt/gitlab/data

docker run \
    --detach \
    --publish 8443:443 \
    --publish 8090:80 \
    --name gitlab \
    --restart unless-stopped \
    -v /mnt/gitlab/etc:/etc/gitlab \
    -v /mnt/gitlab/log:/var/log/gitlab \
    -v /mnt/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest




```





# [GitLab安装与基础使用](https://cloud.tencent.com/developer/article/1728804)腾讯云











# [2020年支持java8的Java反编译工具汇总](https://blog.csdn.net/yannqi/article/details/80847354)



# [Java调用第三方http接口的方式](https://www.cnblogs.com/swordfall/p/10757499.html)

### plsq官网

https://www.allroundautomations.com/registered-plsqldev/





String appid="200922_app_20201112103529";

String appPwd="d8fcf61aacb04fd6824a3fe8f7ae10ac";

账户还是原来那个tch_dindo123 

密码123456

http://61.185.238.225:22247/authKey/home



数据库中为number类型的字段，在[Java](http://lib.csdn.net/base/java)类型中对应的有Integer和BigDecimal都会出现；





### maven中引入oracle驱动报错Missing artifact com.oracle:ojdbc14:jar:10.2.0.4.0

```powershell
mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.4.0 -Dpackaging=jar -Dfile=ojdbc6.jar
```



```properties
spring:
  #spring.datasource.schema=tc-quartz
  datasource:
    driver-class-name: oracle.jdbc.OracleDriver
    url: jdbc:oracle:thin:@(description=(FAILOVER=ON)(RETRIES=50)(DELAY=5)(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP) (HOST = 10.5.70.128)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = 10.5.70.129)(PORT = 1521)))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME =sxgafw)))
    username: tc_webjj
    password: Dzzwk_5827
    max-active: 5
```

### navicat premium 连接oracle

![image-20201221105100867](images/image-20201221105100867.png)

![image-20201221105157344](images/image-20201221105157344.png)













#### 这里边可以查一些jar包

http://jar.fyicenter.com/index.php?K=20





#### ***\*7.2\*******\*.2办件验证码生成接口\****

HTTP GET

http://61.185.238.225:22247/svcreg/hcp/hcp11012





#### ***\*7.2\*******\*.5线上评价数据接收接口\****

http://61.185.238.225:22247/svcreg/hcp/hcp11001

![image-20201128151027984](images/image-20201128151027984.png)



C:\Program Files\VanDyke Software\Clients\SecureFX.exe





Error:Failed to load project configuration: cannot parse file C:\Users\Administrator\Desktop\互联网+\赵鹏交接资料\自助机前端\t_kiosk\.idea\libraries\Maven__ch_qos_logback_logback_classic_1_2_3.xml: ParseError at [row,col]:[1,1]
Message: 前言中不允许有内容。









查过这个原因,说是ojdbc版本不对
本人Tomcat8 oracle11g  使用ojdbc6.jar 





#### ***\*7.2\*******\*.8获取评价指标接口\****

#### getevaluationindicators.jsp

```

```

http://61.185.238.225:22247/svcreg/hcp/hcp11003

#### ***\*7.2\*******\*.9事项星级数据分析查询接口\****

用于从好差评系统获取事项星级数据分析的接口

http://61.185.238.225:22247/svcreg/hcp/hcp11006



#### ***\*7.2\*******\*.10事项指标数据分析查询接口\****

#### 用于从好差评系统获取事项指标数据分析的接口****

http://61.185.238.225:22247/svcreg/hcp/hcp11007





#### ***\*7.2\*******\*.11\*******\*待文字评价审核数据推送接口\****

文档未定



#### ***\*7.2\*******\*.13\*******\*差评待处理数据推送接口\****

文档未定



#### ***\*7.2\*******\*.17\*******\*好评有文字评价数据推送接口\****



文档未定





E:\应用\NetSarang\Xshell 6\Xshell.exe





```json
{"serviceList":["04f0a23d64aedc4ce12ab6bb2199d4708ea60f0d2c8cabdec606d28750fc126f1c249b4e5e0fb9db57d0cf7b2da96159c97e97e93d2e7e9a303b562ca1f65b4c252732a93226d2bcbda850b424edcb3b5cd2d895f14244368d87d0fbb7ee6c5c9f6aed"],"txnCommCom":{"tCurrTotalPage":"0","tCurrTotalRec":"10","totalRec":"0"}}
```







# 2020.12.3。需要测试！

向推送一条数据

#### ***\*7.2\*******\*.3统一办件数据接收接口\****

根据办件信息的办件编号推送一条评价数据





```sql
select * from TC_COMMON.T_ALTER_CATEGORY;

select * from T_DOBUS;

-- 最终的查询语句
select * from  t_dobus where  sbusdotype in ('1','2')  and  sopinion  is not null  and csource='7';

select * from  t_dobus where  sbusdotype in ('1','2')  and  sopinion  is not null ;

select * from tc_tools.T_REQ_DOCENTER;
-- sbusintro
select sbusintro  from  T_BUS_DEPLOY ;
select ssatisfaction from  t_dobus;
select DBSJ from  t_dobus;
select count(1)  from  t_dobus;
select *  from  t_dobus;
select * from  T_COMMONER;


CREATE OR REPLACE VIEW   tc_webjj.T_UNIFIEDOFFICE
AS SELECT * from T_DOBUS ;


drop view T_UNIFIEDOFFICE;


SELECT T_DOBUS.* from T_DOBUS,T_BUS_DEPLOY,T_COMMONER where T_DOBUS.SBUSNO=T_BUS_DEPLOY.SBUSNO and T_COMMONER.PERSONID=T_DOBUS.SUSERNO ;



SELECT T_DOBUS.*,T_BUS_DEPLOY.* from T_DOBUS left join T_BUS_DEPLOY on T_DOBUS.SBUSNO=T_BUS_DEPLOY.SBUSNO left join T_COMMONER on T_COMMONER.PERSONID=T_DOBUS.SUSERNO ;

select * from t_dobus where ROWNUM< 10;
select * from t_dobus where ROWNUM< 10;
select * from tc_webjj.T_HAOCHAPING_LOGS;

SELECT T_DOBUS.*, T_BUS_DEPLOY.*,T_COMMONER.*
  from T_DOBUS
  left join T_BUS_DEPLOY on T_DOBUS.SBUSNO = T_BUS_DEPLOY.SBUSNO
  left join T_COMMONER on T_COMMONER.PERSONID = T_DOBUS.SUSERNO where  T_DOBUS.sbusdotype in( '1','2') and state = '43'

```



# 服务器账号密码：

阳虹的堡垒机

```
Dzzwk1
Dzzwkyyh&5827
每次链接的密码：Dzzwk&5827
```



```
堡垒机账号：Dzzwk8
堡垒机密码：User!@#029
服务器密码：Dzzwk8&5827
```





### 好差评部署地址（34）

```powershell
[user@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]$ ll
total 36904
-rwxrwxrwx 1 user user 37781331 Dec 11 15:46 haochaping.jar
drwxrwxrwx 5 user user     4096 Dec 11 16:35 jre
drwxrwxrwx 2 user user       60 Dec 11 16:50 logs
-rwxrwxrwx 1 user user     1382 Dec 11 16:46 spring-boot.sh
```

### 部署脚本

```powershell
#!/bin/bash  
#这里可替换为你自己的执行程序，其他代码无需更改  
APP_NAME=haochaping.jar
  
#使用说明，用来提示输入参数  
usage() {  
    echo "Usage: sh 执行脚本.sh [start|stop|restart|status]"  
    exit 1  
}  
  
#检查程序是否在运行  
is_exist(){  
  pid=`ps -ef|grep $APP_NAME|grep -v grep|awk '{print $2}' `  
  #如果不存在返回1，存在返回0       
  if [ -z "${pid}" ]; then  
   return 1  
  else  
    return 0  
  fi  
}  
  
#启动方法  
start(){  
  is_exist  
  if [ $? -eq "0" ]; then  
    echo "${APP_NAME} is already running. pid=${pid} ."  
  else 
    nohup ./jre/bin/java -jar $APP_NAME > /opt/tc_szf_hcp/logs/out.log 2>&1 &  
  fi
}  
  
#停止方法  
stop(){  
  is_exist  
  if [ $? -eq "0" ]; then  
    kill -9 $pid  
  else  
    echo "${APP_NAME} is not running"  
  fi    
}  
  
#输出运行状态  
status(){  
  is_exist  
  if [ $? -eq "0" ]; then  
    echo "${APP_NAME} is running. Pid is ${pid}"  
  else  
    echo "${APP_NAME} is NOT running."  
  fi  
}  
  
#重启  
restart(){  
  stop  
  start  
}  
  
#根据输入参数，选择执行对应方法，不输入则执行使用说明  
case "$1" in  
  "start")  
    start  
    ;;  
  "stop")  
    stop  
    ;;  
  "status")  
    status  
    ;;  
  "restart")  
    restart  
    ;;  
  *)  
    usage  
    ;;  
esac  

```



服务器

```powershell
# 授权可执行权限
chmod +x haochaping.sh

chmod -R 777 jre/


[user@GAT-NET-5534-C-X64-ZHFW ~]$ cd /opt/
[user@GAT-NET-5534-C-X64-ZHFW opt]$ ll
total 0
drwxrwxrwx 6 user user 98 Nov 26  2016 IBM
drwxrwxrwx 5 user user 94 Oct 31 13:07 tc_spb_getpid
drwxrwxrwx 4 user user 69 Dec 14 16:45 tc_szf_hcp
[user@GAT-NET-5534-C-X64-ZHFW opt]$ cd tc_szf_hcp/
[user@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]$ ll
total 36908
-rw-rw-r-- 1 user user 37781758 Dec 14 16:44 haochaping.jar
drwxrwxrwx 5 user user     4096 Dec 11 16:35 jre
drwxrwxrwx 2 user user      141 Dec 14 00:00 logs
-rwxrwxrwx 1 user user     1382 Dec 11 16:46 spring-boot.sh
[user@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]$ ./spring-boot.sh stop
./spring-boot.sh: line 36: kill: (31884) - Operation not permitted
[user@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]$ su root
Password: 
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# ./spring-boot.sh stop
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# ./spring-boot.sh status
haochaping.jar is NOT running.
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# ./spring-boot.sh status
haochaping.jar is NOT running.
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# ll
total 36908
-rw-rw-r-- 1 user user 37781753 Dec 15 16:26 haochaping.jar
drwxrwxrwx 5 user user     4096 Dec 11 16:35 jre
drwxrwxrwx 2 user user      141 Dec 14 00:00 logs
-rwxrwxrwx 1 user user     1382 Dec 11 16:46 spring-boot.sh
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# ./spring-boot.sh start
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# ./spring-boot.sh status
haochaping.jar is running. Pid is 4349
[root@GAT-NET-5534-C-X64-ZHFW tc_szf_hcp]# 
```



# [MYSQL数据库迁移到ORACLE数据库](https://www.cnblogs.com/telwanggs/p/7263653.html)

# [MySQL数据库迁移至Oracle](https://blog.csdn.net/qq_45003757/article/details/108034152)

使用SQL Developer迁移数据库

### [Oracle SQL Developer](https://www.oracle.com/cn/database/technologies/appdev/sqldeveloper-landing.html)

[下载地址](https://www.oracle.com/tools/downloads/sqldev-downloads.html)

oracle 账号密码

284511796@qq.com

hyctl18H



# [sqldeveloper mysql 连接错误 The server time zone](https://www.cnblogs.com/panjinzhao/p/13321961.html)

```powershell
执行请求的操作时遇到错误:

The server time zone value '�й���׼ʱ��' is unrecognized or represents more than one time zone. You must configure either the server or JDBC driver (via the 'serverTimezone' configuration property) to use a more specifc time zone value if you want to utilize time zone support.
```



```powershell
解决方案：端口添加
3306/?serverTimezone=America/Winnipeg&dummyparam=
```

![img](images/297688-20200716120545157-1115350044.png)





### 使用Oracle SQL Developer访问Oracle

![image-20201216153417291](images/image-20201216153417291.png)





### 在Oracle创建新用户并授予权限

```mysql
--创建数据库新用户，world 为数据库用户，Dzzwk_5827 为密码
create user tc_sqhjw IDENTIFIED BY Dzzwk_5827;
--把connect，resource权限授权给新用户
grant CONNECT,resource to tc_sqhjw;
--把dba权限授权给新用户
grant dba to tc_sqhjw


```

```properties
spring:
  #spring.datasource.schema=tc-quartz
  datasource:
    driver-class-name: oracle.jdbc.OracleDriver
    url: jdbc:oracle:thin:@(description=(FAILOVER=ON)(RETRIES=50)(DELAY=5)(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP) (HOST = 10.5.70.128)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = 10.5.70.129)(PORT = 1521)))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME =sxgafw)))
    username: world
    password: Dzzwk_5827
    max-active: 5
```





![image-20201216165912593](images/image-20201216165912593.png)





# [Mysql数据迁移到Oracle上(完美解决！！！)](https://blog.csdn.net/qq_21221361/article/details/105561692?utm_medium=distribute.pc_relevant.none-task-blog-searchFromBaidu-4.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-searchFromBaidu-4.control)

# Mysql数据迁移到Oracle上(完美解决！！！)



[积硅步至千里.](https://blog.csdn.net/qq_21221361) 2020-04-16 17:21:04 ![img](images/articleReadEyes-1608172445327.png) 1934 ![img](images/tobarCollect-1608172445327.png) 收藏 11

**背景：**

从Github上down的项目，数据库是mysql的，而我们公司都是用的oracle，遂计划把mysql数据迁移的oracle上。于是，百度搜索迁移方法，试了各种方法如：Navicat Premium的数据传输，SQL Developer的迁移，Convert Mysql to Oracle，还有kettle(挺麻烦的)无一列外，都没没能成功！！！垂头丧气...

后来，想着整个迁移不行。那就按表(一个一个表转换)来吧，用的Oracle SQL Developer，竟然成功了，大喜！！！！遂开博记录下(其实很简单的，哎...)

**工具：**Oracle SQL Developer，网上下载解压即可用。

**具体过程：**

1.新建一个Oracle数据库（我建的是test）来接受数据，不用建表。

打开Oracle SQL Developer，点击右上角绿色加号，连接上test数据库。

![img](images/20200416170822507.jpg)

2.连接mysql数据库(源数据库)

数据库类型选择mysql，注意要点击”选择数据库“，选择test数据库

![img](images/20200416170919881.jpg)

3.数据库都连接成功后，在MySQL数据库上选择要迁移的表，右键选择”复制到oracle“。这里选的是tb_user表

![img](images/20200416171218338.jpg)

4.弹出提示框，输入mysql连接时的用户密码(应该是确认下)

![img](images/20200416171437989.jpg)

5.复制的设置框，这里不用修改，点击确定即可

![img](images/20200416171559431.jpg)

6.等待一会，弹出复制成功的框子。就迁移表成功了。

![img](images/20200416171642552.jpg)

7.看看结果。oracle数据库，tb_user表和数据都有了。

![img](images/20200416171820229.jpg)





## [使用Oracle SQL Developer迁移MySQL至Oracle数据库](https://www.cnblogs.com/xusweeter/p/6512521.html)

```mysql
--(1)用具有dba权限的账户登录,查询用户的进程,然后kill.
select sid,serial# from v$session where username='world';
alter system kill session 'sid,serial#';
--(2)删除用户及用户下的所有表
drop user world cascade;
--查询是否已被删除:
select * from dba_users where username = 'world';
--(3)创建用户并赋予管理员权限:
create user migrater identified by Dzzwk_5827;
alter user migrater account unlock;
grant dba to migrater;
grant connect, resource to migrater;
grant create session to migrater;
commit;

--删除一张表
DROP TABLE COUNTRY;



spring:
  #spring.datasource.schema=tc-quartz
  datasource:
    driver-class-name: oracle.jdbc.OracleDriver
    url: jdbc:oracle:thin:@(description=(FAILOVER=ON)(RETRIES=50)(DELAY=5)(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP) (HOST = 10.5.70.128)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = 10.5.70.129)(PORT = 1521)))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME =sxgafw)))
    username: migrater
    password: Dzzwk_5827
    max-active: 5
    
```













# SpringMVC之@InitBinder注解详解

#### 说明与作用

springmvc并不是能对所有类型的参数进行绑定的，如果对日期Date类型参数进行绑定，就会报错IllegalStateException错误。所以需要注册一些类型绑定器用于对参数进行绑定。InitBinder注解就有这个作用。

```java
@Controller
public class InitBinderController {

    @RequestMapping("/testInitBinder")
    private String testInitBinder(Date date){
        System.out.println("date = " + date);
        return "RequsetInitBindDemo";
    }
}
123456789
```

![在这里插入图片描述](images/20200627164559573.png)
不能把String类型转换为Date类型报错。

此时就需要一个`日期类型转换器。`

```java
@InitBinder
    public void dateTypeBinder(WebDataBinder webDataBinder){
        //往数据绑定器中添加一个DateFormatter日期转化器。
        webDataBinder.addCustomFormatter(new DateFormatter("yyyy-mm-dd"));
    }
123456
```

![在这里插入图片描述](images/20200627164946929.png)
![在这里插入图片描述](images/20200627165252221.png)
日期类型转换成功了。

InitBinder注解：

```java
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InitBinder {

	//指定参数名，这个不知控制器方法上形参的参数名，而是请求参数名，可以指定多个。指定后只有这些参数需要用到该转换器。如果不指定，默认所有。
	String[] value() default {};

}
123456789
```

并且使用InitBinder 注册的绑定器只有在当前Controller中才有效，不会作用于其他Controller。

在其他controller中定义一个接收请求的方法。

![在这里插入图片描述](images/20200627170049926.png)
![在这里插入图片描述](images/20200627170131352.png)
请求失败。

#### 使用@ControllerAdvice定义全局绑定器

可以使用@ControllerAdvice定义全局绑定器。ControllerAdvice的使用可以看文章

```java
@ControllerAdvice
public class InitBinderAdviseController {

    @InitBinder
    public void dateTypeBinder(WebDataBinder webDataBinder){
        //往数据绑定器中添加一个DateFormatter日期转化器。
        webDataBinder.addCustomFormatter(new DateFormatter("yyyy-mm-dd"));

    }
}
12345678910
```

结果：
![在这里插入图片描述](images/20200627170451494.png)
![在这里插入图片描述](images/20200627170513855.png)
不同controller的方法都能作用到。

#### 使用其他格式转化器

我们可以自定义格式转化器，实现Formatter接口就可。还可以添加验证器等等。

```java
public class StringFormatter implements Formatter<String> {
    private static final String PREFIX = "convertString == ";

    @Override
    public String parse(String text, Locale locale) throws ParseException {
    	//所以String类型参数都加上一个前缀。
        String result = PREFIX + text;
        return result;
    }

    @Override
    public String print(String object, Locale locale) {
        return object;
    }
}


1234567891011121314151617
```

添加
![在这里插入图片描述](images/20200627171351248.png)

测试：

```java
@RequestMapping("/testInitBinder2")
    private String testInitBinder1(String name){
        System.out.println("name = " + name);
        return "RequsetInitBindDemo";
    }

123456
```

结果：
![在这里插入图片描述](images/20200627171454893.png)
![在这里插入图片描述](images/20200627171443670.png)
前缀有了。



## SQL SELECT DISTINCT 语句

在表中，可能会包含重复值。这并不成问题，不过，有时您也许希望仅仅列出不同（distinct）的值。

关键词 DISTINCT 用于返回唯一不同的值。

### 语法：

```Sql
SELECT DISTINCT 列名称 FROM 表名称
```

## 使用 DISTINCT 关键词

如果要从 "Company" 列中选取所有的值，我们需要使用 SELECT 语句：

```sql
SELECT Company FROM Orders
```

### "Orders"表：

| Company  | OrderNumber |
| :------- | :---------- |
| IBM      | 3532        |
| W3School | 2356        |
| Apple    | 4698        |
| W3School | 6953        |

### 结果：

| Company  |
| :------- |
| IBM      |
| W3School |
| Apple    |
| W3School |

请注意，在结果集中，W3School 被列出了两次。

如需从 Company" 列中仅选取唯一不同的值，我们需要使用 SELECT DISTINCT 语句：

```sql
SELECT DISTINCT Company FROM Orders 
```

### 结果：

| Company  |
| :------- |
| IBM      |
| W3School |
| Apple    |

现在，在结果集中，"W3School" 仅被列出了一次。







### 三秦回家网

```
三秦回家网
账号：fanzhide
密码：fanzhide_0

三秦回家网
fanzhide01
fanzhide01_
```



### 三秦回家网数据的地址

https://pan.baidu.com/share/init?surl=2InVGGwHAF6jy8JMnyfi7A   ck2a



# ACCESS数据库转ORACLE数据库分享_网页设计教程

网上有很多文章介绍[access](http://www.mb5u.com/biancheng/access/)转oracle数据库的方法，本人都尝试了，不是很成功，列举一下，后来人不必盲目试了，基本不成功：

1、Access-->excel-->PL/SQL-->Oracle

2、Access-->txt-->Sqlload-->Oracle

以上两种字段出错很厉害。

3、SQL Server和winsql，这种操作相当复杂，特别是winsql

4、通过ODBC的数据源直接Access-->Oracle

基本能行，但access数据库的表结构、表数据等影响转换成功。比如access需表名大写要求。

后再找了很多oracle图形化管理软件如SQLyog Enter[pr](http://t.mb5u.com/GooglePR/)is、PLSQL Developer、toad for oracle 10、Navicat for Oracle等，一个个试过来，最后用Navicat for Oracle完成了转换，而且简单快速。

建议大家以后就用Navicat for Oracle来导access数据库。

**下面图解下access导入到oracle过程：**

1、先下载安装Navicat for Oracle，我用的版本是9.0的，版本无所谓，不要低于8.0即可。

2、打开Navicat for Oracle，点[菜单](http://www.mb5u.com/jquery/menu/)-文件-新建连接，弹出下图：

![img](images/13BG0032540-11537.gif)

用户名这里要注意：oracle一般启用SYS、SYSTEM、DBSNMP、SYSMAN四个默认用户，虽然SYS用户最高权限，但必须填SYSTEM用户名连接才行。

![img](images/13BG0032K0-22I7.gif)

点击连接测试，连接成功。

3、接下来打开如下界面

![img](images/13BG0032Z-39137.gif)

4、右键点“表”，点“导入向导”，弹出下图窗口：

![img](images/13BG0033030-45417.gif)

5、点一下步后，选择要打开access数据库，可以选择要导入的表，可以多选：

![img](images/13BG0033110-56020.gif)

6、选择相关表名后点“下一步”，出现下图。可以指定要导入进oracle哪个表，也可以输入新的表名。

![img](images/13BG0033210-61505.gif)

7、下一步，选择所需导的字段

![img](images/13BG0033420-OA6.gif)

8、选好后，下一步，选择要导入的模式，一般选第一个即可

![img](images/13BG0033530-T1A.gif)

9、点下一步，开始转换，很快就出现下列窗口。

![img](images/13BG0033630-c255.gif)

10、导入成功。





# [SpringBoot之HandlerInterceptorAdapter](https://www.cnblogs.com/weianlai/p/11358768.html)

 

在SpringBoot中我们可以使用HandlerInterceptorAdapter这个适配器来实现自己的拦截器。这样就可以拦截所有的请求并做相应的处理。

**应用场景**

- 日志记录，可以记录请求信息的日志，以便进行信息监控、信息统计等。
- 权限检查：如登陆检测，进入处理器检测是否登陆，如果没有直接返回到登陆页面。
- 性能监控：典型的是慢日志。

在HandlerInterceptorAdapter中主要提供了以下的方法：
preHandle：在方法被调用前执行。在该方法中可以做类似校验的功能。如果返回true，则继续调用下一个拦截器。如果返回false，则中断执行，也就是说我们想调用的方法 不会被执行，但是你可以修改response为你想要的响应。
postHandle：在方法执行后调用。
afterCompletion：在整个请求处理完毕后进行回调，也就是说视图渲染完毕或者调用方已经拿到响应。

在HandlerInterceptorAdapter中主要提供了以下的方法：

- preHandle：在方法被调用前执行。在该方法中可以做类似校验的功能。如果返回true，则继续调用下一个拦截器。如果返回false，则中断执行，也就是说我们想调用的方法 不会被执行，但是你可以修改response为你想要的响应。
- postHandle：在方法执行后调用。
- afterCompletion：在整个请求处理完毕后进行回调，也就是说视图渲染完毕或者调用方已经拿到响应。

**HandlerInterceptor**

[![img](images/1766852-20190816131638476-2071188545.png)](https://img2018.cnblogs.com/blog/1766852/201908/1766852-20190816131638476-2071188545.png)

**拦截器适配器HandlerInterceptorAdapter**

[![复制代码](images/copycode-1608538637065.gif)](javascript:void(0);)

```java
public abstract class HandlerInterceptorAdapter implements AsyncHandlerInterceptor {

   /**
    * This implementation always returns {@code true}.
    */
   @Override
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
         throws Exception {

      return true;
   }

   /**
    * This implementation is empty.
    */
   @Override
   public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
         @Nullable ModelAndView modelAndView) throws Exception {
   }

   /**
    * This implementation is empty.
    */
   @Override
   public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
         @Nullable Exception ex) throws Exception {
   }

   /**
    * This implementation is empty.
    */
   @Override
   public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response,
         Object handler) throws Exception {
   }

}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

有时候我们可能只需要实现三个回调方法中的某一个，如果实现HandlerInterceptor接口的话，三个方法必须实现，不管你需不需要，此时spring提供了一个HandlerInterceptorAdapter适配器（种适配器设计模式的实现），允许我们只实现需要的回调方法。
这样在我们业务中比如要记录系统日志，日志肯定是在afterCompletion之后记录的，否则中途失败了，也记录了，那就扯淡了。一定是程序正常跑完后，我们记录下那些对数据库做个增删改的操作日志进数据库。所以我们只需要继承HandlerInterceptorAdapter，并重写afterCompletion一个方法即可，因为preHandle默认是true。

运行流程总结如下：

1. 拦截器执行顺序是按照Spring配置文件中定义的顺序而定的。
2. 会先按照顺序执行所有拦截器的preHandle方法，一直遇到return false为止，比如第二个preHandle方法是return false，则第三个以及以后所有拦截器都不会执行。若都是return true，则按顺序加载完preHandle方法。
3. 然后执行主方法（自己的controller接口），若中间抛出异常，则跟return false效果一致，不会继续执行postHandle，只会倒序执行afterCompletion方法。
4. 在主方法执行完业务逻辑（页面还未渲染数据）时，按倒序执行postHandle方法。若第三个拦截器的preHandle方法return false，则会执行第二个和第一个的postHandle方法和afterCompletion（postHandle都执行完才会执行这个，也就是页面渲染完数据后，执行after进行清理工作）方法。（postHandle和afterCompletion都是倒序执行）

下面用一个demo来演示执行流程


**定义一个类继承HandlerInterceptorAdapter，并重写方法**


快捷键ctrl+o打开可以重写的方法面板选择

![img](images/1766852-20190815162344375-1168756969.png)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```java
package com.example.demo.config;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.err.println("================================== preHandle1 ===========================================");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.err.println("================================== postHandle1 ===========================================");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.err.println("================================== afterCompletion1 ===========================================");
    }

}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

WebMvcConfigurerAdapter 抽象类是对WebMvcConfigurer接口的简单抽象（增加了一些默认实现），但在在SpringBoot2.0及Spring5.0中WebMvcConfigurerAdapter已被废弃 。官方推荐直接实现WebMvcConfigurer或者直接继承WebMvcConfigurationSupport

**实现WebMvcConfigurer配置拦截器**

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```java
package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LogInterceptor());
    }
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

在控制器中写一个方法并访问

```java
@ApiOperation(value = "test mybatis", notes = "")
@RequestMapping(value = "getuser", method = RequestMethod.GET)
public User getUser() {
    return userService.getUser();
}
```

会在控制台输出

![img](images/1766852-20190815162542727-117124894.png)

此时在加入一个拦截器，会按照配置的顺序执行，配置如下

[![复制代码](images/copycode-1608538637065.gif)](javascript:void(0);)

```java
package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LogInterceptor());
        registry.addInterceptor(new LogInterceptor2());
    }
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

控制的输出反映了配置多个拦截器的执行流程：

![img](images/1766852-20190815162624992-1316477236.png)

如果controller出现异常，则不会继续执行postHandle，只会倒序执行afterCompletion方法

![img](images/1766852-20190815162651599-1473969609.png)

![img](images/1766852-20190815162702966-1547101516.png)





# [java bean validation 参数验证](https://www.cnblogs.com/jpfss/p/11097753.html)

## 一、前言

　　在后台开发过程中，对参数的校验成为开发环境不可缺少的一个环节。比如参数不能为null，email那么必须符合email的格式，如果手动进行if判断或者写正则表达式判断无意开发效率太慢，在时间、成本、质量的博弈中必然会落后。所以把校验层抽象出来是必然的结果，下面说下几种解决方案。

## 二、几种解决方案

　　1、struts2的valid可以通过配置xml，xml中描述规则和返回的信息，这种方式比较麻烦、开发效率低，不推荐

　　`2、validation bean 是基于JSR-303标准开发出来的，使用注解方式实现，及其方便，但是这只是一个接口，没有具体实现. Hibernate Validator是一个hibernate独立的包，可以直接引用，他实现了validation bean同时有做了扩展，比较强大 ，实现图如下：`

　　　　![img](images/827161-20161022141655263-2092739140.png)

　　　　[点此查看中文官方手册](http://docs.jboss.org/hibernate/validator/4.2/reference/zh-CN/html_single/#preface)

　　　3、[oval](http://oval.sourceforge.net/) 是一个可扩展的Java对象数据验证框架，验证的规则可以通过配置文件、Annotation、POJOs 进行设定。可以使用纯 Java 语言、JavaScript 、Groovy 、BeanShell 等进行规则的编写，本次不过多讲解

## 三、bean validation 框架验证介绍

　　bean validation 包放在maven上维护,最新包的坐标如下：

```xml
<dependency>
  <groupId>javax.validation</groupId>
  <artifactId>validation-api</artifactId>
  <version>2.0.1.Final</version>
</dependency>
```

　　 [点击这里查看最新的坐标地址](http://search.maven.org/#search|gav|1|g%3A"javax.validation" AND a%3A"validation-api"　)

　　 下载之后打开这个包，有个package叫constraints，里面放的就是验证的的注解：

　　 ![img](https://images2015.cnblogs.com/blog/827161/201610/827161-20161022150209967-1863141332.png)

　　 下面开始用代码实践一下：

　　 1、定义一个待验证的bean:Student.java

```
 1 package com.shishang;
 2 
 3 import javax.validation.constraints.*;
 4 import java.io.Serializable;
 5 import java.math.BigDecimal;
 6 import java.util.Date;
 7 
 8 public class Student implements Serializable {
 9 
10 
11     @NotNull(message = "名字不能为空")
12     private String name;
13 
14     @Size(min = 6,max = 30,message = "地址应该在6-30字符之间")
15     private String address;
16 
17     @DecimalMax(value = "100.00",message = "体重有些超标哦")
18     @DecimalMin(value = "60.00",message = "多吃点饭吧")
19     private BigDecimal weight;
20 
21     private String friendName;
22     @AssertTrue
23     private Boolean isHaveFriend(){
24         return friendName != null?true:false;
25     }
26 
27     @Future(message = "生日必须在当前实践之前")
28     private Date birthday;
29 
30     @Pattern(regexp = "^(.+)@(.+)$",message = "邮箱的格式不合法")
31     private String email;
32 
33 
34     public String getName() {
35         return name;
36     }
37 
38     public void setName(String name) {
39         this.name = name;
40     }
41 
42     public String getAddress() {
43         return address;
44     }
45 
46     public void setAddress(String address) {
47         this.address = address;
48     }
49 
50     public BigDecimal getWeight() {
51         return weight;
52     }
53 
54     public void setWeight(BigDecimal weight) {
55         this.weight = weight;
56     }
57 
58     public String getFriendName() {
59         return friendName;
60     }
61 
62     public void setFriendName(String friendName) {
63         this.friendName = friendName;
64     }
65 
66     public Date getBirthday() {
67         return birthday;
68     }
69 
70     public void setBirthday(Date birthday) {
71         this.birthday = birthday;
72     }
73 
74     public String getEmail() {
75         return email;
76     }
77 
78     public void setEmail(String email) {
79         this.email = email;
80     }
81 }
```

![复制代码](https://common.cnblogs.com/images/copycode.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

   2、测试类:StudentTest.java

```
 1 package com.use;
 2 
 3 import javax.validation.ConstraintViolation;
 4 import javax.validation.Validation;
 5 import javax.validation.Validator;
 6 import javax.validation.ValidatorFactory;
 7 import java.io.Serializable;
 8 import java.math.BigDecimal;
 9 import java.util.ArrayList;
10 import java.util.Date;
11 import java.util.List;
12 import java.util.Set;
13 
14 public class StudentTest implements Serializable {
15     public static void main(String[] args) {
16         Student xiaoming = getBean();
17         List<String> validate = validate(xiaoming);
18         validate.forEach(row -> {
19             System.out.println(row.toString());
20         });
21 
22     }
23 
24     private static Student getBean() {
25         Student bean = new Student();
26         bean.setName(null);
27         bean.setAddress("北京");
28         bean.setBirthday(new Date());
29         bean.setFriendName(null);
30         bean.setWeight(new BigDecimal(30));
31         bean.setEmail("xiaogangfan163.com");
32         return bean;
33     }
34 
35     private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
36 
37     public static <T> List<String> validate(T t) {
38         Validator validator = factory.getValidator();
39         Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);
40 
41         List<String> messageList = new ArrayList<>();
42         for (ConstraintViolation<T> constraintViolation : constraintViolations) {
43             messageList.add(constraintViolation.getMessage());
44         }
45         return messageList;
46     }
47 }
```

![复制代码](https://common.cnblogs.com/images/copycode.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

   3、运行testValidation()方法，输处如下：

```
地址应该在6-30字符之间
邮箱的格式不合法
生日必须在当前时间之前
多吃点饭吧
名字不能为空 
```

   4、总结 

- 像@NotNull、@Size等比较简单也易于理解，不多说
- 因为bean validation只提供了接口并未实现，使用时需要加上一个provider的包，例如hibernate-validator
- @Pattern 因为这个是正则，所以能做的事情比较多，比如中文还是数字、邮箱、长度等等都可以做
- @AssertTRue 这个与其他的校验注解有着本质的区别，这个注解适用于多个字段。例子中isHaveFriend方法依赖friendName字段校验
- 验证的api是经过我加工了一下，这样可以批量返回校验的信息
- 有时我们需要的注解可能没有提供，这时候就需要自定义注解，写实现类，下面说一下自定义注解的使用

 

## 四、自定义bean validation 注解验证

　　有时框架自带的没法满足我们的需求，这时就需要自己动手丰衣足食了，恩恩 ，这个不难，下面说下。

　　这个例子验证字符串是大写还是小写约束标注，代码如下:

　　1、**枚举类型`CaseMode`, 来表示大写或小写模式**

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
 1 package com.defineconstrain;
 2 
 3 /**
 4  * created by xiaogangfan
 5  * on 16/10/25.
 6  */
 7 public enum CaseMode {
 8     UPPER,
 9     LOWER;
10 }
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

　　2、**定义一个CheckCase的约束标注** 

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
 1 package com.defineconstrain;
 2 
 3 /**
 4  * created by xiaogangfan
 5  * on 16/10/25.
 6  */
 7 import static java.lang.annotation.ElementType.*;
 8 import static java.lang.annotation.RetentionPolicy.*;
 9 
10 import java.lang.annotation.Documented;
11 import java.lang.annotation.Retention;
12 import java.lang.annotation.Target;
13 
14 import javax.validation.Constraint;
15 import javax.validation.Payload;
16 
17 @Target( { METHOD, FIELD, ANNOTATION_TYPE })
18 @Retention(RUNTIME)
19 @Constraint(validatedBy = CheckCaseValidator.class)
20 @Documented
21 public @interface CheckCase {
22 
23     String message() default "{com.mycompany.constraints.checkcase}";
24 
25     Class<?>[] groups() default {};
26 
27     Class<? extends Payload>[] payload() default {};
28 
29     CaseMode value();
30 
31 }
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 　3、**约束条件`CheckCase`的验证器**

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
 1 package com.defineconstrain;
 2 
 3 /**
 4  * created by xiaogangfan
 5  * on 16/10/25.
 6  */
 7 import javax.validation.ConstraintValidator;
 8 import javax.validation.ConstraintValidatorContext;
 9 
10 public class CheckCaseValidator implements ConstraintValidator<CheckCase, String> {
11 
12     private CaseMode caseMode;
13 
14     public void initialize(CheckCase constraintAnnotation) {
15         this.caseMode = constraintAnnotation.value();
16     }
17 
18     public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
19 
20         if (object == null)
21             return true;
22 
23         if (caseMode == CaseMode.UPPER)
24             return object.equals(object.toUpperCase());
25         else
26             return object.equals(object.toLowerCase());
27     }
28 
29 }
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

　　4、在Student.java中增加一个属性

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

```
1 @CheckCase(value = CaseMode.LOWER,message = "名字的拼音需要小写")
2     private String spellName;
```

　　5、在StudentTest.java的getBean()方法中增加一行

```
bean.setSpellName("XIAOGANGFAN");
```

　　6、运行testValidation()方法，输处如下：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

![复制代码](https://common.cnblogs.com/images/copycode.gif)

```
地址应该在6-30字符之间
邮箱的格式不合法
生日必须在当前时间之前
多吃点饭吧
名字的拼音需要小写
名字不能为空
```

![复制代码](https://common.cnblogs.com/images/copycode.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

　　7、说明新增的约束生效了，大功告成

代码下载地址：git@github.com:xiaogangfan/vaidation.git



# 在线文档地址：

http://localhost:9096/swagger-ui/

http://localhost:9096/doc.html#/home

http://192.168.1.107:9096/doc.html#/home

http://10.5.55.40:9096/doc.html#/home



# Postman发送Map参数



请求方式POST
在Body中选择x-www-form-urlencoded的方式，将map中所需的key和value值输入即可

![image-20201223161903086](images/image-20201223161903086.png)







# [获取客户端真实IP地址](https://www.cnblogs.com/wang1001/p/9605761.html)

## Java-Web获取客户端真实IP：

https://www.cnblogs.com/wang1001/p/9605761.html





## java中的关键字transient，这篇文章你再也不发愁了

将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会被序列化。

这个关键字的作用其实我在写java的序列化机制中曾经写过，不过那时候只是简单地认识，只要其简单的用法，没有深入的去分析。这篇文章就是去深入分析一下transient关键字。

先给出这篇文章的大致脉络

首先，介绍了transient的基本概念和基本用法、然后，介绍深入分析一下transient关键字，并介绍几个需要掌握的问题最后，来个总结

**一、初识transient关键字**

其实这个关键字的作用很好理解，就是简单的一句话：将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会被序列化。

概念也很好理解，下面使用代码去验证一下：

```java
package com.tctel.tcstudy.transientdemo;

import lombok.Data;

import java.io.Serializable;

/**
 * 将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会被序列化
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID=123456L;
    private transient int age;
    private String name;
}

```

然后我们在Test中去验证一下：

```java
package com.tctel.tcstudy.transientdemo;

import java.io.*;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        serializerUser();
        deSerializeUser();
    }

    private static void deSerializeUser() throws IOException, ClassNotFoundException {
        File file = new File("G://Test/template");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        User newUser = (User) ois.readObject();
        System.out.println("添加了transient关键字饭序列化：age=" + newUser.getAge());
    }

    private static void serializerUser() throws IOException {
        User user = new User();
        user.setName("java的架构师技术栈");
        user.setAge(24);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("G://Test/template"));
        oos.writeObject(user);
        oos.close();
        System.out.println("添加了transient关键字序列化：age=" + user.getAge());
    }
}
```

从上面可以看出，在序列化SerializeUser方法中，首先创建一个序列化user类，然后将其写入到G://Test/template路径中。在反序列化DeSerializeUser方法中，首先创建一个File，然后读取G://Test/template路径中的数据。

这就是序列化和反序列化的基本实现，而且我们看一下结果，也就是被transient关键字修饰的age属性是否被序列化。

```powershell
添加了transient关键字序列化：age=24
添加了transient关键字饭序列化：age=0

Process finished with exit code 0
```

从上面的这张图可以看出，age属性变为了0，说明被transient关键字修饰之后没有被序列化。

**二、深入分析transient关键字**

为了更加深入的去分析transient关键字，我们需要带着几个问题去解读：

（1）transient底层实现的原理是什么？

（2）被transient关键字修饰过得变量真的不能被序列化嘛？

（3）静态变量能被序列化吗？被transient关键字修饰之后呢？

带着这些问题一个一个来解决：

**1、transient底层实现原理是什么？**

java的serialization提供了一个非常棒的存储对象状态的机制，说白了`serialization就是把对象的状态存储到硬盘上 去，等需要的时候就可以再把它读出来使用`。有些时候像银行卡号这些字段是不希望在网络上传输的，transient的作用就是把这个字段的生命周期仅存于调用者的内存中而不会写到磁盘里持久化，意思是transient修饰的age字段，他的生命周期仅仅在内存中，不会被写到磁盘中。

**2、被transient关键字修饰过得变量真的不能被序列化嘛？**

想要解决这个问题，首先还要再重提一下对象的序列化方式：

Java序列化提供两种方式。

一种是实现Serializable接口

另一种是实现Exteranlizable接口。 需要重写writeExternal和readExternal方法，它的效率比Serializable高一些，并且可以决定哪些属性需要序列化（即使是transient修饰的），但是对大量对象，或者重复对象，则效率低。

从上面的这两种序列化方式，我想你已经看到了，使用Exteranlizable接口实现序列化时，我们自己指定那些属性是需要序列化的，即使是transient修饰的。下面就验证一下

首先我们定义User1类：这个类是被Externalizable接口修饰的

![img](images/c83d70cf3bc79f3d74143e04f26e3315738b2965.jpeg)

然后我们就可以测试了

![img](images/d058ccbf6c81800a376a8a40f8facdfe808b47c0.jpeg)

上面，代码分了两个方法，一个是序列化，一个是反序列化。里面的代码和一开始给出的差不多，只不过，User1里面少了age这个属性。

然后看一下结果：

![img](images/fd039245d688d43fb49df1fe34d12c1f0ff43bdd.jpeg)

结果基本上验证了我们的猜想，也就是说，实现了Externalizable接口，哪一个属性被序列化使我们手动去指定的，即使是transient关键字修饰也不起作用。

**3、静态变量能被序列化吗？没被transient关键字修饰之后呢？**

这个我可以提前先告诉结果，`静态变量是不会被序列化的，即使没有transient关键字修饰。`下面去验证一下，然后再解释原因。

首先，在User类中对age属性添加transient关键字和static关键字修饰。

然后，在Test类中去测试

![img](images/37d12f2eb9389b5058d0f2a8cdfa1bd9e6116e74.jpeg)

最后，测试一下，看看结果

![img](images/4610b912c8fcc3ceea3ea085d58a288cd63f20d9.jpeg)

结果已经很明显了。现在解释一下，为什么会是这样，其实在前面已经提到过了。因为`静态变量在全局区,本来流里面就没有写入静态变量,我打印静态变量当然会去全局区查找,而我们的序列化是写到磁盘上的，所以JVM查找这个静态变量的值，是从全局区查找的，而不是磁盘上。`user.setAge(18);年龄改成18之后，被写到了全局区，其实就是方法区，只不过被所有的线程共享的一块空间。因此可以总结一句话：

`静态变量不管是不是transient关键字修饰，都不会被序列化`

**三、transient关键字总结**

java 的transient关键字为我们提供了便利，你`只需要实现Serilizable接口，将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会序列化到指定的目的地中。像银行卡、密码等等这些数据。`这个需要根据业务情况了。





### 跨域的配置类

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .allowedOrigins("*")
            .allowCredentials(true)
            .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
            .maxAge(3600);
    }
}
```



阿里云对象存储微服务，主要用于图片资源的存储获取和删除，其他微服务有图片相关的保存和删除都会调用该微服务，

阿里云vod存储，主要用于视频的上传保存存储和加解密的播放相关功能；



### 项目统一异常处理

统一异常处理，设计一个枚举类型，三个字段，成功与否，状态码，返回显示信息，根据具体业务项目统一规定，

```java
package com.atguigu.guli.common.base.result;

import lombok.Getter;
import lombok.ToString;

/**
 * @author helen
 * @since 2019/12/25
 */
@Getter
@ToString
public enum ResultCodeEnum {

    SUCCESS(true, 20000,"成功"),
    UNKNOWN_REASON(false, 20001, "未知错误"),

    BAD_SQL_GRAMMAR(false, 21001, "sql语法错误"),
    JSON_PARSE_ERROR(false, 21002, "json解析异常"),
    PARAM_ERROR(false, 21003, "参数不正确"),

    FILE_UPLOAD_ERROR(false, 21004, "文件上传错误"),
    FILE_DELETE_ERROR(false, 21005, "文件刪除错误"),
    EXCEL_DATA_IMPORT_ERROR(false, 21006, "Excel数据导入错误"),

    VIDEO_UPLOAD_ALIYUN_ERROR(false, 22001, "视频上传至阿里云失败"),
    VIDEO_UPLOAD_TOMCAT_ERROR(false, 22002, "视频上传至业务服务器失败"),
    VIDEO_DELETE_ALIYUN_ERROR(false, 22003, "阿里云视频文件删除失败"),
    FETCH_VIDEO_UPLOADAUTH_ERROR(false, 22004, "获取上传地址和凭证失败"),
    REFRESH_VIDEO_UPLOADAUTH_ERROR(false, 22005, "刷新上传地址和凭证失败"),
    FETCH_PLAYAUTH_ERROR(false, 22006, "获取播放凭证失败"),

    URL_ENCODE_ERROR(false, 23001, "URL编码失败"),
    ILLEGAL_CALLBACK_REQUEST_ERROR(false, 23002, "非法回调请求"),
    FETCH_ACCESSTOKEN_FAILD(false, 23003, "获取accessToken失败"),
    FETCH_USERINFO_ERROR(false, 23004, "获取用户信息失败"),
    LOGIN_ERROR(false, 23005, "登录失败"),

    COMMENT_EMPTY(false, 24006, "评论内容必须填写"),

    PAY_RUN(false, 25000, "支付中"),
    PAY_UNIFIEDORDER_ERROR(false, 25001, "统一下单错误"),
    PAY_ORDERQUERY_ERROR(false, 25002, "查询支付结果错误"),

    ORDER_EXIST_ERROR(false, 25003, "课程已购买"),

    GATEWAY_ERROR(false, 26000, "服务不能访问"),

    CODE_ERROR(false, 28000, "验证码错误"),

    LOGIN_PHONE_ERROR(false, 28009, "手机号码不正确"),
    LOGIN_MOBILE_ERROR(false, 28001, "账号不正确"),
    LOGIN_PASSWORD_ERROR(false, 28008, "密码不正确"),
    LOGIN_DISABLED_ERROR(false, 28002, "该用户已被禁用"),
    REGISTER_MOBLE_ERROR(false, 28003, "手机号已被注册"),
    LOGIN_AUTH(false, 28004, "需要登录"),
    LOGIN_ACL(false, 28005, "没有权限"),
    SMS_SEND_ERROR(false, 28006, "短信发送失败"),
    SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL(false, 28007, "短信发送过于频繁");


    private Boolean success;

    private Integer code;

    private String message;

    ResultCodeEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}

```

统一异常工具类

```java
package com.atguigu.guli.common.base.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author helen
 * @since 2019/9/25
 */
public class ExceptionUtils {

    public static String getMessage(Exception e) {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            // 将出错的栈信息输出到 printWriter 中
            e.printStackTrace(pw);
            pw.flush();
            sw.flush();
        } finally {
            if (sw != null) {
                try {
                    sw.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (pw != null) {
                pw.close();
            }
        }
        return sw.toString();
    }
}
```

全局统一返回结果定义

```java
package com.atguigu.guli.common.base.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author helen
 * @since 2019/12/25
 */
@Data
@ApiModel(value = "全局统一返回结果")
public class R {

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String, Object> data = new HashMap<String, Object>();

    public R(){}

    public static R ok(){
        R r = new R();
        r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        r.setCode(ResultCodeEnum.SUCCESS.getCode());
        r.setMessage(ResultCodeEnum.SUCCESS.getMessage());
        return r;
    }

    public static R error(){
        R r = new R();
        r.setSuccess(ResultCodeEnum.UNKNOWN_REASON.getSuccess());
        r.setCode(ResultCodeEnum.UNKNOWN_REASON.getCode());
        r.setMessage(ResultCodeEnum.UNKNOWN_REASON.getMessage());
        return r;
    }

    public static R setResult(ResultCodeEnum resultCodeEnum){
        R r = new R();
        r.setSuccess(resultCodeEnum.getSuccess());
        r.setCode(resultCodeEnum.getCode());
        r.setMessage(resultCodeEnum.getMessage());
        return r;
    }

    public R success(Boolean success){
        this.setSuccess(success);
        return this;
    }

    public R message(String message){
        this.setMessage(message);
        return this;
    }

    public R code(Integer code){
        this.setCode(code);
        return this;
    }

    public R data(String key, Object value){
        this.data.put(key, value);
        return this;
    }

    public R data(Map<String, Object> map){
        this.setData(map);
        return this;
    }
}
```

统一异常处理器

```java
package com.atguigu.guli.service.base.handler;

import com.atguigu.guli.common.base.result.R;
import com.atguigu.guli.common.base.result.ResultCodeEnum;
import com.atguigu.guli.common.base.util.ExceptionUtils;
import com.atguigu.guli.service.base.exception.GuliException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理器
 */
@ControllerAdvice //切面专门处理系统中的错误信息
@Slf4j  //日志记录器的注解
public class GlobalExceptionHandler {


    @ExceptionHandler(Exception.class)  //异常处理器类
    @ResponseBody  // json 格式的
    public R error(Exception e) {
//        e.printStackTrace();  //错误跟踪栈
        log.error(ExceptionUtils.getMessage(e));  //将错误信息打印到控制台
        return R.error();
    }

    //    BadSqlGrammarException  捕获指定的异常，匹配异常处理器类
    @ExceptionHandler(BadSqlGrammarException.class)  //异常处理器类
    @ResponseBody
    public R error(BadSqlGrammarException e) {
        log.error(ExceptionUtils.getMessage(e));//打印错误信息，这样会在控制台显示，没写就不会显示
        return R.setResult(ResultCodeEnum.BAD_SQL_GRAMMAR);// R.setResult 传递一个枚举类型的值
    }

    // 特定异常处理
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public R error(HttpMessageNotReadableException e) {
        log.error(ExceptionUtils.getMessage(e));//打印异常信息
        return R.setResult(ResultCodeEnum.JSON_PARSE_ERROR);
    }


    @ExceptionHandler(GuliException.class)
    @ResponseBody
    public R error(GuliException e) {
        log.error(ExceptionUtils.getMessage(e));
        return R.error().message(e.getMessage()).code(e.getCode());
    }
}
```



### MyBatisCodeHelper-Pro学习文档

https://gejun123456.github.io/MyBatisCodeHelper-Pro/#/README



# HttpServletRequest详解

HttpServletRequest对象代表客户端的请求，当客户端通过HTTP协议访问服务器时，`HTTP请求头中的所有信息都封装在这个对象中`，通过这个对象提供的方法，可以获得客户端请求的所有信息。
**获得客户机信息：**

| getRequestURL()   | 返回客户端发出请求时的完整URL。                              |
| ----------------- | ------------------------------------------------------------ |
| getRequestURI()   | 返回请求行中的参数部分。                                     |
| getQueryString () | 方法返回请求行中的参数部分（参数名+值）                      |
| getRemoteHost()   | 返回发出请求的客户机的完整主机名。                           |
| getRemoteAddr()   | 返回发出请求的客户机的IP地址。                               |
| getPathInfo()     | 返回请求URL中的额外路径信息。额外路径信息是请求URL中的位于Servlet的路径之后和查询参数之前的内容，它以"/"开头。 |
| getRemotePort()   | 返回客户机所使用的网络端口号。                               |
| getLocalAddr()    | 返回WEB服务器的IP地址。                                      |
| getLocalName()    | 返回WEB服务器的主机名。                                      |

举例：返回客户端发出请求时的完整URL

```java
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {
		String requestUrl = request.getRequestURL().toString();// 得到请求的URL地址
		resp.setCharacterEncoding("UTF-8");// 设置将字符以"UTF-8"编码输出到客户端浏览器
		// 通过设置响应头控制浏览器以UTF-8的编码显示数据，如果不加这句话，那么浏览器显示的将是乱码
		resp.setHeader("content-type", "text/html;charset=UTF-8");
		PrintWriter out = resp.getWriter();
		out.write("请求的URL地址：" + requestUrl);
	}
}
123456789101112131415161718192021222324252627
```

![在这里插入图片描述](images/20190516090451528.png)
**获得客户机请求头**

| 方法                    |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| getHeader(string name)  | 以 String 的形式返回指定请求头的值。如果该请求不包含指定名称的头，则此方法返回 null。如果有多个具有相同名称的头，则此方法返回请求中的第一个头。头名称是不区分大小写的。可以将此方法与任何请求头一起使用 |
| getHeaders(String name) | 以 String 对象的 Enumeration 的形式返回指定请求头的所有值    |
| getHeaderNames()        | 返回此请求包含的所有头名称的枚举。如果该请求没有头，则此方法返回一个空枚举 |

**获得客户机请求参数**

| getParameter(String name)       | 根据name获取请求参数(常用)                                   |
| ------------------------------- | ------------------------------------------------------------ |
| getParameterValues(String name) | 根据name获取请求参数列表(常用)                               |
| getParameterMap()               | 返回的是一个Map类型的值，该返回值记录着前端（如jsp页面）所提交请求中的请求参数和请求参数值的映射关系。(编写框架时常用) |

**请求转发：**
指一个web资源收到客户端请求后，通知服务器去调用另外一个web资源进行处理。
1.通过ServletContext的getRequestDispatcher(String path)方法，该方法返回一个RequestDispatcher对象，调用这个对象的forward方法可以实现请求转发。
例如：将请求转发的test.jsp页面

```java
RequestDispatcher reqDispatcher =this.getServletContext().getRequestDispatcher("/test.jsp");
reqDispatcher.forward(request, response);
```

2.通过request对象提供的getRequestDispatche(String path)方法，该方法返回一个RequestDispatcher对象，调用这个对象的forward方法可以实现请求转发。**
例如：将请求转发的test.jsp页面

```java
request.getRequestDispatcher("/test.jsp").forward(request, response);
```

request对象同时也是一个域对象(Map容器)，开发人员通过request对象在实现转发时，把数据通过request对象带给其它web资源处理。

```java
String data="心若静，风奈何";
        //将数据存放到request对象中,此时把request对象当作一个Map容器来使用
        request.setAttribute("data", data);
        //客户端访问RequestDemo06这个Servlet后，RequestDemo06通知服务器将请求转发(forward)到test.jsp页面进行处理
        request.getRequestDispatcher("/test.jsp").forward(request, response);
12345678
```

request对象作为一个域对象(Map容器)使用时，主要是通过以下的四个方法来操作
*
setAttribute(String name,Object o)方法，将数据作为request对象的一个属性存放到request对象中，例如：request.setAttribute(“data”, data);
*
getAttribute(String name)方法，获取request对象的name属性的属性值，例如：request.getAttribute(“data”)
*
removeAttribute(String name)方法，移除request对象的name属性，例如：request.removeAttribute(“data”)
*
getAttributeNames方法，获取request对象的所有属性名，返回的是一个，例如：Enumeration
attrNames = request.getAttributeNames()；





### 测试请求头携带信息

http://localhost:9096/api/test/

![image-20201225145412659](images/image-20201225145412659.png)

![image-20201225145757431](images/image-20201225145757431.png)

```java
@Api(tags = "获取ip地址")
@RestController
@RequestMapping("/api/")
public class IpController {

//    http://localhost:9096/api/ip
    @ApiOperation(value = "根据请求获取ip地址")
    @GetMapping("/ip")
    public String mac(HttpServletRequest request) {
        String ip = IPUtils.getIpAddr(request);
        System.out.println(ip);
        return "ip:" + ip;
    }

//    http://localhost:9096/api/test/
    @ApiOperation(value = "获取请求信息")
    @PostMapping("/test/")
    public String clintMessage(HttpServletRequest request) {
        String ip = IPUtils.getIpAddr(request);
        //获取请求头中携带的信息
        System.out.println(request.getHeader("hyc"));
        System.out.println(ip);
        return "ip:" + ip;
    }
}
```

![image-20201225145844652](images/image-20201225145844652.png)





#### Java web基础总结五之—— HttpServletRequest与HttpServletResponse

​     在前面总结过，每当客户端给Web服务器发送一个http请求，web服务器就会针对每一次请求，分别创建一个用于代表请求的request对象、和代表响应的response对象。request和response对象就代表请求和响应，所以我们可以通过request对象获得请求相关的数据和操作。通过response对象进行对响应相关的数据封装和一些其他的操作。

##### 一．HttpServletRequest与HttpServletResponse的实现类

​    我们会发现，HttpServletRequest与HttpServletResponse都是接口，那么它们在运行的时候的实现类是什么？是由谁负责实例化它们的实现类呢？

   通过对上一篇文章的HelloWorldServlet进行调试，可以得到答案。HelloWorldServlet的部分代码的截图如下所示：

![img](images/20150510121542272)

   这张图是调试时获得的：

![img](images/20150510121745859)

​     通过上面的图我们可以看到，HttpServletRequest与HttpServletResponse的对象req,resp的实际的类型是RequestFacade和ResponseFacade。这两个类都是org.apache.catalina.connector包下面的。也就是我使用的web服务器tomcat的两个类。所以还是由web容器来负责实例化HttpServletRequest与HttpServletResponse的对象。

##### 二．HttpServletRequest简介

   HttpServletRequest对象代表客户端的请求，当客户端通过HTTP协议访问服务器时，HTTP请求头中的所有信息都封装在这个对象中。

   request常用的方法和操作：

###### 1.获得客户端的信息

getRequestURL方法返回客户端发出请求时的完整URL。

getRequestURI方法返回请求行中的资源名部分，去掉主机名的部分。

getRemoteAddr方法返回发出请求的客户机的IP地址

getRemoteHost方法返回发出请求的客户机的完整主机名

getRemotePort方法返回客户机所使用的端口号

getLocalAddr方法返回WEB服务器的IP地址。

getLocalName方法返回WEB服务器的主机名

getMethod得到客户机请求方式，如GET,POST

###### 2.获得请求头的一些方法

getHead(name)方法

getHeaders(String name)方法

getHeaderNames方法

###### 3.获得请求参数，也就是客户端提交的数据的一些方法。

getParameter(name)方法

getParameterValues（String name）方法

getParameterNames方法

getParameterMap方法 

 

###### 4.HttpServletRequest实现转发

​    请求转发指一个web资源收到客户端请求后，通知服务器去调用另外一个web资源进行处理。request对象提供了一个getRequestDispatcher方法，该方法返回一个RequestDispatcher对象，调用这个对象的forward方法可以实现请求转发。

###### 5. request域

​     request对象同时也是一个域对象，我们通过request对象在实现转发时，可以把数据通过request对象带给其它web资源处理。下面是常用的一些对域中的属性的操作的方法：

setAttribute方法

getAttribute方法 

removeAttribute方法

getAttributeNames方法

###### 6. Request的getParameter和getAttribute方法的区别。

​    由于request也是一个域对象，所以既可以从它获得参数，即Parameter。也可以获得域中的属性。但是他们的意义是完全不一样的。

​    getParameter(String name)获得客户端传送给服务器的参数值,该参数是由name指定的,通常是表单中的参数。而且参数只能是字符串形式的键值对。

​     getAttribute(String name):返回有name 指定的属性值,如果指定的属性值不存在,则会返回一个null值。这里存放的也是一个键值对，不同的是，这里的值可以是任意的类型。

 

##### 三．HttpServletResponse简介

  HttpServletResponse则是对服务器的响应对象。这个对象中封装了向客户端发送数据、发送响应头，发送响应状态码的方法。

  

response常用的方法和操作：

###### 1.常用的方法

addCookie(Cookie cookie) 向客户端写入Cookie

addHeader(java.lang.String name, java.lang.String value) 写入给定的响应头

encodeURL(java.lang.Stringurl) 默认cookie中包含Session ID，如果客户端不支持 Cookie，就在参数 url 中加入 Session ID 信息，可以解决用户禁用cookie的问题。

setStatus(intsc) 设置响应的状态码。

 

###### 2. getOutputStream和getWriter方法的区别

getOutputStream和getWriter方法分别用于得到输出二进制数据、输出文本数据的ServletOuputStream、Printwriter对象。getOutputStream和getWriter这两个方法互相排斥，调用了其中的任何一个方法后，就不能再调用另一方法。 

这两个方法写入的数据会作为响应消息的正文，与响应状态行和各响应头组合后输出到客户端。Serlvet的service方法结束后，web容器将检查getWriter或getOutputStream方法返回的输出流对象是否已经调用过close方法，如果没有，web容器将调用close方法关闭该输出流对象。

###### 3. HttpServletResponse实现重定向

​    重定向指的是一个web资源收到客户端请求后，web服务器通知客户端去访问另外一个web资源，这称之为请求重定向。实现方式是调用response.sendRedirect()方法。实现的原理就是给客户端返回了302状态码和location头。

 

##### 四．转发forward和重定向Redirect的区别

​    转发是在服务器端实现的。一个web资源收到客户端请求后，通知服务器去调用另外一个web资源进行处理，称之为请求转发。调用RequestDispatcher.forward 方法的请求转发过程结束后，浏览器地址栏保持初始的URL地址不变。

​    而重定向是在客户端实现的。一个web资源收到客户端请求后，通知客户端的浏览器去访问另外一个web资源，称之为请求重定向。所以调用HttpServletResponse.sendRedirect方法重定向的访问过程结束后，浏览器地址栏中显示的URL会发生改变，由初始的URL地址变成重定向的目标URL。

​     RequestDispatcher.forward方法只能将请求转发给同一个WEB应用中的其他资源； sendRedirect方法还可以重定向到同一个站点上的其他应用程序中的资源，甚至是使用绝对URL重定向到其他站点的资源。

​     HttpServletResponse.sendRedirect方法对浏览器的请求直接作出响应，响应的结果就是告诉浏览器去重新发出对另外一个URL的访问请求；RequestDispatcher.forward方法在服务器端将请求转发给另外一个资源，相当过程于对客户端不可见。

​     RequestDispatcher.forward方法的调用者与被调用者之间共享相同的request对象和response对象，它们属于同一个访问请求和响应过程；而HttpServletResponse.sendRedirect方法调用者与被调用者使用各自的request对象和response对象，它们属于两个独立的访问请求和响应过程。也就是说，重定向生成了新的request对象和response对象。



### springboot中@EnableAsync与@Async注解使用

@Async为异步注解，放到方法上，表示调用该方法的线程与此方法异步执行，需要配合@EnableAsync注解使用。





### knife4j 开发文档

https://doc.xiaominfo.com/knife4j/



```json
{
  "address": "",
  "birthday": "1927-12-10",
  "bloodCollection": 0,
  "disappeared": "县刘集镇",
  "dna": 0,
  "email": "",
  "feature": "颜景发，男，1927年出生，于2009年11月17日走失于刘集镇吕当村，身高178厘米。",
  "hometown": "陕西,渭南,富平县刘集镇",
  "idCard": "",
  "image": "http://qwe.com",
  "missingAge": 82,
  "missingHeight": "178厘米左右",
  "missingIdCard": "142702198808184219",
  "missingPlace": "陕西,渭南,富平县刘集镇吕当村",
  "missingTime": "2009-11-17",
  "name": "颜野鸭",
   "missingName": "小明",
  "otherInformation": "颜景发，男，1927年出生，于2009年11月17日走失于刘集镇吕当村，身高178厘米。走失时上身穿蓝色中山装，下身穿黑色裤子，头戴蓝色火车头棉帽，脚穿布棉鞋，手拄着拐杖。",
  "otherInstructions": "无",
  "othercontact": "wu",
  "phone": "111",
  "qq": "111",
  "relationShip": "亲属",
  "releaseTime": "2020-12-12 12:12:12",
  "retrieve": 0,
  "sex": "男",
  "status": 0,
  "telephone": "18888888888",
  "type": 1
}
```





# **[ vue-admin-template](https://github.com/PanJiaChen/vue-admin-template)**

https://github.com/PanJiaChen/vue-admin-template/blob/master/README-zh.md



# 注解@Retention的详解

注解@Retention可以用来修饰注解，是注解的注解，称为`元注解`。
Retention注解有一个属性value，是RetentionPolicy类型的，Enum RetentionPolicy是一个枚举类型，
这个枚举决定了Retention注解应该如何去保持，也可理解为Rentention 搭配 RententionPolicy使用。
RetentionPolicy有3个值：CLASS RUNTIME SOURCE

按生命周期来划分可分为3类：
1、RetentionPolicy.SOURCE：注解只保留在源文件，当Java文件编译成class文件的时候，注解被遗弃；
2、RetentionPolicy.CLASS：注解被保留到class文件，但jvm加载class文件时候被遗弃，这是默认的生命周期；
3、RetentionPolicy.RUNTIME：注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在；
这3个生命周期分别对应于：Java源文件(.java文件) —> .class文件 —> 内存中的字节码。

那怎么来选择合适的注解生命周期呢？
首先要明确`生命周期长度 SOURCE < CLASS < RUNTIME` ，所以`前者能作用的地方后者一定也能作用。`
`一般如果需要在运行时去动态获取注解信息，那只能用 RUNTIME 注解`，比如@Deprecated使用RUNTIME注解

​		注解@Deprecated，用来表示某个类或属性或方法已经过时，不想别人再用时，在属性和方法上用@Deprecated修饰

```java
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value={CONSTRUCTOR, FIELD, LOCAL_VARIABLE, METHOD, PACKAGE, PARAMETER, TYPE})
public @interface Deprecated {
}
```



如果要在编译时进行一些预处理操作，比如生成一些辅助代码（如 ButterKnife），就用 CLASS注解；
如果只是做一些检查性的操作，比如 @Override 和 @SuppressWarnings，使用SOURCE 注解。

​		注解@Override用在方法上，当我们想重写一个方法时，在方法上加@Override，当我们方法的名字出错时，编译器就会报错

```java
package java.lang;

import java.lang.annotation.*;

/**
 * Indicates that a method declaration is intended to override a
 * method declaration in a supertype. If a method is annotated with
 * this annotation type compilers are required to generate an error
 * message unless at least one of the following conditions hold:
 *
 * <ul><li>
 * The method does override or implement a method declared in a
 * supertype.
 * </li><li>
 * The method has a signature that is override-equivalent to that of
 * any public method declared in {@linkplain Object}.
 * </li></ul>
 *
 * @author  Peter von der Ah&eacute;
 * @author  Joshua Bloch
 * @jls 9.6.1.4 @Override
 * @since 1.5
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface Override {
}
```



​		注解@SuppressWarnings用来压制程序中出来的警告，比如在没有用泛型或是方法已经过时的时候

```java
package java.lang;

import java.lang.annotation.*;
import static java.lang.annotation.ElementType.*;

/**
 * Indicates that the named compiler warnings should be suppressed in the
 * annotated element (and in all program elements contained in the annotated
 * element).  Note that the set of warnings suppressed in a given element is
 * a superset of the warnings suppressed in all containing elements.  For
 * example, if you annotate a class to suppress one warning and annotate a
 * method to suppress another, both warnings will be suppressed in the method.
 *
 * <p>As a matter of style, programmers should always use this annotation
 * on the most deeply nested element where it is effective.  If you want to
 * suppress a warning in a particular method, you should annotate that
 * method rather than its class.
 *
 * @author Josh Bloch
 * @since 1.5
 * @jls 4.8 Raw Types
 * @jls 4.12.2 Variables of Reference Type
 * @jls 5.1.9 Unchecked Conversion
 * @jls 5.5.2 Checked Casts and Unchecked Casts
 * @jls 9.6.3.5 @SuppressWarnings
 */
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE})
@Retention(RetentionPolicy.SOURCE)
public @interface SuppressWarnings {
    /**
     * The set of warnings that are to be suppressed by the compiler in the
     * annotated element.  Duplicate names are permitted.  The second and
     * successive occurrences of a name are ignored.  The presence of
     * unrecognized warning names is <i>not</i> an error: Compilers must
     * ignore any warning names they do not recognize.  They are, however,
     * free to emit a warning if an annotation contains an unrecognized
     * warning name.
     *
     * <p> The string {@code "unchecked"} is used to suppress
     * unchecked warnings. Compiler vendors should document the
     * additional warning names they support in conjunction with this
     * annotation type. They are encouraged to cooperate to ensure
     * that the same names work across multiple compilers.
     * @return the set of warnings to be suppressed
     */
    String[] value();
}

```



# JAVA注解学习-@Documented注解

这个注解只是用来标注生成javadoc的时候是否会被记录。

用法

在自定义注解的时候可以使用@Documented来进行标注，如果使用@Documented标注了，在生成javadoc的时候就会把@Documented注解给显示出来。

总结

@Documented注解只是用来做标识，没什么实际作用，了解就好。



@Documented注解

Documented注解表明这个注释是由 javadoc记录的，在默认情况下也有类似的记录工具。 如果一个类型声明被注释了文档化，它的注释成为公共API的一部分。



@Target({ElementType.TYPE})注解

ElementType 这个枚举类型的常量提供了一个简单的分类：注释可能出现在Java程序中的语法位置（这些常量与元注释类型(@Target)一起指定在何处写入注释的合法位置）



# 数据字典是什么

> # **数据字典**

**数据字典是一种通用的程序设计方法。可以认为，不论什么程序，都是为了处理一定的主体，这里的主体可能是人员、商品（超子）、网页、接口、数据库表、甚至需求分析等等。当主体有很多的属性，每种属性有很多的取值，而且属性的数量和属性取值的数量是不断变化的，特别是当这些数量的变化很快时，就应该考虑引入数据字典的设计方法。**

> # ***\*数据字典有两种形式\****

**一、把主体的属性代码化放入独立的表中，不是和主体放在一起，主体中只保留属性的代码。这里属性的数量是不变的，而属性取值的数量可以是变化的。**

**二、用一个表来放结构相同的所有属性信息，不同属性的不同取值统一编码，用“类型”来区别不同的属性，主体中保留属性代码的列表。这样主体所拥有的属性数量就是可变的了。**

第二种数据字典比第一种更抽象，层级更高，也更具一般性、通用性。

这两种形式的归纳有些抽象，为说明这两种数据字典和它们的各种优点，下面举个简单的例子来说明：

现在有个需求，要在程序中处理“职员”信息。这里的主体就是“职员”，开始时“职员”有“国籍”、“证件”和“学历”等属性。

比如，对于一个“职员信息”页面上的“国籍”下拉列表，我们可以就用第一种的数据字典来存储不同的国家。如果不采取这样的方法，就需要手动的把所有可能的国家名称敲到页面上。这首先有个效率的问题，每个需要用到国籍的地方都要敲一次，要敲多久？还有，如果有一天，像南斯拉夫，突然国家换名了，是不是要所有涉及的页面都要手动地改变呢？

又比如，如果有一天一个代码的名称需要换一个，是不是要到数据库中把已经经存在的所有数据都更新一遍呢？如“证件”，现在叫“身份证”，有一天想改为叫“居民身份证”。原来如果没有用数据字典，就意味着，要把“身份证”这几个字存到《职员表》等信息表中：

> 《职员表》
>
> 姓名　　证件　　性别
>
> 张三　　身份证　男
>
> 李四    身份证  女
>
> ....

这样，改名后就要手动改数据库。但如果使用了数据字典，《职员表》里面存的就是：

> 《职员表》
>
> 姓名　　证件　　性别
>
> 张三　　001      男
>
> 李四    001      女
>
> ....

另外增加了《证件表》：

> 《证件表》
>
> 证件id  证件名
>
> 001   身份证
>
> 002   暂住证
>
> ...

《证件表》就是第一种数据字典。要改变证件名称时，只要把其中的“身份证”改成“居民身份证”就好了，只需修改一次。而且，《职员表》不用做任何修改，页面上如果用到“证件”，也不用做修改。

还有在程序中有时需要判断业务逻辑时，用：“select * from 职员表 where证件= ***”，原来***是“身份证”，使用数据字典后，就是001。证件改名后，就不用手动到程序里去改，程序也就不用重新测试、发布等等。

但第一种数据字典也有局限性。

使用第一种数据字典后，程序中除“职员”类外，还就需要有一个“国籍”类、一个“证件”类和一个“学历”类，对应的数据库中也需要增加一张“国籍”表、一张“证件”表和一张“学历”表。“职员”类则需要包含一个对“国籍”类的引用、一个对“证件”类的引用和一个对“学历”类的引用，对应的数据库中“职员”表中也需要三个外键分别指向“国籍”表、“证件”表和“学历”表。这样的设计在类似“国籍”和“学历”这样的属性比较少时是可行的，但是随着系统复杂性的增加，系统中会出现大量结构类似的信息表和信息类，数量一直会增加到一个不可接受的地步。这里的“职员”，已经有了国籍、证件和学历三个属性，但如果职员还要增加“职位”属性，那么必然要多出个“职位表”，如果还有其它...那即，当取得一条主体的完全数据时，那将进行几十个表的联接（join）操作。

如何解决呢？

通过分析上述问题，可以发现的一个特征是：这些信息类的内容都是需要动态维护的，但是所需的属性是一样的，对应的数据库表中包含的字段也是一样的。关键的字段就是两个：“标识”和“名称”。“标识”用于表示不变的主键，“名称”用于表示程序界面上显示的文字。

第二种数据字典就是为了解决上述问题而设计的。

还是以上面的例子为例。在系统中去掉《国籍表》、《证件表》、《学历表》….，引入《系统代码分类表》和《系统代码表》:

> 《系统代码分类表》
>
> 分类标识      分类名称
>
> Country       国籍
>
> ID            证件
>
> …

> 《系统代码表》
>
> 标识          分类         内容
>
> 001          Contry       中国
>
> 002          Contry       美国
>
> …..
>
> 501          ID          身份证
>
> 502          ID          暂住证
>
> ……

《系统代码表》的“分类”字段都指向《系统代码分类表》中的“分类标识”。这样，在程序需要获得国籍信息时，只要通过“Country”这个标识去《系统代码表》中检索就可以了。这样的设计也便于建立一个单独的程序模块来维护所有的这些公共信息。

对于《职员表》，使用第一种数据字典时，其表结构是：

职员ID、姓名、国籍ID、证件ID、学历ID…….

 

采用第二种数据字典后，其表结构是：

职员ID、姓名

另外增加《属性表》，该表是《职员表》和《系统代码表》的关系表，其表结构是：

属性ID、职员ID、系统代码表_标识

如：

> 《职员表》
>
> 职员ID      姓名
>
> 1           张三
>
> 2           李四
>
> …..

> 《属性表》
>
> 属性ID      职员ID        系统代码表_标识
>
> 1             1             001 （表示张三是中国籍）
>
> 2             1             501 （表示张三的证件是身份证）
>
> 3             2             002 （表示李四是美国籍）
>
> 4             2             501 （表示李四的证件是身份证）
>
> …..

可以看出《职员表》的设计大为简化，系统也更加灵活了，完全可以适应主体属性的大量变更了。程序的设计应用第二种数据字典时和数据库表的方法一样。

> # **数据字典的\**优点\****

1. 在一定程度上，通过系统维护人员即可改变系统的行为（功能），不需要开发人员的介入。使得系统的变化更快，能及时响应客户和市场的需求。
2. 提高了系统的灵活性、通用性，减少了主体和属性的耦合度
3. 简化了主体类的业务逻辑
4. 能减少对系统程序的改动，使数据库、程序和页面更稳定。特别是数据量大的时候，能大幅减少开发工作量
5. 使数据库表结构和程序结构条理上更清楚，更容易理解，在可开发性、可扩展性、可维护性、系统强壮性上都有优势。

> # **数据字典的\**缺点\****

1. 数据字典是通用的设计，在系统效率上会低一些。
2. 程序算法相对复杂一些。
3. 对于开发人员，需要具备一定抽象思维能力，所以对开发人员的要求较高。

所以，当属性的数量不多时，用第一种数据字典即可。对于大型的，未定型的系统，可以采用第二种数据字典来设计。至于具体的系统里怎么设计，还是要在看实际情况来找寻通用性和效率二者间的平衡。无论怎么做，关系理论和范式仍是基础。

> # ***\*数据字典的一般设计\****

下面给出一个用数据库实现的第二种数据字典表的设计。要注意这个设计不是唯一的，完全可以用XML、字符串等形式来设计数据字典。

数据字典表(Dictionary)：

| 字段名   | 类型         | 说明                                                         |
| -------- | ------------ | ------------------------------------------------------------ |
| 编号     | Char(16)     | 间断增量(Not Null,PK)                                        |
| 分类名称 | Varchar(64)  | 用来进行过滤选取字典表相关域                                 |
| 内容     | Varchar(255) |                                                              |
| 父级编号 | Char(16)     | 取Dictionary的编号(FK)，用来进行等级设计。使之成为树型结构。 |





```java
@Test
void test4() {
    int[] arr = {1, 3, 5, 7, 9};
    int length = arr.length;
    //进行数组的遍历
    for (int i = 0; i < length; i++){
        System.out.println(arr[i]);
    }
}
```



前后端进行数据交互时候 要优先考虑json格式即键值对形式,[{}] 列表形式 等便于操作的数据结构





# [SpringMVC学习(十)——SpringMVC与前台的json数据交互](https://www.cnblogs.com/telwanggs/p/6912637.html)

json数据格式在接口调用中、html页面中比较常用，json格式比较简单，解析也比较方便，所以使用很普遍。在SpringMVC中，也支持对json数据的解析和转换，这篇文章主要总结一下在SpringMVC中如何和前台交互json数据。

# 两种交互形式

SpringMVC和前台交互主要有两种形式，如下图所示： 
![img](images/48590-20170527140316950-1204883095.png)
可以看出，前台传过来的方式有两种，一种是传json格式的数据过来，另一种就是在url的末尾传普通的key/value串过来，针对这两种方式，在Controller类中会有不同的解析，但是在Controller类中返回的json格式的数据都是一样的。下面来具体分析一下SpringMVC是如何与前台进行json数据的交互的。在讲之前先认识两个注解。

# @RequestBody注解

@RequestBody注解用于读取http请求的内容(字符串)，通过SpringMVC提供的HttpMessageConverter接口将读到的内容转换为json、xml等格式的数据并绑定到Controller类方法的参数上。 
本例子应用：@RequestBody注解实现接收http请求的json数据，将json数据转换为java对象。如下： 
![img](images/48590-20170527140328825-1849477734.png)

![img](images/48590-20170527140344419-1742253803.png)

# @ResponseBody注解

@ResponseBody注解用于将Controller类的方法返回的对象，通过HttpMessageConverter接口转换为指定格式的数据如：json、xml等，通过Response响应给客户端。 
本例子应用：@ResponseBody注解实现将Controller类方法返回对象转换为json响应给客户端，如下： 
![img](images/48590-20170527140357779-25279971.png)

![img](images/48590-20170527140545669-1401070681.png)
经过我如此细致地讲解，想必大家已认识到这两个注解的意思了。好了，下面来具体分析一下SpringMVC是如何与前台进行json数据的交互的。

# 环境的准备

## 加载json的jar包

SpringMVC默认用MappingJacksonHttpMessageConverter对json数据进行转换，需要加入jackson的包，又因为SpringMVC3和SpringMVC4针对json交互的jar包有区别，我用的是SpringMVC4，需要导入如下三个jar包： 
![img](images/48590-20170527140627857-1048637068.png) 
读者千万不要忘了导入jQuery的类库，因为我是使用jQuery的ajax提交json串的，就像下图这样： 
![img](images/48590-20170527140637622-2073293882.png)

## 配置json转换器

配置json转换器有两种方式，如果是配置了注解适配器org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter的话，需要在该适配器中配置json转换器，如下：

```xml
<!-- 用于将对象转换为 JSON  -->  
<bean id="stringConverter" class="org.springframework.http.converter.StringHttpMessageConverter">  
    <property name="supportedMediaTypes">  
        <list>  
            <value>text/plain;charset=UTF-8</value>  
        </list>  
    </property>  
</bean>  
<bean id="jsonConverter" class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter"></bean>  

<bean class="org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter">  
    <property name="messageConverters">  
        <list>  
            <ref bean="stringConverter" />  
            <ref bean="jsonConverter" />  
        </list>  
    </property>  
</bean>  
```

但是如果使用``注解驱动的话就不用以上的配置了，默认已经配好了。建议使用这种，比较方便。

# json交互的测试

这里，我使用jQuery的ajax提交json串，对输出的json结果进行解析。前台的程序如下：

```javascript
<button onclick="sendJson()">json数据交互测试</button>
<script type="text/javascript">
    function sendJson() {
        $.ajax({
            type:"post",
            url:"${pageContext.request.contextPath }/item/json_test.action",
            data:'{"id":"1","name":"电冰箱","price":"1999"}',
            contentType:"application/json;charset=utf-8",
            success:function(data) {
                alert(data.id + ":" + data.name);
            }
        });
    }
</script>
```

那么前台itemList.jsp页面的内容就应改造为： 
![img](images/48590-20170527140656810-388787205.png)
接着编辑ItemController类，并在该类中编写如下方法：

```java
// JSON数据交互
// @RequestBody：接收json数据并转换成pojo对象
// @ResponseBody：响应json数据，把pojo对象转换成json数据并响应
@RequestMapping("/json_test")
@ResponseBody
public Items jsonTest(@RequestBody Items items) {
    return items;
}
```

由于前台传的是id、name和price三个属性，所以在后台就用Items类来接收了，这个类中也有这三个属性。重点是@RequestBody注解，它是将前台传过来的json串转换成items对象，然后再将该对象return回去，通过@ResponseBody注解将items对象转成json格式返回给前台。这样前台接收到了后就可以解析了。我们看一下测试的结果： 
![img](images/48590-20170527140714419-1538346551.png)
响应的结果，null表示空值而已，这里就返回了原对象，也就只有id、name和price属性。 
![img](images/48590-20170527140758325-95009939.png)
springmvc与前台json数据的交互就总结这么多。



https://blog.csdn.net/yerenyuan_pku/article/details/72514022





# [SpringBoot实现前后端数据交互、json数据交互、Controller接收参数的几种常用方式](https://blog.csdn.net/qq_20957669/article/details/89227840?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.control)





# [Failed to convert value of type 'java.lang.String' to required type 'java.util.Date';的方法](https://developer.aliyun.com/article/652831)





# [手把手带你入门 Spring Security！](https://www.cnblogs.com/lenve/p/11242055.html)

Spring Security 是 Spring 家族中的一个安全管理框架，实际上，在 Spring Boot 出现之前，Spring Security 就已经发展了多年了，但是使用的并不多，安全管理这个领域，一直是 Shiro 的天下。

相对于 Shiro，在 SSM/SSH 中整合 Spring Security 都是比较麻烦的操作，所以，Spring Security 虽然功能比 Shiro 强大，但是使用反而没有 Shiro 多（Shiro 虽然功能没有 Spring Security 多，但是对于大部分项目而言，Shiro 也够用了）。

自从有了 Spring Boot 之后，Spring Boot 对于 Spring Security 提供了 自动化配置方案，可以零配置使用 Spring Security。

因此，一般来说，常见的安全管理技术栈的组合是这样的：

- SSM + Shiro
- Spring Boot/Spring Cloud + Spring Security

**注意，这只是一个推荐的组合而已，如果单纯从技术上来说，无论怎么组合，都是可以运行的。**

我们来看下具体使用。

# 1.项目创建

在 Spring Boot 中使用 Spring Security 非常容易，引入依赖即可：

![img](images/747810-20190725084928495-101065734.png)

pom.xml 中的 Spring Security 依赖：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

只要加入依赖，项目的所有接口都会被自动保护起来。

# 2.初次体验

我们创建一个 HelloController:

```java
@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }
}
```

访问 `/hello` ，需要登录之后才能访问。

![img](images/747810-20190725084941700-348719928.png)

当用户从浏览器发送请求访问 `/hello` 接口时，服务端会返回 `302` 响应码，让客户端重定向到 `/login` 页面，用户在 `/login` 页面登录，登陆成功之后，就会自动跳转到 `/hello` 接口。

另外，也可以使用 `POSTMAN` 来发送请求，使用 `POSTMAN` 发送请求时，可以将用户信息放在请求头中（这样可以避免重定向到登录页面）：

![img](images/747810-20190725084955603-316397913.png)

通过以上两种不同的登录方式，可以看出，Spring Security 支持两种不同的认证方式：

- 可以通过 form 表单来认证
- 可以通过 HttpBasic 来认证

# 3.用户名配置

默认情况下，登录的用户名是 `user` ，密码则是项目启动时随机生成的字符串，可以从启动的控制台日志中看到默认密码：

![img](images/747810-20190725085007857-523177844.png)

这个随机生成的密码，每次启动时都会变。对登录的用户名/密码进行配置，有三种不同的方式：

- 在 application.properties 中进行配置
- 通过 Java 代码配置在内存中
- 通过 Java 从数据库中加载

前两种比较简单，第三种代码量略大，本文就先来看看前两种，第三种后面再单独写文章介绍，也可以参考我的[微人事项目](https://www.cnblogs.com/lenve/p/11242055.html)。

## 3.1 配置文件配置用户名/密码

可以直接在 application.properties 文件中配置用户的基本信息：

```properties
spring.security.user.name=javaboy
spring.security.user.password=123
```

配置完成后，重启项目，就可以使用这里配置的用户名/密码登录了。

## 3.2 Java 配置用户名/密码

也可以在 Java 代码中配置用户名密码，首先需要我们创建一个 Spring Security 的配置类，集成自 WebSecurityConfigurerAdapter 类，如下：

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //下面这两行配置表示在内存中配置了两个用户
        auth.inMemoryAuthentication()
                .withUser("javaboy").roles("admin").password("$2a$10$OR3VSksVAmCzc.7WeaRPR.t0wyCsIj24k0Bne8iKWV1o.V9wsP8Xe")
                .and()
                .withUser("lisi").roles("user").password("$2a$10$p1H8iWa8I4.CA.7Z8bwLjes91ZpY.rYREGHQEInNtAp4NzL6PLKxi");
    }
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
```

这里我们在 configure 方法中配置了两个用户，用户的密码都是加密之后的字符串(明文是 123)，从 Spring5 开始，强制要求密码要加密，如果非不想加密，可以使用一个过期的 PasswordEncoder 的实例 NoOpPasswordEncoder，但是不建议这么做，毕竟不安全。

Spring Security 中提供了 BCryptPasswordEncoder 密码编码工具，可以非常方便的实现密码的加密加盐，相同明文加密出来的结果总是不同，这样就不需要用户去额外保存`盐`的字段了，这一点比 Shiro 要方便很多。

# 4.登录配置

对于登录接口，登录成功后的响应，登录失败后的响应，我们都可以在 WebSecurityConfigurerAdapter 的实现类中进行配置。例如下面这样：

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    VerifyCodeFilter verifyCodeFilter;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(verifyCodeFilter, UsernamePasswordAuthenticationFilter.class);
        http
        .authorizeRequests()//开启登录配置
        .antMatchers("/hello").hasRole("admin")//表示访问 /hello 这个接口，需要具备 admin 这个角色
        .anyRequest().authenticated()//表示剩余的其他接口，登录之后就能访问
        .and()
        .formLogin()
        //定义登录页面，未登录时，访问一个需要登录之后才能访问的接口，会自动跳转到该页面
        .loginPage("/login_p")
        //登录处理接口
        .loginProcessingUrl("/doLogin")
        //定义登录时，用户名的 key，默认为 username
        .usernameParameter("uname")
        //定义登录时，用户密码的 key，默认为 password
        .passwordParameter("passwd")
        //登录成功的处理器
        .successHandler(new AuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication authentication) throws IOException, ServletException {
                    resp.setContentType("application/json;charset=utf-8");
                    PrintWriter out = resp.getWriter();
                    out.write("success");
                    out.flush();
                }
            })
            .failureHandler(new AuthenticationFailureHandler() {
                @Override
                public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse resp, AuthenticationException exception) throws IOException, ServletException {
                    resp.setContentType("application/json;charset=utf-8");
                    PrintWriter out = resp.getWriter();
                    out.write("fail");
                    out.flush();
                }
            })
            .permitAll()//和表单登录相关的接口统统都直接通过
            .and()
            .logout()
            .logoutUrl("/logout")
            .logoutSuccessHandler(new LogoutSuccessHandler() {
                @Override
                public void onLogoutSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication authentication) throws IOException, ServletException {
                    resp.setContentType("application/json;charset=utf-8");
                    PrintWriter out = resp.getWriter();
                    out.write("logout success");
                    out.flush();
                }
            })
            .permitAll()
            .and()
            .httpBasic()
            .and()
            .csrf().disable();
    }
}
```

我们可以在 successHandler 方法中，配置登录成功的回调，如果是前后端分离开发的话，登录成功后返回 JSON 即可，同理，failureHandler 方法中配置登录失败的回调，logoutSuccessHandler 中则配置注销成功的回调。

# 5.忽略拦截

如果某一个请求地址不需要拦截的话，有两种方式实现：

- 设置该地址匿名访问
- 直接过滤掉该地址，即该地址不走 Spring Security 过滤器链

推荐使用第二种方案，配置如下：

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/vercode");
    }
}
```

Spring Security 另外一个强大之处就是它可以结合 OAuth2 ，玩出更多的花样出来，这些我们在后面的文章中再和大家细细介绍。

本文就先说到这里，有问题欢迎留言讨论。



# [JSP会被淘汰吗](https://blog.csdn.net/Nidson_IT/article/details/79381033?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control)



renren安全框架

```
INSERT INTO `sys_config` (`param_key`, `param_value`, `status`, `remark`) VALUES ('CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('30', '1', '文件上传', 'modules/oss/oss.html', 'sys:oss:all', '1', 'fa fa-file-image-o', '6');

```





# [干货|一个案例学会Spring Security 中使用 JWT](https://www.cnblogs.com/lenve/p/10668836.html)



`https://felord.cn`来及时获取相关的干货

https://www.felord.cn/pre-learn-spring-security-shiro.html



## 回家网重新建库 tc_sqhjw

原始数据：

```mysql
-- auto-generated definition
create table MISSING_PEOPLE
(
    ID                 NUMBER(12)                not null
        constraint MISSING_PEOPLE_PK
            primary key,
    TYPE               NUMBER(12)     default 1  not null,
    MISSING_NAME       VARCHAR2(50)   default '' not null,
    SEX                VARCHAR2(50)   default '' not null,
    MISSING_ID_CARD    VARCHAR2(50)   default '',
    BIRTHDAY           DATE           default SYSDATE,
    MISSING_HEIGHT     VARCHAR2(50)   default '',
    MISSING_AGE        NUMBER(12)     default -1,
    MISSING_TIME       DATE           default SYSDATE,
    MISSING_PLACE      VARCHAR2(500)  default '',
    HOME_TOWN          VARCHAR2(50)   default '',
    BLOOD_COLLECTION   NUMBER(12)     default 0,
    DNA                NUMBER(12)     default 0,
    IMAGE              VARCHAR2(1000) default '',
    FEATURE            VARCHAR2(200)  default '',
    DISAPPEARED        VARCHAR2(200)  default '',
    OTHER_INFORMATION  VARCHAR2(300)  default '',
    OTHER_INSTRUCTIONS VARCHAR2(300)  default '',
    RELEASE_TIME       TIMESTAMP(6),
    STATUS             NUMBER(12)     default 0,
    RETRIEVE           NUMBER(12)     default 0
)
/

comment on column MISSING_PEOPLE.ID is '寻亲编号'
/

comment on column MISSING_PEOPLE.TYPE is '寻亲类别(1,家寻信息 2,寻家信息, 3、其他寻人)'
/

comment on column MISSING_PEOPLE.MISSING_NAME is '姓名'
/

comment on column MISSING_PEOPLE.SEX is '性别'
/

comment on column MISSING_PEOPLE.MISSING_ID_CARD is '身份证号'
/

comment on column MISSING_PEOPLE.BIRTHDAY is '出生日期'
/

comment on column MISSING_PEOPLE.MISSING_HEIGHT is '失踪时身高'
/

comment on column MISSING_PEOPLE.MISSING_AGE is '失踪时年龄已审寻亲'
/

comment on column MISSING_PEOPLE.MISSING_TIME is '失踪时间'
/

comment on column MISSING_PEOPLE.MISSING_PLACE is '失踪人所在地'
/

comment on column MISSING_PEOPLE.HOME_TOWN is '失踪人籍贯'
/

comment on column MISSING_PEOPLE.BLOOD_COLLECTION is '是否采血(0 否,1 是)'
/

comment on column MISSING_PEOPLE.DNA is 'DNA是否入全国打拐数据库(0 否,1 是)'
/

comment on column MISSING_PEOPLE.IMAGE is '失踪人图片信息'
/

comment on column MISSING_PEOPLE.FEATURE is '寻亲者特征描述，家庭背景以及线索资料'
/

comment on column MISSING_PEOPLE.DISAPPEARED is '失踪经过'
/

comment on column MISSING_PEOPLE.OTHER_INFORMATION is '其他资料'
/

comment on column MISSING_PEOPLE.OTHER_INSTRUCTIONS is '其他说明'
/

comment on column MISSING_PEOPLE.RELEASE_TIME is '发布时间'
/

comment on column MISSING_PEOPLE.STATUS is '待审寻亲 0 | 已审寻亲 1 | 审核未通过寻亲 2'
/

comment on column MISSING_PEOPLE.RETRIEVE is '待找回寻亲(0) | 已找回寻亲(1)'
/





-- auto-generated definition
create table CONTACT_PERSON
(
    ID            NUMBER(12)              not null
        constraint CONTACT_PERSON_PK
            primary key,
    NAME          VARCHAR2(50) default '' not null,
    RELATION_SHIP VARCHAR2(50) default '' not null,
    TELEPHONE     VARCHAR2(50) default '' not null,
    PHONE         VARCHAR2(50) default '',
    EMAIL         VARCHAR2(50) default '',
    QQ            VARCHAR2(50) default '',
    ADDRESS       VARCHAR2(50) default '',
    OTHER_CONTACT VARCHAR2(50) default '',
    ID_CARD       VARCHAR2(50) default '',
    IP            VARCHAR2(50),
    MISS_ID       NUMBER(12)
)
/

comment on column CONTACT_PERSON.ID is '寻人编号'
/

comment on column CONTACT_PERSON.NAME is '联系人姓名'
/

comment on column CONTACT_PERSON.RELATION_SHIP is '联系人与失踪人关系'
/

comment on column CONTACT_PERSON.TELEPHONE is '手机'
/

comment on column CONTACT_PERSON.PHONE is '电话'
/

comment on column CONTACT_PERSON.EMAIL is '邮箱'
/

comment on column CONTACT_PERSON.QQ is 'QQ号'
/

comment on column CONTACT_PERSON.ADDRESS is '地址;'
/

comment on column CONTACT_PERSON.OTHER_CONTACT is '其他联系方式'
/

comment on column CONTACT_PERSON.ID_CARD is '联系人身份证号'
/

comment on column CONTACT_PERSON.IP is '发布人的ip地址'
/

comment on column CONTACT_PERSON.MISS_ID is '失踪人员编号'
/


```







```mysql
--创建数据库新用户，tc_sqhjw 为数据库用户，Dzzwk_5827 为密码
create user tc_sqhjw IDENTIFIED BY Dzzwk_5827;
--把connect，resource权限授权给新用户
grant CONNECT,resource to tc_sqhjw;
--把dba权限授权给新用户
grant dba to tc_sqhjw





create table MISSING_PEOPLE
(
    ID                 NUMBER(12)                not null
        constraint MISSING_PEOPLE_PK
            primary key,
    TYPE               NUMBER(12)     default 1  not null,
    MISSING_NAME       VARCHAR2(50)   default '' not null,
    SEX                VARCHAR2(50)   default '' not null,
    MISSING_ID_CARD    VARCHAR2(50)   default '',
    BIRTHDAY           DATE           default SYSDATE,
    MISSING_HEIGHT     VARCHAR2(50)   default '',
    MISSING_AGE        NUMBER(12)     default -1,
    MISSING_TIME       DATE           default SYSDATE,
    MISSING_PLACE      VARCHAR2(500)  default '',
    HOME_TOWN          VARCHAR2(50)   default '',
    BLOOD_COLLECTION   NUMBER(12)     default 0,
    DNA                NUMBER(12)     default 0,
    IMAGE              VARCHAR2(1000) default '',
    FEATURE            VARCHAR2(200)  default '',
    DISAPPEARED        VARCHAR2(200)  default '',
    OTHER_INFORMATION  VARCHAR2(300)  default '',
    OTHER_INSTRUCTIONS VARCHAR2(300)  default '',
    RELEASE_TIME       DATE                     ,
    
    
    STATUS             VARCHAR2(12)     default 0,
    RETRIEVE           VARCHAR2(12)     default 0
)
/

comment on column MISSING_PEOPLE.ID is '寻亲编号'
/

comment on column MISSING_PEOPLE.TYPE is '寻亲类别(1,家寻信息 2,寻家信息, 3、其他寻人)'
/

comment on column MISSING_PEOPLE.MISSING_NAME is '姓名'
/

comment on column MISSING_PEOPLE.SEX is '性别'
/

comment on column MISSING_PEOPLE.MISSING_ID_CARD is '身份证号'
/

comment on column MISSING_PEOPLE.BIRTHDAY is '出生日期'
/

comment on column MISSING_PEOPLE.MISSING_HEIGHT is '失踪时身高'
/

comment on column MISSING_PEOPLE.MISSING_AGE is '失踪时年龄已审寻亲'
/

comment on column MISSING_PEOPLE.MISSING_TIME is '失踪时间'
/

comment on column MISSING_PEOPLE.MISSING_PLACE is '失踪人所在地'
/

comment on column MISSING_PEOPLE.HOME_TOWN is '失踪人籍贯'
/

comment on column MISSING_PEOPLE.BLOOD_COLLECTION is '是否采血(0 否,1 是)'
/

comment on column MISSING_PEOPLE.DNA is 'DNA是否入全国打拐数据库(0 否,1 是)'
/

comment on column MISSING_PEOPLE.IMAGE is '失踪人图片信息'
/

comment on column MISSING_PEOPLE.FEATURE is '寻亲者特征描述，家庭背景以及线索资料'
/

comment on column MISSING_PEOPLE.DISAPPEARED is '失踪经过'
/

comment on column MISSING_PEOPLE.OTHER_INFORMATION is '其他资料'
/

comment on column MISSING_PEOPLE.OTHER_INSTRUCTIONS is '其他说明'
/

comment on column MISSING_PEOPLE.RELEASE_TIME is '发布时间'
/

comment on column MISSING_PEOPLE.STATUS is '待审寻亲 0 | 已审寻亲 1 | 审核未通过寻亲 2'
/

comment on column MISSING_PEOPLE.RETRIEVE is '待找回寻亲(0) | 已找回寻亲(1)'
/



http://10.5.55.40/uploadfile/webfile/20210108/61000001610075471840.jpg




create table CONTACT_PERSON
(
    ID            NUMBER(12)              not null
        constraint CONTACT_PERSON_PK
            primary key,
    NAME          VARCHAR2(50) default '' not null,
    RELATION_SHIP VARCHAR2(50) default '' not null,
    TELEPHONE     VARCHAR2(50) default '' not null,
    PHONE         VARCHAR2(50) default '',
    EMAIL         VARCHAR2(50) default '',
    QQ            VARCHAR2(50) default '',
    ADDRESS       VARCHAR2(50) default '',
    OTHER_CONTACT VARCHAR2(50) default '',
    ID_CARD       VARCHAR2(50) default '',
    IP            VARCHAR2(50),
    MISS_ID       NUMBER(12)
)
/

comment on column CONTACT_PERSON.ID is '寻人编号'
/

comment on column CONTACT_PERSON.NAME is '联系人姓名'
/

comment on column CONTACT_PERSON.RELATION_SHIP is '联系人与失踪人关系'
/

comment on column CONTACT_PERSON.TELEPHONE is '手机'
/

comment on column CONTACT_PERSON.PHONE is '电话'
/

comment on column CONTACT_PERSON.EMAIL is '邮箱'
/

comment on column CONTACT_PERSON.QQ is 'QQ号'
/

comment on column CONTACT_PERSON.ADDRESS is '地址;'
/

comment on column CONTACT_PERSON.OTHER_CONTACT is '其他联系方式'
/

comment on column CONTACT_PERSON.ID_CARD is '联系人身份证号'
/

comment on column CONTACT_PERSON.IP is '发布人的ip地址'
/

comment on column CONTACT_PERSON.MISS_ID is '失踪人员编号'
/





```



### @RequestMapping中的produces的作用和使用方式

1.produces：指定返回值类型和返回值编码

2.consumes：指定处理请求的提交内容类型（Content-Type），例如 application/json, text/html;

一、produces的例子

produces 第一种使用，返回json数据，下边的代码可以省略produces属性，因为我们已经使用了注解@responseBody就是返回值是json数据：

```java
@Controller  
@RequestMapping(value = "/pets/{petId}", method = RequestMethod.GET, produces="application/json")  
@ResponseBody  
```

produces 第二种使用，返回json数据的字符编码为utf-8.：


```java
@Controller  
@RequestMapping(value = "/pets/{petId}", produces="MediaType.APPLICATION_JSON_VALUE"+";charset=utf-8")  
@ResponseBody  
```

二、consumes的例子（方法仅处理request Content-Type为“application/json”类型的请求。）


```java
@Controller  
@RequestMapping(value = "/pets", method = RequestMethod.POST, consumes="application/json")  
```





### 阿里云Code（code.aliyun）提交代码时报错fatal: Authentication failed for‘https://code.aliyun.com/...‘身份验证失败



阿里云Code（code.aliyun）提交代码时报错fatal: Authentication failed for’https://code.aliyun.com/…'身份验证失败，是因为阿里云登录的用户名、密码和code.aliyun的用户名、密码不是同一个。
用户名：
链接：https://code.aliyun.com/profile
![在这里插入图片描述](images/20200416221500832.png)
如果以前没有设置过code.aliyun的密码，那密码就是空的，点击“忘记私人令牌”重新设置密码即可。
链接：https://code.aliyun.com/profile/password/edit
![在这里插入图片描述](images/2020041622172886.png)





# [使用java8的java.util.Base64报“java.lang.IllegalArgumentException: Illegal base64 character d”的问题](https://blog.csdn.net/kevin_mails/article/details/87878601)















### 后端管理系统要求

注册实名，用户添加信息（外网），民警也能添加信息（内网），民警审核民警发布， 

寻亲，留言；









### [Method annotated with @Bean is called directly. Use dependency injection instead.](https://www.cnblogs.com/king0207/p/13360820.html)

未添加@Configuration注解，导致@Bean之间相互调用出错





### github下载代理地址

https://d.serctl.com/?dl_list=0

### nacos启动路径不能包含汉字

```shell
startup.cmd -m standalone
```

com.alibaba.csp.sentinel.slots.block.flow.controller.DefaultController

com.alibaba.csp.sentinel.slots.block.flow.controller.WarmUpController

com.alibaba.csp.sentinel.slots.block.flow.controller.RateLimiterController

```

```

